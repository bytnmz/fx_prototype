'use strict';

import $ from 'jquery';

export default class WffmAddRemoveFields {
	constructor() {
		let	collapsibleFormLabels = JSON.parse($('#collapsibleFormLabels').html());
		let $addRemoveFieldsForm = $('fieldset.CollapsibleFieldSet'),
			$form = $addRemoveFieldsForm.parent();
			// _moreSectionButton = `<span class="add-more-sections" style="cursor: pointer; color: #d92231; display: none;"> + ${collapsibleFormLabels.addSection}</span>`,
			// _lessSectionButton = `<span class="remove-one-section" style="cursor: pointer; color: #d92231; padding-left: 15px; display: none;"> - ${collapsibleFormLabels.removeSection}</span>`,
			// sectionCount = 1;


		// Append button if more than one field present
		// if($addRemoveFieldsForm.length > 1){
			// $('.form-submit-border').before(_moreSectionButton);
			// $('.form-submit-border').before(_lessSectionButton);
			// $('.add-more-sections', $form).show();
			// $('.remove-one-field', $this).hide();
		// }

		$addRemoveFieldsForm.map(function(i, ele) {
			let $this = $(ele);
			$this.data('reset', 'true');

			// Hide all sections except first field
			// if(i != 0){
			// 	$this.hide();
			// }

			// For the fields
			let $fields = $('.form-group', $this),
				_moreButton = `<span class="add-more-fields ${i}" style="cursor: pointer; color: #d92231; display: none;"> + ${collapsibleFormLabels.addField}</span>`,
				_deleteButton = `<span class="remove-one-field" style="cursor: pointer; color: #d92231; padding-left: 15px; display: none;"> - ${collapsibleFormLabels.removeField}</span>`,
				fieldsCount = 1;

			// Append button if more than one field present
			if($fields.length > 1){
				$this.append(_moreButton);
				$this.append(_deleteButton);
				$('.add-more-fields', $this).show();
				// $('.remove-one-field', $this).hide();
			}

			// Hide all fields except first field
			$fields.map(function(i, ele) {
				let $thisField = $(ele);

				if(i != 0){
					$thisField.hide();
				}
			});

			// Add one more field
			$('.add-more-fields', $this).on('click', function(){
				if($this.data('reset') === 'true'){
					fieldsCount = 1;
				}

				if(fieldsCount < $fields.length){
					$fields.eq(fieldsCount).show();
					fieldsCount++;
					$this.data('reset', 'false');

					if(fieldsCount == $fields.length){
						$('.add-more-fields', $this).hide();
						$('.remove-one-field', $this).css('padding-left', '0');
					}
					if(fieldsCount >= 2){
						$('.remove-one-field', $this).show();
					}
				}
			});

			// Remove one field
			$('.remove-one-field', $this).on('click', function(){
				if(fieldsCount > 1){
					fieldsCount--;
					$fields.eq(fieldsCount).hide();
					$fields.eq(fieldsCount).find('input').val('');

					if(fieldsCount < $fields.length){
						$('.add-more-fields', $this).show();
						$('.remove-one-field', $this).css('padding-left', '15px');
					}
					if(fieldsCount < 2){
						$('.remove-one-field', $this).hide();
					}
				}
			});
		});

		// // Add one more section
		// $('.add-more-sections', $form).on('click', function(){
		// 	if(sectionCount < $addRemoveFieldsForm.length){
		// 		$addRemoveFieldsForm.eq(sectionCount).show();
		// 		sectionCount++;

		// 		if(sectionCount == $addRemoveFieldsForm.length){
		// 			$('.add-more-sections', $form).hide();
		// 			$('.remove-one-section', $form).css('padding-left', '0');
		// 		}
		// 		if(sectionCount >= 2){
		// 			$('.remove-one-section', $form).show();
		// 		}
		// 	}
		// });

		// // Remove one section
		// $('.remove-one-section', $form).on('click', function(){
		// 	if(sectionCount > 1){
		// 		sectionCount--;
		// 		$addRemoveFieldsForm.eq(sectionCount).hide();
		// 		$addRemoveFieldsForm.eq(sectionCount).find('input').val('');

		// 		// Hide all fields except first field in removed section
		// 		$addRemoveFieldsForm.eq(sectionCount).find('.form-group').map(function(i, ele) {
		// 			let $clearThese = $(ele);

		// 			$addRemoveFieldsForm.eq(sectionCount).data('reset', 'true');
		// 			$('.remove-one-field', $addRemoveFieldsForm.eq(sectionCount)).hide();

		// 			if(i != 0){
		// 				$clearThese.hide();
		// 			}
		// 		});

		// 		if(sectionCount < $addRemoveFieldsForm.length){
		// 			$('.add-more-sections', $form).show();
		// 			$('.remove-one-section', $form).css('padding-left', '15px');
		// 		}
		// 		if(sectionCount < 2){
		// 			$('.remove-one-section', $form).hide();
		// 		}
		// 	}
		// });
	}
}
