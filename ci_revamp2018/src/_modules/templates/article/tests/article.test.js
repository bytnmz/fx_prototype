'use strict';

import Article from '../article';

describe('Article View', function() {

  beforeEach(() => {
    this.article = new Article();
  });

  it('Should run a few assertions', () => {
    expect(this.article).toBeDefined();
  });

});
