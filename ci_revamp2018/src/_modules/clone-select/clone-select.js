'use strict';

import $ from 'jquery';

export default class CloneSelect {
	constructor() {

		if($('.solutions-filter').length || $('.event-mapper').length || $('.success-stories-mapper').length || $('.reseller-locator__filters').length) {
			let $select = $('select').not('.scfForm select');

			$select.map((i,ele) => {
				let $this = $(ele);

				let firstOption = $('option', $this).first().text(),
					firstValue = $('option', $this).first().val(),
					_clone = `<div class="dropdown-copy"><button type="button" value="${firstValue}">${firstOption}</button></div>`;

				$this.before(_clone);
			});
		}
		else {
			let $select = $('select').not('.scfForm select');

			$select.map((i,ele) => {
				let $this = $(ele),
					$option = $('option', $this);

				let firstOption = $('option', $this).first().text(),
					firstValue = $('option', $this).first().val(),
					_clone = `<div class="dropdown-copy"><button type="button" value="${firstValue}">${firstOption}</button><ul>`;

				$option.each(function() {
					let text = $(this).text(),
						value = '';

					if($(this).attr('value')){
						value = $(this).attr('value');
					}

					let _optionClone = `<li><button type="button" value="${value}"><span>${text}</span></button></li>`;
					_clone += _optionClone;
				});

				_clone = `${_clone}</ul></div>`;

				$this.after(_clone);
			});
		}
	}

	toggleDropdown($object){
		$object.toggle();
	}

	closeDropdown($object){
		$object.hide();
	}
}
