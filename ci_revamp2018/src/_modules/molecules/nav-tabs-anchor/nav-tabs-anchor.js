'use strict';

import $ from 'jquery';

export default class NavTabsAnchor {
	constructor() {
		// let $anchorTabs = $('.anchor-tabs')
		let $anchor = $('.anchor-link');

		let _listWrapper = '<div class="anchor-tabs"><ul>';

		if($anchor.length){
			$anchor.map((i, ele) => {
				let $this = $(ele);

				let label = $this.data('label'),
					href = $this.attr('id');

				if(typeof href !== "undefined"){
					let _append = `<li><a href="#${href}">${label}</a></li>`;
					_listWrapper += _append;
				}
			});

			_listWrapper += '</ul></div>';

			$('.top-banner').append(_listWrapper);

			let $anchorTabs = $('.anchor-tabs');

			$('a', $anchorTabs).on('click', function(e){
				let navOffset;

				if($('.site-header').hasClass('fix')){
					navOffset = 80;
				}
				else {
					navOffset = 210;
				}

	            e.preventDefault();

	            let $target = $($(this).attr('href'));

	             $('html, body').stop(true, true).animate({
	                scrollTop: $target.offset().top + $(window).scrollTop() - navOffset
	            }, 500);
	        });
		}
	}
}
