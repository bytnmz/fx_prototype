'use strict';

import $ from 'jquery';
import 'jquery-match-height';

export default class NavTabs {
	constructor() {
		let $navTabs = $('.nav-tabs');

		$('li', $navTabs).matchHeight();
	}
}
