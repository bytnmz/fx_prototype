'use strict';

import $ from 'jquery';
import 'jquery-match-height';

export default class CardTiles {
	constructor() {
		let $cardTiles = $('.card-tiles');

		$cardTiles.matchHeight();
	}
}
