'use strict';

import $ from 'jquery';

export default class SubscribeForm {
	constructor() {
		let $subscribeSection = $('.subscribe-form'),
			$formGroup = $('.form-group', $subscribeSection);

		$formGroup.map((i,ele) => {
			let $this = $(ele);
			let $label = $('label', $this),
				$input = $('input', $this);

			let placeholderText = $label.text();

			$input.attr('placeholder', placeholderText)
		});
	}
}
