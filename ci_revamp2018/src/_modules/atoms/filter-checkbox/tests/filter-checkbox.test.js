'use strict';

import FilterCheckbox from '../filter-checkbox';

describe('FilterCheckbox View', function() {

  beforeEach(() => {
    this.filterCheckbox = new FilterCheckbox();
  });

  it('Should run a few assertions', () => {
    expect(this.filterCheckbox).toBeDefined();
  });

});
