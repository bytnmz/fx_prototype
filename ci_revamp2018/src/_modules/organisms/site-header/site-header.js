'use strict';

import $ from 'jquery';
import enquire from 'enquire.js';
import 'jquery-match-height';

export default class SiteHeader {
	constructor() {
		let $siteHeader = $('.site-header'),
			$nav = $('.main-nav', $siteHeader),
			$mainNav = $('.main', $nav),
			$contactNav = $('.share-contact', $nav),
			$hasChild = $('.has-children', $nav),
			$searchBar = $('.search-bar', $siteHeader);

		let timer;

		$('.megamenu__products .nav-level3 li').matchHeight();

		$('.btn-nav-menu', $siteHeader).on('click', () => {
			if(!$siteHeader.hasClass('open')){
				$searchBar.removeClass('open');
				$siteHeader.addClass('open');
				$nav.addClass('slideout');
			}
			else {
				$siteHeader.removeClass('open');
				$('.slideout').removeClass('slideout');
			}
		});

		$('.btn-mobile-search', $siteHeader).on('click', () => {
			if(!$searchBar.hasClass('open')){
				$siteHeader.removeClass('open');
				$('.slideout').removeClass('slideout');
				$searchBar.addClass('open');
			}
			else {
				$searchBar.removeClass('open');
			}
		});

		$('> a', $hasChild).map((i,ele) => {
			let $anchor = $(ele);

			$anchor.on('click', (e) => {
				if($anchor.next().length){
					e.preventDefault();
				}
				$anchor.next().addClass('slideout');
			});
		});

		$('button', $hasChild).map((i,ele) => {
			let $this = $(ele);

			$this.on('click', () => {
				$this.parent().parent().removeClass('slideout');
			});
		});

		$(window).on('scroll', function(){
			let browserHeight = $(window).height(),
				documentHeight = $(document).height();

			if(!$siteHeader.hasClass('fix')){
				if((documentHeight - browserHeight) > 200){
					if($(window).scrollTop() > 50){
						$siteHeader.addClass('fix');
					}
					else{
						$siteHeader.removeClass('fix');
					}
				}
			}
			else {
				if($(window).scrollTop() < 50){
					$siteHeader.removeClass('fix');
				}
			}
		});

		$('.nav-level1 > li').on('mouseenter focusin', function(){
			let $thisIn = $(this);
			clearTimeout(timer);

			$thisIn.siblings().removeClass('hover');

			timer = setTimeout(function(){
				$thisIn.addClass('hover');
			}, 650);
		}).on('mouseleave', function(){
			let $thisOut = $(this);

			clearTimeout(timer);
			$thisOut.removeClass('hover');
		});

		$('.nav-level1 > li:last-child a').last().on('focusout', function(){
			let $thisOut = $(this).closest('.hover');

			clearTimeout(timer);
			$thisOut.removeClass('hover');
		});

		enquire.register("screen and (min-width: 992px)", {
		    match : function() {
		    	$('.slideout').each(function(){
		    		$(this).removeClass('slideout');
		    	});
		    	$siteHeader.removeClass('open');

		    	$('> a', $hasChild).map((i,ele) => {
		    		let $anchor = $(ele);

		    		$anchor.off('click');
		    	});

		    	$('.nav-level1 > li').on('mouseenter', function(){
		    		let $thisIn = $(this);
		    		clearTimeout(timer);

		    		timer = setTimeout(function(){
		    			$thisIn.addClass('hover');
		    		}, 650);
		    	}).on('mouseleave', function(){
		    		let $thisOut = $(this);

		    		clearTimeout(timer);
		    		$thisOut.removeClass('hover');
		    	});
		    },
		    unmatch : function() {
		    	$('> a', $hasChild).map((i,ele) => {
		    		let $anchor = $(ele);

		    		$anchor.on('click', (e) => {
		    			if($anchor.next().length){
		    				e.preventDefault();
		    			}
		    			$anchor.next().addClass('slideout');
		    		});
		    	});

		    	$('.nav-level1 > li').off('mouseenter').off('mouseleave');

		    	$('button', $hasChild).map((i,ele) => {
		    		let $this = $(ele);

		    		$this.on('click', () => {
		    			$this.parent().parent().removeClass('slideout');
		    		});
		    	});
		    }
		});
	}
}
