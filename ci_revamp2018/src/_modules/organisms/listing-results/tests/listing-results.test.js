'use strict';

import ListingResults from '../listing-results';

describe('ListingResults View', function() {

  beforeEach(() => {
    this.listingResults = new ListingResults();
  });

  it('Should run a few assertions', () => {
    expect(this.listingResults).toBeDefined();
  });

});
