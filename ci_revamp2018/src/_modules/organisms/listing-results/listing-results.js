'use strict';

import $ from 'jquery';
import doT from 'dot';
import 'jquery-match-height';

export default class ListingResults {
    constructor() {
        this.name         = 'listing-results';
        this.$status      = $('.listing-content__result-status');
        this.$loader      = $('.loader-animation', this.$status);
        this.loadLimit    = 10;
        this.currpage     = 1;
        this.resultUlEl   = $('.listing-content__results');
        this.results = [];

        let _this = this;

        if($('#results-template').length) {
            _this.resultTempFn = doT.template($('#results-template').html());
        }

        // Load more results
        $('.js-load-more').on('click', function(e){
            e.preventDefault();

            _this.parsePage(_this.currpage);
            _this.toggleLoadMore();
        });
    }

    render(results, categoryChange) {
        let _this = this,
            $status = _this.$status;

        _this.results = results; //Set result set

        if(results.length > 0) {
            if (!categoryChange) {
                $('.total-results', $status).text(results.length);
                $('.success-msg', $status).slideDown();
            } else {
                $('.success-msg', $status).slideUp();
            }
            $('.error-msg', $status).slideUp();

           _this.currpage = 1

           _this.parsePage(_this.currpage);
           _this.toggleLoadMore();
        } else {
            $('.error-msg', $status).slideDown();
            $('.total-results', $status).text(0);
            $('.success-msg', $status).slideUp();
        }
    }

    /**
     * Toggle load more button visibility
     */
    toggleLoadMore() {
        let _this = this;

        if(_this.currpage * _this.loadLimit < _this.results.length) {
            _this.currpage++;

            $('.load-more').show();
        } else {
            $('.load-more').hide();
        }
    }

    parsePage(page) {
        let _this = this,
            lowerLimit = (page - 1) * this.loadLimit,
            upperLimit = page * this.loadLimit,
            pageResults = {"results" : []},
            interval = 200;

        for(let i = lowerLimit; i < upperLimit; i++) {
            if(typeof _this.results[i] !== 'undefined')
                pageResults.results.push(_this.results[i]);
        }

        _this.resultUlEl
            .append(_this.resultTempFn({ results: pageResults.results }));

        $('.listing-content__results > li').matchHeight();

        /**
         * Fade In
         */
        $('li', _this.resultUlEl).each(function(){
            let $this = $(this);

            if($this.hasClass('visible')) return;

            setTimeout(function(){
                $this.addClass('visible');

            }, interval);

            interval = interval + 25;
        });
    }

    clear() {
        this.resultUlEl.empty();
        this.$loader.css({'display': ''});
    }
}
