'use strict';

import doT from 'dot';
import $ from 'jquery';

export default class InsightComponent {
	constructor() {
		let $insightFilter = $('.insights-filter'),
			$filter = $('.filter-item', $insightFilter).not('.filter-item--reset'),
			$reset = $('.filter-item--reset .btn-reset', $insightFilter),
			$insightContent = $('.insight__content .column-row'),
			$copy = $('.dropdown-copy', $insightFilter);

        this.$insightFilter = $insightFilter;
		this.$filter = $filter;
		this.$reset = $reset;
		this.data = {};
		this.loadLimit    = 9;
		this.currpage     = 1;
		this.results = [];
		this.endpoint = $('.insight__content').data('endpoint');

        $filter.map((i, ele) => {
			let $this = $(ele),
				$clone = $('.dropdown-copy', $this),
				$dropdown = $('ul', $this),
				$button = $('> button', $clone);

			$this.on('click', (e) => {
				e.stopPropagation();
	        	$('ul', $filter).not($dropdown).hide();
				this.toggleDropdown($dropdown);
			});

			let $option = $('li button', $dropdown);

			$option.map((j, ele) => {
				let $this2 = $(ele);

				$this2.on('click', () => {
					let value = $this2.val(),
						text = $this2.text();

					$button.text(text);
					$button.val(value);

					let fieldname = $('select', $this).attr('name');

					this.data[fieldname] = value;

					$insightContent.empty();
					this.getResult(this.endpoint, this.data, renderResults);
				});
			});
		});

		$(window).on('click', () => {
	        $('ul', $filter).hide();
	    });


        let renderResults = (response) => {
			let insightTemplate = $('#dotInsightTemplate').html(),
				_content = '';

			let _this = this,
			    lowerLimit = (this.currpage - 1) * this.loadLimit,
			    upperLimit = this.currpage * this.loadLimit,
			    pageResults = [],
			    interval = 200;

			this.results = response;

			for(let i = lowerLimit; i < upperLimit; i++) {
			    if(typeof _this.results[i] !== 'undefined'){
			        pageResults.push(_this.results[i]);
			    }
			}

			$insightContent.append(doT.template(insightTemplate)(pageResults));

			$('.card-tiles').matchHeight();

			/**
			 * Fade In
			 */
			$('.result', this.resultsCont).each(function(){
			    let $this = $(this);

			    if($this.hasClass('visible')) return;

			    setTimeout(function(){
			        $this.addClass('visible');

			    }, interval);

			    interval = interval + 100;
			});

			if(this.currpage === 1){
				this.currpage++;
			}
		}

		$('.show-more').on('click', (e) => {
		    e.preventDefault();

		    renderResults(this.results);
		    this.toggleLoadMore();
		});



		$reset.on('click', () => {
			this.data = {};
			this.resetFilters();
			$insightContent.empty();
			this.getResult(this.endpoint, this.data, renderResults);
		});

		// render on load
		this.getResult(this.endpoint, this.data, renderResults);
	}

	toggleLoadMore() {
	    let _this = this;

	    if(_this.currpage * _this.loadLimit < _this.results.length) {
	        _this.currpage++;

	        $('.show-more').show();
	    } else {
	        $('.show-more').hide();
	    }
	}

	toggleDropdown($object){
		$object.toggle();
	}

	resetFilters() {
		$('select').map((i, ele) => {
			let $this = $(ele);

			$this.val(null).change();
		});

		this.$filter.map((j,ele) => {
			let defaultText = $('li button', $(ele)).first().text(),
				defaultValue = $('li button', $(ele)).first().val();

			$('.dropdown-copy > button', $(ele)).text(defaultText);
			$('.dropdown-copy > button', $(ele)).val(defaultValue);
		});
	}

	/**
	 * Ajax call to endpoint
	 */
	getResult(url, data, cb) {
		this.currpage = 1;
	    $.ajax({
	        url,
	        data,
	        dataType: 'json',
	        cache: false
	    }).done(cb);
	}
}
