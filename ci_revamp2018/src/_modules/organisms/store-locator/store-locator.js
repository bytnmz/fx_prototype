'use strict';

import $ from 'jquery';
import jsonQuery from 'json-query';
import doT from 'dot';

export default class StoreLocator {
	constructor(type) {
		// START: Append map script into body
		// let mapurl = $('.reseller-locator').data('mapurl'),
		// 	apikey = $('.reseller-locator').data('mapkey');

		// if (type === 'google'){
		// 	mapurl = ($('.reseller-locator').data('mapurl')) ? $('.reseller-locator').data('mapurl') : 'https://maps.googleapis.com/maps/api/js?libraries=geometry&key=';
		// 	apikey = ($('.reseller-locator').data('mapkey')) ? $('.reseller-locator').data('mapkey') : 'AIzaSyC3RU1ehPFevI1uGes49wIpZKhR7KHwrcI';

		// 	this.appendMapScripts(mapurl, apikey);
		// } else if (type === 'baidu'){
		// 	mapurl = ($('.reseller-locator').data('mapurl')) ? $('.reseller-locator').data('mapurl') : 'http://api.map.baidu.com/api?v=2.0&ak=';
		// 	apikey = ($('.reseller-locator').data('mapkey')) ? $('.reseller-locator').data('mapkey') : 'GoR7N5kE8U0TVvsai7e6MO6d7iElyhQR';

		// 	this.appendMapScripts(mapurl, apikey);
		// } else {
		// 	mapurl = ($('.reseller-locator').data('mapurl')) ? $('.reseller-locator').data('mapurl') : 'https://openapi.map.naver.com/openapi/v3/maps.js?clientId=';
		// 	apikey = ($('.reseller-locator').data('mapkey')) ? $('.reseller-locator').data('mapkey') : 'YwbCpVG1dKxBUanw9Q8M';

		// 	this.appendMapScripts(mapurl, apikey);
		// }
		// END


		// Definitions
		let _this = this,
			$resellerFilter = $('.reseller-locator__filters'),
			$resellerSearch = $('.btn-reseller-search', $resellerFilter);

		let	provinceData = JSON.parse($('#provinceData').html()),
			productData = JSON.parse($('#productData').html());

		_this.optionTemplate = doT.template("{{~it.data :selectOption:index}}<option value= \"{{=selectOption.value}}\">{{=selectOption.name}}</option>{{~}}");


		// START: Filter UI event handlers
		let $copy = $('.dropdown-copy', $resellerFilter);
		$copy.map((i, ele) => {
			let $this = $(ele),
				$select = $this.next(),
				$button = $this.find('button');

			$select.on('click', (e) => {
				e.stopPropagation();

				if (!$this.hasClass('active')){
					$this.addClass('active');
				}
				else {
					$this.removeClass('active');
				}

				$('.dropdown-copy').not($this).removeClass('active');
			});

			$select.on('change', () => {
				let $option = $select.find(':selected');

				$select.prev().find('button').text($option.text());
			});

			$(window).on('click', () => {
				if ($this.hasClass('active')){
					$this.removeClass('active');
				}
			});
		});
		// END


		// START: Remove all active class on dropdown on ESC key press
		$(document).keyup(function(e) {
			if (e.keyCode == 27) {
				$('.active', $resellerFilter).removeClass('active');
			}
		});
		// END


		// START: Change city options based on province selected
		let cityLabel = $('#cityFilter').parent().data('label');

		$('#provinceFilter').on('change', function(){
			let $this = $(this),
				value = $this.find('option:selected').val(),
				data = {};

			data = jsonQuery('province[value=' + value + '].cities', {data: provinceData}).value;

			_this.updateOptions(data, $('#cityFilter'), cityLabel, _this.optionTemplate);
		});
		// END

		// START: Change product options based on category selected
		let productLabel = $('#productFilter').parent().data('label');

		$('#categoryFilter').on('change', function(){
			let $this = $(this),
				value = $this.find('option:selected').val(),
				data = {};

			data = jsonQuery('product[value=' + value + '].products', {data: productData}).value;

			_this.updateOptions(data, $('#productFilter'), productLabel, _this.optionTemplate);
		});
		// END

		// START: Switch view
		$('.switch-view').on('click', function(e){
			e.preventDefault();

			if($('.reseller-partners--content').hasClass('hide')){
				if ($('#resellerMap').length) {
					$('#resellerMap').removeClass('hide');
				}
				$('.reseller-partners--content').removeClass('hide');
				$('.reseller-partners--list').addClass('hide');
				$resellerSearch.trigger('click');
				_this.setPaddings();
			}
			else {
				if ($('#resellerMap').length) {
					$('#resellerMap').addClass('hide');
				}
				$('.reseller-partners--content').addClass('hide');
				$('.reseller-partners--list').removeClass('hide');
			}
		});
		// END


		// START: Validation check and API call on search
		$resellerSearch.on('click', (e) => {
			e.preventDefault();

			let $provinceFilter = $('#province'),
				province = $('#provinceFilter').val(),
				$cityFilter = $('#city'),
				city = $('#cityFilter').val(),
				$resellerFilter = $('#reseller'),
				resellerType = $('#resellerFilter').val(),
				$categoryFilter = $('#category'),
				productCategory = $('#categoryFilter').val(),
				$productFilter = $('#product'),
				productName = $('#productFilter').val(),
				$dealerFilter = $('#dealer'),
				dealerName = $('#dealerSearch').val(),
				filterValidated = false,
				url = $('.reseller-locator').data('endpoint'),
				data = {};

			// test naver
			// url = '/assets/fuji-xerox/api/new-reseller2.json';

			if (province === "") {
				$('.error-msg', $provinceFilter).addClass('active');
			} else {
				$('.error-msg', $provinceFilter).removeClass('active');
				filterValidated = true;
			}

			if(filterValidated){
				data = { province, city, resellerType, productCategory, productName, dealerName };

				if (type === 'google'){
					_this.getResults(url, data, _this.processMapResults, type);
				} else if (type === 'baidu'){
					_this.getResults(url, data, _this.processMapResults, type);
				} else {
					// console.log('naver');
					_this.getResults(url, data, _this.processMapResults, type);
				}
			}
		});
	}

	appendMapScripts(mapurl, apikey) {
		let scriptStr = mapurl + apikey;

		let script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = scriptStr;
		// document.getElementsByTagName('head')[0].appendChild(script);
		document.body.appendChild(script);
	}

	processMapResults(data, type) {
		renderResults(data);

		if($('#resellerMap').length){
			if (type === 'google'){
				if($('.switch-view').hasClass('hide')){
					$('.switch-view').removeClass('hide');
				}

				if($('#resellerMap').hasClass('hide')){
					if (!$('.reseller-partners--content').hasClass('hide')){
						$('#resellerMap').removeClass('hide');
					}
				}
				// Map options
				let map,
					bound = new google.maps.LatLngBounds(),
					tooltip = new google.maps.InfoWindow({
						maxWidth: 200
					}),
					mapOptions = {
						zoom: 12,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};

				map = new google.maps.Map(document.getElementById("resellerMap"), mapOptions);

				if(data.length){
					data.forEach(function(store, index){
						// START: Create marker and add marker
						let position = new google.maps.LatLng(store.lat, store.long);
						let marker = new google.maps.Marker({
							position: position,
							map: map,
							title: "<div class='map-pop-up' style = 'height:auto;width:200px'><strong>" + store.name +
									"</strong><br />" + store.address
						});

						// END

						// START: Make info window for each marker and add to map
						marker.addListener('click', () => {
							tooltip.setContent(marker.title);
							tooltip.open(map, marker);
						});

						map.addListener('click', () => {
							tooltip.close();
						});
						// END

						bound.extend(new google.maps.LatLng(store.lat, store.long));
					});

					map.panTo(bound.getCenter());
				}
				else {
					if(!$('.switch-view').hasClass('hide')){
						$('.switch-view').addClass('hide');
					}

					if(!$('#resellerMap').hasClass('hide')){
						$('#resellerMap').addClass('hide');
					}
					else {
						return;
					}
					// if (navigator.geolocation) {
					// 	navigator.geolocation.getCurrentPosition(function(p) {
					// 		let pos = {
					// 			lat: p.coords.latitude,
					// 			lng: p.coords.longitude
					// 		};

					// 		let myPos = new google.maps.LatLng(pos.lat, pos.lng);

					// 		map.panTo(myPos);
					// 	});
					// }
				}

				// START: Show on map button handler
				$(document).on('click','.btn-map-show',function(e){
					e.preventDefault();

					let $target = $('.reseller-map');

					 $('html, body').stop(true, true).animate({
						scrollTop: $target.offset().top - 100
					}, 500);

					let $this = $(this),
						lat = $this.data('lat'),
						long = $this.data('long');

					let showMarker = (data) => {
						let position = new google.maps.LatLng(lat, long);

						map.setZoom(15);
						map.panTo(position);
					}

					showMarker();
				});
				// END

			} else if (type === 'baidu'){
				if($('.switch-view').hasClass('hide')){
					$('.switch-view').removeClass('hide');
				}

				if ($('#resellerMap').hasClass('hide')) {
					if (!$('.reseller-partners--content').hasClass('hide')) {
						$('#resellerMap').removeClass('hide');
					}
				}

				let map = new BMap.Map("resellerMap"),
					bounds = map.getBounds(),
					points = [];

				if(data.length){
					map.enableScrollWheelZoom();
					map.enableContinuousZoom();

					data.forEach(function(store, index){
						// START: Create marker and add marker
						let point = new BMap.Point(store.long, store.lat),
							marker = new BMap.Marker(point);

						bounds.extend(point);
						points.push(point);

						let opts = {
							width: 200,
							height: 100,
							title : store.name
						}

						let content = store.address;

						map.addOverlay(marker);
						// END

						// START: Make info window for each marker and add to map
						marker.addEventListener('click', function(e){
							let p = e.target;

							let point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);

							let infoWindow = new BMap.InfoWindow(content, opts);
							map.openInfoWindow(infoWindow, point);
						});
						// END
					});

					// map.centerAndZoom(bounds.getCenter(), 13);
					map.setViewport(points);
				}
				else {
					if(!$('.switch-view').hasClass('hide')){
						$('.switch-view').addClass('hide');
					}

					if(!$('#resellerMap').hasClass('hide')){
						$('#resellerMap').addClass('hide');
					}
					else {
						return;
					}
					// let geolocation = new BMap.Geolocation();
					// geolocation.getCurrentPosition(function(r){
					// 	if(this.getStatus() == BMAP_STATUS_SUCCESS){
					// 		let mk = new BMap.Marker(r.point);
					// 		map.addOverlay(mk);
					// 		map.centerAndZoom(r.point, 13);
					// 	}
					// 	else {
					// 		return;
					// 	}
					// },{enableHighAccuracy: true});
				}

				// START: Show on map button handler
				$(document).on('click','.btn-map-show',function(e){
					e.preventDefault();

					let $target = $('.reseller-map');

					 $('html, body').stop(true, true).animate({
						scrollTop: $target.offset().top - 100
					}, 500);

					let $this = $(this),
						lat = $this.data('lat'),
						long = $this.data('long');

					let showMarker = (data) => {
						let position = new BMap.Point(long, lat);

						map.setZoom(15);
						map.panTo(position);
					}

					showMarker();
				});

			} else {
				if($('.switch-view').hasClass('hide')){
					$('.switch-view').removeClass('hide');
				}

				if ($('#resellerMap').hasClass('hide')) {
					if (!$('.reseller-partners--content').hasClass('hide')) {
						$('#resellerMap').removeClass('hide');
					}
				}

				let map,
					bound,
					tooltip = new naver.maps.InfoWindow({
						maxWidth: 200
					}),
					mapOptions = {
						zoom: 6,
						mapTypeId: naver.maps.MapTypeId.NORMAL
					},
					markers = [],
					infoWindows = [],
					points = [];

				map = new naver.maps.Map('resellerMap', mapOptions);

				if(data.length){
					let minLat = data[0].lat,
						maxLat = data[0].lat,
						minLong = data[0].long,
						maxLong = data[0].long;

					data.forEach(function(store, index){
						// START: Create marker and add marker
						let position = new naver.maps.LatLng(store.lat, store.long);
						let marker = new naver.maps.Marker({
							map: map,
							position: position,
							title: "<div class='map-pop-up' style = 'height:auto;width:200px'><strong>" + store.name +
									"</strong><br />" + store.address
						});
						// END

						points.push(position);

						// START: Get min/max bound
						if(minLat <= store.lat){
							minLat = minLat;
						} else {
							minLat = store.lat
						}

						if(minLong <= store.long){
							minLong = minLong;
						} else {
							minLong = store.long
						}

						if(maxLat >= store.lat){
							maxLat = maxLat;
						} else {
							maxLat = store.lat
						}

						if(maxLong >= store.long){
							maxLong = maxLong;
						} else {
							maxLong = store.long
						}
						// END

						// START: Make info window for each marker and add to map
						let infoWindow = new naver.maps.InfoWindow({
							content: marker.title
						});

						markers.push(marker);
						infoWindows.push(infoWindow);
						// END

						naver.maps.Event.addListener(map, "click", function(e) {
							infoWindow.close();
						});
					});

					if(points.length === 1){
						map.setCenter(points[0]);
					}
					else {
						// bound = new naver.maps.LatLngBounds(new naver.maps.LatLng(minLat, minLong), new naver.maps.LatLng(maxLat, maxLong));
						map.fitBounds(points);
					}

					// map.setCenter(bound.getCenter());


					// START: Info window handlers
					naver.maps.Event.addListener(map, 'idle', function() {
						updateMarkers(map, markers);
					});

					function updateMarkers(map, markers) {

						let mapBounds = map.getBounds();
						let marker, position;

						for (let i = 0; i < markers.length; i++) {

							marker = markers[i]
							position = marker.getPosition();

							if (mapBounds.hasLatLng(position)) {
								showMarker(map, marker);
							} else {
								hideMarker(map, marker);
							}
						}
					}

					function showMarker(map, marker) {

						if (marker.setMap()) return;
						marker.setMap(map);
					}

					function hideMarker(map, marker) {

						if (!marker.setMap()) return;
						marker.setMap(null);
					}

					function getClickHandler(seq) {
						return function(e) {
							let marker = markers[seq],
								infoWindow = infoWindows[seq];

							if (infoWindow.getMap()) {
								infoWindow.close();
							} else {
								infoWindow.open(map, marker);
							}
						}
					}

					for (let i=0, ii=markers.length; i<ii; i++) {
						naver.maps.Event.addListener(markers[i], 'click', getClickHandler(i));
					}
					// END


					// START: Show on map button handler
					$(document).on('click','.btn-map-show',function(e){
						e.preventDefault();

						let $target = $('.reseller-map');

						 $('html, body').stop(true, true).animate({
							scrollTop: $target.offset().top - 100
						}, 500);

						let $this = $(this),
							lat = $this.data('lat'),
							long = $this.data('long');

						let showMarker = (data) => {
							let position = new naver.maps.LatLng(lat, long);

							map.setZoom(10);
							map.panTo(position);
						}

						showMarker();
					});
					// END
				}
				else {
					if(!$('.switch-view').hasClass('hide')){
						$('.switch-view').addClass('hide');
					}

					if(!$('#resellerMap').hasClass('hide')){
						$('#resellerMap').addClass('hide');
					}
					else {
						return;
					}
					// if (navigator.geolocation) {
				 //        navigator.geolocation.getCurrentPosition(onSuccessGeolocation, onErrorGeolocation);
				 //    } else {
				 //        return;
				 //    }

					// function onSuccessGeolocation(position) {
					//     let location = new naver.maps.LatLng(position.coords.latitude,
					//                                          position.coords.longitude);

					//     let marker = new naver.maps.Marker({
					//     	map: map,
					//     	position: location
					//     });

					//     map.setCenter(location); // 얻은 좌표를 지도의 중심으로 설정합니다.
					//     map.setZoom(10); // 지도의 줌 레벨을 변경합니다.
					// }

					// function onErrorGeolocation() {
					//     let center = map.getCenter();

					//     infowindow.setContent('<div style="padding:20px;">' +
					//         '<h5 style="margin-bottom:5px;color:#f00;">Geolocation failed!</h5>'+ "latitude: "+ center.lat() +"<br />longitude: "+ center.lng() +'</div>');

					//     infowindow.open(map, center);
					// }
				}
			}
		}

		function renderResults(data) {
			let resellerTemplate = $('#reseller-template').html();
			let resellerListTemplate = $('#resellerlist-template').html();
			let resellerContent = '',
				resellerListContent = '';

			resellerContent = doT.template(resellerTemplate)(data);
			resellerListContent = doT.template(resellerListTemplate)(data);

			$('.reseller-partners--content').html(resellerContent);
			$('.reseller-partners--list').html(resellerListContent);
			$('.resellers-count').html(String(data.length));

			let $first = $('.partner-item').first();
			let textWidth = $('.partner-item__detail .label', $first).first().width();

			$('.partner-item').map((i, ele) => {
				$('.partner-item__detail .label', $(ele)).map((j, label) => {
					if (textWidth < $(label).width()) {
						textWidth = $(label).width();
					}
					else {
						textWidth = textWidth;
					}
				});
			});

			let padding = textWidth + 10;
			$('.padded').css('padding-left', padding + 'px');

			$('.partner-item').map((i, ele) => {
				if ($(ele).find('.contact').length) {
					let faxTextWidth = $('.partner-item__detail.fax .label', $(ele)).width();
					faxTextWidth += 10;
					$('.padded-fax', $(ele)).css('padding-left', faxTextWidth + 'px');
				}
				else {
					$('.padded-fax', $(ele)).css('padding-left', padding + 'px');
				}
			});

			$('.reseller-map').removeClass('hide');

			// Show/hide legend
			if ($('.medal').length) {
				if (!$('.reseller-partners--legend').hasClass('visible')) {
					$('.reseller-partners--legend').show();
				}
			} else {
				if ($('.reseller-partners--legend').hasClass('visible')) {
					$('.reseller-partners--legend').hide();
				}
			}

			// Hide show on map button if map doesn't exist
			if (!$('#resellerMap').length) {
				$('.btn-map-show').hide();
			}
			else {
				$('.btn-map-show').show();
			}
		}
	}

	setPaddings(){
		let $first = $('.partner-item').first();
		let textWidth = $('.partner-item__detail .label', $first).first().width();

		$('.partner-item').map((i, ele) => {
			$('.partner-item__detail .label', $(ele)).map((j, label) => {
				if (textWidth < $(label).width()) {
					textWidth = $(label).width();
				}
				else {
					textWidth = textWidth;
				}
			});
		});

		let padding = textWidth + 10;
		$('.padded').css('padding-left', padding + 'px');

		$('.partner-item').map((i, ele) => {
			if ($(ele).find('.contact').length) {
				let faxTextWidth = $('.partner-item__detail.fax .label', $(ele)).width();
				faxTextWidth += 10;
				$('.padded-fax', $(ele)).css('padding-left', faxTextWidth + 'px');
			}
			else {
				$('.padded-fax', $(ele)).css('padding-left', padding + 'px');
			}
		});
	}

	updateOptions(data, $filter, label, template) {
		let evalStr = template({data: data});

		$filter
		    .html(evalStr)
		    .prepend($('<option value="" selected>' + label + '</option>'))
		    .parents('.input-group-select');

		$filter.prev().find('button').text(label);
	}

	getResults(url, data, cb, type) {
		$.ajax({
		    url,
		    data,
		    dataType: 'json',
		    cache: false,
		    success: function(data) {
		    	cb(data, type);
		    }
		});
	}
}

// naver api: https://openapi.map.naver.com/openapi/v3/maps.js?clientId=
// naver api: https://openapi.map.naver.com/openapi/v3/maps.js?clientId=YwbCpVG1dKxBUanw9Q8M&submodules=geocoder

// baidu api: http://api.map.baidu.com/api?v=2.0&ak=GoR7N5kE8U0TVvsai7e6MO6d7iElyhQR

// google api: https://maps.googleapis.com//maps/api/js?libraries=geometry&key=AIzaSyC3RU1ehPFevI1uGes49wIpZKhR7KHwrcI
