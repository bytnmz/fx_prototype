'use strict';

import $ from 'jquery';
import doT from 'dot';

export default class ResellerLocator {
	constructor() {
		let $copy = $('.dropdown-copy', this.mapper),
			$reseller = $('.reseller-locator'),
			$partner = $('.reseller-partners', $reseller),
			$partnerContent = $('.reseller-partners--content', $partner),
			$resellerFilter = $('.reseller-locator__filters', $reseller),
			$resellerSearch = $('.btn-reseller-search', $resellerFilter),
			$resellerNear = $('.btn-reseller-near', $resellerFilter),
			url = $partner.data('endpoint'),
			nearMe = $partner.data('radius'),
			checkLocation = $reseller.data('locator'),
			apikey = $reseller.data('apikey'),
			map;

		this.filters = {
			"reseller"  : $('#resellerFilter'),
			"location" : $('#locationFilter')
		};

		this.mapper = $('.reseller-locator__filters');
		this.$partnerContent = $partnerContent;
		this.appendMapScripts(checkLocation, apikey);

		// get reseller data from HTML JSON
		let resellerItem = JSON.parse($('#resellerData').html());

		$copy.map((i, ele) => {
			let $this = $(ele),
				$select = $this.next(),
				$button = $this.find('button');

			$select.on('click', (e) => {
				e.stopPropagation();

				if (!$this.hasClass('active')){
					$this.addClass('active');
				}
				else {
					$this.removeClass('active');
				}

				$('.dropdown-copy').not($this).removeClass('active');
			});

			$select.on('change', () => {
				let $option = $select.find(':selected');

				$select.prev().find('button').text($option.text());
			});

			$(window).on('click', () => {
				if ($this.hasClass('active')){
					$this.removeClass('active');
				}
			});
		});

		$resellerSearch.on('click', (e) => {
			e.preventDefault();

			let $locationFilter = $('#locationFilter'),
				$locationVal = $locationFilter.val(),
				// $resellerFilter = $('#resellerFilter'),
				// $resellerVal = $resellerFilter.val(),
				checkSelect = false;

			// if ($resellerVal === "") {
			// 	$('.error-msg-reseller').addClass('is-active');
			// 	checkSelect = true;
			// } else {
			// 	$('.error-msg-reseller').removeClass('is-active');
			// }

			if ($locationVal === "") {
				$('.error-msg-location').addClass('is-active');
				checkSelect = true;
			} else {
				$('.error-msg-location').removeClass('is-active');
			}

			if (checkSelect) { return false; }

			let mapOptions = {
				zoom: 12,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};

			map = new google.maps.Map(document.getElementById("resellerMap"), mapOptions);

			let storeExists = [],
				resellerCount = $('.resellers-count'),
				count = 0,
				bound = new google.maps.LatLngBounds();

			let tooltip = new google.maps.InfoWindow({
				maxWidth: 200
			});

			$.each( resellerItem, function( key, data ) {
				if (key === $locationVal) {
					$.each( data, function( k, v ) {
						storeExists.push(v);
						count++;
						let position = new google.maps.LatLng(v.lat, v.long);
						let marker = new google.maps.Marker({
							position: position,
							map: map,
							title: "<div class='map-pop-up' style = 'height:auto;width:200px'><strong>" + v.name +
									"</strong><br />" + v.address
						});

						marker.addListener('click', () => {
							tooltip.setContent(marker.title);
							tooltip.open(map, marker);
						});

						map.addListener('click', () => {
							tooltip.close();
						});

						bound.extend(new google.maps.LatLng(v.lat, v.long));
					});
				}
			});

			if(storeExists.length){
				map.panTo(bound.getCenter());
			}
			else {
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function(p) {
						var pos = {
							lat: p.coords.latitude,
							lng: p.coords.longitude
						};

						let myPos = new google.maps.LatLng(pos.lat, pos.lng);

						map.panTo(myPos);
					});
				}
			}

			$('.reseller-map').removeClass('reseller-nodisplay');
			$('.reseller-partners').removeClass('reseller-nodisplay');
			$('.resellers-count').html(count);
			renderResults(storeExists);
		});

		$resellerNear.on('click', (e) => {
			e.preventDefault();

			let storeExists = [],
				radius = nearMe,
				count = 0,
				bound = new google.maps.LatLngBounds();

			let tooltip = new google.maps.InfoWindow({
				maxWidth: 200
			});

			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function (p) {
					let LatLng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
					let mapOptions = {
						center: LatLng,
						zoom: 12,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};

					map = new google.maps.Map(document.getElementById("resellerMap"), mapOptions);

					$.each( resellerItem, function( key, data ) {
						$.each( data, function( k, v ) {
							let position = new google.maps.LatLng(v.lat, v.long),
								distance = google.maps.geometry.spherical.computeDistanceBetween(position, LatLng),
								distanceInKm = ((Math.round(distance / 100) / 10) <= radius) ? 'exists' : '';

							if (distanceInKm === "exists") {
								storeExists.push(v);
								count++;
								let marker = new google.maps.Marker({
									position: position,
									map: map,
									title: "<div class='map-pop-up' style = 'height:auto;width:200px'><strong>" + v.name +
											"</strong><br />" + v.address
								});

								marker.addListener('click', () => {
									tooltip.setContent(marker.title);
									tooltip.open(map, marker);
								});

								map.addListener('click', () => {
									tooltip.close();
								});

								bound.extend(new google.maps.LatLng(v.lat, v.long) );
							}
						});
					});

					// });

					if(storeExists.length){
						map.panTo(bound.getCenter());
					}
					else {
						map.panTo(LatLng);
					}

					$('.reseller-map').removeClass('reseller-nodisplay');
					$('.reseller-partners').removeClass('reseller-nodisplay');
					$('.resellers-count').html(count);
					renderResults(storeExists);
				});
			}
		});

		$(document).on('click','.btn-map-show',function(e){
			e.preventDefault();

			let $target = $('.reseller-map');

			 $('html, body').stop(true, true).animate({
			    scrollTop: $target.offset().top - 100
			}, 500);

			let $this = $(this),
				lat = $this.data('lat'),
				long = $this.data('long');

			let showMarker = (data) => {
				let position = new google.maps.LatLng(lat, long);

				map.setZoom(15);
				map.panTo(position);
			}

			showMarker();

		});

		function renderResults(data) {
			let resellerTemplate = $('#reseller-template').html();
			let resellerContent = '';

			resellerContent = doT.template(resellerTemplate)(data);

			$partnerContent.html(resellerContent);

			let	$first = $('.partner-item').first();
			let textWidth = $('.partner-item__detail .label', $first).first().width();

			$('.partner-item__detail .label', $first).each(function() {
				if(textWidth < $(this).width()){
					textWidth = $(this).width();
				}
				else {
					textWidth = textWidth;
				}
			});

			let padding = textWidth + 10;
			$('.padded').css('padding-left', padding + 'px');

			let faxTextWidth = $('.partner-item__detail.fax .label').width();
			faxTextWidth += 10;
			$('.padded-fax').css('padding-left', faxTextWidth + 'px');
		}
	}

	appendMapScripts(checkLocation, apikey) {
		let scriptStr = '/maps/api/js?libraries=geometry&key=';
		scriptStr = checkLocation + scriptStr + apikey;

		let script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = scriptStr;
		document.body.appendChild(script);
	}
}
