'use strict';

import $ from 'jquery';

export default class Tabs {
	constructor() {
		let $tabsWrapper = $('.tab-wrapper'),
			$content = $('.tab-wrapper__contents', $tabsWrapper);

		$('> *:not(.active)', $content).fadeOut();
		$('.active', $content).fadeIn();

		$tabsWrapper.map((i,ele) => {
			let $this = $(ele),
				$tabs = $('.tab-wrapper__tabs', $this),
				$content = $('.tab-wrapper__contents', $this);

			$('a.tab-controller', $tabs).map((i,ele) => {
				let $thisTab = $(ele);

				$thisTab.on('click', (e) => {
					e.preventDefault();

					let target = $thisTab.attr('href');

					this.switchTabs($thisTab, target, $content, $tabs);
				})
			})
		});

	}

	switchTabs($object, target, $content, $scope) {
		if(!$object.parent().hasClass('active')){
			//button handler
			$('.active', $scope).removeClass('active');
			$object.parent().addClass('active');

			// fade in fade out content
			$('.active', $content).fadeOut(300, function(){
				$('.active', $content).removeClass('active');
				$(target).fadeIn();
				$(target).addClass('active');
				if ($(target).find('.tabbed-carousel').length){
					$('.tabbed-carousel__items', $(target)).slick('setPosition');
				}
			});
		}
		else {
			return;
		}
	}
}
