'use strict';

import $ from 'jquery';

export default class ColFormWrapper {
	constructor() {
		let $colForm = $('.form-horizontal'),
			$title = $('.page-header h1', $colForm),
			$intro = $('form > p', $colForm);

		let titleHeight = $title.height(),
			offset = titleHeight + 20;

		$intro.first().css('top', offset+'px');

		$('.form-group').matchHeight();
	}
}
