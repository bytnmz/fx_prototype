'use strict';

import $ from 'jquery';

export default class ImageLinksWrapper {
	constructor() {
		let $imageLinksWrapper = $('.image-links-wrapper'),
			$imageLinks = $('.link-image', $imageLinksWrapper);

		if($imageLinks.length >= 4){
			$imageLinks.map((i, ele) => {
				let $this = $(ele);

				$this.addClass('float');
			});
		}
	}
}
