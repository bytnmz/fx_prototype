'use strict';

import $ from 'jquery';
import 'jquery-match-height';

export default class PanelsGroup {
	constructor() {
		let $panelGroup = $('.panels'),
			$panelItem = $('.panel-item', $panelGroup);

		$panelItem.matchHeight();
	}
}
