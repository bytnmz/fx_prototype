'use strict';

import doT from 'dot';
import jsonQuery from 'json-query';
import Cookies from 'js-cookie';
import $ from 'jquery';

export default class Mapper {
    // constructor(type, isDebug) {
    constructor(type) {
        this.name = 'mapper';

        if($('#results-template').length) {
            this.resultTempFn = doT.template($('#results-template').html());
        }

        // if(type === 'solution') {
        //     this.optionTempFn = doT.template("{{~it.data :value:index}}<option value= \"{{=value.id}}\">{{=value.name}}</option>{{~}}");
        //     this.mapSolution();

        // } else if(type === 'story') {
        if(type === 'story') {
            this.$loadMoreCont = $('.js-load-more');
            this.filters = {
                "country"  : $('#countryFilter'),
                "solution" : $('#solutionFilter'),
                "product"  : $('#productFilter'),
                "industry" : $('#industryFilter'),
                "year"     : $('#yearFilter')
            };
            this.cookieName = "storyFilter";
            // this.endpoint = (isDebug) ? '/assets/fuji-xerox/api/story-results.json' : '/api/GetStoryListing';
            this.endpoint = $('.success-stories-mapper__results').data('endpoint');
            this.pageSize = $('.success-stories-mapper__results').data('pagesize');
            this.mapper = $('.success-stories-mapper');
            this.resultsCont = $('.success-stories-mapper__results', this.mapper);
            this.noResults = $('.listing-content__result-status .msg');

            this.mapStory();

            let $copy = $('.dropdown-copy', this.mapper);

            $copy.map((i, ele) => {
                let $this = $(ele),
                    $select = $this.next(),
                    $button = $this.find('button');

                $select.on('click', (e) => {
                    e.stopPropagation();

                    if (!$this.hasClass('active')){
                        $this.addClass('active');
                    }
                    else {
                        $this.removeClass('active');
                    }

                    $('.dropdown-copy').not($this).removeClass('active');
                });

                $select.on('change', () => {
                    let $option = $select.find(':selected');

                    $select.prev().find('button').text($option.text());
                });

                $(window).on('click', () => {
                    if ($this.hasClass('active')){
                        $this.removeClass('active');
                    }
                });
            })

        } else if(type === 'event') {
            this.$loadMoreCont = $('.js-load-more');
            this.filters = {
                "country"  : $('#countryFilter'),
                "industry" : $('#industryFilter'),
                "year"     : $('#yearFilter'),
                "month"    : $('#monthFilter')
            };
            this.cookieName = "eventFilter";
            // this.endpoint = (isDebug) ? '/assets/fuji-xerox/api/event-results.json' : '/api/GetEventsListing';
            this.endpoint = $('.event-mapper__results').data('endpoint');
            this.pageSize = $('.event-mapper__results').data('pagesize');
            this.mapper = $('.event-mapper');
            this.resultsCont = $('.event-mapper__results', this.mapper);
            this.noResults = $('.result-status .msg');

            this.mapStory();

            let $copy = $('.dropdown-copy', this.mapper);

            $copy.map((i, ele) => {
                let $this = $(ele),
                    $select = $this.next(),
                    $button = $this.find('button');

                $select.on('click', (e) => {
                    e.stopPropagation();

                    if (!$this.hasClass('active')){
                        $this.addClass('active');
                    }
                    else {
                        $this.removeClass('active');
                    }

                    $('.dropdown-copy').not($this).removeClass('active');
                });

                $select.on('change', () => {
                    let $option = $select.find(':selected');

                    $select.prev().find('button').text($option.text());
                });

                $(window).on('click', () => {
                    if ($this.hasClass('active')){
                        $this.removeClass('active');
                    }
                });
            })
        }
    }

    mapStory() {
        let _this = this,
            $mapper =this.mapper;

        // _this.startIndex = 1;
        _this.pageNo = 1;
        _this.pageSize = this.pageSize;

        if(!$mapper.length) return;

        let resultEndpoint = _this.endpoint,
            filters = _this.filters,
            $mapperResults = _this.resultsCont;

        _this.$clearFilterBtn = $('.btn-reset', $mapper);

        // Reset Button
        _this.$clearFilterBtn.on('click', function(e){
            e.preventDefault();

            for(var key in filters) {
                let $filter = filters[key];

                $filter
                    .val($('option:first-child', filters).val())
                    .prev()
                    .find('button')
                    .text($('option:selected', $filter).text());
            }

            clearView();

            _this.pageNo = 1;

            _this.getResult(resultEndpoint, parseResults, getData(), _this.pageNo, _this.pageSize);
        });

        // Load more button
        $('.js-load-more').on('click', function(e){
            e.preventDefault();

            _this.pageNo++;

            let data = getData(e);

            _this.getResult(resultEndpoint, parseResults, getData(e), _this.pageNo, _this.pageSize);
        });


        // Setup filters
        for(var key in filters) {
            let $filter = filters[key];

            $filter.on('change.filter', function(){
                let value = $('option:selected', $filter).attr('value');

                clearView();
                _this.pageNo = 1;

                if(value !== "" && typeof value !== "undefined") {
                    Cookies.set( _this.cookieName, JSON.stringify(getData()), true);
                }

                _this.getResult(resultEndpoint, parseResults, getData(), _this.pageNo, _this.pageSize);

            });
        }

        // Process results
        let parseResults = (data) => {
            let evalStr = _this.resultTempFn({ snippets: data.results }),
                interval = 200;

            $mapperResults.children('div:first-child').append($(evalStr));

            if(data.results.length == 0){
                _this.noResults.show();
                this.$loadMoreCont.hide();
            }
            else {
                _this.noResults.hide();
                this.$loadMoreCont.show();
            }

            // _this.startIndex = data.endIndex + 1;
            // _this.page = data.page;

            // _this.toggleLoadMore(data.count, data.endIndex);
            _this.toggleLoadMore(_this.pageNo, data.totalPage);

            $('.card-tiles').matchHeight();

            /**
             * Fade In
             */
            $('.result', this.resultsCont).each(function(){
                let $this = $(this);

                if($this.hasClass('visible')) return;

                setTimeout(function(){
                    $this.addClass('visible');
                    // $('.thumbnail', $this).swapImage({breakpoint: 768, reverse: true});

                }, interval);

                interval = interval + 50;
            });
        };

        // Get Filter Data
        let getData = (excludeIndex) => {
            let data = {};

            //Exclude for cookie
            // if(excludeIndex) {
            //     data.startIndex = _this.startIndex
            //     data.page = _this.page;
            // }

            for(var key in filters) {
                let $filter = filters[key];
                data[key] = $('option:selected', $filter).attr('value')
            }

            return data;
        };

        // Clear list
        let clearView = () => {
            Cookies.remove(_this.cookieName);
            $mapperResults.children('div:first-child').empty();
            // _this.startIndex = 1;
            _this.page = 1;
        };

        // Parse Cookies data
        let parseCookies = (obj) => {
            for(var key in obj) {
                let $filter = filters[key],
                    id = obj[key];

                if(typeof id !== 'undefined') {

                    $filter
                        .val(id)
                        .prev()
                        .find('button')
                        .text($('option:selected', $filter).text())
                }
            }
        };

        // Bootstrap function
        {
            let cookie = Cookies.get(_this.cookieName);

            if(typeof cookie !== 'undefined') {
                parseCookies(JSON.parse(cookie));
            }

            _this.getResult(resultEndpoint, parseResults, getData(), _this.pageNo, _this.pageSize);
        }
    }

    /**
     * Toggle Loadmore
     */
    toggleLoadMore(pageCount, totalPage) {

        if(pageCount > totalPage - 1){
            this.$loadMoreCont.hide();
        } else {
            this.$loadMoreCont.show();
        }
    }

    /**
     * Ajax call to endpoint
     */
    getResult(endpoint, cb, data, page, pageSize) {
        let postData = {
            page,
            pageSize
        };

        $.extend(postData, data);

        $.ajax({
            url: endpoint,
            data: postData,
            dataType: 'json',
            cache: false
        }).done(cb);
    }
}
