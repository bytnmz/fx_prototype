'use strict';

import $ from 'jquery';
import 'jquery-match-height';
import doT from 'dot';

export default class SocialMedia {
	constructor() {
		let $socialFeed = $('.social-feed'),
			$socialItem = $('.social-feed-item', $socialFeed),
			$twitterContainer = $('#twitter', $socialFeed),
			$facebookContainer = $('#facebook', $socialFeed),
			$linkedinContainer = $('#linkedin', $socialFeed),
			$youtubeContainer = $('#youtube', $socialFeed);

		let _this = this;

		_this.$youtubeContainer = $youtubeContainer;

		/* Youtube API */
		const apikey = 'AIzaSyABHaUt3AlyGyyX1MG_jxYdRQ8tjU2ru-I';
		const videoID = $('#youtube', $socialFeed).data('id');
		let videoAPI = `https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=${videoID}&key=${apikey}`;

		let renderVideoResult = (data) => {
			let youtubeTemplate = $('#youtube-social').html();
			let youtubeHTML = '';

			let videoData = {};

			videoData.url = `https://www.youtube.com/watch?v=${data.items[0].id}`;
			videoData.thumbnail = data.items[0].snippet.thumbnails.default.url;
			videoData.text = data.items[0].snippet.description;

			youtubeHTML = doT.template(youtubeTemplate)(videoData);

			_this.$youtubeContainer.append(youtubeHTML);

			$socialItem.matchHeight();
		}

		// Run API calls
		this.getYoutubeVideo(videoAPI, renderVideoResult);

		/* ---------- */

	}

	getYoutubeVideo(url, callback) {
		$.ajax({
			url,
			dataType: 'json',
			type: 'GET'
		}).done(callback);
	}
}

