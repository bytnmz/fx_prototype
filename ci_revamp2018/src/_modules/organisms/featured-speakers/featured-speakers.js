'use strict';

import $ from 'jquery';
import 'slick-carousel';
import fancybox from 'fancybox';

export default class FeaturedSpeakers {
	constructor() {
		$('.event-featured-speakers__speaker').matchHeight();

		$('.bio-popup').map((i, ele) => {
		    let $this = $(ele),
		        $parent = $this.parents('.event-featured-speakers__speaker');

		    let _bioHTML = $('.event-speakers__bio', $parent).html();

		    _bioHTML = '<div class="event-speakers__bio">' + _bioHTML + '</div>';

		    $this.on('click', function(e){
		        e.preventDefault();
		        $.fancybox.open(_bioHTML);
		    });

		});
	}
}
