'use strict';

import ListingFilters from '../listing-filters';

describe('ListingFilters View', function() {

  beforeEach(() => {
    this.listingFilters = new ListingFilters();
  });

  it('Should run a few assertions', () => {
    expect(this.listingFilters).toBeDefined();
  });

});
