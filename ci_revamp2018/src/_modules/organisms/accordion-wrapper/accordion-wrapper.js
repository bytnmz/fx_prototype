'use strict';

import $ from 'jquery';

export default class AccordionWrapper {
	constructor() {
		// let $accordionWrapper = $('.accordions'),
		let $accordion = $('.accordion');

		$accordion.map((i, ele) => {
			let $this = $(ele),
				$button = $('.accordion-controller', $this);

			$button.on('click', () => {
				!$this.hasClass('open') ? this.open($this) : this.close($this);
				
				if ($this.hasClass('tab-carousel__accordion')){
					$('.tabbed-carousel__items', $this).slick('setPosition');
				}
			});

		});
	}

	open($accordion) {
		let $content = $('.accordion__content', $accordion);

		$accordion.addClass('open');
		$content.show();
	}

	close($accordion) {
		let $content = $('.accordion__content', $accordion);

		$accordion.removeClass('open');
		$content.hide();
	}
}
