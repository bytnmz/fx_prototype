'use strict';

import $ from 'jquery';

export default class AlipayForm {
	constructor() {
		let $form = $('.alipay-form form'),
			$addInvoiceButton = $('button.addNewInvoice'),
			$newInvoiceFieldContainer = $('#newInvoice');

		let $submitButton = $('button.submitForm', $form);
		let $resetButton = $('button.reset', $form);

		let invoiceFieldNumber = 2;
		let allFieldsFilled = false;

		let totalOrder = 5;
		let selectedRadio = new Array(totalOrder);
		for (let i = 0; i < totalOrder; i++)
		{
		  selectedRadio[i] = null;
		};

		selectedRadio[1] = document.getElementById("raproduct1_1");

		// add new invoice
		$addInvoiceButton.on('click', function(e){
			e.preventDefault();

			addNewInvoice();

			// if(invoiceFieldNumber <=5){
			// 	let __invoiceFieldHTML = `<div><input class="has-outline" id="product${invoiceFieldNumber}" style="width:400px;" name="product${invoiceFieldNumber}" value='' hint="富士施乐订单合同号、发票号" allownull="false" size="36"><br>
			// 			<input class="has-outline" id="raproduct${invoiceFieldNumber}_2" type="radio" name="raproduct${invoiceFieldNumber}" checked=""><label for="raproduct${invoiceFieldNumber}_2">发票号</label>
			// 			<input class="has-outline" id="raproduct${invoiceFieldNumber}_1" type="radio" name="raproduct${invoiceFieldNumber}"><label for="raproduct${invoiceFieldNumber}_1">订单\\合同号</label></div>`;
			// 	$newInvoiceFieldContainer.append(__invoiceFieldHTML);
			// 	invoiceFieldNumber++;

			// 	let countInvoice = document.getElementById("hidCountInvoice").value;
			// 	let newCountInvoice = parseInt(countInvoice) + 1;
			// 	document.getElementById("hidCountInvoice").value = newCountInvoice;
			// }
			// else {
			// 	alert('最多只能有5个订单\合同号、发票号');
			// }
		});

		$resetButton.on('click', function(e){
			e.preventDefault();
			$("input, textarea").val("");
			window.location.reload();
		});

		$form.on('submit', function(e){
			// e.preventDefault();
			return checkForm(this);
			// let price = $('#price').val(),
			//  product = $('#product1').val(),
			//  customerName = $('#customerName').val(),
			//  phone = $('#txtPhone').val(),
			//  company = $('#customercompany').val();

			// if(!price || !product || !customerName || !phone || !company){
			//  alert('请填写应付总价。');

			//  if(!price){
			//      $('#price').focus();
			//      scrollToField($('#price'));
			//  }
			//  else if (!product) {
			//      $('#product1').focus();
			//      scrollToField($('#product1'));
			//  }
			//  else if (!customerName) {
			//      $('#customerName').focus();
			//      scrollToField($('#customerName'));
			//  }
			//  else if (!phone) {
			//      $('#txtPhone').focus();
			//      scrollToField($('#txtPhone'));
			//  }
			//  else {
			//      $('#customercompany').focus();
			//      scrollToField($('#customercompany'));
			//  }

			//  return false;
			// }
		});

		function addNewInvoice()
		{
			let countInvoice = document.getElementById("hidCountInvoice").value;
			if( countInvoice >= totalOrder ) {
				// max order count
				alert("最多只能有"+totalOrder+"个订单\合同号、发票号");
			} else {
				let newCountInvoice = parseInt(countInvoice) + 1;
				document.getElementById("hidCountInvoice").value = newCountInvoice;

				let newInput = document.createElement("input");
				newInput.type="text";
				newInput.name= "product" + newCountInvoice;
				newInput.id="product" + newCountInvoice;
				newInput.value = "";
				newInput.size = "36";

				let prodSpan = document.getElementById("newInvoice");

				let br = document.createElement("p");
				br.style.marginBottom="10px";
				prodSpan.appendChild(br);
				prodSpan.appendChild(newInput);

				let span0 = document.createElement("span");
				span0.innerHTML = "&nbsp;";
				prodSpan.appendChild(span0);

				let ra2 = document.createElement("input");
				ra2.type = "radio";
				ra2.name = "raproduct" + newCountInvoice;
				ra2.id = "raproduct" + newCountInvoice + "_2";
				ra2.checked = "checked";
				ra2.checked = true;
				selectedRadio[newCountInvoice] = ra2;
				ra2.onclick = function() {
					  this.checked = true;
					  if (selectedRadio[newCountInvoice] != this) {
						selectedRadio[newCountInvoice].checked = false;
						selectedRadio[newCountInvoice] = this;
					  }
				};
				prodSpan.appendChild(ra2);
				let span2 = document.createElement("span");
				span2.innerHTML = "发票号";
				prodSpan.appendChild(span2);

				let ra = document.createElement("input");
				ra.type = "radio";
				ra.name = "raproduct" + newCountInvoice;
				ra.id = "raproduct" + newCountInvoice + "_1";

				ra.onclick = function() {
					  this.checked = true;
					  if (selectedRadio[newCountInvoice] != this) {
						selectedRadio[newCountInvoice].checked = false;
						selectedRadio[newCountInvoice] = this;
					  }
				};
				prodSpan.appendChild(ra);
				let span1 = document.createElement("span");
				span1.innerHTML = "订单\\合同号";
				prodSpan.appendChild(span1);

				document.getElementById("raproduct" + newCountInvoice + "_2").checked = "checked";
			}
		}

		function checkForm(objform){
			// validate comapany
			let comp = document.getElementsByName("CompanyName");


			// validate price
			if (document.getElementById("price").value == "")
			{
				alert("请填写应付总价。");
				document.getElementById("price").focus();
				return false;
			} else {
				// validate that price is numerical
				let price = document.getElementById("price").value,
					// reg=/^[0-9]+.?[0-9]*$/;
					// reg = /^\d+(\.\d{0,2})?$/;
					reg = /^(?:\d+\.\d{1,2}|\d+)$/;

				if( !reg.test(price) ) {
					alert("您填写的应付总价格式不正确。\n金额格式例如: 52.2或3");
					return false;
				}
			}

			// Validate products
			if (document.getElementById("product1").value == "")
			{
				alert("请填写第一个富士施乐订单\\合同号、发票号。");
				document.getElementById("product1").focus();
				return false;
			}
			//
			let count = document.getElementById("hidCountInvoice").value;
			for (let i = 1; i <= count; i++) {
				let invpo = document.getElementById("product" + i);
				let invpoVal = String(invpo.value);
				if (invpoVal != "")
				{
					if (document.getElementById("raproduct" + i +"_1").checked) {
						if (!isPoNo(invpoVal)) {
							alert("订单\\合同号必须是3位字母加上10位数字！");
							invpo.focus();
							return false;
						}
					}
					else {
						if (!isInvoiceNo(invpoVal)) {
							alert("发票号必须是8位数字！");
							invpo.focus();
							return false;
						}
					}
				}
			}

			// Validate name
			if (document.getElementById("customerName").value == "")
			{
				alert("请填写客户姓名。");
				document.getElementById("customerName").focus();
				return false;
			}

			// Validate phone number
			if (document.getElementById("txtPhone").value == "")
			{
				alert("请填写客户联系电话。");
				document.getElementById("txtPhone").focus();
				return false;
			}
			// Validate phone number format
			let phone = document.getElementById("txtPhone").value;
			if (!isPhoneNo(phone)) {
				alert("您填写的客户联系电话格式不正确。\n必须全是数字！");
				document.getElementById("txtPhone").focus();
				return false;
			}

			// Validate customer company
			if (document.getElementById("customercompany").value == "")
			{
				alert("请填写客户公司名称。");
				document.getElementById("customercompany").focus();
				return false;
			}

			if (!SummaryProduct())
			{
				return false;
			}
		}

		function SummaryProduct() {
			let productAll = new Array();
			let count = document.getElementById("hidCountInvoice").value;
			for (let i = 1; i <= count; i++) {
				let invpo = document.getElementById("product" + i);
				let invpoVal = String(invpo.value);
				if (invpoVal != "") {
					if (document.getElementById("raproduct" + i + "_1").checked) {
						productAll.push( "PONo-" + invpoVal.toUpperCase() );
					}
					else {
						productAll.push( "InvoiceNo-" + invpoVal );
					}
				}
			}
			productAll = productAll.join("|");

			let customerName = document.getElementById("customerName").value;
			let customerTel = document.getElementById("txtPhone").value;
			let customerCompany = document.getElementById("customercompany").value;
			productAll = "客户姓名-" + customerName +"|电话-" + customerTel +"|公司-" +customerCompany + "|" + productAll;
			//productAll = "客户信息-" + customerName +"-" + customerTel +"-" +custmerCompany + "|" + productAll;

			if (productAll.length > 128) {
				alert("一次输入的发票号码太多，请清除几条记录。");
				return false;
			}

			document.getElementById("hidproduct").value = productAll;
			document.getElementById("hidbody").value = "";
			//document.getElementById("hidbody").value ="客户姓名: "&customerName&"; 客户联系电话: "&customerTel&"; 客户公司名称: "&custmerCompany;
			return true;
		}

		// function scrollToField($target){
		//  $('html, body').animate({
		//      scrollTop: $target.offset().top - 150
		//  }, 500);
		// }

		// Validate invoice number format function
		function isInvoiceNo(inv) {
			let re = /^\d{8}$/;
			if (!re.test(inv)) {
				return false;
			}
			else {
				return true;
			}
		}
		// Validate invoice format function
		function isPoNo(inv) {
			let re = /^[A-Za-z]{3}\d{10}$/;
			if (!re.test(inv)) {
				return false;
			}
			else {
				return true;
			}
		}
		// Validate phone number format function
		function isPhoneNo(inv) {
			let re = /^[0-9]*$/;
			if (!re.test(inv)) {
				return false;
			}
			else {
				return true;
			}
		}
	}
}
