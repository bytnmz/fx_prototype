'use strict';

import RelatedPages from '../related-pages';

describe('RelatedPages View', function() {

  beforeEach(() => {
    this.relatedPages = new RelatedPages();
  });

  it('Should run a few assertions', () => {
    expect(this.relatedPages).toBeDefined();
  });

});
