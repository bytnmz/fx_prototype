'use strict';

import $ from 'jquery';
import enquire from 'enquire.js';
import doT from 'dot';
import 'jquery-match-height';

export default class NewsroomWrapper {
	constructor() {
		this.name = 'newsroom';
		// console.log('%s module', this.name);

		let $newsroom = $('.newsroom'),
		    $newsListing = $('.newsroom-listing', $newsroom),
		    // corporateNewsURL = $newsListing.data('corporate'),
		    // standardNewsURL = $newsListing.data('standard');
		    corporateNewsURL = '/api/getNewsByCategory',
		    standardNewsURL = '/api/getNewsByCategory';

		enquire.register("screen and (max-width: 767px)", {
		    match: function(){
		        $('.newsroom-featured').append( $('.panel-blog'));
		    },

		    unmatch: function(){
		        $('.newsroom-featured .column-row:nth-child(2)').prepend( $('.panel-blog'));
		    }
		});


		/**
		 * On page load
		 */
	    var $corporateNews    = $('.corporate-news', $newsListing),
	        $standardNews     = $('.standard-news', $newsListing),
	        $yearFilter       = $('.newsroom-listing__sort-result select', $newsListing),
	        corporateNewsPage = 1,
	        standardNewsPage  = 1,
	        corporateEnd      = false,
	        standardEnd       = false,
	        source            = $('#results-template').html(),
	        template          = doT.template(source);

        $('.no-result').css({'display': 'none'});
        loadNews('standard', standardNewsURL, $standardNews, standardNewsPage);
        loadNews('corporate', corporateNewsURL, $corporateNews, corporateNewsPage);

        $('.newsroom-featured__block .desc, .newsroom-featured__download-block .desc').matchHeight();
        $('.newsroom-featured > .col-sm-7, .newsroom-featured > .col-sm-5').matchHeight();
        $('.newsroom-featured__block .desc, .newsroom-featured__download-block .desc').matchHeight._afterUpdate = function() {
            $('.newsroom-featured > .col-sm-7, .newsroom-featured > .col-sm-5').matchHeight();
        };

	    /**
	     * load more standard news
	     */
	    $('.js-load-more', $standardNews).on('click', function(e) {
	        e.preventDefault();

	        loadNews('standard', standardNewsURL, $standardNews, standardNewsPage);

	        $(this).blur();
	    });

	    /**
	     * load more corporate news
	     */
	    $('.js-load-more', $corporateNews).on('click', function(e) {
	        e.preventDefault();

	        loadNews('corporate', corporateNewsURL, $corporateNews, corporateNewsPage);

	        $(this).blur();
	    });


	    /**
	     * Load all news sections
	     */
	    $('.js-load-all', $newsroom).on('click', function(e){
	        e.preventDefault();

	        loadNews('standard', standardNewsURL, $standardNews, standardNewsPage);
	        loadNews('corporate', corporateNewsURL, $corporateNews, corporateNewsPage);

	        $(this).blur();
	    });

	    let $sorter = $('.newsroom-listing__sort-result'),
	        $sortSelect = $('.newsroom-listing__sort-result select');

	    $sorter.map((i, ele) => {
	        let $this = $(ele),
	            $clone = $('.dropdown-copy', $this),
	            $dropdown = $('ul', $this),
	            $button = $('> button', $clone);

	        $button.on('click', (e) => {
	            e.stopPropagation();
	            $dropdown.toggle();
	        });

	        let $option = $('li button', $dropdown);

	        $option.map((j, ele) => {
	            let $this2 = $(ele);

	            $this2.on('click', () => {
	                let value = $this2.val(),
	                    text = $this2.text();

	                $button.text(text);
	                $button.val(value);
	                $('select', $this).val(value);
	                $sortSelect.trigger('change');
	            });
	        });
	    });

	    $(window).on('click', () => {
	        $('ul', $sorter).hide();
	    });

	    /**
	     * On year archive filter
	     */
	     $yearFilter.on('change', function(){
	        $('.news-results', $standardNews).empty();
	        $('.news-results', $corporateNews).empty();

	        corporateNewsPage = 1;
	        standardNewsPage  = 1;

	        loadNews('standard', standardNewsURL, $standardNews, standardNewsPage);
	        loadNews('corporate', corporateNewsURL, $corporateNews, corporateNewsPage);
	    });

	    function loadNews(type, url, $newsObj, pageNum) {

	        var $btn = $('.js-load-more', $newsObj);

	        $('.loader-animation', $newsObj).css({'display': 'inline-block'});
	        $.ajax({
	            url: url,
	            data: {'category': $newsObj.data('category'), 'page': pageNum, 'year': $yearFilter.val()},
	            dataType: 'json',
	            cache: false
	        }).done(function(data) {
	            var pageCount,
	                $noMoreResult = $('.no-result');

	            $('.loader-animation', $newsObj).css({'display': ''});

	            // Type of news request
	            if(type === 'standard') {
	                standardNewsPage++;
	                pageCount = standardNewsPage;
	            } else {
	                corporateNewsPage++;
	                pageCount = corporateNewsPage;
	            }

	            // Append results if end page is not reached
	            if(pageCount - 2 < parseInt(data.totalPage)) {
	                $('.news-results', $newsObj).append(template({news: data.news}));
	            } else {
	                $('.no-result', $newsObj).css({'display': 'inline-block'});

	                setTimeout(function(){
	                    $noMoreResult.slideUp(function(){
	                        $('.no-result', $newsObj).css({'display': 'none'});
	                    });
	                }, 2000);
	            }

	            if(pageCount > parseInt(data.totalPage)){
	                $btn.hide();
	            } else {
	                $btn.show();
	            }
	        });
	    }
	}
}
