'use strict';

import $ from 'jquery';
import 'jquery-match-height';
import 'slick-carousel';

export default class Carousel {
	constructor() {
		$('.carousel').map((i, ele) => {
			let $this = $(ele);

			$('.carousel__items', $this).slick({
				slidesToShow: 3,
				dots: true,
				arrows: false,
				infinite: false,
				swipe: false,
				appendDots: $('.carousel__dots', $this),
				responsive: [
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							swipe: true,
							infinite: true
						}
					}
				]
			});
		});

		$('.two-col-carousel').map((i, ele) => {
			let $this = $(ele);

			$('.two-col-carousel__items', $this).slick({
				slidesToShow: 2,
				dots: false,
				arrows: true,
				infinite: true,
				appendDots: $('.two-col-carousel__dots', $this),
				prevArrow: '<button type="button" class="slick-prev prev"><span>Previous</span></button>',
				nextArrow: '<button type="button" class="slick-next next"><span>Next</span></button>',
				responsive: [
					{
						breakpoint: 768,
						settings: {
							dots: true,
							arrows: false,
							slidesToShow: 1,
							slidesToScroll: 1,
							swipe: true,
							infinite: true
						}
					}
				]
			});
		});

		$('.three-four-carousel').map((i, ele) => {
			let $this = $(ele);
			$('.three-four-carousel__items', $this).slick({
				slidesToShow: 1,
				dots: false,
				arrows: true,
				infinite: true,
				appendDots: $('.three-four-carousel__dots', $this),
				prevArrow: '<button type="button" class="slick-prev prev"><span>Previous</span></button>',
				nextArrow: '<button type="button" class="slick-next next"><span>Next</span></button>',
				responsive: [
					{
						breakpoint: 768,
						settings: {
							dots: true,
							arrows: false,
							slidesToShow: 1,
							slidesToScroll: 1,
							swipe: true,
							infinite: true
						}
					}
				]
			});
		});

		$('.tabbed-carousel').map((i,ele) => {
			let $this = $(ele);

			$('.tabbed-carousel__items', $this).slick({
				slidesToShow: 4,
				dots: false,
				arrows: true,
				infinite: true,
				appendDots: $('.tabbed-carousel__dots', $this),
				prevArrow: '<button type="button" class="slick-prev prev"><span>Previous</span></button>',
				nextArrow: '<button type="button" class="slick-next next"><span>Next</span></button>',
				responsive: [
					{
						breakpoint: 768,
						settings: {
							dots: true,
							arrows: false,
							slidesToShow: 1,
							slidesToScroll: 1,
							swipe: true,
							infinite: true
						}
					}
				]
			});
		});
	}
}
