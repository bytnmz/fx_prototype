'use strict';

import $ from 'jquery';

export default class TabCarousel {
	constructor() {
		let $tabsCarousel = $('.tab-carousel');

		$tabsCarousel.map((i,ele) => {
			let $tabs = $('.tab-carousel__tabs ul li', $(ele));

			if($tabs.length){
				switch($tabs.length) {
					case 1:
						$tabs.css('width', '100%');
						break;
					case 2:
						$tabs.css('width', '50%');
						break;
					case 3:
						$tabs.css('width', '33.333333%');
						break;
					case 4:
						$tabs.css('width', '25%');
						break;
				}
			}
		});
	}
}
