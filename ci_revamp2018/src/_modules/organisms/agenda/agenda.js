'use strict';

import $ from 'jquery';

export default class Agenda {
    constructor() {
        this.name = 'agenda';
        // console.log('%s module', this.name);

        let $agendas = $('.event-agenda'),
            _this = this;

        $agendas.each(function(){
            _this.init($(this));
        });
    }

    init($obj) {
        let $agenda = $obj,
            $agendaTables = $('.event-agenda__day-table', $agenda),
            $dayToggles = $('.day-toggle a', $agenda);

        //Toggle agenda time table for the day
        $dayToggles.on('click', function(e){
            e.preventDefault();

            let $this = $(this),
                idx = $this.index(),
                $agendaTable = $agendaTables.eq(idx),
                $twinButtons =  $this.attr('class');

            $dayToggles.removeClass('active');

            $agendaTables.slideUp();
            $agendaTable.stop().slideDown();

            $('.' + $twinButtons).addClass('active');

            $('html, body').stop().animate({'scrollTop': $agenda.offset().top});
        });


        //Setup Synopsis toggle
        $('.synopsis', $agendaTables).each(function(){

            let $this = $(this),
                $toggle = $this.parent().find('.js-synopsis-toggle');

            $this.hide();

            $toggle.on('click', function(e){
                e.preventDefault();

                if($this.css('display') !== 'block') {
                    $this.slideDown();
                    $toggle.addClass('visible');
                } else {
                    $this.slideUp();
                    $toggle.removeClass('visible');
                }
            });
        });


        // //Setup tabble accordion
        // $('.event-agenda__day-table table thead').on('click', function(e){
        //     e.preventDefault();

        //     let $this = $(this),
        //         $accordion = $this.next();

        //     $accordion.not(':animated').slideToggle();
        // });
    }
}
