'use strict';

import doT from 'dot';
import jsonQuery from 'json-query';
import Cookies from 'js-cookie';
import $ from 'jquery';

export default class SolutionsServices {
	constructor() {
		let $solutionFilter = $('.solutions-filter'),
			$filter = $('.filter-item', $solutionFilter).not('.filter-item--reset'),
			$reset = $('.filter-item--reset .btn-reset', $solutionFilter),
			$copy = $('.dropdown-copy', $solutionFilter);

		this.loadLimit    = 9;
		this.currpage     = 1;
		this.results = [];
		this.resultTempFn = doT.template($('#dotSolutionsTemplate').html());
		this.optionTempFn = doT.template("{{~it.data :value:index}}<option value= \"{{=value.id}}\">{{=value.name}}</option>{{~}}");
        this.resultsCont = $('.solution-services__content', $('.solution-services'));
        this.mapSolution();

        $copy.map((i, ele) => {
        	let $this = $(ele),
        		$select = $this.next(),
        		$button = $this.find('button');

	        $select.on('click', (e) => {
				e.stopPropagation();

				if (!$this.hasClass('active')){
	        		$this.addClass('active');
				}
				else {
	        		$this.removeClass('active');
				}

				$('.dropdown-copy').not($this).removeClass('active');
	        });

			$select.on('change', () => {
				let $option = $select.find(':selected');

				$select.prev().find('button').text($option.text());
			});

			$(window).on('click', () => {
				if ($this.hasClass('active')){
					$this.removeClass('active');
				}
			});
        })
	}

	mapSolution() {
	    let _this = this,
	    	$solutionFilter = $('.solutions-filter'),
	        $mapper = $('.solution-mapper');

	    let resultEndpoint = $('.solution-services__content').data('endpoint'),
	    	optionsEndpoint = $('.solutions-filter').data('endpoint'),
	        $mapperResults       = $('.solution-services__content .column-row'),
	        selectionData = {},
	        filters = {
	            "industry"   : $('#industryFilter'),
	            "department" : $('#departmentFilter'),
	            "service"    : $('#serviceFilter'),
	            "business"   : $('#businessFilter')
	        },
	        labels = {
	            "industry"   : $('#industryFilter').data('label'),
	            "department" : $('#departmentFilter').data('label'),
	            "service"    : $('#serviceFilter').data('label'),
	            "business"   : $('#businessFilter').data('label')
	        }

	    _this.$clearFilterBtn = $('.filter-item--reset .btn-reset', $solutionFilter);

	    // Reset filters
	    _this.$clearFilterBtn.on('click', function(e){
	        e.preventDefault();

	        filters.industry
	            .val($('option:first-child', filters.industry).val())
	            .trigger('change');

	        hideFilters('industry');
            clearResults();
            resetDropdownText();
	    });

	    // Setup filters
	    for(var key in filters) {
	        let $filter = filters[key];


	        $filter.data('key', key);

	        $filter.on('change.filter', function(){
	            let value = $('option:selected', $filter).attr('value'),
	                dataKey = $filter.data('key'),
	                data = {
	                    industry: $('option:selected', filters.industry).attr('value'),
	                    department: $('option:selected', filters.department).attr('value'),
	                    service: $('option:selected', filters.service).attr('value'),
	                    business: $('option:selected', filters.business).attr('value')
	                };

	            if(value !== "" && typeof value !== "undefined") {
	                updateFilterOnChange(dataKey, value);
	                Cookies.set('solutionFilters', JSON.stringify(data));
	            } else {
	                hideFilters(dataKey);

	                if(dataKey === 'industry') {
	                    $mapperResults.empty;

	                    return;
	                }
	            }
	            _this.resetPage();
	            $mapperResults.empty();
	            _this.getResult(resultEndpoint, parseResults, data);
	        });
	    }

	    // Parse filters option data
	    let parseFilters = (data) => {
	        selectionData = data;

	        let cookie = Cookies.get('solutionFilters');

	        _this.updateOptions(selectionData.industries, filters.industry, labels.industry);

	        if(typeof cookie !== 'undefined') {
	            parseCookies(JSON.parse(cookie));
	        } else {
	            clearResults();
	        }
	    };

	    // Parse Cookies data
	    let parseCookies = (obj) => {
	        let $lastFilter = {};

	        for(var key in obj) {
	            let $filter = filters[key],
	                id = obj[key];

	            if(typeof id !== 'undefined') {
	                updateFilterOnChange(key, id);

	                $filter
	                    .val(id)
	                    .prev()
	                    .find('button')
	                    .text($('option:selected', $filter).text())

	                $lastFilter = $filter
	            }
	        }

	        $lastFilter.trigger('change.filter');
	    };

	    // Clear Results
	    let clearResults = () => {
	    	let data = {
	    	    queryAll: 'true'
	    	}

	        _this.resetPage();
	        $mapperResults.empty();
	    	_this.getResult(resultEndpoint, parseResults, data);
	        Cookies.remove('solutionFilters');
	    };

	    // Parse Result set
	    let parseResults = (data) => {
	        let solutionsTemplate = $('#dotSolutionsTemplate').html(),
	        	_content = '';

	        let _this = this,
	            lowerLimit = (_this.currpage - 1) * _this.loadLimit,
	            upperLimit = _this.currpage * _this.loadLimit,
	            pageResults = [],
	            interval = 200;

	        _this.results = data;

	        for(let i = lowerLimit; i < upperLimit; i++) {
	            if(typeof _this.results[i] !== 'undefined'){
	                pageResults.push(_this.results[i]);
	            }
	        }

	        if(_this.results.length > 9) {
		        $('.show-more').show();
		    } else {
		        $('.show-more').hide();
		    }

	        $mapperResults.append(doT.template(solutionsTemplate)(pageResults));

	        $('.card-tiles').matchHeight();

	        /**
	         * Fade In
	         */
	        $('.result', this.resultsCont).each(function(){
	            let $this = $(this);

	            if($this.hasClass('visible')) return;

	            setTimeout(function(){
	                $this.addClass('visible');
	                // $('.thumbnail', $this).swapImage({breakpoint: 768, reverse: true});

	            }, interval);

	            interval = interval + 50;
	        });

	        if(_this.currpage === 1){
	        	_this.currpage++;
	        }
	    };

	    // Update the next filter
	    let updateFilterOnChange = (key, id) => {
	        let dataset = {};

	        switch(key) {
	            case 'industry':
	                dataset = jsonQuery('industries[id=' + id +'].departments', {data: selectionData}).value;
	                _this.updateOptions(dataset, filters.department, labels.department);
	                break;

	            case 'department':
	                dataset = jsonQuery('industries.departments[id=' + id +'].services', {data: selectionData}).value;
	                _this.updateOptions(dataset, filters.service, labels.service);
	                break;

	            case 'service':
	                dataset = jsonQuery('industries.departments.services[id=' + id +'].businesses', {data: selectionData}).value;
	                _this.updateOptions(dataset, filters.business, labels.business);
	                break;
	        }
	    };

	    // Hide the filters
	    let hideFilters = (key) => {
	        switch(key) {
	            case 'industry':
	                filters.department
	                    .val(null)
	                    .parent().fadeOut();
	                /* falls through */
	            case 'department':
	                filters.service
	                    .val(null)
	                    .parent().fadeOut();
	                /* falls through */
	            case 'service':
	                filters.business
	                    .val(null)
	                    .parent().fadeOut();
	                /* falls through */
	            default:
	                break;
	        }
	    };

	    let resetDropdownText = () => {
	    	$('.filter-item:not(".filter-item--reset")').map((i, ele) => {
	    		let $this = $(ele);

	    		let defaultLabel = $this.find('select option:first-child').text();
	    		$this.find('.dropdown-copy button').text(defaultLabel);
	    	});
	    }

	    //Load more tiles
	    $('.show-more').on('click', (e) => {
	        e.preventDefault();

	        parseResults(this.results);
	        this.toggleLoadMore();
	    });

	    _this.getResult(optionsEndpoint, parseFilters);
	}

	updateOptions(context, $filter, label) {
	    let evalStr = this.optionTempFn({data: context});

	    $filter
	        .html(evalStr)
	        .prepend($('<option val = "" selected>' + label + '</option>'))
	        .parent()
	        .fadeIn();
	}

	toggleLoadMore() {
	    let _this = this;

	    if(_this.currpage * _this.loadLimit < _this.results.length) {
	        _this.currpage++;

	        $('.show-more').show();
	    } else {
	        $('.show-more').hide();
	    }
	}

	resetPage() {
		let _this = this;

		_this.currpage = 1;
	}

	/**
	 * Ajax call to endpoint
	 */
	getResult(endpoint, cb, data) {
	    let postData = {};

	    $.extend(postData, data);

	    $.ajax({
	        url: endpoint,
	        data: postData,
	        dataType: 'json',
	        cache: false
	    }).done(cb);
	}
}
