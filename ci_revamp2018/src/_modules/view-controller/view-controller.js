'use strict';

import $ from 'jquery';
import 'jquery-match-height';

export default class ViewController {
	constructor() {
		let $viewController = $('.filter-item--view');

		$('> button', $viewController).map((i, ele) => {
			let $this = $(ele);

			$this.on('click', () => {
				let viewType = $this.data('view');

				if($this.hasClass('active')){
					return;
				}
				else {
					$this.parent().find('.active').removeClass('active');
					$this.addClass('active');
				}

				if (viewType === 'list') {
					$('.content-holder').addClass('list');

					$('.card-tiles').each(function() {
						let $this = $(this);

						if(!$('.card-tiles__image', $this).length){
							if(!$('.card-tiles__text', $this).hasClass('full')){
								$('.card-tiles__text', $this).addClass('full');
							}
						}
					});
				}
				else {
					$('.content-holder').removeClass('list');
				}

				$('.match-height').matchHeight._update();

			});
			
		});
		
	}
}
