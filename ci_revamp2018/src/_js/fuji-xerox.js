// Main javascript entry point
// Should handle bootstrapping/starting application

'use strict';

import 'match-media';
import $ from 'jquery';
import 'fancybox';
import 'jquery-match-height';
import 'slick-carousel';
import 'tablesorter';
import objectFitImages from 'object-fit-images';
import objectFitVideos from 'object-fit-videos';
import TableResponsive from '../_modules/table-responsive/table-responsive';
import CloneSelect from '../_modules/clone-select/clone-select';
import ViewController from '../_modules/view-controller/view-controller';

import NavTabsAnchor from '../_modules/molecules/nav-tabs-anchor/nav-tabs-anchor';
import SubscribeForm from '../_modules/molecules/subscribe-form/subscribe-form';
import Modal from '../_modules/organisms/modal/modal';

// import SocialMedia from '../_modules/organisms/social-media/social-media';
import Carousel from '../_modules/organisms/carousel/carousel';
import SolutionsServices from '../_modules/organisms/solutions-services/solutions-services';
import InsightComponent from '../_modules/organisms/insight-component/insight-component';
import AccordionWrapper from '../_modules/organisms/accordion-wrapper/accordion-wrapper';
import ListingFilters from '../_modules/organisms/listing-filters/listing-filters';
import SiteHeader from '../_modules/organisms/site-header/site-header';
import Agenda from '../_modules/organisms/agenda/agenda';
import FeaturedSpeakers from '../_modules/organisms/featured-speakers/featured-speakers';
import NewsroomWrapper from '../_modules/organisms/newsroom-wrapper/newsroom-wrapper';
import ResellerLocator from '../_modules/organisms/reseller-locator/reseller-locator';
import ImageLinksWrapper from '../_modules/organisms/image-links-wrapper/image-links-wrapper';
import Mapper from '../_modules/organisms/mapper/mapper';
import Form from '../_modules/organisms/form/form';
import Tabs from '../_modules/organisms/tabs/tabs';
import TabCarousel from '../_modules/organisms/tab-carousel/tab-carousel';
import WffmAddRemoveFields from '../_modules/wffm-add-remove-fields/wffm-add-remove-fields';
import StoreLocator from '../_modules/organisms/store-locator/store-locator';
import AlipayForm from '../_modules/organisms/alipay-form/alipay-form';

(function($){
	$(() => {
		//Polyfill for object-fit for images and videos
		objectFitImages();
		objectFitVideos();

		// Apply wrapper for table
		if ($('table').length) {
			new TableResponsive();
		}

		if ($('.sorting-table').length) {
			$('.sorting-table').tablesorter();
		}

		new CloneSelect();
		new SiteHeader();
		new Modal();
		new Carousel();
		new TabCarousel();
		new NavTabsAnchor();
		new AccordionWrapper();
	    new ImageLinksWrapper();
		new Tabs();

	    if($('.subscribe-form').length){
	    	new SubscribeForm();
	    }

		if($('.event-agenda').length){
			new Agenda();
		}

		if($('.event-featured-speakers-wrapper').length){
			new FeaturedSpeakers();
		}

	    new Form();

        if($('fieldset.CollapsibleFieldSet').length){
    		new WffmAddRemoveFields();
    	}

		if($('.newsroom').length){
			new NewsroomWrapper();
		}

		if($('.product-listing').length){
	        new ListingFilters('products');
	    }

		if($('.career-listing').length){
	        new ListingFilters('careers');
	    }

	    if($('.solution-services').length){
	    	new SolutionsServices();
	    }

	    if($('.insight').length){
	    	new ViewController();
	    	new InsightComponent();
	    }

	    if($('.success-stories-mapper').length){
	        new Mapper('story');
	    }
	    if($('.event-mapper').length){
	        new Mapper('event');
	    }

	    if($('.reseller-locator').length){
			if($('.google-locator').length){
				new StoreLocator('google');
			}

			else if($('.baidu-locator').length){
				new StoreLocator('baidu');
			}

			else if($('.naver-locator').length){
				new StoreLocator('naver');
			}

			else {
				new ResellerLocator();
			}
		}

		if($('.alipay-form').length){
			new AlipayForm();
		}

		// match height //
		$('.match-height').matchHeight();

		/**
		 * Focus outline handler
		 */
		$('body').addClass().on('keydown', function(e){
			if(e.which === 9){
				$('body').removeClass('no-focus-outline');
			}
		}).on('mouseup', function(){
			$('body').addClass('no-focus-outline');
		});

	    /**
		 * Back to top
		 */
		let backToTopText = $('#back-to-top').data('text');
		let $wrapper = $('<div/>'),
		    $anchor = $('<i class="icon-angle-up"></i><span class="label">' + backToTopText + '</span>');

		$wrapper
		    .addClass('back-to-top')
		    .append($anchor);

		$('body').append($wrapper);

		/**
		 * Bind Behaviours
		 */
		$wrapper
		    .on('click', function(e){
		        $('html, body').stop().animate({'scrollTop': 0});
		        e.preventDefault();
		    });

		var scrolling = function(){
		    var scrollTop = $(window).scrollTop();

		    if(scrollTop > $(window).height()) {
		        $wrapper.fadeIn();
		    } else {
		        $wrapper.fadeOut();
			}

			console.log(scrollTop);

			if (scrollTop > 80){
				if(!$('.sticky-buttons').hasClass('visible')){
					$('.sticky-buttons').addClass('visible');
				}

				if (scrollTop > $(document).height() - $(window).height() - 80) {
					if ($('.sticky-buttons').hasClass('visible')) {
						$('.sticky-buttons').removeClass('visible');
					}
				}
			} else {
				if ($('.sticky-buttons').hasClass('visible')) {
					$('.sticky-buttons').removeClass('visible');
				}
			}
		};

		/**
		 * On Scroll
		 */
		$(window).on('scroll.window', scrolling).trigger('scroll.window');
	});
})(jQuery)
