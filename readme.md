Read Me
====

Use the gruntfile as a template task runner for your Front End project, and edit the tasks to fit your project requirements.

The gruntfile applies to the following project structure by default. 
All minified vendor javascript files should be named with the convention '**/*.min.js'.
Minified files are excluded from modules.js and plugins.js to prevent them from going through uglification again. Remove exclusion in concat task and remove 'concat:combine' from tasks to use modules.js and plugins.js with all JS.

    _scss
        lib
    _scripts
        modules
        plugins
            vendor
        vendor
    _images
    _json
    _font
    _styles
    _templates

###Dependencies

**Ruby Gem**

* sass ([http://sass-lang.com/](http://sass-lang.com/))
* scss-lint ([https://github.com/causes/scss-lint](https://github.com/causes/scss-lint))

**Grunt Plugins**

* grunt-contrib-jshint
* grunt-contrib-uglify
* grunt-contrib-copy
* grunt-contrib-clean
* grunt-contrib-concat
* grunt-contrib-watch
* grunt-contrib-imagemin
* grunt-contrib-cssmin
* grunt-shell
* grunt-combine-media-queries
* grunt-processhtml

###Available Tasks

    default
    dist
    validate
    watch


###Task Descriptions

####default
* Concatenate, uglify and modules and plugins folder
* Copy images, templates, fonts and vendor stylesheet to dist
* Run Sass
* Minify generated css files

#### dist
* Concatenate, uglify and modules and plugins folder
* Copy images, templates, fonts and vendor stylesheet to dist folder
* Run Sass
* Minify generated css files
* Compress images in dist folder

####validate
* Run jshint on js files in _scripts folder
* Run scss lint on scss files in _scss folder(require .scss-lint.yml configuration file in _scss folder)
