/**
 * Modals
 */
var RR = (function(parent, $, undefined){

    var $modals = $('.modal'),
        $html   = $('html'),
        $fixedEle = $('.side-menu, .quick-links'),
        $win    = $(window),
        resizeTimeout;

    (function(){
        if(!$modals.length) return;

        $modals.each(function(){
            var $modal = $(this),
                $closeBtn = $('.modal-close-button a', $modal);

            $closeBtn.on('click', function(e){
                e.preventDefault();

                $modal.fadeOut(function(){
                     $html.removeClass('fancybox-margin fancybox-lock');
                     $fixedEle.removeClass('fancybox-margin');
                     console.log('close')

                     clearInterval(resizeTimeout);
                });
            });
        });

        $win.on('resize.modalbox scroll.modalbox', function(){
            var scrollTop      =  $win.scrollTop(),
                viewportHeight = $win.innerHeight();

            // if($win.width() < 1024) {
            //     viewportHeight += 200;
            // }

            $modals.css({ 'top': scrollTop, 'height': viewportHeight });

        }).trigger('resize.modalbox');
    }());

    /**
     * Modal Box Trigger
     */
    $('body').on('click', '.modalbox', function(e){
        e.preventDefault();

        var $link = $(this),
            $target = $($link.attr('href')),
            productid = $link.data('productid'),
            iframeURL = $link.data('iframe');

        $html.addClass('fancybox-margin fancybox-lock');
        $.each($fixedEle, function() {
            if ($(this).css('position') == 'fixed') {
                $(this).addClass('fancybox-margin');
            }
        });
        $win.trigger('resize.modalbox');
        $target.fadeIn();

        if(typeof productid !== 'undefined') {
            $('#productId').val(productid);
            $('#productField').val($('.product-name', $link.closest('.desc')).text());
            $target.find('.modal-form').html('<iframe src="' + iframeURL + '" />');
            $('iframe').load(function() {
                clearInterval(resizeTimeout);
                var iframe = this,
                oriHeight = $(this.contentWindow.document.body).height();
                this.style.height = ($(this.contentWindow.document.body).height() + 37) + 'px';

                resizeTimeout = setInterval(function() {
                    if ($(iframe.contentWindow.document.body).height() != oriHeight) {
                        iframe.style.height = ($(iframe.contentWindow.document.body).height() + 37) + 'px';
                        clearInterval(resizeTimeout);
                    }
                }, 200);
            });

        } else {
            $('#productId').val('undefined');
        }
    });

    return parent;

}(RR || {}, jQuery));