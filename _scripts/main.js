(function($){
    $(function(){

        /*
         * Find all modal markup
         * and append to the end of the document if not already a direct child of 'body'
         */
        $('.modal').each(function(){
            var $this = $(this);

            if(!$this.parent().is('body')) {
                $('body').append($this);
            }
        });

        /**
         * Search Hijack
         */
        (function(){
            var $input = $('#mod-search-searchword');

            if(!$input.length) return;

            var $form = $input.parents('form');

            $form.on('submit', function(e){

                if(typeof window.searchurl !== 'undefined') {
                    e.preventDefault();

                    window.location =  window.searchurl + $input.val();
                }
            });
        }());

        //$('.home-carousel .slide a').swapeImage({breakpoint: 960});

        /**
         * Swap banner image between background image and inline image
         */
        $('.top-banner__image').swapeImage({breakpoint: 768});
        $('.newsroom-featured__block:not(.highlight-block) .image').swapeImage({breakpoint: 768, reverse: true});
        $('.newsroom-featured__download-block .image').swapeImage({breakpoint: 768, reverse: true});


        /**
         * Custom Select Fields
         */
        $('.custom-select').customSelect();

        /**
         * Stackable Tables
         */
        if ($('.scEnabledChrome').length > 0) {
            $('.stackable').cardtable();
        }

        /**
         * Matchheight
         */
        $('.match-height').matchHeight();
        $('.related-pages__block .category').matchHeight();

        /**
         * Fancybox
         */
        $('.fancybox').fancybox({
            title       : null,
            maxWidth    : 800,
            maxHeight   : '80%',
            fitToView   : false,
            width       : '80%',
            height      : 'auto',
            autoSize    : false,
            closeClick  : false,
            openEffect  : 'none',
            closeEffect : 'none',
            helpers : {
                media : {}
            }
        });

        $('.fancybox.video').fancybox({
            title       : null,
            maxWidth    : 800,
            maxHeight   : '80%',
            fitToView   : false,
            width       : '80%',
            height      : '50%',
            autoSize    : false,
            closeClick  : false,
            openEffect  : 'none',
            closeEffect : 'none',
            helpers : {
                media : {}
            }
        });

        /**
         * Register click event with trackClick class
         */
        $('.trackClick').each(function(){

            $(this).on('click.tracker', function(e) {
                e.preventDefault();

                var $this = $(this),
                    attributes = $this.data('track-attributes'),
                    link = $this.attr('href');

                $.ajax({
                    url: "/api/registerEvent?" + attributes
                });

                setTimeout(function(){
                    window.location.href = link;
                }, 300);
            });
        });

        /*Scroll after submission*/
        $('.sitecore-form form').on('submit', function() {
            $('html, body').animate( {
                scrollTop: $('.sitecore-form').offset().top
            });
        });

        RR.navigation.setup();
        RR.accordion.setup();

        /**
         * EnquireJS
         */
        enquire.register("screen and (max-width: 600px)", {
            match: function(){
                 RR.homeCarousel.setupMobileBanner();
            }
        });

        enquire.register("screen and (max-width: 767px)", {
            match: function(){
                $('.newsroom-featured ').append( $('.newsroom-featured__blog-block'));
            },

            unmatch: function(){
                $('.newsroom-featured__block.highlight-block').after( $('.newsroom-featured__blog-block'));
            }
        });

        enquire.register("screen and (min-width: 960px)", {
            match: function(){
                RR.articleQuicklinks.setupSticky();
                RR.homeCarousel.setupCustomControl();
            },

            unmatch: function(){
                 RR.articleQuicklinks.destroySticky();
            }
        }, true);
    });

}(jQuery));