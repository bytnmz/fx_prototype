// Main javascript entry point
// Should handle bootstrapping/starting application

'use strict';

import $ from 'jquery';

import fancybox from 'fancybox';
import 'helpers'; //Fancybox media helpers

// This don't work unless we use the minified version for 2.1.3 and above
import enquire        from '../../node_modules/enquire.js/dist/enquire.min.js';

import select         from '../_modules/atoms/select/select';

import backTop        from '../_modules/molecules/back-top/back-top';
import quicklinks     from '../_modules/molecules/quicklinks/quicklinks';

import mainNavigation from '../_modules/organisms/main-navigation/main-navigation';
import carousel       from '../_modules/organisms/carousel/carousel';
import modal          from '../_modules/organisms/modal/modal';
import topBanner      from '../_modules/organisms/top-banner/top-banner';
import mapper         from '../_modules/organisms/mapper/mapper';
import sideMenu       from '../_modules/organisms/side-menu/side-menu';
import listingFilters from '../_modules/organisms/listing-filters/listing-filters';
import agenda         from '../_modules/organisms/agenda/agenda';
import eventSpeakers  from '../_modules/organisms/event-speakers/event-speakers';
import accordion      from '../_modules/organisms/accordion/accordion';
import form           from '../_modules/organisms/form/form';

import newsroom       from '../_modules/templates/newsroom/newsroom';





$(() => {
    let isDebug = true;

    let _select         = new select;
    let _mainNavigation = new mainNavigation;
    let _carousel       = new carousel;
    let _sideMenu       = new sideMenu;
    let _modal          = new modal;
    let _topBanner      = new topBanner;
    let _backTop        = new backTop;
    let _quicklinks     = new quicklinks;
    let _accordion      = new accordion;
    let _agenda         = new agenda;
    let _eventSpeakers  = new eventSpeakers;
    let _newsroom       = new newsroom(isDebug);
    let _form           = new form();

    if($('.solution-mapper').length){
        let _solutionMapper = new mapper('solution', isDebug);
    }
    if($('.story-mapper').length){
        let _storyMapper    = new mapper('story', isDebug);
    }
    if($('.event-mapper').length){
        let _eventMapper    = new mapper('event', isDebug);
    }

    if($('.product-listing').length){
        let _productFilters = new listingFilters('products', isDebug);
    }
    if($('.career-listing').length){
        let _careerFilters  = new listingFilters('careers', isDebug);
    }

    /**
     * Matchheight
     */
    $('.match-height').matchHeight();
    $('.related-pages__block .category').matchHeight();

    $('.newsroom-featured__block:not(.highlight-block) .image').swapImage({breakpoint: 768, reverse: true});
    $('.newsroom-featured__download-block .image').swapImage({breakpoint: 768, reverse: true});


    /**
     * Fancybox
     */
    $('.fancybox').fancybox({
        title       : null,
        maxWidth    : 800,
        maxHeight   : '80%',
        fitToView   : false,
        width       : '80%',
        height      : 'auto',
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        helpers : {
            media : {}
        }
    });

    $('.fancybox.video').fancybox({
        title       : null,
        maxWidth    : 800,
        maxHeight   : '80%',
        fitToView   : false,
        width       : '80%',
        height      : '50%',
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        helpers : {
            media : {}
        }
    });

    /**
     * EnquireJS
     */
    enquire.register("screen and (max-width: 600px)", {
        match: function(){
            _carousel.setupMobileBanner();
        }
    });

    enquire.register("screen and (max-width: 767px)", {
        match: function(){
            $('.newsroom-featured ').append( $('.newsroom-featured__blog-block'));
        },

        unmatch: function(){
            $('.newsroom-featured__block.highlight-block').after( $('.newsroom-featured__blog-block'));
        }
    });

    enquire.register("screen and (max-width: 959px)", {
         match: function(){
            _eventSpeakers.init();
        }
    });

    enquire.register("screen and (min-width: 960px)", {
        match: function(){
            _carousel.setupCustomControl();
            _quicklinks.setupSticky();
            _eventSpeakers.destroy()
        },

        unmatch: function(){
            _quicklinks.destroySticky();
        }
    }, true);
});
