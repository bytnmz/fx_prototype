'use strict';

import FeaturedLinkboxs from '../featured-linkboxs';

describe('FeaturedLinkboxs View', function() {

  beforeEach(() => {
    this.featuredLinkboxs = new FeaturedLinkboxs();
  });

  it('Should run a few assertions', () => {
    expect(this.featuredLinkboxs).toBeDefined();
  });

});
