'use strict';

import TopBanner from '../top-banner';

describe('TopBanner View', function() {

  beforeEach(() => {
    this.topBanner = new TopBanner();
  });

  it('Should run a few assertions', () => {
    expect(this.topBanner).toBeDefined();
  });

});
