'use strict';

import $ from 'jquery';
import 'swapImage';

export default class TopBanner {
	constructor() {
		this.name = 'top-banner';
		// console.log('%s module', this.name);

		$('.top-banner__image').swapImage({breakpoint: 768});

	}
}
