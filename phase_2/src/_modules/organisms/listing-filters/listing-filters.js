'use strict';

import listingCategories from '../../molecules/listing-categories/listing-categories';
import sorter from '../../molecules/listing-sorter/listing-sorter';
import listingResults from '../../organisms/listing-results/listing-results';

export default class ListingFilters {
    constructor(type, isDebug) {
        this.name = 'listing-filters';
        this.$listingSideFilters = $('.listing-side-filters');
        this.$filterGroups       = $('.filter-group' , this.$listingSideFilters);
        this.resultViewer        = new listingResults();

        if(type === "products" && $('.product-listing').length) {
            this.dataEndpoint = (isDebug) ? '/ajax/product-listing.json' : '/api/GetProductListing';
            this.$categoryField = $('#categoryField');
            this.sorter = new sorter();
            this.setupFilters();
            this.listProducts();
        } else if(type === "careers" && $('.career-listing').length) {
            this.dataEndpoint = (isDebug) ? '/ajax/career-listing.json' : '/api/GetCareerListing';
            this.setupFilters();
            this.listJobs();
        }
    }

    listJobs() {
        let _this = this;

        if(!_this.$filterGroups.length) return;

        let $clearSelectionBtn = $('.clear-filters-selection-btn', _this.$listingSideFilters),
            $filterForm = $('.submit-btn', _this.$listingSideFilters).parents('form');

         // CLear Selections
        $clearSelectionBtn.on('click', function(e){
            e.preventDefault();

            $('input', _this.$listingSideFilters).prop('checked', false);

            _this.fetchJobs($filterForm);
        });

        // Submit filter options
        $filterForm.on('submit', function(e){
            e.preventDefault();

            var $form = $(this);

            _this.fetchJobs($form);
        });


       _this.fetchJobs( $('.submit-btn', _this.$listingSideFilters).parents('form'), true);
    }

    fetchJobs($form) {
        let _this = this,
            formData = $form.serialize(),
            results,
            parseJobs = (data) => {
                _this.resultViewer.clear();

                results = data.results.slice().concat();

                _this.resultViewer.render(results);
            };

        _this.getResult(_this.dataEndpoint, parseJobs, formData);
    }

    listProducts() {
        let _this = this;

        if(!_this.$filterGroups.length) return;

        _this.listingCategories = new listingCategories(_this);

        let $clearSelectionBtn = $('.clear-filters-selection-btn', _this.$listingSideFilters),
            $filterForm = $('.submit-btn', _this.$listingSideFilters).parents('form');

        // CLear Selections
        $clearSelectionBtn.on('click', function(e){
            e.preventDefault();

            $('input', _this.$listingSideFilters).prop('checked', false);

            _this.fetchProducts($filterForm);
        });

        // Submit filter options
        $filterForm.on('submit', function(e){
            e.preventDefault();

            var $form = $(this);

            _this.fetchProducts($form);
        });

        let $firstChild = $('li', '.categories-toggle').first().find('a');

        if($firstChild.hasClass('active')){
            _this.fetchProducts( $('.submit-btn', _this.$listingSideFilters).parents('form'), true);
            // console.log('true');
        }
    }


    /**
     * setup Filters
     */
    setupFilters() {
        let _this = this;

        // Toggle filter groups
        _this.$filterGroups.each(function(){
            let $this = $(this),
                $filterSubgroups = $('.filter-subgroup', $this),
                $advancedFilters = $('.advanced-filters', $this),
                $advancedFiltersToggle = $('.advanced-options-toggle', $this);

            // Toggle advanced filters
            if($advancedFilters.length) {
                $advancedFiltersToggle.on('click', function(e){
                    e.preventDefault();

                    if($advancedFilters.css('display') === 'block') {
                        $advancedFilters.slideUp();
                        $('.icon', $advancedFiltersToggle)
                            .addClass('icon-plus')
                            .removeClass('icon-minus');
                    } else {
                        $advancedFilters.slideDown();
                        $('.icon', $advancedFiltersToggle)
                            .addClass('icon-minus')
                            .removeClass('icon-plus');
                    }
                });
            }

            //For each filters
            $filterSubgroups.each(function(){
                var $subgroup = $(this),
                    $fields = $('.filter-subgroup__fields', $subgroup),
                    $toggle = $('h4', $subgroup);

                if($subgroup.index() === 0) {
                    $fields.show();
                    $toggle.append($('<i class = "icon icon-minus"/>'));
                } else {
                    $toggle.append($('<i class = "icon icon-plus"/>'));
                }

                $toggle.attr('tabindex', 0);

                // Collapse/expand the fields
                $toggle.on('click keypress', function(){
                    var $this = $(this),
                        $filters = $this.parent().parent();

                    if($fields.css('display') === 'none') {
                        $('.filter-subgroup__fields', $filters).stop().slideUp();
                        $('h4 .icon', $subgroup.parent())
                            .removeClass('icon-minus')
                            .addClass('icon-plus');

                        $fields.stop().slideDown();

                        $('.icon', $toggle)
                            .removeClass('icon-plus')
                            .addClass('icon-minus');
                    } else {
                        $fields.stop().slideUp();
                        $('.icon', $toggle)
                            .removeClass('icon-minus')
                            .addClass('icon-plus');
                    }
                });

                // Check/uncheck all inputs in a group with scope 'all'
                $('input[type="checkbox"]', $fields).each(function(){
                    var $cb = $(this);

                    if($cb.data('scope') === 'all') {

                        $cb.on('change', function(){
                           if($cb[0].checked) {
                                $('input[type="checkbox"]', $fields).prop('checked', true);
                            } else {
                                $('input[type="checkbox"]', $fields).prop('checked', false);
                            }
                        });
                    }
                });
            });
        });
    }

    /**
     * Switch filter menu
     */
    swapMenu(classname, categoryValue) {
        let _this = this;

        _this.$categoryField.val(categoryValue);
        _this.$filterGroups.hide();

        $(classname, _this.$listingSideFilters).show();

        _this.fetchProducts( $('.submit-btn', _this.$listingSideFilters).parents('form'), true);
    }

    /**
     * Fetch products
     */
    fetchProducts($form, categoryChange) {
        categoryChange = categoryChange || false;

        let _this = this,
            formData = $form.serialize(),
            results,
            parseProducts = (data) => {
                _this.resultViewer.clear();

                results = data.results.slice().concat();

                if (typeof window.recommendedProducts !== 'undefined') {
                    // Get recommended products' id and update to existing result json
                    var recommendedProductIds = [];

                    recommendedProducts.forEach(function(product, index) {
                        recommendedProductIds.push(product.id);
                    });

                    results.forEach(function(result, index){
                        if ($.inArray(result.productid, recommendedProductIds) > -1) {
                            result['recommended'] = true;
                        };
                    });
                }

                // _this.resultViewer.render(results, categoryChange);
                _this.sorter.setResults(results);
                _this.sorter.setView(_this.resultViewer);
                $('.listing-content__sort-result select').trigger('change');
            };

        _this.getResult(_this.dataEndpoint, parseProducts, formData);
    }


    /**
     * Ajax call to endpoint
     */
    getResult(endpoint, cb, data) {

        $.ajax({
            url: endpoint,
            data: data,
            dataType: 'json',
            cache: false
        }).done(cb);
    }
}
