'use strict';

import EventSpeakers from '../event-speakers';

describe('EventSpeakers View', function() {

  beforeEach(() => {
    this.eventSpeakers = new EventSpeakers();
  });

  it('Should run a few assertions', () => {
    expect(this.eventSpeakers).toBeDefined();
  });

});
