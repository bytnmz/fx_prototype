'use strict';

import $ from 'jquery';
import 'slick-carousel';
import fancybox from 'fancybox';

export default class EventSpeakers {
    constructor() {
        this.name = 'event-speakers';
        // console.log('%s module', this.name);

        this.$slider = $('.event-speakers__slider');
        this.slickActivated = false;

        $('.event-speakers__speaker').matchHeight();

        $('.bio-popup').on('click', function(e){
            e.preventDefault();

            let $this = $(this),
                $parent = $this.parents('.event-speakers__speaker');

            $.fancybox({
                maxWidth    : 800,
                maxHeight   : '80%',
                fitToView   : false,
                width       : '80%',
                height      : 'auto',
                autoSize    : false,
                closeClick  : false,
                openEffect  : 'none',
                closeEffect : 'none',
                wrapCSS     : 'event-speakers__bio',
                content     : $('.event-speakers__bio', $parent).html()
            });
        });

        // let labelWidth = $('.bio-popup .label').width();
        // let labelHeight = $('.bio-popup .label').height();
        // let thumbnailWidth = $('.bio-popup').width();
        // let thumbnailHeight = $('.bio-popup').height();

        // $('.bio-popup .label').css("left", calcMidpoint(labelWidth, thumbnailWidth));

        // $('.bio-popup').on('mouseover', e => {
        //     $('.bio-popup .label').css("top", calcMidpoint(labelHeight, thumbnailHeight));
        // }).on('mouseout', e => {
        //     $('.bio-popup .label').css("top", "70%");
        // });

        // function calcMidpoint(label, thumbnail) {
        //     let centerT = thumbnail / 2;
        //     let centerL = label /2;

        //     let diff = centerT - centerL;
        //     let midpoint = diff / thumbnail * 100;
        //     let percent = midpoint.toString() + "%";

        //     return percent;
        // }

    }

    destroy() {
        if(this.slickActivated) {
            this.$slider.slick('unslick');
            this.slickActivated = false;
        }
    }

    init() {

        this.slickActivated = true;

        this.$slider.slick({
            dots: false,
            arrows: false,
            infinite: true,
            autoplay: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                }
            ]
        });

    }
}
