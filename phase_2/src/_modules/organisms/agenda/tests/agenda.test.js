'use strict';

import Agenda from '../agenda';

describe('Agenda View', function() {

  beforeEach(() => {
    this.agenda = new Agenda();
  });

  it('Should run a few assertions', () => {
    expect(this.agenda).toBeDefined();
  });

});
