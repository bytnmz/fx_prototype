'use strict';

import ArticleContent from '../article-content';

describe('ArticleContent View', function() {

  beforeEach(() => {
    this.articleContent = new ArticleContent();
  });

  it('Should run a few assertions', () => {
    expect(this.articleContent).toBeDefined();
  });

});
