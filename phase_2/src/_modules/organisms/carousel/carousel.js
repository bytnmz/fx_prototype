'use strict';

import $ from 'jquery';

import 'slick-carousel';

export default class Carousel {
    constructor() {
        this.name = 'carousel';
        // console.log('%s module', this.name);

        let $homeCarousel = $('.home-carousel'),
            _this = this;

        _this.homepage = {};

        {
            if(!$homeCarousel.length) return;

            let $slider,
                $slides,
                $customControl,
                $playToggle     = $('.slider-custom-control__play-toggle'),
                $playToggleIcon = $('.icon', $playToggle),
                intervalSpeed   = $homeCarousel.data('carousel-interval');

            if(typeof intervalSpeed === "undefined") intervalSpeed = 10000;

            $slider        = $('.slider', $homeCarousel);
            $customControl = $('.slider-custom-control', $homeCarousel);
            $slides        = $('.slide', $homeCarousel);

            $slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                $('.slider-custom-control__pager a', $customControl).removeClass('active');
                $('.slider-custom-control__pager li', $customControl)
                    .eq(nextSlide)
                    .children('a')
                    .addClass('active');
            });

            $slider.slick({
                dots: false,
                arrows: false,
                infinite: true,
                speed: 300,
                autoplay: true,
                autoplaySpeed: parseInt(intervalSpeed),
                responsive: [
                    {
                        breakpoint: 960,
                        settings: {
                            dots: true,
                            autoplay: false
                        }
                    }]
            });

            $('a', $playToggle).on('click', function(e){
                e.preventDefault();

                if($playToggle.hasClass('autoplay')) {
                    $slider.slick('slickPause');
                    $playToggle.removeClass('autoplay');
                    $playToggleIcon
                        .removeClass('icon-controller-paus')
                        .addClass('icon-controller-play');
                } else {
                    $slider.slick('slickPlay');
                    $playToggle.addClass('autoplay');
                    $playToggleIcon
                        .removeClass('icon-controller-play')
                        .addClass('icon-controller-paus');
                }
            });

            _this.homepage = {
                $homeCarousel: $homeCarousel,
                $slider: $slider,
                $slides: $slides,
                $customControl:  $customControl
            }
        }
    }

    setupMobileBanner() {
        if(!this.homepage.$homeCarousel) return;

        let $slides = this.homepage.$slides;

        $slides.each(function(){
            var $slide = $(this);

            $slide.css({'background-image': 'url('+ $slide.data('mobile-banner')+')'});
        });
    }

    setupCustomControl() {
        let $customControl = this.homepage.$customControl || {},
            $homeCarousel = this.homepage.$homeCarousel || {};

        if($('.slider-custom-control__pager', $customControl).length || !$homeCarousel.length) return;

        let $customControlPager = $('<div class = "slider-custom-control__pager" />'),
            $ulEl          = $('<ul/>'),
            $liEl          = $('<li/>'),
            $slides = this.homepage.$slides,
            $slider = this.homepage.$slider;

        $slides.each(function(){
            let $this = $(this),
                $liElClone = $liEl.clone();

            $liElClone.html($this.html());

            if($this.index() === 1) {
                $('a', $liElClone).addClass('active');
            }

            $ulEl.append( $liElClone );
        });

         $('a', $ulEl).on('click', function(e){
            var $this = $(this);

            e.preventDefault();

            $('a', $ulEl).removeClass('active');

            $this.addClass('active');

            $slider.slick('slickGoTo', $this.parent().index());

        });

        $customControlPager.append($ulEl);
        $customControl.prepend( $customControlPager);

    }
}
