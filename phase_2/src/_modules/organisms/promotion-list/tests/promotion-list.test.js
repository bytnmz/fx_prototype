'use strict';

import PromotionList from '../promotion-list';

describe('PromotionList View', function() {

  beforeEach(() => {
    this.promotionList = new PromotionList();
  });

  it('Should run a few assertions', () => {
    expect(this.promotionList).toBeDefined();
  });

});
