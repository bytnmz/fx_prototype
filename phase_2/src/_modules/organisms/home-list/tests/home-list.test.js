'use strict';

import HomeList from '../home-list';

describe('HomeList View', function() {

  beforeEach(() => {
    this.homeList = new HomeList();
  });

  it('Should run a few assertions', () => {
    expect(this.homeList).toBeDefined();
  });

});
