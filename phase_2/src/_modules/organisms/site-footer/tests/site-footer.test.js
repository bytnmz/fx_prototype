'use strict';

import SiteFooter from '../site-footer';

describe('SiteFooter View', function() {

  beforeEach(() => {
    this.siteFooter = new SiteFooter();
  });

  it('Should run a few assertions', () => {
    expect(this.siteFooter).toBeDefined();
  });

});
