'use strict';

export default class Form {
  constructor() {

  	let $eventCheckList = $('.events-form-checklist');
  	$('.checkbox label', $eventCheckList).each(function(e) {
  		let textLabel = $(this).text();
  		let regexPatt = /\[(.*?)\]/g;

  		//get time
  		let time = textLabel.match(regexPatt);

  		//remove the time from text
  		let newText = textLabel.replace(regexPatt, '');

  		//empty original string
  		let inputHTML = $(this).find('input').empty();
  		$(this).html(inputHTML);

  		//remove brackets
  		let newTime = time[0].replace('[', '');
  		newTime = newTime.replace(']', '');

  		//generate new html
  		let newHTML = '<span class="event-form-time">' + newTime + '</span><span class="event-form-detail">' + newText + '</span>';
  		$(this).find('input').after(newHTML);
  	});
  }
}
