'use strict';

import SideMenu from '../side-menu';

describe('SideMenu View', function() {

  beforeEach(() => {
    this.sideMenu = new SideMenu();
  });

  it('Should run a few assertions', () => {
    expect(this.sideMenu).toBeDefined();
  });

});
