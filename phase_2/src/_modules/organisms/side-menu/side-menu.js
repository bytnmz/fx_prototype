'use strict';

export default class SideMenu {
    constructor() {
        this.name = 'side-menu';
        // console.log('%s module', this.name);

        let $sideMenu = $('.side-menu'),
            $anchors = $('ul li a', $sideMenu);

        let apiTriggerGoal = '/api/TriggerGoal?goalId=';

        {
            if(!$sideMenu.length) return;

            $anchors.on('click', function(e){
                let goalId = $(this).data('goalid'),
                    url = `${apiTriggerGoal}${goalId}`;

                $.ajax({
                    url: url,
                    method: 'post',
                    cache: false
                });
            });

            // var $toggle = $('<a href = "#" class = "side-menu-toggle"><i class = "icon-navicon"></i></a>');

            // function hideMenu(){
            //     $sideMenu.removeClass('active');
            //     setTimeout(function(){
            //         $('ul', $sideMenu).css({'display' : 'none'});
            //     }, 200);
            // }

            // $toggle.on('click', function(e){
            //     e.preventDefault();

            //     if($sideMenu.hasClass('active')) {
            //         hideMenu();
            //     } else {
            //         $('ul', $sideMenu).show();
            //         $sideMenu.addClass('active');
            //     }
            // });

            // $('li a', $sideMenu).on('click.sidemenu', function(){
            //     // hideMenu();
            // });

            // $('body').on('click.sidemenu', function(e){
            //     var $target = $(e.target);

            //     if(!$target.hasClass('side-menu') && !$target.closest('.side-menu').length) {
            //         hideMenu();
            //     }
            // });

            // $sideMenu.append($toggle);

            // $('ul', $sideMenu).css({'display' : 'none'});
        };
    }
}
