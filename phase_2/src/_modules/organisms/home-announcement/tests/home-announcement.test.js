'use strict';

import HomeAnnouncement from '../home-announcement';

describe('HomeAnnouncement View', function() {

  beforeEach(() => {
    this.homeAnnouncement = new HomeAnnouncement();
  });

  it('Should run a few assertions', () => {
    expect(this.homeAnnouncement).toBeDefined();
  });

});
