'use strict';

import Mapper from '../mapper';

describe('Mapper View', function() {

  beforeEach(() => {
    this.mapper = new Mapper();
  });

  it('Should run a few assertions', () => {
    expect(this.mapper).toBeDefined();
  });

});
