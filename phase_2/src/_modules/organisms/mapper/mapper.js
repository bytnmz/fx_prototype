'use strict';

import doT from 'dot';
import jsonQuery from 'json-query';
import Cookies from 'js-cookie';

export default class Mapper {
    constructor(type, isDebug) {
        this.name = 'mapper';

        if($('#results-template').length) {
            this.resultTempFn = doT.template($('#results-template').html());
        }

        if(type === 'solution') {
            this.optionTempFn = doT.template("{{~it.data :value:index}}<option value= \"{{=value.id}}\">{{=value.name}}</option>{{~}}");
            this.mapSolution(isDebug);

        } else if(type === 'story') {
            this.$loadMoreCont = $('.load-more');
            this.filters = {
                "country"  : $('#countryFilter'),
                "solution" : $('#solutionFilter'),
                "product"  : $('#productFilter'),
                "industry" : $('#industryFilter'),
                "year"     : $('#yearFilter')
            };
            this.cookieName = "storyFilter";
            this.endpoint = (isDebug) ? '/ajax/story-results.json' : '/api/GetStoryListing';
            this.mapper = $('.story-mapper');
            this.resultsCont = $('.story-mapper__results', this.mapper);
            this.noResults = $('.listing-content__result-status .msg');

            this.mapStory();

        } else if(type === 'event') {
            this.$loadMoreCont = $('.load-more');
            this.filters = {
                "country"  : $('#countryFilter'),
                "industry" : $('#industryFilter'),
                "year"     : $('#yearFilter'),
                "month"    : $('#monthFilter')
            };
            this.cookieName = "eventFilter";
            this.endpoint = (isDebug) ? '/ajax/event-results.json' : '/api/GetEventsListing';
            this.mapper = $('.event-mapper');
            this.resultsCont = $('.event-mapper__results', this.mapper);
            this.noResults = $('.listing-content__result-status .msg');

            this.mapStory();
        }
    }

    /**
     * Set up Solution Map
     */
    mapSolution(isDebug) {
        let _this = this,
            $mapper = $('.solution-mapper');

        if(!$mapper.length) return;

        let resultEndpoint = (isDebug) ? '/ajax/solution-results.json' : '/api/getFilterResults',
            optionsEndpoint = (isDebug) ? '/ajax/solution-map.json' : '/api/getFilterOptions',
            $mapperIntro         = $('.solution-mapper__intro', $mapper),
            $mapperResults       = $('.solution-mapper__results', $mapper),
            selectionData = {},
            filters = {
                "industry"   : $('#industryFilter'),
                "department" : $('#departmentFilter'),
                "service"    : $('#serviceFilter'),
                "business"   : $('#businessFilter')
            },
            labels = {
                "industry"   : $('#industryFilter').data('label'),
                "department" : $('#departmentFilter').data('label'),
                "service"    : $('#serviceFilter').data('label'),
                "business"   : $('#businessFilter').data('label')
            }

        _this.$clearFilterBtn = $('.btn-clear-filter', $mapper);
        _this.$btnShowAll     = $('.btn-show-all', $mapper);

        // Reset filters
        _this.$clearFilterBtn.on('click', function(e){
            e.preventDefault();

            filters.industry
                .val($('option:first-child', filters.industry).val())
                .trigger('change');

            hideFilters('industry');

            clearResults();
        });

        // Show all results
        _this.$btnShowAll.on('click', function(e){
            e.preventDefault();

            let data = {
                queryAll: 'true'
            }

            _this.getResult(resultEndpoint, parseResults, data);

            _this.$btnShowAll.fadeOut(function(){
                _this.$clearFilterBtn.fadeIn();
            });
        });

        // Setup filters
        for(var key in filters) {
            let $filter = filters[key];

            $filter.data('key', key);

            $filter.on('change.filter', function(){
                let value = $('option:selected', $filter).attr('value'),
                    dataKey = $filter.data('key'),
                    data = {
                        industry: $('option:selected', filters.industry).attr('value'),
                        department: $('option:selected', filters.department).attr('value'),
                        service: $('option:selected', filters.service).attr('value'),
                        business: $('option:selected', filters.business).attr('value')
                    };

                if(value !== "" && typeof value !== "undefined") {
                    updateFilterOnChange(dataKey, value);
                    Cookies.set('solutionFilters', JSON.stringify(data));
                } else {
                    hideFilters(dataKey);

                    if(dataKey === 'industry') {
                        $mapperResults.find('div:first-child').empty;
                        $mapperIntro.slideDown();

                        return;
                    }
                }

                _this.getResult(resultEndpoint, parseResults, data);
            });
        }

        // Parse filters option data
        let parseFilters = (data) => {
            selectionData = data;

            let cookie = Cookies.get('solutionFilters');

            _this.updateOptions(selectionData.industries, filters.industry, labels.industry);

            if(typeof cookie !== 'undefined') {
                parseCookies(JSON.parse(cookie));
            } else {
                clearResults();
            }
        };

        // Parse Cookies data
        let parseCookies = (obj) => {
            let $lastFilter = {};

            for(var key in obj) {
                let $filter = filters[key],
                    id = obj[key];

                if(typeof id !== 'undefined') {
                    updateFilterOnChange(key, id);

                    $filter
                        .val(id)
                        .parent()
                        .find('.label')
                        .text($('option:selected', $filter).text())

                    $lastFilter = $filter
                }
            }



            $lastFilter.trigger('change.filter');
        };

        // Clear Results
        let clearResults = () => {
             _this.$clearFilterBtn.fadeOut(function(){
                _this.$btnShowAll.fadeIn();
            });

            $mapperIntro.slideDown();
            $mapperResults.find('div:first-child').empty();

            Cookies.remove('solutionFilters');
        };

        // Parse Result set
        let parseResults = (data) => {
            let evalStr = _this.resultTempFn({ categories: data.categories });
            $mapperResults.find('div:first-child').html(evalStr);
            $mapperIntro.slideUp();
        };

        // Update the next filter
        let updateFilterOnChange = (key, id) => {
            let dataset = {};

            _this.$btnShowAll.fadeOut(function(){
                _this.$clearFilterBtn.fadeIn();
            });

            switch(key) {
                case 'industry':
                    dataset = jsonQuery('industries[id=' + id +'].departments', {data: selectionData}).value;
                    _this.updateOptions(dataset, filters.department, labels.department);
                    break;

                case 'department':
                    dataset = jsonQuery('industries.departments[id=' + id +'].services', {data: selectionData}).value;
                    _this.updateOptions(dataset, filters.service, labels.service);
                    break;

                case 'service':
                    dataset = jsonQuery('industries.departments.services[id=' + id +'].businesses', {data: selectionData}).value;
                    _this.updateOptions(dataset, filters.business, labels.business);
                    break;
            }
        };

        // Hide the filters
        let hideFilters = (key) => {
            switch(key) {
                case 'industry':
                    filters.department
                        .val(null)
                        .parents('.input-group-select').fadeOut();
                        clearResults();
                    /* falls through */
                case 'department':
                    filters.service
                        .val(null)
                        .parents('.input-group-select').fadeOut();
                    /* falls through */
                case 'service':
                    filters.business
                        .val(null)
                        .parents('.input-group-select').fadeOut();
                    /* falls through */
                default:
                    break;
            }
        };

        _this.getResult(optionsEndpoint, parseFilters);
    }

    mapStory() {
        let _this = this,
            $mapper =this.mapper;

        _this.startIndex = 1;

        if(!$mapper.length) return;

        let resultEndpoint = _this.endpoint,
            filters = _this.filters,
            $mapperResults = _this.resultsCont;

        _this.$clearFilterBtn = $('.btn-clear-filter', $mapper);

        // Reset Button
        _this.$clearFilterBtn.on('click', function(e){
            e.preventDefault();

            for(var key in filters) {
                let $filter = filters[key];

                $filter
                    .val($('option:first-child', filters).val())
                    .parent()
                    .find('.label')
                    .text($('option:selected', $filter).text());
            }

            clearView();

            _this.getResult(resultEndpoint, parseResults, getData());
        });

        // Load more button
        $('.js-load-more').on('click', function(e){
            e.preventDefault();

            let data = getData(e);

            _this.getResult(resultEndpoint, parseResults, getData(e));
        });

        // Setup filters
        for(var key in filters) {
            let $filter = filters[key];

            $filter.on('change.filter', function(){
                let value = $('option:selected', $filter).attr('value');

                clearView();

                if(value !== "" && typeof value !== "undefined") {
                    Cookies.set( _this.cookieName, JSON.stringify(getData()), true);
                }

                _this.getResult(resultEndpoint, parseResults, getData());

            });
        }

        // Process results
        let parseResults = (data) => {
            let evalStr = _this.resultTempFn({ snippets: data.results }),
                interval = 200;

            $mapperResults.children('div:first-child').append($(evalStr));

            if(data.results.length == 0){
                _this.noResults.show();
                this.$loadMoreCont.hide();
            }
            else {
                _this.noResults.hide();
                this.$loadMoreCont.show();
            }

            _this.startIndex = data.endIndex + 1;

            _this.toggleLoadMore(data.count, data.endIndex);

            /**
             * Fade In
             */
            $('.result', this.resultsCont).each(function(){
                let $this = $(this);

                if($this.hasClass('visible')) return;

                setTimeout(function(){
                    $this.addClass('visible');
                    $('.thumbnail', $this).swapImage({breakpoint: 768, reverse: true});

                }, interval);

                interval = interval + 50;
            });
        };

        // Get Filter Data
        let getData = (excludeIndex) => {
            let data = {};

            //Exclude for cookie
            if(excludeIndex) {
                data.startIndex = _this.startIndex
            }

            for(var key in filters) {
                let $filter = filters[key];
                data[key] = $('option:selected', $filter).attr('value')
            }

            return data;
        };

        // Clear list
        let clearView = () => {
            Cookies.remove(_this.cookieName);
            $mapperResults.children('div:first-child').empty();
            _this.startIndex = 1;
        };

        // Parse Cookies data
        let parseCookies = (obj) => {
            for(var key in obj) {
                let $filter = filters[key],
                    id = obj[key];

                if(typeof id !== 'undefined') {

                    $filter
                        .val(id)
                        .parent()
                        .find('.label')
                        .text($('option:selected', $filter).text())
                }
            }
        };

        // Bootstrap function
        {
            let cookie = Cookies.get(_this.cookieName);

            if(typeof cookie !== 'undefined') {
                parseCookies(JSON.parse(cookie));
            }

            _this.getResult(resultEndpoint, parseResults, getData());
        }
    }

    /**
     * Toggle Loadmore
     */
    toggleLoadMore(count, endIndex) {

        if(endIndex >= count) {
            this.$loadMoreCont.hide();
        } else {
            this.$loadMoreCont.show();
        }

    }

    /**
     * Update filter options
     */
    updateOptions(context, $filter, label) {
        let evalStr = this.optionTempFn({data: context});

        $filter
            .html(evalStr)
            .prepend($('<option val = "" selected>' + label + '</option>'))
            .parents('.input-group-select')
            .fadeIn();
    }

    /**
     * Ajax call to endpoint
     */
    getResult(endpoint, cb, data) {
        let postData = {};

        $.extend(postData, data);

        $.ajax({
            url: endpoint,
            data: postData,
            dataType: 'json',
            cache: false
        }).done(cb);
    }
}
