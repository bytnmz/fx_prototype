'use strict';

import MainNavigation from '../main-navigation';

describe('MainNavigation View', function() {

  beforeEach(() => {
    this.mainNavigation = new MainNavigation();
  });

  it('Should run a few assertions', () => {
    expect(this.mainNavigation).toBeDefined();
  });

});
