import $ from 'jquery'

'use strict';

export default class MainNavigation {
    constructor() {
        this.name = 'main-navigation';
        // console.log('%s module', this.name);

        let $mainNav = $('.main-navigation__menu'),
            $flyovers = $('.flyover', $mainNav),
            timer;

        {
            if(!$mainNav.length) return;

            $('.parent', $mainNav).each(function(){
                let $this = $(this),
                    $submenu = $('.flyout', $this);

                $this.on('mouseenter', function(){
                    timer = setTimeout(function(){
                        $this.addClass('hover-on');
                    }, 100);
                });

                 $this.on('mouseleave', function(){
                    clearTimeout(timer);
                    $('.parent', $mainNav).removeClass('hover-on');
                });
            });
        }
    }
}
