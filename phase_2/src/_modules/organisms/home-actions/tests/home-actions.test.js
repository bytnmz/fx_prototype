'use strict';

import HomeActions from '../home-actions';

describe('HomeActions View', function() {

  beforeEach(() => {
    this.homeActions = new HomeActions();
  });

  it('Should run a few assertions', () => {
    expect(this.homeActions).toBeDefined();
  });

});
