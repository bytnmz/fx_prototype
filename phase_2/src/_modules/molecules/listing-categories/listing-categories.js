'use strict';

import 'matchHeight';

export default class ListingCategories {
    constructor(filters) {
        let _this = this,
            $categories =  $('.categories-toggle'),
            $categoryTabs = $('.category-tabs-content');

        $('ul li a', $categories).matchHeight();

        $('.category-tabs-content__item', $categoryTabs).each(function() {
            if(!$(this).hasClass('active')) {
                $(this).hide();
            }
        });

        //decode URL
        let listing = _this.getQueryVariable('listing'),
            decoded = decodeURI(listing);

        if(decoded.indexOf('+')){
            decoded = decoded.split('+').join(' ');
        }

        $('li', $categories).each(function(){
            let $this = $(this),
                $anchor = $('a', $this),
                categoryValue = $anchor.text().toLowerCase();
                // categoryValue = $anchor.text(),

            $anchor.on('click', function(e){
                let $thisAnchor = $(this);
                if($thisAnchor.hasClass('active')){
                    return;
                }
                else {
                    $('a', $categories).removeClass('active');
                    $(this).addClass('active');
                }

                if($thisAnchor.data('filter')){
                    e.preventDefault();

                    //tab function
                    let tabRef = $(this).attr('href');
                    let _active = $('.active', $categoryTabs);
                    _active.fadeOut();
                    _active.removeClass('active');
                    $(tabRef).addClass('active');
                    $(tabRef).fadeIn();

                    //meta handlers
                    let title = $(this).data('title'),
                        desc = $(this).data('description'),
                        keywords = $(this).data('keywords');

                    $('title').text(title);
                    $('meta[name=description]').attr('content', desc);
                    $('meta[name=keywords]').attr('content', keywords);

                    filters.swapMenu($(this).data('filter'), $anchor.data('value'));
                }

            });

            //trigger anchor click
            if(categoryValue === decoded) {
                $anchor.click();
            }
        });
    }

    getQueryVariable(variable){
        let query = window.location.search.substring(1),
            vars = query.split("&");

        for (let i=0;i<vars.length;i++) {
                let pair = vars[i].split("=");
                if(pair[0] == variable){return pair[1];}
        }

        return false;
    }
}
