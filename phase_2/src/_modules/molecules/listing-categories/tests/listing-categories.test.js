'use strict';

import ListingCategories from '../listing-categories';

describe('ListingCategories View', function() {

  beforeEach(() => {
    this.listingCategories = new ListingCategories();
  });

  it('Should run a few assertions', () => {
    expect(this.listingCategories).toBeDefined();
  });

});
