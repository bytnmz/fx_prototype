'use strict';

export default class Quicklinks {
    constructor() {
        this.name = 'quicklinks';
        // console.log('%s module', this.name);

        this.$quicklinks = $('.quick-links'),
        this.$article    = $('.article'),
        this.$topBanner  = $('.top-banner'),
        this.offsetTop   = 0;

        let _this = this;

        {
            if(!_this.$quicklinks.length) return;

            let $ulEl = $('<ul/>'),
                $liEl = $('<li/>'),
                $anchorEl = $('<a href = ""/>'),
                $anchorLinks = $('a.anchor-link', _this.$article),
                linkNum = $anchorLinks.length,
                $newliEl,
                $newAnchorEl,
                $target;

            for(var i = 1; i < linkNum + 1; i++) {
                $newliEl = $liEl.clone();
                $newAnchorEl = $anchorEl.clone();
                $target = $anchorLinks.eq(i - 1);

                $target.attr('id', 'content' + i);

                $newAnchorEl.text($target.data('label'));
                // if ($target.data('label').indexOf('Register') >= 0 ) {
                //     $newAnchorEl.addClass('linkout');
                // }
                $newAnchorEl.attr('href', '#' + $target.attr('id'));

                // if( $target.data('link') ) {
                //     $newAnchorEl.addClass('linkout');
                //     $newAnchorEl.attr('href', $target.data('link') );
                // }

                $newliEl.append($newAnchorEl);
                $ulEl.append($newliEl);
            }

            $('.wingspan', _this.$quicklinks).append($ulEl);

            $('a', _this.$quicklinks).on('click', function(e){

                // if(!$(this).hasClass('linkout')) {
                e.preventDefault();

                var $target = $($(this).attr('href'));

                 $('html, body').stop(true, true).animate({
                    scrollTop: $target.offset().top - _this.offsetTop
                }, 500);
                 // }
            });
        }
    }

    setupSticky() {
        let _this = this;

        if(!_this.$quicklinks.length) return;

        // var quicklinksOffset = $quicklinks.offset().top;

        _this.offsetTop  =  _this.$quicklinks.height() + 20;

        $(window).on('scroll.sticky', function(){
            let scrollAmt = $(this).scrollTop(),
                topbannerBottom = _this.$topBanner.offset().top + _this.$topBanner.height();

            if(topbannerBottom < scrollAmt) {
                _this.$quicklinks.addClass('sticky');
                _this.$article.css({'padding-top': _this.offsetTop});

                $('.linkout', _this.$quicklinks).addClass('color-orange');

            } else {
                _this.$quicklinks.removeClass('sticky');
                _this.$article.css({'padding-top': ''});

                $('.linkout', _this.$quicklinks).removeClass('color-orange');
            }
        }).trigger('scroll.sticky');
    }

    destroySticky() {
        let _this = this;

        _this.offsetTop = 10;

        $(window).off('scroll.sticky');
        _this.$quicklinks.removeClass('sticky');
        _this.$article.css({'padding-top': ''});
    }
}
