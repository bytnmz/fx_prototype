'use strict';

import Quicklinks from '../quicklinks';

describe('Quicklinks View', function() {

  beforeEach(() => {
    this.quicklinks = new Quicklinks();
  });

  it('Should run a few assertions', () => {
    expect(this.quicklinks).toBeDefined();
  });

});
