'use strict';

import PromotionDetails from '../promotion-details';

describe('PromotionDetails View', function() {

  beforeEach(() => {
    this.promotionDetails = new PromotionDetails();
  });

  it('Should run a few assertions', () => {
    expect(this.promotionDetails).toBeDefined();
  });

});
