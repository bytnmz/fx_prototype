'use strict';

export default class BackTop {
    constructor() {
        this.name = 'back-top';
        // console.log('%s module', this.name);

        let $wrapper = $('<div/>'),
            $anchor = $('<i class="icon-angle-up"></i><span class="label">Top</span>');

        $wrapper
            .addClass('back-to-top')
            .append($anchor);

        $('body').append($wrapper);

        /**
         * Bind Behaviours
         */
        $wrapper
            .on('click', function(e){
                $('html, body').stop().animate({'scrollTop': 0});
                e.preventDefault();
            });


        var scrolling = function(){
            var scrollTop = $(window).scrollTop();

            if(scrollTop > $(window).height()) {
                $wrapper.fadeIn();
            } else {
                $wrapper.fadeOut();
            }
        };

        /**
         * On Scroll
         */
        $(window)
            .on('scroll.window', scrolling)
            .trigger('scroll.window');
    }
}
