'use strict';

import BackTop from '../back-top';

describe('BackTop View', function() {

  beforeEach(() => {
    this.backTop = new BackTop();
  });

  it('Should run a few assertions', () => {
    expect(this.backTop).toBeDefined();
  });

});
