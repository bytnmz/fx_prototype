'use strict';

import $ from 'jquery';

export default class ListingSorter {
	constructor() {
		this.name = 'listing-sorter';
		// console.log('%s module', this.name);

		this.results = [];

		this.$sortSelect = $('.listing-content__sort-result select');

		let _this = this;

		_this.$sortSelect.on('change', function(){
			var results = [].concat(_this.results); // to preserve default results
			results = _this.toggleSort($(this).val(), results);

			_this.view.clear();
			_this.view.render(results, true);
		});
	}

	setResults(results) {
		this.results = results.slice().concat(); // Deep copy results from original data to reset sorting.
	}

	setView(fn) {
		this.view = fn;
	}

	toggleSort(sortParam, results) {
		let parameter = '',
			sortParameter = '',
			sortedResults = [];

		let mergeSortTopDown = (array, parameter) => {
			if(array.length < 2) {
				return array;
			}

			let middle = Math.floor(array.length / 2);
			let left = array.slice(0, middle);
			let right = array.slice(middle);

			return mergeTopDown(mergeSortTopDown(left, parameter), mergeSortTopDown(right, parameter), parameter);
		}

		let mergeTopDown = (left, right, parameter) => {
			let array = [];

			while(left.length && right.length) {
				if(parameter == 'compareDate'){
					if(left[0][parameter] > right[0][parameter]) {
						array.push(left.shift());
					} else {
						array.push(right.shift());
					}
				}
				else {
					if(left[0][parameter] < right[0][parameter]) {
						array.push(left.shift());
					} else {
						array.push(right.shift());
					}
				}
			}
			return array.concat(left.slice()).concat(right.slice());
		}

		let customSort = (results, parameter) => {
			let valid = [],
				invalid = [];

			if(parameter == 'date'){
				for(let i = 0; i < results.length; i++){
					if(new Date(results[i][parameter]) == 'Invalid Date'){
						invalid.push(results[i]);
					}
					else{
						valid.push(results[i]);
					}
				}

				for(let i = 0; i < valid.length; i++){
					valid[i].compareDate = new Date(valid[i][parameter]);
				}

				sortParameter = 'compareDate';
				valid = mergeSortTopDown(valid.slice(), sortParameter);
				invalid = mergeSortTopDown(invalid.slice(), 'name');
			}
			else {
				if(parameter == 'recommended'){
					for(let j = 0; j < results.length; j++){
						if(typeof results[j][parameter] !== 'undefined'){
							valid.push(results[j]);
						}
						else{
							invalid.push(results[j]);
						}
					}
				}
				else {
					for(let k = 0; k < results.length; k++){
						if(results[k][parameter] === true){
							valid.push(results[k]);
						}
						else{
							invalid.push(results[k]);
						}
					}
				}

				sortParameter = 'name';
				valid = mergeSortTopDown(valid.slice(), sortParameter);
				invalid = mergeSortTopDown(invalid.slice(), sortParameter);
			}

			$.merge(valid, invalid);

			return valid;
		}

		switch(sortParam) {
			case 'latest' :
				parameter = 'date';
				sortedResults = customSort(results, parameter);
				break;

				// results.sort(function(a,b){
				// 		if (new Date(a.date) == 'Invalid Date'){
				// 			return 1;
				// 		}
				// 		if (new Date(b.date) == 'Invalid Date'){
				// 			return -1;
				// 		}
				// 		if (new Date(a.date) < new Date(b.date)) {
				// 			return 1;
				// 		}
				// 		if (new Date(a.date) > new Date(b.date)) {
				// 			return -1;
				// 		}
				// 		if (new Date(a.date) == new Date(b.date)){
				// 			return 0;
				// 		}
				// 	}
				// );

			case 'promotion' :
				parameter = 'promoted';
				sortedResults = customSort(results, parameter);
				break;

				// results.sort(function(a,b){
				// 		// if(a.promoted == true && b.promoted == true) return 0;
				// 		// if(a.promoted == true && b.promoted == false) return -1;
				// 		// if(a.promoted == false && b.promoted == true) return 1;
				// 		if(a.promoted === b.promoted){
				// 			return 0;
				// 		}
				// 		else{
				// 			if(a.promoted){
				// 				return -1;
				// 			}
				// 			else{
				// 				return 1;
				// 			}
				// 		}
				// 	}
				// );

			case 'best seller' :
				parameter = 'bestseller';
				sortedResults = customSort(results, parameter);
				break;

				// results.sort(function(a,b){
				// 		// if(a.bestseller == true && b.bestseller == true) return 0;
				// 		// if(a.bestseller == true && b.bestseller == false) return -1;
				// 		// if(a.bestseller == false && b.bestseller == true) return 1;
				// 		if(a.bestseller === b.bestseller){
				// 			return 0;
				// 		}
				// 		else{
				// 			if(a.bestseller){
				// 				return -1;
				// 			}
				// 			else{
				// 				return 1;
				// 			}
				// 		}
				// 	}
				// );

			case 'recommended' :
				parameter = 'recommended';
				sortedResults = customSort(results, parameter);
				break;

				// results.sort(function(a,b){
				// 		if(typeof a.recommended !== 'undefined' && typeof b.recommended !== 'undefined') return 0;
				// 		if(typeof a.recommended !== 'undefined' && typeof b.recommended === 'undefined') return -1;
				// 		if(typeof a.recommended === 'undefined' && typeof b.recommended !== 'undefined') return 1;
				// 	}
				// );
		}

		return sortedResults;
	}

	/**
	 * Bubble promotion in results to the top
	 */
	bubblePromotion() {
		let temp;

		for (let i = 0; i < this.results.length - 1; i++) {

			if(typeof this.results[i].promoted !== 'undefined') {

				 for (var x = i; x > 0; x--) {
					temp = this.results[x-1];
					this.results[x-1] = this.results[x];
					this.results[x] = temp;
				 }
			}
		}
	}
}
