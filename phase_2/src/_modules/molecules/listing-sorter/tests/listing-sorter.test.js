'use strict';

import ListingSorter from '../listing-sorter';

describe('ListingSorter View', function() {

  beforeEach(() => {
    this.listingSorter = new ListingSorter();
  });

  it('Should run a few assertions', () => {
    expect(this.listingSorter).toBeDefined();
  });

});
