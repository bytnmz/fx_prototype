'use strict';

import Homepage from '../homepage';

describe('Homepage View', function() {

  beforeEach(() => {
    this.homepage = new Homepage();
  });

  it('Should run a few assertions', () => {
    expect(this.homepage).toBeDefined();
  });

});
