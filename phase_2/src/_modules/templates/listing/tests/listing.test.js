'use strict';

import Listing from '../listing';

describe('Listing View', function() {

  beforeEach(() => {
    this.listing = new Listing();
  });

  it('Should run a few assertions', () => {
    expect(this.listing).toBeDefined();
  });

});
