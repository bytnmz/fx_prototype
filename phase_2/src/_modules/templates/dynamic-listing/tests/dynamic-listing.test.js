'use strict';

import DynamicListing from '../dynamic-listing';

describe('DynamicListing View', function() {

  beforeEach(() => {
    this.dynamicListing = new DynamicListing();
  });

  it('Should run a few assertions', () => {
    expect(this.dynamicListing).toBeDefined();
  });

});
