'use strict';

import Standard from '../standard';

describe('Standard View', function() {

  beforeEach(() => {
    this.standard = new Standard();
  });

  it('Should run a few assertions', () => {
    expect(this.standard).toBeDefined();
  });

});
