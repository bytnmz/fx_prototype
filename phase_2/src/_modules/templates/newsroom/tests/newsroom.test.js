'use strict';

import Newsroom from '../newsroom';

describe('Newsroom View', function() {

  beforeEach(() => {
    this.newsroom = new Newsroom();
  });

  it('Should run a few assertions', () => {
    expect(this.newsroom).toBeDefined();
  });

});
