'use strict';

import Iframe from '../iframe';

describe('Iframe View', function() {

  beforeEach(() => {
    this.iframe = new Iframe();
  });

  it('Should run a few assertions', () => {
    expect(this.iframe).toBeDefined();
  });

});
