'use strict';

import path from 'path';

export default function(gulp, plugins, args, config, taskTarget, browserSync) {
  let dirs = config.directories;
  let dest = path.join(taskTarget);

  // Copy
  gulp.task('copy', () => {

    //Copy icomoon fonts
    var icomoon = gulp.src('./icomoon/fonts/**/*')
        .pipe(gulp.dest(dest + '/fonts/'));

    return gulp.src([
      path.join(dirs.source, '**/*'),
      '!' + path.join(dirs.source, '{**/\_*,**/\_*/**}'),
      '!' + path.join(dirs.source, '**/*.pug')
    ])
    .pipe(plugins.changed(dest))
    .pipe(gulp.dest(dest));
  });
}
