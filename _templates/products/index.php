<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Product Listing';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><span>Product Listing</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Listing -->
	<div class = "listing dynamic-listing">
		<div class = "row-col-12 wingspan">
			<h1 class = "top-heading">Product Listing</h1>

			<div class = "categories-toggle">
				<ul>
					<li><a href="#" class = "active" data-filter = ".printers-filter-group">Office Printers</a></li>
					<li><a href="#" data-filter = ".production-printers-filter-group">Production Printers</a></li>
					<li><a href="#" data-filter = ".supplies-filter-group">Supplies</a></li>
					<li><a href="#" data-filter = ".software-filter-group">Software</a></li>
				</ul>
			</div>

			<!-- Listing Filters -->
			<div class = "listing-side-filters">

				<!-- Filter Panel -->
				<div class = "filter-panel">
					<form>
						<fieldset>
							<legend class = "sr-only">Product Finder Filter</legend>

							<input type = "hidden" name = "categoryField" id = "categoryField" value = "printers"/>

							<!-- Category Selection -->
							<!-- <div class = "input-group-select">
								<select class="custom-select" id="productCategory" name="productCategory">
									<option>Select a category</option>
									<option>Printers</option>
									<option>Supplies</option>
									<option>Software</option>
								</select>
							</div> -->
							<!-- End: Category Selection -->
							<div class = "filter-panel-header">
								<h3>Options</h3>

								<a href = "#" class = "clear-filters-selection-btn">Clear Selection</a>
							</div>


							<!-- Software Filter Group -->
							<div class = "filter-group software-filter-group">
								<div class = "basic-filters">
									<div class = "filter-subgroup">
										<h4>Usage</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Office</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Production</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All uses</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End: Software Filter Group -->

							<!-- Supplies Filter Group -->
							<div class = "filter-group supplies-filter-group">
								<div class = "basic-filters">
									<div class = "filter-subgroup">
										<h4>Usage</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Paper</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Binders</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Laminators</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Labels</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Xerox Mobility Chart</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Coffee</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All uses</label>
											</div>
										</div>
									</div>
									<div class = "filter-subgroup">
										<h4>Paper Size</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>A3 (16.5 x 11.7in)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>A3+ (12.95 x 19in)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Roll Paper</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All sizes</label>
											</div>
										</div>
									</div>
									<div class = "filter-subgroup">
										<h4>Paper Weight(gsm)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>70</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>80</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>90</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All weight</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End: Supplies Filter Group -->

							<!-- Printers Filter Group -->
							<div class = "filter-group printers-filter-group">
								<div class = "basic-filters">
									<div class = "filter-subgroup">
										<h4>Type</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Multifunction</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Desktop</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Production</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Digital Finishing</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>3D</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All types</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Usage</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Personal</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Office</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Production</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Wide Format</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All uses</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Purpose</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Copying</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Printing</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Scan</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Fax</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Scan to Email</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Network Printing</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All functions</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Colour</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox" name = "color"/>Colour</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" name = "color"/>Black &amp; White</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All Colours</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Paper Size(inches)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>A0 (46.8 x 33.1)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>A1 (33.1 x 23.4)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>A2 (23.4 x 16.5)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>A3 (16.5 x 11.7)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>A4 (11.7 x 8.3)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>A5 (8.3 x 5.8)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All paper sizes</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Storage(GB)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>2</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>4</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>6</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>8</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>10</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All sizes</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Features</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Wireless</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>ConnectKey Technology</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All features</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Price</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>$X-X</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>$X-X</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>$X-X</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>$X-X</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>$X-X</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All prices</label>
											</div>
										</div>
									</div>
								</div>

								<a href = "#" class = "advanced-options-toggle">Advanced Options<i class = "icon icon-plus"></i></a>

								<!-- Advanced Filters -->
								<div class = "advanced-filters">
									<div class = "filter-subgroup">
										<h4>Print Speed (pages per minute)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>3</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>4-7</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>20-40</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>41-60</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>61-80</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>81-130</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>131-300</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All print speeds</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Paper Weight</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>60-150</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>151-200</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All paper weights</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Scanning Resolution (dpi)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Standard (200 x 100)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Fine (200 x 200)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Extra Fine (400 x 400)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Super Fine (600 x 600)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All resolutions</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Finishing</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Staple</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Hole Punch</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Booklet</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All finishing options</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Print Volume(pages)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Less than 5000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>5001 - 20,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>20,001 - 40,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All volumes</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Print Volume (per month)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Less than 750</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>751 - 1,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>1,001 - 2,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>100,000 - 500,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>500,001 - 1,000,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>1,000,001 - 2,000,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All volumes</label>
											</div>
										</div>
									</div>
								</div>
								<!-- End: Advanced Filters -->
							</div>
							<!-- End: Printers Filter Group -->

							<!-- Production Printers Filter Group -->
							<div class = "filter-group production-printers-filter-group">
								<div class = "basic-filters">
									<div class = "filter-subgroup">
										<h4>Type</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Multifunction</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Desktop</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Production</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Digital Finishing</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>3D</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All types</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Usage</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Personal</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Office</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Production</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Wide Format</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All uses</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Purpose</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Copying</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Printing</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Scan</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Fax</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Scan to Email</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Network Printing</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All functions</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Colour</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox" name = "color"/>Colour</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" name = "color"/>Black &amp; White</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All Colours</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Paper Size(inches)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>A0 (46.8 x 33.1)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>A1 (33.1 x 23.4)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>A2 (23.4 x 16.5)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>A3 (16.5 x 11.7)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>A4 (11.7 x 8.3)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>A5 (8.3 x 5.8)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All paper sizes</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Storage(GB)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>2</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>4</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>6</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>8</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>10</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All sizes</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Features</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Wireless</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>ConnectKey Technology</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All features</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Price</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>$X-X</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>$X-X</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>$X-X</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>$X-X</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>$X-X</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All prices</label>
											</div>
										</div>
									</div>
								</div>

								<a href = "#" class = "advanced-options-toggle">Advanced Options<i class = "icon icon-plus"></i></a>

								<!-- Advanced Filters -->
								<div class = "advanced-filters">
									<div class = "filter-subgroup">
										<h4>Print Speed (pages per minute)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>3</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>4-7</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>20-40</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>41-60</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>61-80</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>81-130</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>131-300</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All print speeds</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Paper Weight</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>60-150</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>151-200</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All paper weights</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Scanning Resolution (dpi)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Standard (200 x 100)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Fine (200 x 200)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Extra Fine (400 x 400)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Super Fine (600 x 600)</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All resolutions</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Finishing</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Staple</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Hole Punch</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>Booklet</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All finishing options</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Print Volume(pages)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Less than 5000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>5001 - 20,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>20,001 - 40,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All volumes</label>
											</div>
										</div>
									</div>

									<div class = "filter-subgroup">
										<h4>Print Volume (per month)</h4>

										<div class = "filter-subgroup__fields">
											<div class = "checkbox">
												<label><input type = "checkbox"/>Less than 750</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>751 - 1,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>1,001 - 2,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>100,000 - 500,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>500,001 - 1,000,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox"/>1,000,001 - 2,000,000</label>
											</div>
											<div class = "checkbox">
												<label><input type = "checkbox" data-scope = "all"/>All volumes</label>
											</div>
										</div>
									</div>
								</div>
								<!-- End: Advanced Filters -->
							</div>
							<!-- End: Production Printers Filter Group -->

						</fieldset>

						<div class = "filter-panel-btn-group">
							<button type = "submit" class = "submit-btn">Submit</button>
						</div>

					</form>
				</div>
				<!-- End: Filter Panel -->
			</div>
			<!-- End: Listing Filters -->

			<!-- Listing Content -->
			<div class = "listing-content push-left-25">



				<div class = "listing-content__sort-result">
					<div class = "pull-right">
						<label>Sort by</label>
						<select class = "custom-select">
							<option value = "latest">Latest</option>
							<option value = "promotion">Promotion</option>
							<option value = "recommended">Recommended</option>
							<option value = "best seller">Best Seller</option>
						</select>
					</div>
				</div>

				<div class = "listing-content__result-status">
					<p class = "msg success-msg">Your search criteria has returned <span class = "total-results">0</span> results.</p>
					<p class = "msg error-msg">Sorry, your search criteria has returned 0 result. Please try again.</p>
					<div class = "loader-animation">
						<div class = "inner"></div>
						<div class = "inner"></div>
						<div class = "inner"></div>
					</div>
				</div>

				<ul class = "listing-content__results row-col-12">
					<!-- li class = "col-sm-12 col-md-6">
						<div class = "desc">
							<h5><a href = "#">ApeosPort - IV 7080/6080</a></h5>
							<ul>
								<li>High speed printer</li>
								<li>Cost effective</li>
								<li>Environment friendly</li>
							</ul>

							<p>Printing Speed:<br>65ppm – 75ppm</p>

							<p><a href = "#" class = "contact-link">Contact our sales team</a></p>
						</div>

						<div class = "thumbnail">
							<a href = "#"><img src= "/assets/fuji-xerox/images/content/printer-1.png" alt = ""/></a>
						</div>
					</li>

					<li class = "col-sm-12 col-md-6">
						<div class = "desc">
							<h5><a href = "#">ApeosPort - IV 7080/6080</a></h5>
							<ul>
								<li>High speed printer</li>
								<li>Cost effective</li>
								<li>Environment friendly</li>
							</ul>

							<p>Printing Speed:<br>65ppm – 75ppm</p>

							<p><a href = "#" class = "contact-link">Contact our sales team</a></p>
						</div>

						<div class = "thumbnail">
							<a href = "#"><img src= "/assets/fuji-xerox/images/content/printer-1.png" alt = ""/></a>
						</div>
					</li> -->

				</ul>

				<div class = "load-more-results">
					<a href = "#" class = "load-more-btn">Load More</a>
				</div>
			</div>
			<!-- End: Listing Content -->

		</div>
	</div>
	<!-- End: Listing -->

	<script id="results-template" type="text/x-handlebars-template">
		{{#each results}}
			<li class = "col-sm-12 col-md-6">
				<div class = "desc">
					<h5><a href = "{{url}}">{{name}}</a></h5>

					<div>
						{{{desc}}}
					</div>

					<p><a href = "#productForm" class = "contact-link {{buttontype}}" data-iframe="/modal-form.php" data-productid = "{{productid}}" >{{buttonlabel}}</a></p>
				</div>

				<div class = "thumbnail">
					{{#if recommended}}
						<span class = "label recommended">Recommended</span>
					{{else}}
						{{#if promoted}}
							<span class = "label promoted">Promotion</span>
						{{else}}
							{{#if newitem}}
								<span class = "label new-item">New</span>
							{{else}}
								{{#if bestseller}}
								<span class = "label best-seller">Best Seller</span>
								{{/if}}
							{{/if}}
						{{/if}}
					{{/if}}

					<a href = "#"><img src= "{{thumbnail}}" alt = ""/></a>
				</div>
			</li>
		{{/each}}
	</script>

	<script type="text/javascript">
		var recommendedProducts = [
			{id: "5"},
			{id: "9"}
		];
	</script>



	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>