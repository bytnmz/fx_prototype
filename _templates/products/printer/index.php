<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Color Management Solutions & Services';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/">Products</a></li>
			<li><a href = "/">Printers</a></li>
			<li><span>DocuCentre-V C7785/C6685/C5585</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/docucenter.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4 col-md-4">
				<h1>DocuCentre-V C7785/C6685/C5585</h1>
				<p>Delivering a new standard in versatility and performance</p>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 col-md-8 offset-sm-1 offset-md-2">
				<section class = "section product-introduction">
					<a id = "content_1" class = "anchor-link" data-label = "Overview"></a>

					<div class = "row-col-12">


						<div class = "col-sm-7 desc">
							<span class="label new-item">NEW</span>

							<h2 class = "product-name">DocuCentre - V C7785/C6685/C5585</h2>

							<p><a href="#enquiryForm" class="contact-link modalbox" target="_blank" data-productid="12345">Contact our sales team</a></p>

							<p>Fast, beautiful and more convenient.</p>

							<p>Delivering top- of - the - line performance.</p>

							<p>In addition to outstanding printing productivity of 70 pages/ minute*(colour) and 75 page/ minute(monochrome), our premium DocuCentre models deliver optimal performance, including outstanding image quality, high energy efficiency and solid durability.</p>

							<p>Thanks to the support for cloud services and enhanced compatibility with mobile devices (such as smartphones and tablets), you can input and output while on the go for even greater convenience.</p>

							<p>DocuCentre-V series satisfies your need for speedy business development.</p>
						</div>

						<div class = "col-sm-5 image push-left-10">
							
							<img src = "/assets/fuji-xerox/images/content/product/product-image.jpg" alt = ""/>
						</div>
					</div>
				</section>

				<section class = "section">
					<a id = "content_2" class = "anchor-link" data-label = "Features"></a>
					<h2>Features</h2>

					<h3>Smart Technology for Maximum Efficiency</h3>

					<ul>
						<li><strong>Convert scanned data and documents to numerous editable digital formats</strong></li>
						<li><strong>Enhanced flexibility of Scans</strong> where it can be made available to computer address and supports bigger file size and skip blank page flip can be disabled using ScanAuto.</li>
						<li><strong>Ease of scan storage location</strong> with the use of ScanDelivery where file naming rules and scan attributes can be automatically set up.</li>
					</ul>

					<h3>Supports High Volume Productivity</h3>

					<ul>
						<li><strong>High speed processing</strong> with sleep mode to full operation in 3.1 seconds</strong></li>
						<li><strong>Ultra high speed and high print resolution</strong>, up to 75 ppm photocopy and print speeds and full colour resolution of 2,400 by 2,400 dpi</li>
						<li><strong>Wide range of finisheroptions</strong> for stapling, booklet making and folding.</li>
						<li><strong>Exceptional media flexibility</strong>, supporting media weight of up to 300 gsm and media sizes up to A3 and SRA3.</li>
						<li><strong>Auto Duplex print on Heavy paper</strong>; be it coated or art paper from 64gsm to 300 GSM</li>
					</ul>

					<h3>Convenience at Your Fingertips</h3>

					<ul>
						<li><strong>User friendly control panel</strong> with Four different gestures- Tap, double-tap, drag and flick to scroll and switch pages smoothly.</li>
						<li><strong>Sever-less authentication linkage</strong> where administrators do not have to configure authentication settings for each machine.</li>
						<li><strong>Ease of access to device</strong> through Wi-Fi Direct connection. Access points are not needed.</li>
						<li>Supports Air Print &amp; Mopria for ease of printing</li>
					</ul>

					<h3>Reduced Energy Consumption</h3>

					<ul>
						<li><strong>Consume less power</strong> with LED Scanner delivering a level of brilliance equivalent to a Xenon</li>
						<li><strong>Reduce power and achieve a brilliant output</strong> with Fuji Xerox EA- Eco Toner.</li>
						<li><strong>Energy Star certified</strong></li>
						<li>Low TEC Value: From 6.8 kWh</li>
					</ul>

				</section>

				<section class = "section">
					<a id = "content_3" class = "anchor-link" data-label = "Benefits"></a>
					<h2>Benefits</h2>
					<ul>
						<li>Capacity to accommodate heavy duty prints with high print resolution</li>
						<li>Efficient operational features that help to streamline business processes</li>
						<li>Improve business uptime with advanced in-house finishing options</li>
					</ul>

				</section>

				<!-- Resources Section -->
				<section class = "section resources">
					<a id = "content_4" class = "anchor-link" data-label = "Specifications"></a>
					<h2>Specifications</h2>

					<div class = "resources__block">
						<dl>
							<dt>File Size:</dt>
							<dd>4100kb</dd>
							<dt>Document type:</dt>
							<dd>Portable Document Format</dd>
						</dl>

						<!-- DEV NOTE:  -->
						<!-- IF Email Subscription is switched off -->
						<!-- <a href = "#" class = "resources__block__download-btn" title = "Download Colour Management Brochure"><i class = "icon-download"></i><span class = "sr-only">Download Resource</span></a> -->
						<!-- ELSE -->
						<a href = "#subscriptionCall" class = "fancybox resources__block__download-btn" title = "Download Brochure"><i class = "icon-download"></i><span class = "sr-only">Download Brochure</span></a>
						<!-- END -->
					</div>

					<!-- Subscription Call -->
					<div class = "subscribe" id = "subscriptionCall">
						<div class = "row-col-12">
							<div class = "col-sm-6 col-md-6 col match-height">
								<h3>You may download the PDF here</h3>
								<a href="#" target = "_blank" class = "btn-download">Brochure<i class="icon-download"></i></a>
							</div>
							<div class = "col-sm-6 col-md-6 col match-height">
								<h3>Subscribe to our newsletter</h3>
								<!-- <p>Keep yourself update to Fuji Xerox latest product!</p> -->
								<div class = "form-row row-col-12">
									<div class = "col-sm-6">
										<div class = "input-group-text">
											<label for = "subscribeName">Name<span class = "required">*</span></label>
											<input type = "text" id = "subscribeName" placeholder = "Name">
										</div>
										<div class = "input-group-text">
											<label for = "subscribeEmail">Email<span class = "required">*</span></label>
											<input type = "email" id = "subscribeEmail" placeholder = "abc@example.com">
										</div>
									</div>
								</div>
								<div class = "btn-group">
									<button type = "submit">Submit<i class = "icon-chevron-with-circle-right"></i></button>
								</div>
							</div>
						</div>
					</div>
					<!-- End: Subscription Call -->
				</section>
				<!-- End: Resources Section -->

				<section class = "section">
					<a id = "content_5" class = "anchor-link" data-label = "How to Buy"></a>
					<h2>How to Buy</h2>

					<p>We make it easy for you to purchase products, supplies and services from us. Our extensive network of representatives can help you choose the right solutions for your business needs.</p>

					<p><a href = "#">For sales enquiries, simply fill up our online form.</a></p>
				</section>


				<section class = "section related-products">
					<div class = "related-products__block">
						<span class = "label new-item">NEW</span>
						<div class = "thumbnail">
							<a href = "#">
								<img src="/assets/fuji-xerox/images/content/printer-1.png" alt="">
							</a>
						</div>

						<h3><a href = "#">ApeosPort-V C7775/C6675/C5575/C4475/C3375/C3373/C2275</a></h3>
						<div>
							<ul>
								<li class="visible">High speed printer</li>
								<li class="visible">Cost effective</li>
								<li class="visible">Environment friendly</li>
							</ul>

							<p>Printing Speed:<br>
								65ppm – 75ppm
							</p>

							<p>Paper Size:<br>
								A4
							</p>
						</div>
					</div>

					<div class = "related-products__block">
						<span class = "label new-item">NEW</span>
						<div class = "thumbnail">
							<a href = "#">
								<img src="/assets/fuji-xerox/images/content/printer-1.png" alt="">
							</a>
						</div>

						<h3><a href = "#">ApeosPort-V C7775/C6675/C5575/C4475/C3375/C3373/C2275</a></h3>
						<div>
							<ul>
								<li class="visible">High speed printer</li>
								<li class="visible">Cost effective</li>
								<li class="visible">Environment friendly</li>
							</ul>

							<p>Printing Speed:<br>
								65ppm – 75ppm
							</p>

							<p>Paper Size:<br>
								A4
							</p>
						</div>
					</div>

					<div class = "related-products__block">
						<span class = "label new-item">NEW</span>
						<div class = "thumbnail">
							<a href = "#">
								<img src="/assets/fuji-xerox/images/content/printer-1.png" alt="">
							</a>
						</div>

						<h3><a href = "#">ApeosPort-V C7775/C6675/C5575/C4475/C3375/C3373/C2275</a></h3>
						<div>
							<ul>
								<li class="visible">High speed printer</li>
								<li class="visible">Cost effective</li>
								<li class="visible">Environment friendly</li>
							</ul>

							<p>Printing Speed:<br>
								65ppm – 75ppm
							</p>

							<p>Paper Size:<br>
								A4
							</p>
						</div>
					</div>
				</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3>Related Links</h3>
					<ul>
						<li><a href = "#">Submit Meter Readings</a></li>
						<li><a href = "#">Toner Ordering</a></li>
						<li><a href = "#">Relocate Machine Request</a></li>
						<li><a href = "#">Quick Reference Guide</a></li>
						<li><a href = "#">Make a Service Request</a></li>
						<li><a href = "#">Case Studies</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>