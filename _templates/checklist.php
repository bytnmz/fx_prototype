<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Checklist';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><span>Checklist</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<article class = "article article-content wingspan">
		<h1>Checklist</h1>

		<table class = "full-width">
			<thead>
				<tr>
					<th>Pages</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<a href = "/" target = "_blank">Homepage Template</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/article.php" target = "_blank">Article Template</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/products/printer" target = "_blank">Product Template</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/blank.php" target = "_blank">Blank Template</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/company/newsroom" target = "_blank">Newsroom</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/products" target = "_blank">Product Listing</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/solutions-and-services" target = "_blank">Solutions And Services</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/404" target = "_blank">Page Error Template</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/search" target = "_blank">Search Template</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/form.php" target = "_blank">Form Template</a>
					</td>
				</tr>
			</tbody>
		</table>

		<table class = "full-width">
			<thead>
				<tr>
					<th>Prototypes (Solution &amp; Services)</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<a href = "/solutions-and-services" target = "_blank">/solutions-and-services</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/solutions-and-services/office-solutions" target = "_blank">/solutions-and-services/office-solutions</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/solutions-and-services/office-solutions/print-management-solutions" target = "_blank">/solutions-and-services/office-solutions/print-management-solutions</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/solutions-and-services/office-solutions/office-automation-solutions" target = "_blank">/solutions-and-services/office-solutions/office-automation-solutions</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/solutions-and-services/production-solutions" target = "_blank">/solutions-and-services/production-solutions</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/solutions-and-services/production-solutions/print-management-information-systems" target = "_blank">/solutions-and-services/production-solutions/print-management-information-systems</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/solutions-and-services/production-solutions/customer-communication-management" target = "_blank">/solutions-and-services/production-solutions/customer-communication-management</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/solutions-and-services/enterprise-solutions" target = "_blank">/solutions-and-services/enterprise-solutions</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/solutions-and-services/enterprise-solutions/it-services" target = "_blank">/solutions-and-services/enterprise-solutions/it-services</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/solutions-and-services/global-services" target = "_blank">/solutions-and-services/global-services</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/solutions-and-services/global-services/marketing-communication-services" target = "_blank">/solutions-and-services/global-services/marketing-communication-services</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/solutions-and-services/commercial-financing-solution-services" target = "_blank">/solutions-and-services/commercial-financing-solution-services</a>
					</td>
				</tr>
			</tbody>
		</table>

		<table class = "full-width">
			<thead>
				<tr>
					<th>Prototypes (Products)</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<a href = "/products" target = "_blank">/products</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/products/?listing=printers" target = "_blank">/products/?listing=printers</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/products/?listing=supplies" target = "_blank">/products/?listing=supplies</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/products/?listing=softwares" target = "_blank">/products/?listing=softwares</a>
					</td>
				</tr>
			</tbody>
		</table>

		<table class = "full-width">
			<thead>
				<tr>
					<th>Prototypes (Company)</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<a href = "/company" target = "_blank">/company</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/company/about-fuji-xerox-singapore" target = "_blank">/company/about-fuji-xerox-singapore</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/company/management-team" target = "_blank">/company/management-team</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/company/awards-achievements" target = "_blank">/company/awards-achievements</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/company/substainable-future" target = "_blank">/company/substainable-future</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/company/newsroom" target = "_blank">/company/newsroom</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/company/career-with-us" target = "_blank">/company/career-with-us</a>
					</td>
				</tr>
			</tbody>
		</table>

		<table class = "full-width">
			<thead>
				<tr>
					<th>Prototypes (Contact)</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<a href = "/contact" target = "_blank">/contact</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href = "/contact/customer-feedback" target = "_blank">/contact/customer-feedback</a>
					</td>
				</tr>
			</tbody>
		</table>

	</article>


	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>