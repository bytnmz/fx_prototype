<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Marketing Communications Services';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/solutions-and-services">Solutions &amp; Services</a></li>
			<li><a href = "../">Global Services</a></li>
			<li><span>Marketing Communications Services</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4 col-md-3">
				<h1>Marketing Communications Services</h1>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 offset-sm-1 col-md-8 offset-md-2">
				<section class = "section">
					<a id = "content_1" class = "anchor-link" data-label = "Challenges"></a>
					<h2>Challenges</h2>

					<p>Documents are vital to communication in business world, however the management and production of those documents are often complex using a multiple suppliers. Fuji Xerox Communication & Marketing Services is a holistic managed services from document concept and creation, purchasing, production to delivery across a full range of marketing and communication activities.</p>

					<img src="/assets/fuji-xerox/images/articles/c_bpo-fig-03.png" alt="">
				</section>

				<section class = "section">
					<a id = "content_2" class = "anchor-link" data-label = "Benefits"></a>
					<h2>Benefits</h2>
					<p>Communications &amp; Marketing Services, SkyDesk Media Switch</p>
				</section>

				<section class = "section">
					<a id = "content_3" class = "anchor-link" data-label = "Resources"></a>
					<h2>Resources</h2>
					<p>Communications &amp; Marketing Services, SkyDesk Media Switch</p>
				</section>

				<section class = "section related-pages">
					<a id = "content_4" class = "anchor-link" data-label = "Related Pages"></a>
					<h3>Related Pages</h3>

					<div class = "blocks">
						<div class = "related-pages__block">
							<a href = "#">
								<h4>Customer Communication Management (CCM) Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Color Management Solutions &amp; Services</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>
					</div>
				</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Global Services</a></h3>
					<ul>
						<li><a href="#">Managed Document/Print Services</a></li>
						<li><a href="/solutions-and-services/global-services/marketing-communication-services" class = "active">Marketing Communications Services</a></li>
						<li><a href="#">Imaging &amp; Document Management Services</a></li>
						<li><a href="#">Short-term Rental</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
	</div>
</article>
<!-- End: Article -->

<div class = "modal" id = "subscriptionCall">
	<div class = "modal-close-button">
		<a href = "#"><i class = "icon-cross"></i></a>
	</div>
	<div class = "wingspan">
		<div class = "modal-subscription">
			<h2>Subscribe to our newsletter</h2>

			<div class = "modal-subscription__intro">
				<p>Keep yourself updated with Fuji Xerox latest products!</p>
			</div>

			<div class = "form-row row-col-12">
				<div class = "col-sm-6 push-right-10">
					<div class = "input-group-text">
						<label for = "nameSubscribe">Name</label>
						<input type = "text" id="nameSubscribe" />
					</div>
				</div>
			</div>

			<div class = "form-row row-col-12">
				<div class = "col-sm-6 push-right-10">
					<div class = "input-group-text">
						<label for = "emailSubscribe">Email</label>
						<input type = "text" id="emailSubscribe" />
					</div>
				</div>
			</div>

			<div class = "btn-group">
				<button type = "submit" class = "btn-submit">Subscribe Now <i class = "icon-chevron-with-circle-right"></i></button>
			</div>

		</div>
	</div>
</div>

<?php include('footer.php') ?>
<?php include('side-menu.php') ?>
<?php include('bottom.php') ?>
</body>
</html>