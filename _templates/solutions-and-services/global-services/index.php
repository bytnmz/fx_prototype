<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Global Services';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/solutions-and-services">Solutions &amp; Services</a></li>
			<li><span>Global Services</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4 col-md-3">
				<h1>Global Services</h1>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 offset-sm-1 col-md-8 offset-md-2">
				<a id = "content_1" class = "anchor-link" data-label = "Overview"></a>
				<h2>Overview</h2>

				<p>Various types of documents are used in day to day business activities, from sales and marketing collateral, proposal, product documentation and manual, transactional documents. Those documents consist valuable information essential to the business and have direct impacts to the business, such as improving sales and customer satisfaction.</p>

				<p>Fuji Xerox Communication and Business Process Outsourcing provides various solution such as digitization, indexing, storage and analysis to reduce processing time and shorten time to the market and decision making. We also provide end to end document processes enabling effective customer communication, from design, editing, print and distribution.</p>

				<hr>

				<a id = "content_2" class = "anchor-link" data-label = "Our Solutions"></a>
				<section class = "section related-pages">
					<h3>Our Solutions</h3>

					<div class = "blocks">
						<div class = "related-pages__block">
							<a href = "#">
								<h4>Managed Document/Print Services</h4>
								<span class = "category">Global Services</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "marketing-communication-services/">
								<h4>Marketing Communications Services</h4>
								<span class = "category">Global Services</span>
							</a>
						</div>
						<div class = "related-pages__block">
							<a href = "#">
								<h4>Imaging &amp; Document Management Services</h4>
								<span class = "category">Global Services</span>
							</a>
						</div>
						<div class = "related-pages__block">
							<a href = "#">
								<h4>Short-term Rental</h4>
								<span class = "category">Global Services</span>
							</a>
						</div>
					</div>
				</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Solution &amp; Services</a></h3>
					<ul>
						<li><a href = "/solutions-and-services/office-solutions">Office Solutions</a></li>
						<li><a href = "/solutions-and-services/production-solutions/">Production Solutions</a></li>
						<li><a href = "/solutions-and-services/enterprise-solutions/">Enterprise Solutions</a></li>
						<li><a href = "/solutions-and-services/global-services/" class = "active">Global Services</a></li>
						<li><a href = "/solutions-and-services/commercial-financing-solution-services/">Commercial Financing Solution Services</a></li>
						<li><a href = "#">Industries</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>