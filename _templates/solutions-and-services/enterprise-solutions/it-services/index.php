<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | IT Services';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/solutions-and-services">Solutions &amp; Services</a></li>
			<li><a href = "../">Enterprise Solutions</a></li>
			<li><span>IT Services</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4 col-md-3">
				<h1>IT Services</h1>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 offset-sm-1 col-md-8 offset-md-2">
				<section class = "section">
					<a id = "content_1" class = "anchor-link" data-label = "Challenges"></a>
					<h2>Challenges</h2>
					<p>IT Concierge Service, Data Center &amp; Cloud Managed Services</p>
				</section>

				<section class = "section">
					<a id = "content_2" class = "anchor-link" data-label = "Benefits"></a>
					<h2>Benefits</h2>
					<div class = "points">
					<ul>
						<li>
							<h3>5X</h3>

							<ul>
								<li>Improved Uptime IT Infrastructure</li>
							</ul>
						</li>

						<li>
							<h3>50%</h3>

							<ul>
								<li>Lower IT Costs</li>
							</ul>
						</li>

						<li>
							<h3>2X</h3>

							<ul>
								<li>Reliability Service</li>
							</ul>
						</li>
					</ul>
				</div>

				</section>

				<!-- Resources Section -->
				<section class = "section resources">
					<a id = "content_3" class = "anchor-link" data-label = "Resources"></a>
					<h3>Resources</h3>

					<div class = "resources__block">
						<h4>IT Services Brochure</h4>
						<dl>
							<dt>File Size:</dt>
							<dd>4100kb</dd>
							<dt>Document type:</dt>
							<dd>Portable Document Format</dd>
						</dl>

						<!-- DEV NOTE:  -->
						<!-- IF Email Subscription is switched off -->
						<!-- <a href = "#" class = "resources__block__download-btn" title = "Download Colour Management Brochure"><i class = "icon-download"></i><span class = "sr-only">Download Resource</span></a> -->
						<!-- ELSE -->
						<a href = "#subscriptionCall" class = "fancybox resources__block__download-btn" title = "Download Colour Management Brochure"><i class = "icon-download"></i><span class = "sr-only">Download Resource</span></a>
						<!-- END -->
					</div>

					<!-- Subscription Call -->
					<div class = "subscribe" id = "subscriptionCall">
						<div class = "row-col-12">
							<div class = "col-sm-6 col-md-6 col match-height">
								<h3>You may download the PDF here</h3>
								<a href="http://www.fujixerox.com.sg/images/solutions_services/IT%20Concierge%20Service.pdf?utm_source=eDM&utm_medium=CA&utm_campaign=ICS%20promo" target = "_blank" class = "btn-download">IT Services Brochure<i class="icon-download"></i></a>
							</div>
							<div class = "col-sm-6 col-md-6 col match-height">
								<h3>Subscribe to our newsletter</h3>
								<!-- <p>Keep yourself update to Fuji Xerox latest product!</p> -->
								<div class = "form-row row-col-12">
									<div class = "col-sm-6">
										<div class = "input-group-text">
											<label for = "subscribeName">Name<span class = "required">*</span></label>
											<input type = "text" id = "subscribeName" placeholder = "Name">
										</div>
										<div class = "input-group-text">
											<label for = "subscribeEmail">Email<span class = "required">*</span></label>
											<input type = "email" id = "subscribeEmail" placeholder = "abc@example.com">
										</div>
									</div>
								</div>
								<div class = "btn-group">
									<button type = "submit">Submit<i class = "icon-chevron-with-circle-right"></i></button>
								</div>
							</div>
						</div>
					</div>
					<!-- End: Subscription Call -->
				</section>
				<!-- End: Resources Section -->


				<section class = "section related-pages">
					<a id = "content_4" class = "anchor-link" data-label = "Related Pages"></a>
					<h3>Related Pages</h3>

					<div class = "blocks">
						<div class = "related-pages__block">
							<a href = "#">
								<h4>Customer Communication Management (CCM) Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Color Management Solutions &amp; Services</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>
					</div>
				</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Enterprise Solutions</a></h3>
					<ul>
						<li><a href="#">Business Process Management (BPM)</a></li>
						<li><a href="#">Enterprise Content Management (ECM)</a></li>
						<li><a href="#">Knowledge Management Solutions</a></li>
						<li><a href="#">Enterprise Collaboration Solutions</a></li>
						<li><a href="#">Infrastructure Solutions</a></li>
						<li><a href="#">System Integration Services</a></li>
						<li><a href="/solutions-and-services/enterprise-solutions/it-services/" class = "active">IT Services</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>