<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Enterprise Solutions';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/solutions-and-services">Solutions &amp; Services</a></li>
			<li><span>Enterprise Solutions</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4 col-md-3">
				<h1>Enterprise Solutions</h1>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 offset-sm-1 col-md-8 offset-md-2">
				<a id = "content_1" class = "anchor-link" data-label = "Overview"></a>
				<h2>Overview</h2>

				<p>Supported by a team of consultants and solution architects, we help businesses transform complex information, giving you the ready resource to tap into enterprise knowledge and experience to enhance your competitiveness, accelerate your growth and increase your revenue.</p>

				<a id = "content_2" class = "anchor-link" data-label = "Our Solutions"></a>
				<section class = "section related-pages">
					<h3>Our Solutions</h3>

					<div class = "blocks">
						<div class = "related-pages__block">
							<a href = "#">
								<h4>Business Process Management (BPM)</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Enterprise Content Management (ECM)</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Knowledge Management Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Enterprise Collaboration Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Infrastructure Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>System Integration Services</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "it-services/">
								<h4>IT Services</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>
					</div>
				</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Solution &amp; Services</a></h3>
					<ul>
						<li><a href = "/solutions-and-services/office-solutions">Office Solutions</a></li>
						<li><a href = "/solutions-and-services/production-solutions/">Production Solutions</a></li>
						<li><a href = "/solutions-and-services/enterprise-solutions/" class = "active">Enterprise Solutions</a></li>
						<li><a href = "/solutions-and-services/global-services/">Global Services</a></li>
						<li><a href = "/solutions-and-services/commercial-financing-solution-services/">Commercial Financing Solution Services</a></li>
						<li><a href = "#">Industries</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<div class = "modal" id = "subscriptionCall">
		<div class = "modal-close-button">
			<a href = "#"><i class = "icon-cross"></i></a>
		</div>
		<div class = "wingspan">
			<div class = "modal-subscription">
				<h2>Subscribe to our newsletter</h2>

				<div class = "modal-subscription__intro">
					<p>Keep yourself updated with Fuji Xerox latest products!</p>
				</div>

				<div class = "form-row row-col-12">
					<div class = "col-sm-6 push-right-10">
						<div class = "input-group-text">
							<label for = "nameSubscribe">Name</label>
							<input type = "text" id="nameSubscribe" />
						</div>
					</div>
				</div>

				<div class = "form-row row-col-12">
					<div class = "col-sm-6 push-right-10">
						<div class = "input-group-text">
							<label for = "emailSubscribe">Email</label>
							<input type = "text" id="emailSubscribe" />
						</div>
					</div>
				</div>

				<div class = "btn-group">
					<button type = "submit" class = "btn-submit">Subscribe Now <i class = "icon-chevron-with-circle-right"></i></button>
				</div>

			</div>
		</div>
	</div>

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>