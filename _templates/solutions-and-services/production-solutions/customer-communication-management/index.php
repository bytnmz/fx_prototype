<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Customer Communication Management (CCM) Solutions';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/solutions-and-services">Solutions &amp; Services</a></li>
			<li><a href = "../">Production Solutions</a></li>
			<li><span>Customer Communication Management (CCM) Solutions</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4 col-md-3">
				<h1>Customer Communication Management (CCM) Solutions</h1>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 offset-sm-1 col-md-8 offset-md-2">
				<section class = "section">
					<a id = "content_1" class = "anchor-link" data-label = "Challenges"></a>
					<h2>Challenges</h2>
					<p>DocuSmart, PrintSoft, Print Shop Mail, Transform, Lasernet, DocuAutomation, GMC Inspire, SkyDesk Media Switch, FreeFlow Digital Publisher, XMPie Suite</p>
				</section>

				<section class = "section">
					<a id = "content_2" class = "anchor-link" data-label = "Benefits"></a>
					<h2>Benefits</h2>
					<h3>Increase agility and efficiency with GMC Inspire Legacy Repurposing</h3>

					<p>Enabling businesses to mine, extract, and convert legacy print streams and files into a format that allows them to repurpose and add content such as OMR, full color advertising, logos, barcodes and even optimize content for multichannel.</p>

					<h3>Transform the customer experience with Inspire Dynamic Communication</h3>
					<p>Enabling businesses to provide customer communication in a dynamic portal form through interactive charts and graphs that allow full drill-down/click-through capability and can be accessed through any PC and iOS or android tablet.</p>
					<img src="/assets/fuji-xerox/images/articles/ccm.jpg" alt="">

					<h3>Empower front-line staff with Inspire Interactive</h3>
					<p>Providing front-line staff with a collaborative and controlled platform to customize routine customer service correspondence through predefined templates and step-by-step guided prompts.</p>

					<h3>TransPromo communications: visual appeal and selling power</h3>
					<p>Providing communications that they are typically 10 times more effective than inserts; yet add little cost to the production budget because they are embedded in existing statements. And TransPromo can eliminate the wasted expense of inserts that will likely be tossed anyway.</p>

					<h3>Multichannel: your gateway to profitable communications</h3>
					<p>Offers all the functions you’ll need for a coordinated approach to Multichannel communications, and our modules are designed to work together from the start. Through accelerated response rates, increased customer satisfaction and growing revenues, businesses are observing real results from their Multichannel strategy with the GMC solution.</p>
				</section>

				<!-- Resources Section -->
				<section class = "section resources">
					<a id = "content_3" class = "anchor-link" data-label = "Resources"></a>
					<h3>Resources</h3>

					<div class = "resources__block">
						<h4>Colour Management Brochure</h4>
						<dl>
							<dt>File Size:</dt>
							<dd>4100kb</dd>
							<dt>Document type:</dt>
							<dd>Portable Document Format</dd>
						</dl>

						<a href = "#subscriptionCall" class = "fancybox resources__block__download-btn" title="Download Colour Management Brochure"><i class = "icon-download"></i><span class = "sr-only">Download Resource</span></a>
					</div>
				</section>
				<!-- End: Resources Section -->

				<section class = "section related-pages">
					<a id = "content_4" class = "anchor-link" data-label = "Related Pages"></a>
					<h3>Related Pages</h3>

					<div class = "blocks">
						<div class = "related-pages__block">
							<a href = "#">
								<h4>Customer Communication Management (CCM) Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Color Management Solutions &amp; Services</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>
					</div>
				</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Production Solutions</a></h3>

					<ul>
						<li><a href = "/solutions-and-services/production-solutions/customer-communication-management" class = "active">Customer Communication Management (CCM) Solutions</a></li>
						<li><a href = "#">Print Workflow Automation</a></li>
						<li><a href = "#">Color Management Solutions &amp; Services</a></li>
						<li><a href = "/solutions-and-services/production-solutions/print-management-information-systems">Print Management Information Systems (MIS)</a></li>
						<li><a href = "#">Packaging Solutions</a></li>
						<li><a href = "#">Business Development Consultancy Services</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<!-- Subscription Call -->
	<div class = "subscribe" id = "subscriptionCall">
		<div class = "row-col-12">
			<div class = "col-sm-6 col-md-6 col match-height">
				<h3>You may download the PDF here</h3>
				<a href="#" target = "_blank" class = "btn-download">Colour Management Brochure<i class="icon-download"></i></a>
			</div>
			<div class = "col-sm-6 col-md-6 col match-height">
				<h3>Subscribe to our newsletter</h3>
				<!-- <p>Keep yourself update to Fuji Xerox latest product!</p> -->
				<div class = "form-row row-col-12">
					<div class = "col-sm-6">
						<div class = "input-group-text">
							<label for = "subscribeName">Name</label>
							<input type = "text" id = "subscribeName" placeholder = "Name">
						</div>
						<div class = "input-group-text">
							<label for = "subscribeEmail">Email</label>
							<input type = "email" id = "subscribeEmail" placeholder = "abc@example.com">
						</div>
					</div>
				</div>
				<div class = "btn-group">
					<button type = "submit">Submit<i class = "icon-chevron-with-circle-right"></i></button>
				</div>
			</div>
		</div>
	</div>
	<!-- End: Subscription Call -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>