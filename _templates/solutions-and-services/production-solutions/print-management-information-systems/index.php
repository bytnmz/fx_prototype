<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Print Management Information Systems (MIS)';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/solutions-and-services">Solutions &amp; Services</a></li>
			<li><a href = "../">Production Solutions</a></li>
			<li><span>Print Management Information Systems (MIS)</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4 col-md-3">
				<h1>Print Management Information Systems (MIS)</h1>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 col-md-8 offset-sm-1 offset-md-2">
				<section class = "section">
					<a id = "content_1" class = "anchor-link" data-label = "Overview"></a>
					<h2>Overview</h2>

					<ul>
						<li><b>Modular architecture</b> – FreeFlow Core includes the base software and also two optional modules, Advanced Prepress and Advanced Automation</li>
						<li><b>Browser-based</b> – This solution runs in standard browsers. This greatly simplifies software installation and management</li>
						<li><b>PDF conversion</b> – Supports multiple PDF formats and utilises Adobe licensed software components</li>
						<li><b>Preflight</b> – Identify and even fix problems at any point during your print process. Produce higher quality output.</li>
					</ul>
				</section>

				<section class = "section">
					<a id = "content_2" class = "anchor-link" data-label = "Features"></a>
					<h2>Features</h2>

					<p>You are planning to have a serious growth of your business. So how are you going to expand your need for web infrastructure and IT. You’ve heard about the Cloud but you are not sure how it can really help you. Fuji Xerox has been in the technology space for decades and we are able to deliver to you the broadest and most robust Cloud Solutions and Services for your needs. Regardless of the size of your business, we will help you to deliver the right cloud models for your needs.</p>

					<table class = "stackable">
						<thead>
							<tr>
								<th>Feature</th>
								<th>Benefit(s)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<p>Connect all your digital investments to reduce your costs</p>
								</td>
								<td>
									<p>Streamline all your systems and print engines with a solution that makes your print assets more productive, day in and day out. FreeFlow Core keeps each job moving to make your organisation super-efficient so you spend less time processing jobs in-house and have more time to build new business.</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>Configurable and adaptive workflows, to speed through jobs</p>
								</td>
								<td>
									<p>FreeFlow Core is a modular and open, end-to-end workflow solution that automates the entire print production process, for faster turnaround times with reduced error rates. You choose the functionality and configure the solution to perfectly match your workflow requirements. This adds up to greater efficiency. Real consistency. Quicker delivery. And more growth opportunities.</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>A setup-and-go solution that’s easy to use — and customise</p>
								</td>
								<td>
									<p>FreeFlow Core is browser-based and includes EasyStart workflows to guide users through the process of creating and managing workflows, step-by-step. It hides the complexity of individual operations and workflow programming while it provides easy customisation, with an intuitive graphical user interface. The simple drag-and-drop interface means you don?t need a software engineering degree to create workflows. Now anyone can easily manage incoming jobs and get them done in a fraction of the time.</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>Seamless integration with your current workflows</p>
								</td>
								<td>
									<p>FreeFlow Core integrates with your current workflows, including web-to-print, order entry, management information systems (MIS) and more. It eliminates manual data entry because it incorporates Manifest Automation from Xerox (MAX) and JMF/XSLT to automate job information.</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>Modular, scalable and affordable for any size operation</p>
								</td>
								<td>
									<p>Whether your organisation is 3 or 300 people, FreeFlow Core offers a highly-productive, end-to-end workflow solution. It?s available as individual modules, so you can easily add more functionality and create customised, integrated solutions. And if you have multiple print engines and/or Digital Front Ends, there?s no limit to the number of printers you can connect, all at no additional charge.</p>
								</td>
							</tr>
						</tbody>
					</table>
				</section>

				<section class = "section related-pages">
					<a id = "content_3" class = "anchor-link" data-label = "Related Pages"></a>
					<h3>Related Pages</h3>

					<div class = "blocks">
						<div class = "related-pages__block">
							<a href = "#">
								<h4>Customer Communication Management (CCM) Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Color Management Solutions &amp; Services</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>
					</div>
				</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Production Solutions</a></h3>
					<ul>
						<li><a href = "/solutions-and-services/production-solutions/customer-communication-management">Customer Communication Management (CCM) Solutions</a></li>
						<li><a href = "#">Print Workflow Automation</a></li>
						<li><a href = "#">Color Management Solutions &amp; Services</a></li>
						<li><a href = "/solutions-and-services/production-solutions/print-management-information-systems" class = "active">Print Management Information Systems (MIS)</a></li>
						<li><a href = "#">Packaging Solutions</a></li>
						<li><a href = "#">Business Development Consultancy Services</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<!-- Subscription Call -->
	<div class = "subscribe" id = "subscriptionCall">
		<div class = "row-col-12">
			<div class = "col-sm-6 col-md-6 col match-height">
				<h3>You may download the PDF here</h3>
				<a href="#" target = "_blank" class = "btn-download">Colour Management Brochure<i class="icon-download"></i></a>
			</div>
			<div class = "col-sm-6 col-md-6 col match-height">
				<h3>Subscribe to our newsletter</h3>
				<!-- <p>Keep yourself update to Fuji Xerox latest product!</p> -->
				<div class = "form-row row-col-12">
					<div class = "col-sm-6">
						<div class = "input-group-text">
							<label for = "subscribeName">Name<span class = "required">*</span></label>
							<input type = "text" id = "subscribeName" placeholder = "Name">
						</div>
						<div class = "input-group-text">
							<label for = "subscribeEmail">Email<span class = "required">*</span></label>
							<input type = "email" id = "subscribeEmail" placeholder = "abc@example.com">
						</div>
					</div>
				</div>
				<div class = "btn-group">
					<button type = "submit">Submit<i class = "icon-chevron-with-circle-right"></i></button>
				</div>
			</div>
		</div>
	</div>
	<!-- End: Subscription Call -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>