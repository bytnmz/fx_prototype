<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Print Management Solutions';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/solutions-and-services">Solutions &amp; Services</a></li>
			<li><a href = "../">Office Solutions</a></li>
			<li><span>Print Management Solutions</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4 col-md-3">
				<h1>Print Management Solutions</h1>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 col-md-8 offset-sm-1 offset-md-2">
				<section class = "section">
					<a id = "content_1" class = "anchor-link" data-label = "Engage"></a>
					<h2>Engage our expertise and find more time to concentrate on your business.</h2>

					<p>Fuji Xerox Managed Print Services (MPS) is a suite of software, solutions, services and strategies that is customised to meet the different needs of each organisation, helping companies achieve significant cost savings and improve process efficiency.</p>

					<h3>Managed Print Services - Government</h3>
					<img src="/assets/fuji-xerox/images/articles/MPS.png" alt="MPS">
					<p>Analyse the organisation's printing usage and optimise the document workflow.</p>

					<h3>Enterprise Print Services</h3>
					<img src="/assets/fuji-xerox/images/articles/EPS.png" alt="EPS">
					<p>Increase flexibility and mobility througbh mobile and remote printing.</p>

					<h3>Xerox Print Services</h3>
					<img src="/assets/fuji-xerox/images/articles/XPS.png" alt="XPS">
					<p>Reduce cost of printing and enhance security of confidential and sensitive documents.</p>
				</section>

				<section class = "section">
					<a id = "content_2" class = "anchor-link" data-label = "MPS Provider"></a>
					<h2>MPS Provider</h2>

					<p>
						When you choose Fuji Xerox as your MPS provider, we will give you comprehensive assistance to benefit your overall productivity, reduce ongoing costs and improve your processes. Our experienced team will assist you by organising content, assuring incremental savings and maximising efficiency. With a combination of advanced technology, process management and skilled staff, we will lead your business to holistic improvements. Benefits we offer include:
					</p>

					<ul>
						<li>Reduce costs</li>
						<li>More efficient workflow</li>
						<li>Improved productivity</li>
						<li>Higher performance across numerous operations</li>
						<li>Expanded security</li>
					</ul>
				</section>

				<section class = "section">
					<a id = "content_3" class = "anchor-link" data-label = "Our Services"></a>
					<h2>Our Services</h2>

					<p>We will begin by evaluating your environment, document infrastructure and your workflows. After getting an idea of your current print spending, we will chart out a path for your future operations. Our experts will help in connecting all operations to your IT environment in a secure manner. We will proceed to offer solutions for secure processes and undertake management of your servers and printing queues. After an appropriate review and analysis, we will put our best foot forward in helping you with your managed printing services in Singapore while improving productivity.</p>

					<p>If you would like to find out more, get in touch with our team and we will gladly provide you with any information you need.</p>
				</section>


				<a id = "content_4" class = "anchor-link" data-label = "Related Pages"></a>
				<section class = "section related-pages">
					<h3>Related Pages</h3>

					<div class = "blocks">
						<div class = "related-pages__block">
							<a href = "#">
								<h4>Customer Communication Management (CCM) Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Color Management Solutions &amp; Services</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>
					</div>
				</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Office Solutions</a></h3>
					<ul>
						<li><a href="/solutions-and-services/office-solutions/print-management-solutions/" class = "active">Print Management Solutions</a></li>
						<li><a href="#">Document Security Solutions</a></li>
						<li><a href="/solutions-and-services/office-solutions/office-automation-solutions/">Office Automation Solutions</a></li>
						<li><a href="#">Content Management Solutions</a></li>
						<li><a href="#">Cloud &amp; Mobility Solutions</a></li>
						<li><a href="#">Device Remote Support Services</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<!-- Subscription Call -->
	<div class = "subscribe" id = "subscriptionCall">
		<div class = "row-col-12">
			<div class = "col-sm-6 col-md-6 col match-height">
				<h3>You may download the PDF here</h3>
				<a href="#" target = "_blank" class = "btn-download">Colour Management Brochure<i class="icon-download"></i></a>
			</div>
			<div class = "col-sm-6 col-md-6 col match-height">
				<h3>Subscribe to our newsletter</h3>
				<!-- <p>Keep yourself update to Fuji Xerox latest product!</p> -->
				<div class = "form-row row-col-12">
					<div class = "col-sm-6">
						<div class = "input-group-text">
							<label for = "subscribeName">Name<span class = "required">*</span></label>
							<input type = "text" id = "subscribeName" placeholder = "Name">
						</div>
						<div class = "input-group-text">
							<label for = "subscribeEmail">Email<span class = "required">*</span></label>
							<input type = "email" id = "subscribeEmail" placeholder = "abc@example.com">
						</div>
					</div>
				</div>
				<div class = "btn-group">
					<button type = "submit">Submit<i class = "icon-chevron-with-circle-right"></i></button>
				</div>
			</div>
		</div>
	</div>
	<!-- End: Subscription Call -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>