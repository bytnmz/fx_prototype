<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Office Automation Solutions';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/solutions-and-services">Solutions &amp; Services</a></li>
			<li><a href = "../">Office Solutions</a></li>
			<li><span>Office Automation Solutions</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4 col-md-3">
				<h1>Office Automation Solutions</h1>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 col-md-8 offset-sm-1 offset-md-2">
				<section class = "section">
				<a  id = "content_1" class = "anchor-link" data-label = "Solutions"></a>
				<h2>Office Operations Solutions</h2>

					<p>Understanding that each industry has its own needs, Fuji Xerox provides business and industry-centric solutions to facilitate the smooth running of your businesses.</p>

					<p>For <a href="#">Enterprises</a>, we provide solutions to automate your document processes of Accounts Payables, Receivables, Sales Orders and more, regardless if you are on a SAP platform or looking for a cloud solution. If you are in the Healthcare industry, Fuji Xerox provides an integrated solution to manage your bed turnaround, hospital facilities, meal ordering systems and other solutions through mobility.</p>

					<p>For <a href="#">SMEs</a>, we are able to help you streamline your most fundamental yet critical processes of accounting and inventory management, ensuring that finances are accurate and accounted for. To enable quick and easy access to your business-critical information, a Document Management Solution for SMEs would help you to consolidate your physical and electronic documents.</p>

					<p>We work with the industry's technology leaders to provide the best breed of solutions and services to meet all your business information management needs.</p>
				</section>

				<!-- Accordion Widget -->
				<section class = "section accordion-widget">
					<a id = "content_2" class = "anchor-link" data-label = "Partnership"></a>
					<h2>Partnership</h2>

					<div class = "accordion-widget__blocks">
						<div class = "accordion-widget__block">
							<h3 class = "accordion-widget__block-title">Imagine</h3>
							<div class = "accordion-widget__block-content">

								<div class = "row-col-12">
									<!-- <div class = "col-sm-4 thumbnail">
										<image src = "/assets/fuji-xerox/images/articles/imagine.png" alt = "Imagine Logo" class = "logo"/>
									</div> -->

									<div  class = "description-text">

										<p>Imagine streamlines solutions for business and operations processes, allowing users to leverage on technology to reach operational efficiency.</p>

										<h4>aBzB</h4>

										<p>aBzB is built on a workflow management system that assists businesses to orchestrate various business and operational functions. The system is quick to model after you dynamic business rules. It facilitates effective planning, execution and co-ordination; driven by constant improvement based on feedback collection and process analysis.</p>

										<p>aBzB suite of solutions aids your business in terms of:</p>

										<ul>
											<li>Accounting and Finance</li>
											<li>CRM &amp; Sales</li>
											<li>Human Resource</li>
											<li>Inventory &amp; Warehousing</li>
											<li>Project management</li>
											<li>Document management</li>
											<li>Training module</li>
											<li>Business Intelligence</li>
											<li>Events</li>
										</ul>

										<h4>Janison</h4>

										<p>Deliver online learning to your internal and external clients simply and quickly across a variety of platforms and devices with Janison’s Cloud Learning System (CLS).</p>

										<p>With Janison, you will be able to:</p>

										<ul>
											<li>Take control of your learning management and delivery, with our easy-to-use interface</li>
											<li>Create dynamic, interactive and engaging online content for many different purposes, without extensive HTML knowledge</li>
											<li>Quickly create separate sites for a limitless number of clients</li>
											<li>Access from any device and browser</li>
											<li>Easily manage and extend the platform’s functionality</li>
											<li>Work with the best in the industry</li>
										</ul>

									</div>

								</div>

							</div>
						</div>

						<div class = "accordion-widget__block">
							<h3 class = "accordion-widget__block-title">WMD</h3>
							<div class = "accordion-widget__block-content">

								<div class = "row-col-12">
									<!-- <div class = "col-sm-4 thumbnail">
										<image src = "/assets/fuji-xerox/images/articles/wmd.jpg" alt = "WMD Logo" class = "logo"/>
									</div> -->

									<div  class = "description-text">

										<p>WMD delivers best-practice solutions based on SAP Business Workflow, particularly for DMS projects interfacing with SAP. All solutions are entirely SAP-integrated and take advantage of the SAP infrastructure that you already have in place. Two products from WMD's xSuite for SAP - xFlow Invoice and Interface - are SAP-certified, carrying the designation "powered by SAP NetWeaver".</p>

										<h4>WMD x Suite &reg; - Comprehensive Workflow Solutions for Business Processes in SAP Standard</h4>

										<p>WMD xSuite is a generic solution based on SAP Standard and designed to optimise your business processes. xSuite delivers an automated method for mapping your paper-based processes within SAP. It delivers an best-practice approach and solutions that are regularly enhanced by further updates and releases.</p>

										<p>Click here to view the <a href="#">WMD xSuite&reg;</a> brochure</p>

									</div>

								</div>

							</div>
						</div>

						<div class = "accordion-widget__block">
							<h3 class = "accordion-widget__block-title">Esker</h3>
							<div class = "accordion-widget__block-content">

								<div class = "row-col-12">
									<!-- <div class = "col-sm-4 thumbnail">
										<image src = "/assets/fuji-xerox/images/articles/esker.jpg" alt = "Esker Logo" class = "logo"/>
									</div> -->

									<div  class = "description-text">

										<p>Esker is helping organisations eliminate paper and improve business processes with on-demand document automation solutions. Thousands of Esker solutions worldwide enable industry leaders to greatly increase their competitive advantages.</p>

										<h4>Document Delivery</h4>

										<p>Send business documents via cloud fax or mail services directly from desktop or enterprise applications. Esker offers both on-demand and on-premise solutions to automate document delivery.</p>

										<ul>
											<li>Mail Services</li>
											<li>Fax Services</li>
											<li>Fax Servers</li>
										</ul>

										<h4>Accounts Receivables Automation</h4>

										<p>Integrating directly with invoicing applications, Esker AR solutions allow companies to automate the delivery and archiving of electronic and paper invoices. Esker AR is the first SaaS (Software as a Service) solution to offer 100% automation of customer invoice delivery.</p>

										<h4>Accounts Payable Automation</h4>

										<p>Esker solutions make paper-free AP a reality with intelligent capture, touchless processing capabilities and electronic workflow. Invoice approval workflow is simplified and consolidated. Organisations can cut operational and administrative AP costs by 40-60%. Esker offerings include the first complete Software as a Service (SaaS) solution fully integrated with SAP applications.</p>

										<h4>Automated Sales Order Processing</h4>

										<p>Esker Sales Order Processing solution automates every phase of sales order processing - from the reception of a customer document to the creation of a corresponding sales order in your ERP system. With the ability to handle any type of incoming order format, the Esker solution makes every order electronic and instantly accessible. Esker offerings include solutions with specific content mappings between the Esker platform and the sales order management module of SAP applications.</p>

										<p>Click here to view the <a href="#">Eskter On-Demand Solutions</a> brochure.</p>

									</div>

								</div>

							</div>
						</div>

						<div class = "accordion-widget__block">
							<h3 class = "accordion-widget__block-title">AutoCount</h3>
							<div class = "accordion-widget__block-content">

								<div class = "row-col-12">
									<!-- <div class = "col-sm-4 thumbnail">
										<image src = "/assets/fuji-xerox/images/articles/AutoCount.jpg" alt = "AutoCount Logo" class = "logo"/>
									</div> -->

									<div  class = "description-text">

										<p>AutoCount Sdn Bhd develop and deliver quality business software solution, AutoCount Business Suite which comprises of Accounting, Stock Control, Payroll, Point of Sales and Mobile solutions that integrates seamlessly to ensure the smooth running of customer business.</p>

										<h4>AutoCount Accounting</h4>

										<p>With its advanced technology such as Microsoft .Net Framework, SQL Server 2005, and internet ready, AutoCount Accounting stands out as one of the best accounting software for today business requirements. Characterized by its easy-to-learn and integrated features, AutoCount Accounting helps to streamline your business operation. </p>


									</div>

								</div>

							</div>
						</div>
					</div>

					<div class = "accordion-widget__block">
						<h3 class = "accordion-widget__block-title">Wavex</h3>
						<div class = "accordion-widget__block-content">

							<div class = "row-col-12">
								<!-- <div class = "col-sm-4 thumbnail">
									<image src = "/assets/fuji-xerox/images/articles/Wavex.jpg" alt = "Wavex logo" class = "logo"/>
								</div> -->

								<div  class = "description-text">

									<p>Wavex provides state-of-the-art technologies in the RFID and Wireless industry.</p>

									<ul>
										<li>RFID Readers</li>
										<li>RFID Handhelds</li>
										<li>RFID Accessories</li>
										<li>RFID Labels/Tags/Cards</li>
									</ul>

								</div>

							</div>

						</div>
					</div>

					<div class = "accordion-widget__block">
						<h3 class = "accordion-widget__block-title">K2</h3>
						<div class = "accordion-widget__block-content">

							<div class = "row-col-12">
								<!-- <div class = "col-sm-4 thumbnail">
									<image src = "/assets/fuji-xerox/images/articles/Infenion.jpg" alt = "Infenion Logo" class = "logo"/>
								</div> -->

								<div  class = "description-text">
									<p>Infenion Tech Pte Ltd is an integrated information technology (IT) solutions provider of smart phones application, web-based business solutions and enterprise infrastructure that delivers the best value IT solutions that help our customers achieve better business performance.</p>

									<h4>Apple iOS, Google Android and BlackBerry Mobile Application Development</h4>

									<p>Infenion Tech Pte Ltd are experienced in designing and developing software applications for your business needs. We are specialized with mobile solutions, especially Apple iOS (iPhone, iPad & iPod) , Google Android platforms and BlackBerry; not forgotten the know-how of our web-based solutions on both desktop and mobile browsers.</p>

									<h4>System Development &amp; Integration (Web/Mobile Application &amp; System Application)</h4>

									<p>Systems integration is all about combining your disparate systems and processes to create one business system that successfully services your customers. Infenion Tech is a full-service Information Technology consulting firm that assists IT professionals in selecting, implementing, and integrating core business technologies within a robust infrastructure.</p>

								</div>

							</div>

						</div>
					</div>

					<div class = "accordion-widget__block">
						<h3 class = "accordion-widget__block-title">Faxcore</h3>
						<div class = "accordion-widget__block-content">

							<div class = "row-col-12">
								<!-- <div class = "col-sm-4 thumbnail">
									<image src = "/assets/fuji-xerox/images/articles/Faxcore.jpg" alt = "Faxcore Logo" class = "logo"/>
								</div> -->

								<div class = "description-text">
									<p>Built on the Microsoft .NET Framework (3.5 and above) the FaxCore high-performance engine is designed to deliver fax traffic over enterprise networks of any size. FaxCore lets you fax enable the entire organization, locally or globally, plus provides full access for road warriors and employees working from home offices.</p>

									<p>FaxCore is integrated and is certified for most Microsoft applications including Microsoft SQL Server, Exchange (all versions), SharePoint, Microsoft CRM and Windows Server 2008 without cumbersome connectors or expensive add-ons.</p>

								</div>

							</div>

						</div>
					</div>

					<div class = "accordion-widget__block">
						<h3 class = "accordion-widget__block-title">EF Software</h3>
						<div class = "accordion-widget__block-content">

							<div class = "row-col-12">
								<!-- <div class = "col-sm-4 thumbnail">
									<image src = "/assets/fuji-xerox/images/articles/EF-software.jpg" alt = "EF software Logo" class = "logo"/>
								</div> -->

								<div  class = "description-text">
									<p>EF Software is a solution company that specialise in healthcare support services applications. From the top-down approach of viewing consolidated reports on hospital operations to the management of patients’ meals or beds by nurses or housekeepers, EF Software can aid the healthcare segment in addressing their problems and improve the overall patient’s healthcare experience.</p>

									<h4>eBed Turnaround System (eBTS)</h4>

									<p>eBTS is the application in the eMobileCare suite that is designed for the housekeeping department. It is 1 of our most popular and highly sought-after product that had won several awards such as the "TEC Public Service Innovation Award" awarded by Prime Minister's Office in year 2006.</p>

									<p>This revolutionary system enables hospitals to reduce bed turnover time and maximize resources. It can be integrated with the hospitals' patient information system such as SAP using the HL7 protocol. It is particularly useful for both nursing staffs and housekeepers.</p>

									<h4>ePorter</h4>

									<p>ePorter is the application in the eMobileCare suite that is designed for the portering department.</p>

									<p>It replaces the manual process that required hardcopy requests and faxes which used up a lot of resources like papers and toners. With ePorter, nursing staffs can submit portering services requests online via the web-based interface.</p>

									<h4>eMeal Ordering System (eMOS)</h4>

									<p>eMOS is the applications in the eMobileCare suite that is designed for the food service department.</p>

									<p>It is designed to auto-retrieve patient data through the integration with patients information system such as SAP and for hospitals to capture patient's daily three meal orders submitted either via the web-based interface used by nursing staffs or the PDA / Smart Phone program used by food service attendants.</p>

									<h4>eMedical Record Sheet (eMRS)</h4>

									<p>eMRS is the applications in the eMobileCare suite that is designed for the emergency department.</p>

									<p>It provided the emergency department a central storage to store all the important information such as the diagnosis and inspection carried out by the doctor and the nurses before transferring the patients to the wards. Through eMRS, all the information starting from which doctor or nurses served the patient and the diagnosis can be retrieved instantaneously from eMRS.</p>

									<h4>eLaundry RFID</h4>

									<p>eLaundry is the application in the eMobileCare suite that is designed for the Linen Supply department and their laundry partners.</p>

									<p>It replaces the manual process of tracking the items that are send for laundry services. With eLaundry, each item movements can be traced and its actual location can be retrieved with a click away. This system is packaged together with the RFID tag and readers for ease of use.</p>

								</div>

							</div>

						</div>
					</div>

					<div class = "accordion-widget__block">
						<h3 class = "accordion-widget__block-title">InspireTech</h3>
						<div class = "accordion-widget__block-content">

							<div class = "row-col-12">
								<!-- <div class = "col-sm-4 thumbnail">
									<image src = "/assets/fuji-xerox/images/articles/Inspiretech.jpg" alt = "Inspiretech Logo" class = "logo"/>
								</div> -->

								<div class = "description-text">
									<p>Inspire-Tech's passion lies in creating comprehensive mobile applications, their core competency.</p>

									<h4>EasiShare</h4>

									<p>EasiShare is a mobility-enabling solution that allows easy sync and share with team members, in a secure environment. Enterprises and security-minded organisations may now ensure balance between mobility and productivity, while adhering to stringent IT protocol compliance.</p>

									<p>Users enjoy security and true mobility when collaborating on documents, resulting in greater productivity for enterprises and government organisations.</p>

									<p>Click to view the <a href="#">EasiShare</a> brochure.</p>

								</div>

							</div>

						</div>
					</div>
				</section>
				<!-- End: Accordion Widget -->

				<section class = "section">
					<a id = "content_3" class = "anchor-link" data-label = "Case Study"></a>
					<h2>Construction Case Studies</h2>
					<h3>Leading Concrete Supplier</h3>

					<a href="#">
						<img src="/assets/fuji-xerox/images/articles/case-study.jpg" alt="Case Study" />
						<p>Fuji Xerox helps Leading Construction Firm Implement Barcode Tracking System</p>
					</a>
				</section>

				<section class = "section related-pages">
					<a id = "content_4" class = "anchor-link" data-label = "Related Pages"></a>
					<h3>Related Pages</h3>

					<div class = "blocks">
						<div class = "related-pages__block">
							<a href = "#">
								<h4>Customer Communication Management (CCM) Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Color Management Solutions &amp; Services</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>
					</div>
				</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Office Solutions</a></h3>
					<ul>
						<li><a href="/solutions-and-services/office-solutions/print-management-solutions/">Print Management Solutions</a></li>
						<li><a href="#">Document Security Solutions</a></li>
						<li><a href="/solutions-and-services/office-solutions/office-automation-solutions/" class = "active">Office Automation Solutions</a></li>
						<li><a href="#">Content Management Solutions</a></li>
						<li><a href="#">Cloud &amp; Mobility Solutions</a></li>
						<li><a href="#">Device Remote Support Services</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<!-- Subscription Call -->
	<div class = "subscribe" id = "subscriptionCall">
		<div class = "row-col-12">
			<div class = "col-sm-6 col-md-6 col match-height">
				<h3>You may download the PDF here</h3>
				<a href="#" target = "_blank" class = "btn-download">Colour Management Brochure<i class="icon-download"></i></a>
			</div>
			<div class = "col-sm-6 col-md-6 col match-height">
				<h3>Subscribe to our newsletter</h3>
				<!-- <p>Keep yourself update to Fuji Xerox latest product!</p> -->
				<div class = "form-row row-col-12">
					<div class = "col-sm-6">
						<div class = "input-group-text">
							<label for = "subscribeName">Name<span class = "required">*</span></label>
							<input type = "text" id = "subscribeName" placeholder = "Name">
						</div>
						<div class = "input-group-text">
							<label for = "subscribeEmail">Email<span class = "required">*</span></label>
							<input type = "email" id = "subscribeEmail" placeholder = "abc@example.com">
						</div>
					</div>
				</div>
				<div class = "btn-group">
					<button type = "submit">Submit<i class = "icon-chevron-with-circle-right"></i></button>
				</div>
			</div>
		</div>
	</div>
	<!-- End: Subscription Call -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>