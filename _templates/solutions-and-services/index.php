<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Solutions & Services';
$primary = 1;
$secondary = 1;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><span>Solutions &amp; Services </span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4 col-md-3">
				<h1>Solutions &amp; Services</h1>

				<p>Explore our extensive offering of cutting-edge solutions and tailor made services ranging from cloud services to sustainable business solutions.</p>
			</div>
		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Solution Mapper -->
	<section class = "section solution-mapper wingspan">

		<!-- Solution Mapper Filters -->
		<div class = "solution-mapper__filters row-col-15">
			<div class = "input-group-select col-sm-3">
				<select class = "custom-select" id = "industryFilter">
					<!-- <option value = "" selected>Select Industry</option>
					<option value = "Industry Type 1">Industry Type 1</option>
					<option value = "Industry Type 2">Industry Type 2</option>
					<option value = "Industry Type 3">Industry Type 3</option> -->
				</select>
				<script type="text/x-handlebars-template">
					<option value = "" selected>Select Industry</option>
					{{#each industries}}
					<option value= "{{id}}">{{name}}</option>
					{{/each}}
				</script>
			</div>

			<div class = "input-group-select col-sm-3">
				<select class = "custom-select" id = "departmentFilter">
					<!-- <option value = "" selected>Select Department</option>
					<option value = "Department Type 1">Department Type 1</option>
					<option value = "Department Type 2">Department Type 2</option>
					<option value = "Department Type 3">Department Type 3</option> -->
				</select>
				<script type="text/x-handlebars-template">
					<option value = "" selected>Select Department</option>
					{{#each departments}}
					<option value= "{{id}}">{{name}}</option>
					{{/each}}
				</script>
			</div>

			<div class = "input-group-select col-sm-3">
				<select class = "custom-select" id = "serviceFilter">
					<!-- <option value = "" selected>Select Service</option>
					<option value = "Service Type 1">Service Type 1</option>
					<option value = "Service Type 2">Service Type 2</option>
					<option value = "Service Type 3">Service Type 3</option> -->
				</select>
				<script type="text/x-handlebars-template">
					<option value = "" selected>Select Service</option>
					{{#each services}}
					<option value= "{{id}}">{{name}}</option>
					{{/each}}
				</script>
			</div>

			<div class = "input-group-select col-sm-3">
				<select class = "custom-select" id = "businessFilter">
					<!-- <option value = "" selected>Select Business</option>
					<option value = "Business Type 1">Business Type 1</option>
					<option value = "Business Type 2">Business Type 2</option>
					<option value = "Business Type 3">Business Type 3</option> -->
				</select>
				<script type="text/x-handlebars-template">
					<option value = "" selected>Select Business</option>
					{{#each businesses}}
					<option value= "{{id}}">{{name}}</option>
					{{/each}}
				</script>
			</div>

			<div class = "solution-mapper__filters__controls">
				<button class = "btn-clear-filter col-sm-2">Reset</button>
				<button class = "btn-show-all col-sm-2">Show All</button>
			</div>
		</div>
		<!-- End: Solution Mapper Filters -->

		<!-- Solution Mapper Introduction -->
		<div class = "solution-mapper__intro">
			<div>
				<div class = "col solution-mapper__intro-block">
					<a href = "">
						<img src = "/assets/fuji-xerox/images/solutions/box-1.png" alt = ""/>
						<h2>Office Solutions</h2>
						<p>Increase the office productivity with all-in-one-go solutions and software</p>
					</a>
				</div>

				<div class = "col solution-mapper__intro-block">
					<a href = "/solutions-and-services/production-solutions">
						<img src = "/assets/fuji-xerox/images/solutions/box-2.png" alt = ""/>
						<h2>Production Solutions</h2>
						<p>Automate your daily operations and optimise printing investment</p>
					</a>
				</div>

				<div class = "col solution-mapper__intro-block">
					<a href = "/solutions-and-services/enterprise-solutions">
						<img src = "/assets/fuji-xerox/images/solutions/box-3.png" alt = ""/>
						<h2>Enterprise Solutions</h2>
						<p>A team of consultants and solution architects help you to accelerate your growth and increase your revenue.</p>
					</a>
				</div>

				<div class = "col solution-mapper__intro-block">
					<a href = "/solutions-and-services/global-services/">
						<img src = "/assets/fuji-xerox/images/solutions/box-4.png" alt = ""/>
						<h2>Global Services</h2>
						<p>Your Document Outsourcing Services Partner</p>
					</a>
				</div>

				<div class = "col solution-mapper__intro-block">
					<a href = "/solutions-and-services/commercial-financing-solution-services/">
						<img src = "/assets/fuji-xerox/images/solutions/box-5.png" alt = ""/>
						<h2>Commercial Financing Solution Services</h2>
						<p>Full suite of financial solutions bring convenience and financial support to our corporate customers.</p>
					</a>
				</div>

				<div class = "col solution-mapper__intro-block">
					<a href = "#">
						<img src = "/assets/fuji-xerox/images/solutions/box-6.png" alt = ""/>
						<h2>Industries</h2>
						<p>Solutions that are tailor-made to meet individual industry's unique needs and requirements.</p>
					</a>
				</div>
			</div>
		</div>
		<!-- End: Soution Mapper Introduction -->

		<!-- Solution Mapper Results -->
		<div class = "solution-mapper__results">
			<!-- <div class = "solution-mapper__results--group row-col-15">
				<div class = "col-sm-5 push-right-20">
					<div class = "solution-mapper__intro-block">
						<a href = "#">
							<img src = "/assets/fuji-xerox/images/solutions/box-5.png" alt = ""/>
							<h2>Commercial Financing Solution Services</h2>
						</a>
					</div>
				</div>

				<div class = "section related-pages col-sm-10">
					<div class = "blocks">
						<div class = "related-pages__block">
							<a href = "#">
								<h4>Customer Communication Management (CCM) Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Color Management Solutions &amp; Services</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Customer Communication Management (CCM) Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Color Management Solutions &amp; Services</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Customer Communication Management (CCM) Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Color Management Solutions &amp; Services</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>
					</div>
				</div>
			</div> -->
			<div></div>
		</div>
		<!-- End: Solution Mapper Results -->
	</section>
	<!-- End: Solution Mapper -->

	<script id="results-template" type="text/x-handlebars-template">
		{{#each categories}}
		<div class = "solution-mapper__results--group row-col-12">
			<div class = "col-sm-3 col-md-4 push-right-20">
				<div class = "solution-mapper__intro-block">
					<a href = "{{url}}">
						<img src = "{{thumbnail}}" alt = ""/>
						<h2>{{title}}</h2>
						<p>{{description}}</p>
					</a>
				</div>
			</div>

			<div class = "section related-pages col-sm-9 col-md-8">
				<div class = "blocks">
					{{#each results}}
					<div class = "related-pages__block">
						<a href = "{{url}}">
							<h4>{{title}}</h4>
							<span class = "category">
								{{../title}}
								{{#if new }}
									<span class = "label">New</span>
								{{/if}}
							</span>
						</a>
					</div>
					{{/each}}
				</div>
			</div>
		</div>
		{{/each}}
	</script>

	<!-- Featured Linkboxs -->
	<section class = "section featured-linkboxs wingspan">
		<div class = "section-content">
			<h2 class = "section-header">Take the next step</h2>
			<div>
				<div class = "col match-height">
					<a href = "https://www.youtube.com/embed/qtS3rz31KII" class = "fancybox video">
						<img src = "/assets/fuji-xerox/images/icons/camcorder_wht.png" alt = ""/>
						<p>Featured Videos</p>
					</a>
				</div>

				<div class = "col match-height">
					<a href = "#enquiryForm" class = "modalbox">
						<img src = "/assets/fuji-xerox/images/icons/contact.png" alt = ""/>
						<p>Enquiry</p>
					</a>
				</div>

				<div class = "col match-height">
					<a href = "#" onclick="window.open('https://secure.livechatinc.com/licence/2559781/open_chat.cgi?groups=0','newChatWindow','height=520,width=530,top=' + (screen.height / 2 - 260) + ',left=' + (screen.width / 2 - 265) + ',resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');">
						<img src = "/assets/fuji-xerox/images/icons/chat_white.png" alt = ""/>
						<p>Live Chat</p>
					</a>
				</div>
			</div>
		</div>
	</section>
	<!-- End: Featured Linkboxs -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>