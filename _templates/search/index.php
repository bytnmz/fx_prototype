<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Blank Template';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><span>Search Results</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<div class = "listing search-results wingspan">
		<h1>Search</h1>

		<div class = "listing-content">
			<div class="listing-content__result-status">
				<p class="msg success-msg" style="display: block;">Total: <span class="total-results">16</span> results found.</p>
			</div>

			<ul class = "listing-content__search-results">
				<li>
					<h2><a href = "#">Fuji Xerox Singapore Launches Four Office Multifunction Devices with Enhanced Mobile Flexibility and Connectivity</a></h2>
					<span class = "url">https://www.fujixerox.com.sg/news/2015-oct-06</span>
					<p class = "excerpt">When equipped with an optional Wireless LAN Converter, the models can also be directly connected with mobile devices with Wi-Fi Direct® for convenient printing. Working seamlessly with Fuji Xerox’s cloud-based ...</p>
				</li>

				<li>
					<h2><a href = "#">VersantTM 2100 Press is Awarded the Good Design Award 2014</a></h2>
					<span class = "url">https://www.fujixerox.com.sg/news/2014-oct-01</span>
					<p class = "excerpt">With the Installed Environment TOKYO, October 1, 2014 – Versant TM 2100 Press, the color on-demand publishing system developed by Fuji Xerox Co., Ltd., has been awarded the Good Design Award 2014 by ...</p>
				</li>

				<li>
					<h2><a href = "#">Fuji Xerox Ranked Highest in J.D. Power Asia Pacific Color Copier And Color Printer Customer Satisfaction Studies for Five Consecutive Years</a></h2>
					<span class = "url">https://www.fujixerox.com.sg/news/2014-aug-14</span>
					<p class = "excerpt">2014 August 14 Fuji Xerox Ranked Highest in J.D. Power Asia Pacific Color Copier And Color Printer Customer Satisfaction Studies for Five Consecutive Years Fuji Xerox Ranked Highest ...</p>
				</li>

				<li>
					<h2><a href = "#">Versant 2100 Press</a></h2>
					<span class = "url">https://www.fujixerox.com.sg/products/production/production-printing/colour-digital-production-presses/versant-80-press</span>
					<p class = "excerpt">the Versant™ 2100 Press, you can respond with more confidence. VersantTM 2100 Press Productivity Print in full color at speeds up to 100 ppm (pages per minute) 350 gsm auto duplex ...</p>
				</li>

				<li>
					<h2><a href = "#">Device Log Service</a></h2>
					<span class = "url">https://www.fujixerox.com.sg/products/software/office-software/device-log-service</span>
					<p class = "excerpt">consolidates all device usage data for statistical analysis. By setting unit prices for Color and B&W, you can now view all departments and users printing costs. * The unit price is common to all ...</p>
				</li>

				<li>
					<h2><a href = "#">Fuji Xerox Develops Technology That Facilitates Image Editing Based on Human Visual Perception</a></h2>
					<span class = "url">https://www.fujixerox.com.sg/news/2014-jul-02</span>
					<p class = "excerpt">of an image, such as colors and shapes, based on human visual perception. Through this technology, the impression that an image gives can be changed by enhancing visibility and shapes. To develop ...</p>
				</li>
			</ul>

			<div class = "pagination">
				<ul>
					<li class = "pagination-prev"><a title="Prev" href="javascript:void(0)"><span class = "pagenav">Prev</span></a></li>
					<li><a href = "javascript:void(0)"><span class = "pagenav">4</span></a></li>
					<li><a href = "javascript:void(0)"><span class = "pagenav">5</span></a></li>
					<li><a href = "javascript:void(0)"><span class = "pagenav">6</span></a></li>
					<li><a href = "javascript:void(0)"><span class = "pagenav">7</span></a></li>
					<li class = "active"><span class = "pagenav">8</span></li>
					<li><a href = "javascript:void(0)"><span class = "pagenav">9</span></a></li>
					<li><a href = "javascript:void(0)"><span class = "pagenav">10</span></a></li>
					<li><a href = "javascript:void(0)"><span class = "pagenav">11</span></a></li>
					<li><a href = "javascript:void(0)"><span class = "pagenav">12</span></a></li>
					<li class="pagination-next"><a title="Next" href="javascript:void(0)"><span class = "pagenav">Next</span></a></li>
				</ul>
			</div>

		</div>

	</div>


	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>