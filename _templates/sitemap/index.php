<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Sitemap';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><span>Sitemap</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<div class = "article sitemap wingspan">
		<h1>Sitemap</h1>

		<div class = "sitemap__mega-menu">
			<div class = "sitemap__mega-menu-section">
				<h2><a href="#">Solutions &amp; Services</a></h2>

				<ul class = "sitemap__mega-menu-links row-col-12">
					<li class = "col-sm-3">
						<a href="#">Office Solutions</a>

						<div class = "row-col-12">

							<ul class = "sitemap__mega-menu-links--lvl3">
								<li><a href = "#">Print Management Solutions</a></li>
								<li><a href = "#">Document Security Solutions</a></li>
								<li><a href = "#">Office Automation Solutions</a></li>
								<li><a href = "#">Content Management Solutions</a></li>
								<li><a href = "#">Cloud &amp; Mobility Solutions</a></li>
								<li><a href = "#">Device Remote Support Services</a></li>
							</ul>
						</div>
					</li>

					<li class = "col-sm-3">
						<a href="#">Production Solutions</a>

						<div class = "row-col-12">

							<ul class = "sitemap__mega-menu-links--lvl3">
								<li><a href = "#">Customer Communication Management (CCM) Solutions</a></li>
								<li><a href = "#">Print Workflow Automation </a></li>
								<li><a href = "#">Color Management Solutions &amp; Services</a></li>
								<li><a href = "#">Print Management Information Systems (MIS)</a></li>
								<li><a href = "#">Packaging Solutions</a></li>
								<li><a href = "#">Business Development Consultancy Services</a></li>
							</ul>
						</div>
					</li>

					<li class = "col-sm-3">
						<a href="#">Enterprise Solutions</a>

						<div class = "row-col-12">

							<ul class = "sitemap__mega-menu-links--lvl3">
								<li><a href = "#">Business Process Management (BPM)</a></li>
								<li><a href = "#">Enterprise Content Management (ECM)</a></li>
								<li><a href = "#">Knowledge Management Solutions</a></li>
								<li><a href = "#">Enterprise Collaboration Solutions</a></li>
								<li><a href = "#">Infrastructure Solutions</a></li>
								<li><a href = "#">System Integration Services</a></li>
								<li><a href = "#">IT Services</a></li>
							</ul>
						</div>
					</li>

					<li class = "col-sm-3">
						<a href="#">Global Services</a>

						<div class = "row-col-12">

							<ul class = "sitemap__mega-menu-links--lvl3">
								<li><a href = "#">Managed Document/Print Services</a></li>
								<li><a href = "#">Marketing Communications Services</a></li>
								<li><a href = "#">Imaging &amp; Document Management Services</a></li>
								<li><a href = "#">Short-term Rental</a></li>
							</ul>
						</div>
					</li>
				</ul>

				<ul class = "sitemap__mega-menu-links row-col-12">
					<li class = "col-sm-3">
						<a href="#">Commercial Financing Solution Services</a>
					</li>


					<li class = "col-sm-3">
						<a href="#">Industries</a>

						<div class = "row-col-12">

							<ul class = "sitemap__mega-menu-links--lvl3">
								<li><a href = "#">Managed Document/Print Services</a></li>
								<li><a href = "#">Marketing Communications Services</a></li>
								<li><a href = "#">Imaging &amp; Document Management Services</a></li>
								<li><a href = "#">Short-term Rental</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>

			<div class = "sitemap__mega-menu-section">
				<h2><a href="#">Products</a></h2>

				<ul class = "sitemap__mega-menu-links">
					<li>
						<a href="#">Printers</a>

						<div class = "row-col-12">

							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">DocuCentre - V C7785/C6685/C5585</a></li>
								<li><a href = "#">ApeosPort - V C7785/C6685/C5585 </a></li>
								<li><a href = "#">ApeosPort - V C7776/C6676/C5576/C4476/C3376/C3374/C2276 </a></li>
								<li><a href = "#">DocuCentre - V C7776/C6676/C5576/C4476/C3376/C3374/C2276 </a></li>
								<li><a href = "#">DocuCentre - V C2265/C2263 </a></li>
								<li><a href = "#">ApeosPort - V C3320</a></li>
								<li><a href = "#">ApeosPort - V C7775/C6675/C5575/C4475/C3375/C3373/C2275</a></li>
								<li><a href = "#">DocuCentre - V C7775/C6675/C5575/C4475/C3375/C3373/C2275</a></li>
								<li><a href = "#">ApeosPort - V C7780/6680/5580</a></li>
							</ul>

							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">DocuCentre - V C7780/C6680/C5580</a></li>
								<li><a href = "#">DocuCentre - IV C2265 N /C2263 N</a></li>
								<li><a href = "#">ApeosPort - IV C4430</a></li>
								<li><a href = "#">DocuWide C842</a></li>
								<li><a href = "#">DocuWide 6035/6055</a></li>
								<li><a href = "#">DocuWide 3035MF</a></li>
								<li><a href = "#">DocuWide 2055/2055MF/2055EC</a></li>
								<li><a href = "#">Phaser 6700</a></li>
								<li><a href = "#">DocuPrint CP305 d</a></li>
							</ul>

							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">DocuPrint C3055DX</a></li>
								<li><a href = "#">DocuPrint CM305 df</a></li>
								<li><a href = "#">Phaser 4600</a></li>
								<li><a href = "#">Phaser 5550</a></li>
								<li><a href = "#">DocuPrint M355 df</a></li>
								<li><a href = "#">DocuPrint 3105</a></li>
								<li><a href = "#">DocuPrint P355 d</a></li>
								<li><a href = "#">Color 1000i Press</a></li>
								<li><a href = "#">Color C60/C70 Printer</a></li>
							</ul>

							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">Versant<sup>TM</sup> 80 Press</a></li>
								<li><a href = "#">Versant<sup>TM</sup> 2100 Press</a></li>
								<li><a href = "#">Color J75 Press</a></li>
								<li><a href = "#">DocuColor 1450 GA</a></li>
								<li><a href = "#">D136 Copier/Printer</a></li>
								<li><a href = "#">Xerox Nuvera® 200/288/314 EA</a></li>
								<li><a href = "#">Xerox Nuvera® 120/144/157 EA</a></li>
								<li><a href = "#">D95/D110/D125 Copier/Printer; D110/D125 Printer</a></li>
								<li><a href = "#">Xerox 495 Continuous Feed Duplex Printer</a></li>
							</ul>
						</div>
					</li>

					<li>
						<a href="#">Supplies</a>

						<div class = "row-col-15">
							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">Paper 1</a></li>
								<li><a href = "#">Paper 2</a></li>
								<li><a href = "#">Paper 3</a></li>
								<li><a href = "#">Paper 4</a></li>
							</ul>

							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">Paper 5</a></li>
								<li><a href = "#">Paper 6</a></li>
								<li><a href = "#">Paper 7</a></li>
								<li><a href = "#">Binder 1</a></li>
							</ul>

							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">Binder 2</a></li>
								<li><a href = "#">Binder 3</a></li>
								<li><a href = "#">Binder 4</a></li>
								<li><a href = "#">Laminator 1</a></li>
							</ul>

							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">Laminator 2</a></li>
								<li><a href = "#">Shredder 1</a></li>
								<li><a href = "#">Shredder 2</a></li>
								<li><a href = "#">Label</a></li>
							</ul>

							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">Xerox Mobility Chart</a></li>
								<li><a href = "#">Coffee</a></li>
							</ul>
						</div>
					</li>

					<li>
						<a href="#">Software</a>

						<div class = "row-col-15">
							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">Office 1</a></li>
								<li><a href = "#">Office 2</a></li>
								<li><a href = "#">Office 3</a></li>
							</ul>

							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">Office 4</a></li>
								<li><a href = "#">Office 5</a></li>
								<li><a href = "#">Office 6</a></li>
							</ul>

							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">Office 7</a></li>
								<li><a href = "#">Office 8</a></li>
								<li><a href = "#">Office 9</a></li>
							</ul>

							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">Office 10</a></li>
								<li><a href = "#">Office 11</a></li>
								<li><a href = "#">Production 1</a></li>
							</ul>

							<ul class = "sitemap__mega-menu-links--lvl3 col-sm-3">
								<li><a href = "#">Production 2</a></li>
								<li><a href = "#">Production 3</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>

			<div class = "sitemap__mega-menu-section">
				<h2><a href="#">Support &amp; Driver</a></h2>
			</div>

			<div class = "sitemap__mega-menu-section">
				<h2><a href="#">Company</a></h2>

				<ul class = "sitemap__mega-menu-links">
					<li>
						<ul>
							<li><a href = "#">About Fuji Xerox Singapore</a></li>
							<li><a href = "#">Management Team</a></li>
							<li><a href = "#">Awards &amp; Achievements</a></li>
							<li><a href = "#">Towards a Sustainable Future</a></li>
							<li><a href = "#">Fuji Xerox Dream Service</a></li>
							<li><a href = "#">Success Stories</a></li>
							<li><a href = "#">Newsroom</a></li>
							<li><a href = "#">Career with Us</a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class = "sitemap__mega-menu-section">
				<h2><a href="#">Contact</a></h2>
				<ul class = "sitemap__mega-menu-links">
					<li>
						<ul>
							<li><a href = "#">Request Pricing</a></li>
							<li><a href = "#">Customer Feedback</a></li>
							<li><a href = "#">Technical Support</a></li>
							<li><a href = "#">Submit Meter Readings</a></li>
							<li><a href = "#">Toner Ordering</a></li>
							<li><a href = "#">Customer Software Training</a></li>
							<li><a href = "#">Customer Referral Program</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>


	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>