<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Office Automation Solutions';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/solutions-and-services">Solutions &amp; Services</a></li>
			<li><a href = "../">Office Solutions</a></li>
			<li><span>Office Automation Solutions</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4 col-md-3">
				<h1>Office Supplies</h1>
				<p>Presentation Media papers, pre-printing requests or Corporate gift sourcing services</p>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 col-md-8 offset-sm-1 offset-md-2">
				<section class = "section">
					<a  id = "content_1" class = "anchor-link" data-label = "About"></a>
					<h2>We can customize to your business needs</h2>

					<p>Reach us for your request at +65 6766 8888, press 2. Or call your Account Manager for further assistance. We will be glad to assist you.</p>
					<p>It's a small way of saying thanks!</p>
					<p>It's on Fuji Xerox, enjoy $10 vouchers when you send us a quotation requesting for Corporate gift sourcing service, presentation media paper request or pre-printing requirements!</p>
					<p>Submit your request below for a follow up call from your Sales Manager</p>

					<h4>About This Special Offer</h4>

					<p><img src = "/assets/fuji-xerox/images/dummy/office-supplies/threesteps.png" alt = "" class = "pull-left" width = "300"/></p>
					<p>Sourcing for the right office supplies can be challenging. Save the hassle and speak to us for any company events that require customization of gifts. We will handle the rest.</p>
					<p>We believe that quality media presentation papers will add perks to your business proposals and presentations. Creating a competitive edge for your business can be easy – start by choosing the right paper.</p>
					<p>Occasionally, your business might require you to print letter heads and statements. Reach out to us and we will extend our affordable services to you.</p>
					<p>Your one-stop office supplies provider. Call us at 67668888 press 2 to find out more.</p>

					<hr/>
					<a  id = "content_2" class = "anchor-link" data-label = "Fuji Xerox Singapore"></a>
					<h2>About Fuji Xerox Singapore</h2>

					<p>Established in 1965, Fuji Xerox Singapore is the country's leading provider of new class document solutions.</p>
					<p>We offer an unparalleled portfolio of document technologies, services, software, supplies and document-centric outsourcing.</p>
					<p>At Fuji Xerox, we understand the importance of helping our customers and partners maintain business uptime. Service excellence is one of the cornerstones that define Fuji Xerox Singapore’s success.</p>

					<h4>Awards &amp; Achievements</h4>
					<p>Fuji Xerox Singapore has received numerous recognitions for excellence and quality.</p>
					<table style = "width: 100%;">
						<tbody>
							<tr>
								<td style = "text-align: center; background: #fff; padding: 0; "><img src = "/assets/fuji-xerox/images/dummy/office-supplies/01.png" alt = "" width = "74"/></td>
								<td style = "text-align: center; background: #fff; padding: 0; "><img src = "/assets/fuji-xerox/images/dummy/office-supplies/02.jpg" alt = "" width = "74"/></td>
								<td style = "text-align: center; background: #fff; padding: 0; "><img src = "/assets/fuji-xerox/images/dummy/office-supplies/03.png" alt = "" width = "74"/></td>
								<td style = "text-align: center; background: #fff; padding: 0; "><img src = "/assets/fuji-xerox/images/dummy/office-supplies/04.png" alt = "" width = "74"/></td>
								<td style = "text-align: center; background: #fff; padding: 0; "><img src = "/assets/fuji-xerox/images/dummy/office-supplies/05.png" alt = "" width = "74"/></td>
								<td style = "text-align: center; background: #fff; padding: 0; "><img src = "/assets/fuji-xerox/images/dummy/office-supplies/06.png" alt = "" width = "74"/></td>
							</tr>
						</tbody>
					</table>
					<hr/>
				</section>

				<section>
					<a  id = "content_2" class = "anchor-link" data-label = "Send Request"></a>
					<div class = "sitecore-form">

							<div class="scfForm" id="form_6A9985C58EFE43209A66B4A98BDE5EA0">
								<input type="hidden" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$formreference" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_formreference" value="form_6A9985C58EFE43209A66B4A98BDE5EA0">
								<h2 id="form_6A9985C58EFE43209A66B4A98BDE5EA0_title" class="scfTitleBorder">Send us your request today</h2>
								<p>By sending us your detailed request, please be assured our Sales Manager will follow back promptly within 3 working days.</p>
								<p>What's could have been better, receive $10 NTUC vouchers when it is a qualified request.</p>
								<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0__summary" class="scfValidationSummary" style="display:none;"></div>
								<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_form_6A9985C58EFE43209A66B4A98BDE5EA0_submitSummary" class="scfSubmitSummary" style="color:Red;"></div>
								<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_fieldContainer" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'form_6A9985C58EFE43209A66B4A98BDE5EA0_form_6A9985C58EFE43209A66B4A98BDE5EA0_submit')">
									<div>
										<fieldset class="scfSectionBorderAsFieldSet">
											<div class="scfSectionContent">
												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_scope" class="scfSingleLineTextBorder fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d name.Single+Line">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_text" class="scfSingleLineTextLabel">First Name</label>
													<div class="scfSingleLineGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_ADB11D7524A8411C8089A8DD897A031A" type="text" maxlength="256" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" class="scfSingleLineTextBox">
														<span class="scfSingleLineTextUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">Single Line must have at least 0 and no more than 256 characters.</span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">The value of the Single Line field is not valid.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_scope" class="scfSingleLineTextBorder fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d name.Single+Line">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_text" class="scfSingleLineTextLabel">Last Name</label>
													<div class="scfSingleLineGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_ADB11D7524A8411C8089A8DD897A031A" type="text" maxlength="256" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" class="scfSingleLineTextBox">
														<span class="scfSingleLineTextUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">Single Line must have at least 0 and no more than 256 characters.</span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">The value of the Single Line field is not valid.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_scope" class="scfSingleLineTextBorder fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d name.Single+Line">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_text" class="scfSingleLineTextLabel">Company Name</label>
													<div class="scfSingleLineGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_ADB11D7524A8411C8089A8DD897A031A" type="text" maxlength="256" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" class="scfSingleLineTextBox">
														<span class="scfSingleLineTextUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">Single Line must have at least 0 and no more than 256 characters.</span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">The value of the Single Line field is not valid.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_scope" class="scfSingleLineTextBorder fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d name.Single+Line">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_text" class="scfSingleLineTextLabel">Job Title</label>
													<div class="scfSingleLineGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_ADB11D7524A8411C8089A8DD897A031A" type="text" maxlength="256" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" class="scfSingleLineTextBox">
														<span class="scfSingleLineTextUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">Single Line must have at least 0 and no more than 256 characters.</span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">The value of the Single Line field is not valid.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>


												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A_scope" class="scfEmailBorder fieldid.%7bCAE5C925-52B5-4F53-86C6-12395C37DC5A%7d name.Email">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A_text" class="scfEmailLabel">Business Email</label>
													<div class="scfEmailGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_CAE5C92552B54F5386C612395C37DC5A" type="text" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A" class="scfEmailTextBox">
														<span class="scfEmailUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A5D10AF7533054C39908EB25E8CB4ABDC_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bCAE5C925-52B5-4F53-86C6-12395C37DC5A%7d inner.1" style="display:none;">Enter a valid e-mail address.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4_scope" class="scfTelephoneBorder fieldid.%7b7C714B5E-B312-4C7E-B843-559D45928CE4%7d name.Telephone">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4_text" class="scfTelephoneLabel">Business Phone</label>
													<div class="scfTelephoneGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_7C714B5EB3124C7EB843559D45928CE4" type="text" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4" class="scfTelephoneTextBox"><span class="scfTelephoneUsefulInfo" style="display:none;"></span><span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4C3CC8A327EF14D138163826E11482E4D_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b7C714B5E-B312-4C7E-B843-559D45928CE4%7d inner.1" style="display:none;">Enter a valid telephone number.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>


												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E_scope" class="scfMultipleLineTextBorder fieldid.%7bD1C52150-7AA9-4035-AB45-8A744E0CE06E%7d name.Multi+Line">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E_text" class="scfMultipleLineTextLabel">What are your requirements?</label>
													<div class="scfMultipleLineGeneralPanel">
														<textarea name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_D1C521507AA94035AB458A744E0CE06E" rows="4" cols="20" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E" class="scfMultipleLineTextBox"></textarea>
														<span class="scfMultipleLineTextUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bD1C52150-7AA9-4035-AB45-8A744E0CE06E%7d inner.1" style="display:none;">Multi Line must have at least 0 and no more than 512 characters.</span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bD1C52150-7AA9-4035-AB45-8A744E0CE06E%7d inner.1" style="display:none;">The value of the Multi Line field is not valid.</span>
													</div>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1" class="scfCheckBoxListBorder fieldid.%7bDC7BC868-43CD-40E0-A491-51F63DC557F1%7d name.Check+box+list">
													<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1text" class="scfCheckBoxListLabel"></span><div class="scfCheckBoxListGeneralPanel">
														<table id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list" class="scfCheckBoxList">
															<tbody><tr>
																<td><input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list_0" type="checkbox" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_DC7BC86843CD40E0A49151F63DC557F1list$0" value="first"><label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list_0">Corporate Gift Sourcing Services</label></td>
															</tr><tr>
																<td><input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list_1" type="checkbox" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_DC7BC86843CD40E0A49151F63DC557F1list$1" value="second"><label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list_1">Specialty Media Paper(e.g. for presentation/brochures)</label></td>
															</tr><tr>
																<td><input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list_2" type="checkbox" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_DC7BC86843CD40E0A49151F63DC557F1list$2" value="third"><label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list_2">Pre-Printing Services (e.g. company letterheads, credit statements)</label></td>
															</tr>
														</tbody></table><span class="scfCheckBoxListUsefulInfo" style="display:none;"></span>
													</div>
												</div>
												<div class="scfCaptcha fieldid.%7b55A59538-2B41-493C-9A97-3407AA4CB8F4%7d name.captcha validationgroup.form_6A9985C58EFE43209A66B4A98BDE5EA0_submit" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border">
													<div class="scfCaptchTop">
														<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCodeBorder" class="scfCaptchaBorder">
															<span class="scfCaptchaLabel">&nbsp;</span>
															<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_Panel1" class="scfCaptchaGeneralPanel">
																<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCodeText" class="scfCaptchaLabel" style="display:none;"> </span>
																<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCodePanel" class="scfCaptchaLimitGeneralPanel">
																	<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode">
																		<table cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
																			<tbody>
																				<tr>
																					<td>
																						<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode_captchaCodeim">
																							<input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode_ctl01_uniqID" type="hidden" name="uniqID" value="91c10441-0f66-4f4b-888f-a63fc3692c8f">
																							<div style="background-color:White;">
																								<img src="/assets/fuji-xerox/images/dummy/captcha/captcha.jpg" width="180" height="50" alt="Captcha"/>
																							</div>
																						</div>
																					</td>
																					<td>
																						<input type="image" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$captchaCode$ctl02" title="Display another text." src="/assets/fuji-xerox/images/dummy/captcha/refresh.png" alt="Display another text." onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$captchaCode$ctl02&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))"><br>
																						<input type="image" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$captchaCode$captchaCodepb" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode_captchaCodepb" title="Play audio version of text." src="/assets/fuji-xerox/images/dummy/captcha/loudspeaker.png" alt="Play audio version of text." onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$captchaCode$captchaCodepb&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" href="CaptchaAudio.axd?guid=91c10441-0f66-4f4b-888f-a63fc3692c8f">
																					</td>
																				</tr>
																			</tbody>
																		</table>
																		<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode_captchaCodeph"></div>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div>
														<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaTextBorder" class="scfCaptchaBorder">
															<span class="scfCaptchaLabel">&nbsp;</span>
															<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaTextPanel" class="scfCaptchaGeneralPanel">
																<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_field_55A595382B41493C9A973407AA4CB8F4" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaTextTitle" class="scfCaptchaLabelText">Captcha</label>
																<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchLimitTextPanel" class="scfCaptchaLimitGeneralPanel">
																	<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchStrongTextPanel" class="scfCaptchStrongTextPanel">
																		<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$field_55A595382B41493C9A973407AA4CB8F4" type="text" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_field_55A595382B41493C9A973407AA4CB8F4" class="scfCaptchaTextBox scWfmPassword">
																		<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaTextHelp" class="scfCaptchaUsefulInfo" style="display:none"></span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</fieldset>
									</div>
								</div>
								<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_footer" class="scfFooterBorder">
								</div>
								<div class="scfSubmitButtonBorder">
									<input type="submit" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$form_6A9985C58EFE43209A66B4A98BDE5EA0_submit" value="Submit" onclick="$scw.webform.lastSubmit = this.id;WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;form_6A9985C58EFE43209A66B4A98BDE5EA0$form_6A9985C58EFE43209A66B4A98BDE5EA0_submit&quot;, &quot;&quot;, true, &quot;form_6A9985C58EFE43209A66B4A98BDE5EA0_submit&quot;, &quot;&quot;, false, false));$scw.webform.validators.setFocusToFirstNotValid('form_6A9985C58EFE43209A66B4A98BDE5EA0_submit')" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_form_6A9985C58EFE43209A66B4A98BDE5EA0_submit" class="scfSubmitButton">
								</div>


							</div>

						</div>


				</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">

			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<!-- Subscription Call -->
	<div class = "subscribe" id = "subscriptionCall">
		<div class = "row-col-12">
			<div class = "col-sm-6 col-md-6 col match-height">
				<h3>You may download the PDF here</h3>
				<a href="#" target = "_blank" class = "btn-download">Colour Management Brochure<i class="icon-download"></i></a>
			</div>
			<div class = "col-sm-6 col-md-6 col match-height">
				<h3>Subscribe to our newsletter</h3>
				<!-- <p>Keep yourself update to Fuji Xerox latest product!</p> -->
				<div class = "form-row row-col-12">
					<div class = "col-sm-6">
						<div class = "input-group-text">
							<label for = "subscribeName">Name<span class = "required">*</span></label>
							<input type = "text" id = "subscribeName" placeholder = "Name">
						</div>
						<div class = "input-group-text">
							<label for = "subscribeEmail">Email<span class = "required">*</span></label>
							<input type = "email" id = "subscribeEmail" placeholder = "abc@example.com">
						</div>
					</div>
				</div>
				<div class = "btn-group">
					<button type = "submit">Submit<i class = "icon-chevron-with-circle-right"></i></button>
				</div>
			</div>
		</div>
	</div>
	<!-- End: Subscription Call -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>