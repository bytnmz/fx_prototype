<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Awards & Achievements';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/company">Company</a></li>
			<li><span>Awards &amp; Achievements</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4">
				<h1>Awards &amp; Achievements</h1>
				<p>Driving Excellence in Everything We Do.</p>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 offset-sm-1 col-md-8 offset-md-2">

				<table class = "stackable">
					<thead>
						<tr>
							<th>Awards</th>
							<th>Logo</th>
							<th>Description</td>
							<th>Country</th>
							<th>Date</th>
						</tr>
					<thead>

					<tbody>
						<tr>
							<td>Singapore Health Award</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/Singapore-Health-Award-Logo-Horiz-CMYK.jpg" alt = ""/></td>
							<td>The Singapore HEALTH Award (Helping Employees Achieve Life-Time Health) logo recognizes Fuji Xerox Singapore as one of the workplaces in Singapore with a commendable workplace health promotion programme.  It also indicates the Health Promotion Board’s (HPB) acknowledgement of Fuji Xerox Singapore's commitment to promoting health at work.</td>
							<td>Singapore</td>
							<td>April 2015</td>
						</tr>

						<tr>
							<td>No. 1 in Color and Mono Laser Copiers</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/idc.png" alt = ""/></td>
							<td>With a 43% market share from January to March 2014, Fuji Xerox Singapore has been the A3 Color Market leader for 25 consecutive quarters since 2008. They also led the Mono Market in the same quarter with a 29.8% market share. </td>
							<td>Singapore</td>
							<td>April 2014</td>
						</tr>

						<tr>
							<td>No. 1 in Color Laser Copiers</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/idc.png" alt = ""/></td>
							<td>With a 47.7% market share from October to December 2013, Fuji Xerox Singapore has been the A3 Color Market leader for 24 consecutive quarters since 2008.</td>
							<td>Singapore</td>
							<td>January 2014</td>
						</tr>

						<tr>
							<td>Human Resources Excellence Awards 2013 – Gold Award for “Excellence in CSR Practices”</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/HR Excellence Awards.jpg" alt = ""/></td>
							<td>Fuji Xerox Singapore has been awarded with the Gold Award for “Excellence in CSR Practices” for demonstrating how CSR programmes have been effectively incorporated into the overall HR strategy and have made a difference to its employees as well as impacted business objectives. Nominees were judged by an esteemed panel of HR experts and leading industry professionals on criteria such as the systems in place, social initiatives implemented within the past year, as well as results from employee participation and impact.</td>
							<td>Singapore</td>
							<td>May 2013</td>
						</tr>

						<tr>
							<td>No. 1 in Color and Mono Laser Copiers</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/idc.png" alt = ""/></td>
							<td>With a 47.2% market share from January to March 2013, Fuji Xerox Singapore has been the A3 Color Market leader for 21 consecutive quarters since 2008. They also led the Mono Market for the year 2012 with a 31.8% market share.</td>
							<td>Singapore</td>
							<td>April 2013</td>
						</tr>

						<tr>
							<td>HRM 2013 Capita Award for “Best Engagement Strategies”</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/hrm.png" alt = ""/></td>
							<td>Fuji Xerox Singapore beat eight other strong contenders within the category to garner the award for the “Best Engagement Strategies”. Nominees were evaluated by an esteemed panel of HR experts and leading industry professionals on criteria such as variety of programmes, innovative strategies, retention rates, alignment of engagement with career development strategies and ROI (Return on Investment).</td>
							<td>Singapore</td>
							<td>March 2013</td>
						</tr>

						<tr>
							<td>Work-Life Achiever Award</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/worklifeaward.jpg" alt = ""/></td>
							<td>
								<p>Fuji Xerox Singapore is awarded the Work-Life Achiever Award, recognising the organisation’s firm commitment and efforts in promoting its “Dream Team Core Values” within the company.</p>

								<p>A biennial award that pays tribute to employers for their successful implementation of Work-Life strategies in Singapore workplaces, the Work-Life Achiever Award is organised by the Ministry of Manpower’s Tripartite Committee on Work-Life Strategy.</p></td>
							<td>Singapore</td>
							<td>October 2012</td>
						</tr>

						<tr>
							<td>No. 1 in Color and Mono Laser Copiers</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/idc.png" alt = ""/></td>
							<td>With a 39.1% market share from January to March 2012, Fuji Xerox Singapore has been the A3 Colour Market leader for 17 consecutive quarters since 2008. They also lead the Mono Market in the same quarter with 27.2% market share.</td>
							<td>Singapore</td>
							<td>August 2012</td>
						</tr>

						<tr>
							<td>1st in Southeast Asia to achieve 3 Concurrent Fogra Certifications</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/fogra-cert.png" alt = ""/></td>
							<td> 
								<p>Fogra Graphic Technology Research Association, is an Independent German research organisation, established since 1951. Fogra is one of the members of TC130 (the technical committee that was instrumental in the development of ISO 12647 standard) and they have engineered processes to enable the achievement of the ISO 12647 standard in a structured and scientific manner. </p>

								<p>In 2011, seven of Fuji Xerox Singapore’s staff from the Production Services Business Group and Service Excellence Technical Operations were certified Fogra Digital Print Expert (DPE). Beside the DPEs, a range of Fuji Xerox’s production printing presses were also awarded the FograCert Validation Printing System (VPS) certification which recognizes printers that demonstrate commitment to ISO standards, based on measurable criteria in a production environment. In Jun 2012, one of the DPE from the Production Services Business Group was awarded Fogra Process Standard Digital (PSD). He is also the first to obtain this certification in Singapore.</p></td>
							<td>Singapore</td>
							<td>August 2012</td>
						</tr>

						<tr>
							<td>No. 8 in Superbrand Awards</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/superbrands-logo.jpg" alt = ""/></td>
							<td>Fuji Xerox has been ranked as one of the top ten most well-known and respected business brands by an inaugural survey of leading business brands commissioned by international organisation Superbrands. Conducted by business and market research consultancy BDRC Asia in April 2012, the survey singled out over 200 of Singapore’s business brands across 23 different product and service categories before ranking them.</td>
							<td>Singapore</td>
							<td>April 2012</td>
						</tr>

						<tr>
							<td>OHSAS 18001</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/ohsas-18001-cert.png" alt = ""/></td>
							<td>The OHSAS18001 certificate is awarded by the Singapore Accreditation Council (SAS) which is managed by Spring Singapore, a statutory board under the Ministry of Trade and Industry. It recognizes that Fuji Xerox have achieved a system of international standards in providing reliable products and services. Awardees are thoroughly assessed and periodically monitored before being granted the certificate.</td>
							<td>Singapore</td>
							<td>May 2012</td>
						</tr>

						<tr>
							<td>bizSAFE</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/biz-safe-award.png" alt = ""/></td>
							<td>The BizSafe award recognizes Fuji Xerox Singapore for attaining excellence in Workplace Safety & Health (WSH) Management System. bizSAFE is a five-step programme to promote workplace safety and health (WSH) in enterprises through the recognition of their safety efforts. Awardees will need to achieve all 5 levels in the programme, starting from the commitment of the enterprise’s top management and to progressively enhance their WSH performance through the development of the WSH Management system.</td>
							<td>Singapore</td>
							<td>April 2012</td>
						</tr>

						<tr>
							<td>HRM 2012 “Best Environmental Practices” Award</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/hrm.png" alt = ""/></td>
							<td>The “Best Environmental Practices” award is presented to Fuji Xerox which has demonstrated impressive commitment to sustainability development in business practices, employee and partner engagement. Nominees were judged by an esteemed panel of HR experts and leading industry professionals on criteria such as environment policies and programmes in place, innovative green processes implemented within the past year, as well as results from employee participation and impact.</td>
							<td>Singapore</td>
							<td>February 2012</td>
						</tr>

						<tr>
							<td>No. 1 Colour Market Leader</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/idc.png" alt = ""/></td>
							<td>No. 1 leading the way in Colour for 13 consecutive quarters since 2008.</td>
							<td>Singapore</td>
							<td>August 2011</td>
						</tr>

						<tr>
							<td>Asia's Best Employer Brand Awards 2011 </td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/best_employer_brand.png" alt = ""/></td>
							<td>This award recognizes the best human resource practices in the areas of talent management, development and innovation, and was presented by the employer branding institute CMO Asia, and its strategic partner CMO council. The award is a recognition of Fuji Xerox’s exemplary approach towards employee branding and celebrates its outstanding HR practices. </td>
							<td>Singapore</td>
							<td>July 2011</td>
						</tr>

						<tr>
							<td>Creative Industries Workforce Skills Qualifications ‘Best Supportive Employer’ Award</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/best_supportive_employer.png" alt = ""/></td>
							<td>This award recognizes the best human resource practices in the areas of talent management, development and innovation, and was presented by the employer branding institute CMO Asia, and its strategic partner CMO council. The award is a recognition of Fuji Xerox’s exemplary approach towards employee branding and celebrates its outstanding HR practices. </td>
							<td>Singapore</td>
							<td>July 2011</td>
						</tr>

						<tr>
							<td>MarketProbe Customer Satisfaction Survey - Fuji Xerox Ranked No. 1</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/market_probe.png" alt = ""/></td>
							<td>Fuji Xerox Singapore has been voted number one for three consecutive years in a customer satisfaction benchmark survey since 2011. Conducted by MarketProbe, a global marketing research and consultancy firm, the survey results reflect the overall positive experience and satisfaction customers have with Fuji Xerox in terms of products and services. There were a total of 606 purchasing managers interviewed, and they come from various industries such as financial institutions, manufacturing, government, building construction, retail, tourism and education.</td>
							<td>Singapore</td>
							<td>2011, 2012, 2011</td>
						</tr>

						<tr>
							<td>Service Capability &amp; Performance</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/service-capability-performance-standard.png" alt = ""/></td>
							<td>Fuji Xerox Asia Pacific Pte Ltd has been awarded the prestigious Service Capability &amp; Performance (SCP) (Support Standard/eService Standard), which quantifies the effectiveness of customer technical and eSupport based upon stringent set of performance standards. It represents the best practices in the industry and is a key milestone in the establishment of Fuji Xerox eSupport strategy.</td>
							<td>Singapore</td>
							<td>May 2011</td>
						</tr>

						<tr>
							<td>Singapore Green Label (SGL) Awards</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/green_label.png" alt = ""/></td>
							<td>Launched in May 1992 and administered by Singapore Environment Council (SEC) since 1999, the Singapore Green Labelling Scheme (SGLS) is Singapore’s leading environment standard and certification mark. SEC is the only eco-labelling in Singapore that is a member of Global Ecolabelling Network (GEN) since Oct 2003 and was certified by Global Ecolabelling Network’s Internationally Coordinated Ecolabelling System (GENICES) on 27 Oct 2011. There are almost 2000 products which are currently certified under the Singapore Green Label Scheme.</td>
							<td>Singapore</td>
							<td>Since 2006</td>
						</tr>


						<tr>
							<td>FSC papers</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/fsc-cert.png" alt = ""/></td>
							<td>FSC certification offers recognition to Fuji Xerox for adhering to environmental guidelines on managing the usage of forest based products. As a leading participant in the paper and office equipment market, Fuji Xerox plays an active role in promotion of responsible product stewardship. This certification is awarded by the Forest Stewardship Council (FSC) which is established to promote the responsible management of the world’s forests.</td>
							<td>Singapore</td>
						</tr>


						<tr>
							<td>ISO 14001</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/iso-14001-cert.png" alt = ""/></td>
							<td>Fuji Xerox has been certified to have met the ISO 14001 family of standards relating to environmental management. It s aimed to help organizations minimize their impact on the environment, and to comply with existing environmental regulations.</td>
							<td>Singapore</td>
							<td>2004</td>
						</tr>

						<tr>
							<td>ISO 9001</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/iso-9001-cert.png" alt = ""/></td>
							<td>Fuji Xerox has been certified to have met the ISO 9001 - the internationally recognized standard relating to quality management of businesses. It is designed to help organization ensure that they meet the needs of customers and other stakeholders.</td>
							<td>Singapore</td>
							<td>2008</td>
						</tr>

						<tr>
							<td>Singapore Quality Class (SQC)</td>
							<td><img src = "/assets/fuji-xerox/images/content/awards/singapore-quality-class-award.png" alt = ""/></td>
							<td>Singapore Quality Class (SQC) is a mark of excellence indicating that Fuji Xerox has excelled in the 7 key areas of the business: Leadership, Planning, Information, People, Processes, Customers and Results. It is awarded by Spring Singapore who had assessed Fuji Xerox’s performance using the internationally benchmarked businesses excellence framework and site assessments.</td>
							<td>Singapore</td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Company</a></h3>

					<ul>
						<li><a href="/company/about-fuji-xerox-singapore" >About Fuji Xerox Singapore</a></li>
						<li><a href="/company/management-team">Management Team</a></li>
						<li><a href="/company/awards-achievements" class = "active">Awards &amp; Achievements</a></li>
						<li><a href="/company/substainable-future">Towards a Sustainable Future</a></li>
						<li><a href="#">Fuji Xerox Dream Service</a></li>
						<li><a href="/company/success-stories">Success Stories</a></li>
						<li><a href="/company/newsroom">Newsroom</a></li>
						<li><a href="/company/career-with-us">Career with Us</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>