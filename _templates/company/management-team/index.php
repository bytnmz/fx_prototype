<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Management Team';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/company">Company</a></li>
			<li><span>Management Team</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4">
				<h1>Management Team</h1>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content management-team col-sm-10 offset-sm-1 col-md-8 offset-md-2" id = "team">
				<h2>The Management Team</h2>

				<!-- Management Team Toc -->
				<div class = "management-team__toc row-col-12">

					<div class = "col-sm-6">
						<ul>
							<li>
								<a href = "#team_1" class = "name">Bert Wong</a>
								<span class = "title">Chief Executive Officer,</span>
								<span class = "company">Fuji Xerox Singapore</span>
							</li>

							<li>
								<a href = "#team_2" class = "name">Raphael Foo</a><br>
								<span class = "title">General Manager, Marketing,</span>
								<span class = "company">Fuji Xerox Singapore</span>
							</li>

							<li>
								<a href = "#team_3" class = "name">Pauline Chua</a><br>
								<span class = "title">General Manager, Human Capital &amp; Corporate Social Responsibility,</span>
								<span class = "company">Fuji Xerox Singapore</span>
							</li>

							<li>
								<a href = "#team_3" class = "name">Phang Heng How</a><br>
								<span class = "title">General Manager Finance Management and Operations,</span>
								<span class = "company">Fuji Xerox Singapore</span>
							</li>
						</ul>

					</div>

					<div class = "col-sm-6 push-left-20">
						<ul>
							<li>
								<a href = "#team_4" class = "name">Richard Teo</a><br>
								<span class = "title">Chief Executive Officer,</span>
								<span class = "company">Fuji Xerox Singapore</span>
							</li>

							<li>
								<a href = "#team_5" class = "name">Sharon Kong</a><br>
								<span class = "title">General Manager / Director Industry Solutions Group,</span>
								<span class = "company">Fuji Xerox Singapore Pte Ltd</span>
							</li>

							<li>
								<a href = "#team_6" class = "name"> Tan Yew Luan</a><br>
								<span class = "title">General Manager, Global Services,</span>
								<span class = "company">Fuji Xerox Singapore</span>
							</li>
						</ul>
					</div>
				</div>
				<!-- End: Management Team TOC -->
				<hr>

				<div class = "management-team__table">
					<div class = "management-team__table-block" id = "team_1">
						<div class = "thumbnail">
							<img src = "/assets/fuji-xerox/images/content/mt/mt_bert_wong.png" alt = "Bert Wong"/>
						</div>


						<div class = "desc">
							<h3>Bert Wong</h3>
							<span class = "title">Chief Executive Officer</span>
							<span class = "company">Fuji Xerox Singapore</span>

							<p>Mr Bert Wong, CEO of Fuji Xerox Singapore and Asia Pacific Regional Officer has more than 30 years of experience in Business Process Reengineering and Change Management. Under Bert’s leadership the Fuji Xerox brand in Singapore has been transformed from being a pure play copier company to a market leader in Knowledge Management Solutions.</p>

							<p>In his role on the Asia Pacific Regional Board, Bert will be involved in setting strategic directions for the company in the Asia Pacific and China.</p>

							<p>Bert’s sound foundation and knowledge in the document and enterprise content management arena started way back in 1980 when he first began his career with Fuji Xerox Singapore in the sales department. He was assigned numerous management positions such as the Regional Marketing Director / Senior General Manager for Business Operations prior to his appointment as CEO.</p>

							<p>Bert has been the catalyst on numerous bold business initiatives that have expanded Fuji Xerox Singapore’s business into Systems Integration, Document Process Outsourcing, and Digital Printing and Consultancy services.</p>

							<p>His leadership in these areas has been recognized through many awards: Fuji Xerox occupies the top position for ‘Best in Customer Satisfaction’ in the latest Marketprobe competitive benchmark survey, for the third year running.</p>

							<p>In sales, the company takes the number one position yet again, for the highest number of A3 Color Laser copiers purchased, a feat achieved over 21 consecutive quarters since 2008. Fuji Xerox also won the ‘Asia Best Employer Brand’ Award in 2011.</p>

							<p>The company prides itself on good environmental practices and has just been conferred HRM ‘Best Environmental Practices’ Award and the Singapore Green Label Award.</p>

							<p>Bert’s focus and advocacy in Corporate Social Responsibility (CSR) and Work-Life integration have led to his recent appointments as the Vice President of the Singapore Compact for CSR and one of the Executive Committee members of the Singapore Employer Alliance.</p>

							<div class = "clearfix"><a href = "#team" class = "pull-right">Back to Top</a></div>
						</div>

					</div>

					<div class = "management-team__table-block" id = "team_2">
						<div class = "thumbnail">
							<img src = "/assets/fuji-xerox/images/content/mt/mt_raphael_foo.png" alt = "Raphael Foo"/>
						</div>


						<div class = "desc">
							<h3>Raphael Foo</h3>
							<span class = "title">General Manager, Marketing</span>
							<span class = "company">Fuji Xerox Singapore</span>

							<p>Raphael Foo is the General Manager of Marketing in Fuji Xerox Singapore. He is responsible for overseeing the full spectrum of products in Singapore markets. He is a visionary who sets the strategic marketing direction of the company and leads the company in defining the product and branding positioning.</p>

							<p>In addition, he also plays an instrumental role in driving the pursuit of channel partnerships for the company, with the goal to grow and deepen our product growth. Raphael also leads the product marketing team in formulating creative sales compensation schemes with a focus of achieving pipeline revenue targets.</p>

							<p>Since joining Fuji Xerox Singapore in 1992, Raphael has amassed an invaluable wealth of information in productivity and competitiveness strategy formulation, product management, industrial design, quality assurance and standardization as well as business excellence management. Prior to Fuji Xerox Singapore, Raphael was a sales representative in Rank Xerox. In two years, he progressed to the position of the Area Sales Manager in charge of driving business operations in South Asia.</p>

							<p>With more than 30 years in the IT industry, Raphael possesses a wealth of regional marketing and IT experience, backed by his degree with a Bachelor in Marketing. In his free time, he enjoys playing sports like golf and tennis. An outgoing man, he has a strong interest in Chinese History and loves to travel around the world.</p>

							<div class = "clearfix"><a href = "#team" class = "pull-right">Back to Top</a></div>
						</div>

					</div>

					<div class = "management-team__table-block" id = "team_3">
						<div class = "thumbnail">
							<img src = "/assets/fuji-xerox/images/content/mt/mt_paulina_chua.png" alt = "Pauline Chua"/>
						</div>


						<div class = "desc">
							<h3>Pauline Chua</h3>
							<span class = "title">General Manager, Human Capital &amp; Corporate Social Responsibility</span>
							<span class = "company">Fuji Xerox Singapore</span>

							<p>Pauline has nearly 20 years of work experience in both Singapore and New Zealand. She started her career in consulting in the recruitment industry servicing clients from a wide range of industries including Banking & Finance, IT & Telecommunications, Fast Moving Consumer Goods, Service & Hospitality. While working to fill a Regional HR Director position for a client, she tracked down a HR Director who had just left an Investment Bank to join the Leisure & Service Industry. Intrigued by her move, Pauline interviewed the HR Director and ended up joining her team heading Recruitment & Organisation Development. She spent an exciting six years contributing to the rebranding and transformation of Sentosa Island.</p>

							<p>Her passion for the Service Industry led Pauline to take up the HR Director position at Wildlife Reserves Singapore; where she also oversaw Environmental Health & Safety & Administration. Wildlife Reserves Singapore Parks was the first zoological institution in Southeast Asia to be awarded the Environmental and Employee Health & Safety Certifications (ISO 14001 & OHSAS 18001).</p>

							<p>At Fuji Xerox Singapore, Pauline is responsible for overseeing and managing the company’s human resource and development, corporate social responsibility and integrated office services. Together with her team, Pauline contributes to the exciting growth and development at Fuji Xerox Singapore. She partners with the line in building an engaged and motivated workforce; and a workplace that is not just great but meaningful in its commitment to sustainability.</p>

							<p>Pauline is a graduate with a Bachelor of Arts from University of Auckland and a Bachelor of Arts with Honours in Psychology and Education from University of Wellington, New Zealand.</p>

							<div class = "clearfix"><a href = "#team" class = "pull-right">Back to Top</a></div>
						</div>
					</div>



					<div class = "management-team__table-block" id = "team_4">
						<div class = "thumbnail">
							<img src = "/assets/fuji-xerox/images/content/mt/mt_richard_teo.png" alt = "Richard Teo"/>
						</div>

						<div class = "desc">
							<h3>Richard Teo</h3>
							<span class = "title">General Manager/ Director Service Experience and Technical Operations Division</span>
							<span class = "company">Fuji Xerox Singapore</span>

							<p>Richard Teo currently leads the Service Operations and oversees the profit and loss of the service business. His customer-centric approach to service management has enabled the department to grow in strength. The excellent performance is the evident from the Competitive Benchmark Survey which Fuji Xerox Singapore emerged the Top position in key service pillars for 2 years consecutively. </p>

							<p>With a focus to improve efficiency and streamline processes, Richard has introduced the Order Fulfillment Team OFT concept where the central team will take charge of all coordination work relating to equipment delivery, installation and training. This has resulted in relieving our Account Managers from the coordination works so that they can concentrate on their sales activities. The key benefit of this OFT project is it provides customers a hassle-free experience through a single point contact. The OFT rollout is in implementation stage and estimated to be completed by 2012. </p>

							<p>Prior to his roles as the General Manager for Service division, Richard has held various key positions in the company. In 1980, Richard joined Fuji Xerox Singapore as a Technical Representative. He progressed to be responsible for Electronic Printing System Technical Support when Xerox 4045 and Xerox 3700 laser printers were introduced in Singapore for the first time in 1985. Richard was subsequently promoted to District Technical Manager to take charge of the field operations. </p>

							<p>In 1996 to 1999, Richard was assigned to Fuji Xerox Asia Pacific office to assume the role of Regional Technical Support Manager where he was responsible for introducing technical support programmes for Production Systems in the Asia Pacific regions. Due to his outstanding contributions, Richard was awarded the President’s Award in 1998. In addition, Richard was a member of the regional core team that undertook the designing of the region-wide Xerox Digital Engineer XDE programme which was aimed at upgrading the skill set of the engineers. </p>

							<p>Richard is a graduate with a Diploma in Electrical Engineering from Singapore Polytechnic and holds a C&G Certification in Computer Programming from Systematic Training Centr</p>

							<div class = "clearfix"><a href = "#team" class = "pull-right">Back to Top</a></div>
						</div>

					</div>

					<div class = "management-team__table-block" id = "team_5">
						<div class = "thumbnail">
							<img src = "/assets/fuji-xerox/images/content/mt/mt_sharon_kong.png" alt = "Sharon Kong"/>
						</div>


						<div class = "desc">
							<h3>Sharon Kong</h3>
							<span class = "title">General Manager/Director Industry Solutions Group</span>
							<span class = "company">Fuji Xerox Singapore Pte Ltd</span>

							<p>As a member of the Senior Management Team, Sharon plays an important role in the development and implementation of the strategy and vision of the company. </p>

							<p>Sharon joined Fuji Xerox Singapore 27 years ago in the Sales Department and established her career in Sales, Sales Management and Marketing Management in the Printing Systems and Document Management Solutions areas. Her passion for the business, particularly in the changing landscape from analog to digital led her to successfully complete a Master Degree in Business IT in the 2000’s. </p>

							<p>Armed with these experiences, she moved rapidly to take on the position of Associate Director, and led the Production Services Business Group to its success in pioneering digital production printing, and planted its dominance in digital printing solutions, particularly in the Graphic Communications industry. Today, Fuji Xerox Singapore commands more than 70% market share in the high-end digital production color printing space. </p>

							<p>In 2009, she assumed her present position of General Manager/Director. A sought after speaker in the Digital Printing arena, Sharon is a keen golfer and traveler, and enjoys spending time with her church and family.</p>

							<div class = "clearfix"><a href = "#team" class = "pull-right">Back to Top</a></div>
						</div>
					</div>

					<div class = "management-team__table-block" id = "team_6">
						<div class = "thumbnail">
							<img src = "/assets/fuji-xerox/images/content/mt/mt_tan_yew_luan.jpg" alt = "Tan Yew Luan"/>
						</div>


						<div class = "desc">
							<h3>Tan Yew Luan</h3>
							<span class = "title">General Manager, Global Services</span>
							<span class = "company">Fuji Xerox Singapore</span>

							<p>Tan Yew Luan is the General Manager of Global Services at Fuji Xerox Singapore. At Fuji Xerox, she leads the dynamic Global Services team which focuses on helping organizations to improve processes and support infrastructures to achieve significant and sustainable business results. The Global Services team provides a comprehensive document outsourcing services, encompassing Next Generation Managed Print Services, Customer Communications Management and Communications & Marketing Services, to meet customers' changing needs.</p>

							<p>Yew Luan's career spans more than 30 years within the IT industry. Prior to joining Fuji Xerox Singapore, Yew Luan was Director, Service Lines, Global Technology Services, ASEAN at IBM. Yew Luan began her career at IBM in 1985 as a System Engineer, selling, delivering and supporting banking solutions for the first 10 years of her career. From 2004 to 2015, she was a member of the IBM Global Technology Services Organization where she held a few key profit & loss portfolios both in Singapore and regionally. This includes presales, sales, delivery and support for Singapore and ASEAN. Some of her achievements at IBM include a proven track record of growing services business and transforming a non-performing business.</p>

							<p>Yew Luan is a highly energetic leader who attains job satisfaction not just from delivering financial results, but also from continuously developing the team and herself. She enjoys traveling, reading and taking walks in nature parks.</p>

							<p>Yew Luan holds a Bachelor of Science in Computer Science and Economics and a Master of Business Administration from the National University of Singapore.</p>

							<div class = "clearfix"><a href = "#team" class = "pull-right">Back to Top</a></div>
						</div>

					</div>
				</div>
			</div>

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Company</a></h3>

					<ul>
						<li><a href="/company/about-fuji-xerox-singapore" >About Fuji Xerox Singapore</a></li>
						<li><a href="/company/management-team" class = "active">Management Team</a></li>
						<li><a href="/company/awards-achievements">Awards &amp; Achievements</a></li>
						<li><a href="/company/substainable-future">Towards a Sustainable Future</a></li>
						<li><a href="#">Fuji Xerox Dream Service</a></li>
						<li><a href="/company/success-stories">Success Stories</a></li>
						<li><a href="/company/newsroom">Newsroom</a></li>
						<li><a href="/company/career-with-us">Career with Us</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>