<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Toward a Substainable Future';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/company">Company</a></li>
			<li><span>Toward a Substainable Future</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4">
				<h1>Toward a Substainable Future</h1>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 offset-sm-1 col-md-8 offset-md-2">
				<a id = "content_1" class = "anchor-link" data-label = "Overview"></a>
				<h2>Our approach to Sustainability</h2>

				<p>Fuji Xerox Singapore believes that reputation and credibility are integral to business sustainability. Our relationships with customers, employees, the community and the environment are key to maintaining our reputation and credibility.</p>

				<p>Fuji Xerox Singapore's approach to sustainability focuses on creating value for these key stakeholders through the provision of products and services. Today, sustainability means more than just ‘green' and charity. It is about making measureable impact to the business environment and helps us to constantly innovate and improve our products and services to respond to an evolving set of stakeholders' expectations.</p>

				<p>In the second edition of our Sustainability Report, we highlight our continued commitment to sustainability and transparency, demonstrating how our social and environmental responsibilities are integrated into our core business. We also measure our performance against the Global Reporting Initiative (GRI) requirements, we demonstrated how our social and environmental responsibilities are integrated with our core business. Key initiatives, performance and outcomes are also shared in this report.</p>

				<hr>

				<section class = "section resources">

					<div class = "resources__block">
						<h4>Download Fuji Xerox Sustainability Report 2014 - 2015</h4>
						<dl>
							<dt>Document type:</dt>
							<dd>Portable Document Format</dd>
						</dl>

						<!-- DEV NOTE:  -->
						<!-- IF Email Subscription is switched off -->
						<!-- <a href = "#" class = "resources__block__download-btn" title = "Download Colour Management Brochure"><i class = "icon-download"></i><span class = "sr-only">Download Resource</span></a> -->
						<!-- ELSE -->
						<a href = "#subscriptionCall" class = "fancybox resources__block__download-btn" title = "Download Colour Management Brochure"><i class = "icon-download"></i><span class = "sr-only">Download</span></a>
						<!-- END -->
					</div>

					<!-- Subscription Call -->
					<div class = "subscribe" id = "subscriptionCall">
						<div class = "row-col-12">
							<div class = "col-sm-6 col-md-6 col match-height">
								<h3>You may download the PDF here</h3>
								<a href="#" target = "_blank" class = "btn-download">Colour Management Brochure<i class="icon-download"></i></a>
							</div>
							<div class = "col-sm-6 col-md-6 col match-height">
								<h3>Subscribe to our newsletter</h3>
								<!-- <p>Keep yourself update to Fuji Xerox latest product!</p> -->
								<div class = "form-row row-col-12">
									<div class = "col-sm-6">
										<div class = "input-group-text">
											<label for = "subscribeName">Name<span class = "required">*</span></label>
											<input type = "text" id = "subscribeName" placeholder = "Name">
										</div>
										<div class = "input-group-text">
											<label for = "subscribeEmail">Email<span class = "required">*</span></label>
											<input type = "email" id = "subscribeEmail" placeholder = "abc@example.com">
										</div>
									</div>
								</div>
								<div class = "btn-group">
									<button type = "submit">Submit<i class = "icon-chevron-with-circle-right"></i></button>
								</div>
							</div>
						</div>
					</div>
					<!-- End: Subscription Call -->
				</section>


				<a id = "content_3" class = "anchor-link" data-label = "Key Facts"></a>
				<section class = "section">
						<h3>Key Facts:</h4>

						<ul>
							<li><strong>Fuji Xerox Staff Strength:</strong> 45,040<sup>1</sup></a></li>
							<li><strong>Staff Strength in Singapore:</strong> 850<sup>1</sup></a></li>
							<li><strong>Strengths in:</strong> Innovation, Technology, Customer Service, Sustainability</a></li>
							<li><strong>ISO 9001<sup>1</sup></strong> since 1996<sup>1</sup></a></li>
							<li>Achieved <strong>Singapore Quality Class</strong><sup>3</sup> in 1998</a></li>
							<li><strong>ISO 14001</strong><sup>4</sup> certified in 2004</a></li>
						</ul>

						<ol class = "footnote">
							<li>As of 2013</li>
							<li>SO 9001:2008 specifies requirements for a quality management system where an organization needs to demonstrate its ability to consistently provide product that meets customer & applicable statutory & regulatory requirements</li>
							<li>Launched in 1997, Singapore Quality Class (SQC) is the certification for the overall business excellence standard. Based on the internationally benchmarked business excellence framework, the SQC provides organisations with a holistic model for managing a business for excellence. It has 7 categories, namely, Leadership, Planning, Information, People, Processes, Customers and Results</li>
							<li>ISO 14001:2004 specifies requirements for an environmental management system to enable an organization to develop and implement a policy and objectives which take into account legal requirements and other requirements to which the organization subscribes, and information about significant environmental aspects. It applies to those environmental aspects that the organization identifies as those which it can control and those which it can influence. It does not itself state specific environmental performance criteria.</li>
						</ol>
					</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Company</a></h3>

					<ul>
						<li><a href="/company/about-fuji-xerox-singapore" >About Fuji Xerox Singapore</a></li>
						<li><a href="/company/management-team">Management Team</a></li>
						<li><a href="/company/awards-achievements">Awards &amp; Achievements</a></li>
						<li><a href="/company/substainable-future" class = "active">Towards a Sustainable Future</a></li>
						<li><a href="#">Fuji Xerox Dream Service</a></li>
						<li><a href="/company/success-stories">Success Stories</a></li>
						<li><a href="/company/newsroom">Newsroom</a></li>
						<li><a href="/company/career-with-us">Career with Us</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>