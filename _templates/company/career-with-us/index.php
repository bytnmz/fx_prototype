<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Career With Us';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/company">Company</a></li>
			<li><span>Career With Us</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4">
				<h1>Career With Us</h1>
				<p>Enter the Race with the Winning Team</p>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

		<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 offset-sm-1 col-md-8 offset-md-2">
				<a id = "" class = "anchor-link" data-label = "Overview"></a>
				<h2>Top 10 Reasons to join Fuji Xerox Singapore</h2>

				<p><strong>We adopt an Open Door Policy</strong> – We like to hear from our employees and believe that everyone should have access to their supervisor and the management team, including our Senior Managing Director.</p>

				<p><strong>Be Part of the Winning Team</strong> – We share attributes with the world’s most successful organisations – we focus on innovation and believe that our technology can change the world. We have consistently been voted No. 1 in customer satisfaction benchmark survey conducted by MarketProbe and placed as a leader in IDC‘s 2011 MarketScape report for Managed Print Services.</p>

				<p><strong>Solving Client’s Problems with the Latest Technology</strong> - We work on the latest technologies and leverage on our employees’ technical prowess to solve really complicated problems for our clients.</p>

				<p><strong>Work Hard, Play Hard</strong> – It’s our way of life. Working hard does not necessarily mean putting in the hours. We thrive in high energy environments and believe that work and play can be synonymous.</p>

				<p><strong>We live our Core Values</strong> – Our core values are important to us. It is a strong adhesive that binds us together.</p>

				<p><strong>Integrating Work and Life</strong> – We recognise that it’s important that our employees can exercise control and choice to meet life’s challenges and enable them to live and work both efficiently and effectively.</p>

				<p><strong>We care for the environment</strong> - We are committed towards achieving a sustainable society and environment. Recently, we won the inaugural ‘Best Environment Practices’ award at the HRM Awards 2012.</p>

				<p><strong>Trust and Empowerment</strong> – Our employees are empowered to act like entrepreneurs, to take the road less travelled, and take ownership for their business and work.</p>

				<p><strong>Diversity</strong> – We thrive on diversity. That's how we innovate and remain competitive. We leverage on the strengths and skills of our multi-generational workforce and build bridges for cooperation between generations.</p>

				<p><strong>Giving Back to the Community</strong> - We encourage our employees to contribute to the community and support causes to help the less fortunate. We grant Volunteer Service Leave to our employees when they participate in company-driven community volunteering events and campaigns.</p>

				<hr>

				<a id = "" class = "anchor-link" data-label = "Apply Now"></a>
				<section class = "section related-pages">
					<h2>Apply Now</h2>

					<div class = "blocks">
						<div class = "related-pages__block">
							<a href = "customer-communication-management/">
								<h4>Career Opportunities</h4>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Internship Opportunities</h4>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Commercial Associate Programme</h4>
							</a>
						</div>
					</div>
				</section>

			</div>

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Company</a></h3>

					<ul>
						<li><a href="/company/about-fuji-xerox-singapore" >About Fuji Xerox Singapore</a></li>
						<li><a href="/company/management-team">Management Team</a></li>
						<li><a href="/company/awards-achievements">Awards &amp; Achievements</a></li>
						<li><a href="/company/substainable-future">Towards a Sustainable Future</a></li>
						<li><a href="#">Fuji Xerox Dream Service</a></li>
						<li><a href="/company/success-stories">Success Stories</a></li>
						<li><a href="/company/newsroom">Newsroom</a></li>
						<li><a href="/company/career-with-us" class = "active">Career with Us</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>