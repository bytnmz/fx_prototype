<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Company';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><span>Company</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4">
				<h1>Our Company, Capabilities and Commitment</h1>
				<p>Delivering efficiency, productivity, innovation and sustainability to your business</p>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Solution Mapper Introduction -->
	<div class = "childpage__intro wingspan">
		<div>
			<div class = "col childpage__intro-block">
				<a href = "about-fuji-xerox-singapore">
					<img src = "/assets/fuji-xerox/images/solutions/box-1.png" alt = ""/>
					<h2>About Fuji Xerox Singapore</h2>
					<p>Fuji Xerox Singapore is the country's leading provider of new class document solutions with a global network of 160 countries.</p>
				</a>
			</div>

			<div class = "col childpage__intro-block">
				<a href = "management-team">
					<img src = "/assets/fuji-xerox/images/solutions/box-2.png" alt = ""/>
					<h2>Management Team</h2>
					<p>Our management team comprises of an unrivalled team of industry leading professionals, propelling Fuji Xerox to scale unsurpassed heights.</p>
				</a>
			</div>

			<div class = "col childpage__intro-block">
				<a href = "awards-achievements">
					<img src = "/assets/fuji-xerox/images/solutions/box-3.png" alt = ""/>
					<h2>Awards &amp; Achievements</h2>
					<p>Our proven track record is the outcome of driving excellence in all that we do.</p>
				</a>
			</div>

			<div class = "col childpage__intro-block">
				<a href = "substainable-future">
					<img src = "/assets/fuji-xerox/images/solutions/box-4.png" alt = ""/>
					<h2>Towards a Substainable Future</h2>
					<p>Our Corporate Social Responsibility showcases our commitment and focus on global sustainability, placing the environment at the organization's core.</p>
				</a>
			</div>

			<div class = "col childpage__intro-block">
				<a href = "#">
					<img src = "/assets/fuji-xerox/images/solutions/box-5.png" alt = ""/>
					<h2>Fuji Xerox Dream Service</h2>
					<p>Our customer-centric focus has inspired us to assemble a team of highly qualified customer service engineers to ensure fast resolution and provide proactive services including virtual support.</p>
				</a>
			</div>

			<div class = "col childpage__intro-block">
				<a href = "success-stories">
					<img src = "/assets/fuji-xerox/images/solutions/box-6.png" alt = ""/>
					<h2>Success Stories</h2>
					<p>View how we have worked with our clients to meet their various needs efficiently and effectively.</p>
				</a>
			</div>

			<div class = "col childpage__intro-block">
				<a href = "newsroom">
					<img src = "/assets/fuji-xerox/images/solutions/box-5.png" alt = ""/>
					<h2>Newsroom</h2>
					<p>Our innovations, products and business solutions covered in the news.</p>
				</a>
			</div>

			<div class = "col childpage__intro-block">
				<a href = "career-with-us">
					<img src = "/assets/fuji-xerox/images/solutions/box-6.png" alt = ""/>
					<h2>Career with Us</h2>
					<p>Be a part of the winning team with Fuji Xerox, where exceptional people and ideas thrive.</p>
				</a>
			</div>
		</div>
	</div>
	<!-- End: Soution Mapper Introduction -->


	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>