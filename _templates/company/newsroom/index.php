<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Newsroom';
$primary = 4;
$secondary = 4;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "../">Company</a></li>
			<li><span>Newsroom</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Newsroom -->
	<div class = "newsroom wingspan">
		<h1> Newsroom </h1>
		<!-- Newsroom Featured -->
		<div class = "newsroom-featured row-col-12">
			<div class = "col-sm-5">
				<div class = "newsroom-featured__block highlight-block">
					<a href = "#">
						<div class = "image">
							<img src = "/assets/fuji-xerox/images/content/news_thumbnail_placeholder.jpg" alt = "" />
						</div>

						<div class = "desc">
							<h3>Lorem ipsum dolor sit ame</h3>
							<span class = "date">Mar 7, 2015</span>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet lectus vel arcu pellentesque facilisis et sed urna. Sed tempor odio sed mauris eleifend posuere.</p>
						</div>
					</a>
				</div>


				<div class = "newsroom-featured__blog-block">
					<div class = "desc" style = "background-image:url(/assets/fuji-xerox/images/solutions/box-2.png)">
						<div>
							<h3>Our blog</h3>
							<p>Vestibulum et Lectus malesuada, convallis lectus sit amet, gravida arcu.</p>
							<div class="more-link"><a href="#">Read blog</a></div>
						</div>
					</div>
				</div>
			</div>

			<div class = "col-sm-7">
				<div class = "row-col-12">
					<div class = "newsroom-featured__block col-sm-6 push-left-10">
						<a href = "#">
							<div class = "image">
								<img src = "/assets/fuji-xerox/images/content/news_thumbnail_placeholder.jpg" alt = "" />
							</div>

							<div class = "desc">
								<h3>Lorem ipsum dolor sit ame</h3>
								<span class = "date">Mar 7, 2015</span>
							</div>
						</a>
					</div>

					<div class = "newsroom-featured__block col-sm-6 push-left-10">
						<a href = "#">
							<div class = "image">
								<img src = "/assets/fuji-xerox/images/content/news_thumbnail_placeholder.jpg" alt = "" />
							</div>

							<div class = "desc">
								<h3>Lorem ipsum dolor sit ame</h3>
								<span class = "date">Mar 7, 2015</span>
							</div>
						</a>
					</div>

					<div class = "newsroom-featured__block is-video col-sm-6 push-left-10">
						<a href = "https://www.youtube.com/embed/qtS3rz31KII" data-fancybox-type="iframe" class = "fancybox video">
							<div class = "image">
								<img src = "/assets/fuji-xerox/images/content/video_thumbnail_01.jpg" alt = "" />
							</div>

							<div class = "desc">
								<h3>Lorem ipsum dolor sit ame</h3>
								<span class = "date">Mar 7, 2015</span>
							</div>
						</a>
					</div>

					<div class = "newsroom-featured__download-block col-sm-6 push-left-10">
						<a href = "#">
							<div class = "image">
								<img src = "/assets/fuji-xerox/images/content/doc-cover.jpg" alt = "" />
							</div>

							<div class = "desc">
								<h3>Annual Report</h3>
								<p><i class = "icon-download"></i>Download Fuji Xerox 2016 Annual Report.</p>
								<span class = "date">Mar 7, 2015</span>
							</div>
						</a>
					</div>

					<!-- <div class = "newsroom-featured__block col-sm-6 push-left-10">
						<a href = "#">
							<div class = "image">
								<img src = "/assets/fuji-xerox/images/content/news_thumbnail_placeholder.jpg" alt = "" />
							</div>

							<div class = "desc">
								<h3>Lorem ipsum dolor sit ame</h3>
								<span class = "date">Mar 7, 2015</span>
							</div>
						</a>
					</div> -->

				</div>
			</div>

		</div>
		<!-- End: Newsroom Featured -->


		<!-- Newsroom Newslist -->
		<div class = "newsroom-listing listing-content row-col-12">

			<div class = "listing-content__sort-result">
				<div class = "pull-right">
					<label>Show news from</label>
					<select class = "custom-select">
						<option value = "2016">2016</option>
						<option value = "2015">2015</option>
						<option value = "2014">2014</option>
					</select>
				</div>
			</div>

			<section class = "home-news-list corporate-news col-sm-6 push-right-15" data-category="1">
				<h2>Corporate News</h2>

				<!-- Dynamic List Content (rr.newsroom.js)-->
				<ul class = "news-results"></ul>

				<div class = "loader">
					<div class="loader-animation">
						<div class="inner"></div>
						<div class="inner"></div>
						<div class="inner"></div>
					</div>
				</div>

				<div class = "more-link visible-xs-block"><a href = "#">Load More</a></div>
			</section>

			<section class = "home-news-list standard-news col-sm-6 push-left-15" data-category="2">
				<h2>News</h2>

				<!-- Dynamic List Content (rr.newsroom.js)-->
				<ul class = "news-results"></ul>

				<div class = "loader">
					<div class="loader-animation">
						<div class="inner"></div>
						<div class="inner"></div>
						<div class="inner"></div>
					</div>
				</div>

				<div class = "more-link visible-xs-block"><a href = "#">Load More</a></div>
			</section>
		</div>
		<!-- End: Newsroom Newslist -->

		<div class = "more-link load-all-news-btn hidden-xs"><a href = "#">Load More</a></div>
	</div>
	<!-- End: Newsroom -->

	<script id="results-template" type="text/x-handlebars-template">
		{{#each news}}
			<li>
				<h3><a href = "{{url}}">{{title}}</a></h3>
				{{{desc}}}

				<span class="date">{{date}}</span>
			</li>
		{{/each}}
	</script>

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>