<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | About Fuji Xerox Singapore';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/company">Company</a></li>
			<li><span>About Fuji Xerox Singapore</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4">
				<h1>About Fuji Xerox Singapore</h1>
				<p>Always moving.<br>We're ready for real business</p>

			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 offset-sm-1 col-md-8 offset-md-2">
				<a id = "content_1" class = "anchor-link" data-label = "Overview"></a>
				<h2>Overview</h2>

				<p>Fuji Xerox (Japan) is a subsidiary of FUJIFILM Holdings Corporation (75%) and Xerox Corporation of the USA (25%).</p>

				<p>Fuji Xerox Singapore Pte Ltd is a wholly owned subsidiary of Fuji Xerox (Japan), together with other operating companies in Australia, China, Hong Kong, Malaysia, New Zealand, the Philippines, South Korea, Taiwan, Thailand and Vietnam.</p>

				<p>Established in 1965, Fuji Xerox Singapore is the country’s leading provider of new class document solutions. We offer an unparalleled portfolio of document technologies, services, software, supplies and document-centric outsourcing.</p>

				<p>Supported by an unrivalled team of industry leading professionals, we are committed to help our customers achieve increased productivity and process efficiency gains through innovative document solutions, while meeting their sustainability objectives and reducing costs.</p>

				<p>Our vision is to enable people and businesses to share ideas, information, documents and knowledge effortlessly across any platforms, applications or devices.</p>

				<p>Building on our past success, coupled with the commitment of our people, we are confident of remaining a vendor of choice, an enabler to businesses' needs on improving workflow and knowledge management.</p>

				<hr>

				<a id = "content_2" class = "anchor-link" data-label = "Resources"></a>
				<section class = "section resources">
					<h3>Find out how you can partner with us for your business growth.</h3>

					<div class = "resources__block">
						<h4>Download Fact Sheet</h4>
						<dl>
							<dt>File Size:</dt>
							<dd>2.73MB</dd>
							<dt>Document type:</dt>
							<dd>Portable Document Format</dd>
						</dl>

						<!-- DEV NOTE:  -->
						<!-- IF Email Subscription is switched off -->
						<!-- <a href = "#" class = "resources__block__download-btn" title = "Download Colour Management Brochure"><i class = "icon-download"></i><span class = "sr-only">Download Resource</span></a> -->
						<!-- ELSE -->
						<a href = "#subscriptionCall" class = "fancybox resources__block__download-btn" title = "Download Colour Management Brochure"><i class = "icon-download"></i><span class = "sr-only">Download</span></a>
						<!-- END -->
					</div>

					<div class = "resources__block">
						<h4>Download Corporate Profile Brochure</h4>
						<dl>
							<dt>File Size:</dt>
							<dd>4MB</dd>
							<dt>Document type:</dt>
							<dd>Portable Document Format</dd>
						</dl>

						<!-- DEV NOTE:  -->
						<!-- IF Email Subscription is switched off -->
						<!-- <a href = "#" class = "resources__block__download-btn" title = "Download Colour Management Brochure"><i class = "icon-download"></i><span class = "sr-only">Download Resource</span></a> -->
						<!-- ELSE -->
						<a href = "#subscriptionCall" class = "fancybox resources__block__download-btn" title = "Download Colour Management Brochure"><i class = "icon-download"></i><span class = "sr-only">Download FactSheet</span></a>
						<!-- END -->
					</div>

					<!-- Subscription Call -->
					<div class = "subscribe" id = "subscriptionCall">
						<div class = "row-col-12">
							<div class = "col-sm-6 col-md-6 col match-height">
								<h3>You may download the PDF here</h3>
								<a href="#" target = "_blank" class = "btn-download">Colour Management Brochure<i class="icon-download"></i></a>
							</div>
							<div class = "col-sm-6 col-md-6 col match-height">
								<h3>Subscribe to our newsletter</h3>
								<!-- <p>Keep yourself update to Fuji Xerox latest product!</p> -->
								<div class = "form-row row-col-12">
									<div class = "col-sm-6">
										<div class = "input-group-text">
											<label for = "subscribeName">Name<span class = "required">*</span></label>
											<input type = "text" id = "subscribeName" placeholder = "Name">
										</div>
										<div class = "input-group-text">
											<label for = "subscribeEmail">Email<span class = "required">*</span></label>
											<input type = "email" id = "subscribeEmail" placeholder = "abc@example.com">
										</div>
									</div>
								</div>
								<div class = "btn-group">
									<button type = "submit">Submit<i class = "icon-chevron-with-circle-right"></i></button>
								</div>
							</div>
						</div>
					</div>
					<!-- End: Subscription Call -->
				</section>


				<a id = "content_3" class = "anchor-link" data-label = "Key Facts"></a>
				<section class = "section">
						<h3>Key Facts:</h4>

						<ul>
							<li><strong>Fuji Xerox Staff Strength:</strong> 45,040<sup>1</sup></a></li>
							<li><strong>Staff Strength in Singapore:</strong> 850<sup>1</sup></a></li>
							<li><strong>Strengths in:</strong> Innovation, Technology, Customer Service, Sustainability</a></li>
							<li><strong>ISO 9001<sup>1</sup></strong> since 1996<sup>1</sup></a></li>
							<li>Achieved <strong>Singapore Quality Class</strong><sup>3</sup> in 1998</a></li>
							<li><strong>ISO 14001</strong><sup>4</sup> certified in 2004</a></li>
						</ul>

						<ol class = "footnote">
							<li>As of 2013</li>
							<li>SO 9001:2008 specifies requirements for a quality management system where an organization needs to demonstrate its ability to consistently provide product that meets customer & applicable statutory & regulatory requirements</li>
							<li>Launched in 1997, Singapore Quality Class (SQC) is the certification for the overall business excellence standard. Based on the internationally benchmarked business excellence framework, the SQC provides organisations with a holistic model for managing a business for excellence. It has 7 categories, namely, Leadership, Planning, Information, People, Processes, Customers and Results</li>
							<li>ISO 14001:2004 specifies requirements for an environmental management system to enable an organization to develop and implement a policy and objectives which take into account legal requirements and other requirements to which the organization subscribes, and information about significant environmental aspects. It applies to those environmental aspects that the organization identifies as those which it can control and those which it can influence. It does not itself state specific environmental performance criteria.</li>
						</ol>
					</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Company</a></h3>

					<ul>
						<li><a href="/company/about-fuji-xerox-singapore" class = "active">About Fuji Xerox Singapore</a></li>
						<li><a href="/company/management-team">Management Team</a></li>
						<li><a href="/company/awards-achievements" >Awards &amp; Achievements</a></li>
						<li><a href="/company/substainable-future">Towards a Sustainable Future</a></li>
						<li><a href="#">Fuji Xerox Dream Service</a></li>
						<li><a href="/company/success-stories">Success Stories</a></li>
						<li><a href="/company/newsroom">Newsroom</a></li>
						<li><a href="/company/career-with-us">Career with Us</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>