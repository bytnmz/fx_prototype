<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Form';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><span>Form</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4">
				<h1>Career With Us</h1>
				<p>Enter the Race with the Winning Team</p>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->


	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 offset-sm-1 col-md-8 offset-md-2">

				<div class = "sitecore-form">

							<!-- <div class="scfForm" id="form_6A9985C58EFE43209A66B4A98BDE5EA0">
								<input type="hidden" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$formreference" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_formreference" value="form_6A9985C58EFE43209A66B4A98BDE5EA0">
								<h2 id="form_6A9985C58EFE43209A66B4A98BDE5EA0_title" class="scfTitleBorder">Contact Us</h2>
								<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0__summary" class="scfValidationSummary" style="display:none;"></div>
								<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_form_6A9985C58EFE43209A66B4A98BDE5EA0_submitSummary" class="scfSubmitSummary" style="color:Red;"></div>
								<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_fieldContainer" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'form_6A9985C58EFE43209A66B4A98BDE5EA0_form_6A9985C58EFE43209A66B4A98BDE5EA0_submit')">
									<div>
										<fieldset class="scfSectionBorderAsFieldSet">
											<div class="scfSectionContent">
												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_scope" class="scfSingleLineTextBorder fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d name.Single+Line">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_text" class="scfSingleLineTextLabel">Single Line</label>
													<div class="scfSingleLineGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_ADB11D7524A8411C8089A8DD897A031A" type="text" maxlength="256" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" class="scfSingleLineTextBox">
														<span class="scfSingleLineTextUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:block;">Single Line must have at least 0 and no more than 256 characters.</span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">The value of the Single Line field is not valid.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E_scope" class="scfMultipleLineTextBorder fieldid.%7bD1C52150-7AA9-4035-AB45-8A744E0CE06E%7d name.Multi+Line">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E_text" class="scfMultipleLineTextLabel">Multi Line</label>
													<div class="scfMultipleLineGeneralPanel">
														<textarea name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_D1C521507AA94035AB458A744E0CE06E" rows="4" cols="20" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E" class="scfMultipleLineTextBox"></textarea>
														<span class="scfMultipleLineTextUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bD1C52150-7AA9-4035-AB45-8A744E0CE06E%7d inner.1" style="display:block;">Multi Line must have at least 0 and no more than 512 characters.</span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bD1C52150-7AA9-4035-AB45-8A744E0CE06E%7d inner.1" style="display:none;">The value of the Multi Line field is not valid.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_F76D9515C6BE4B21B99007DFD076CBC8_scope" class="scfPasswordBorder fieldid.%7bF76D9515-C6BE-4B21-B990-07DFD076CBC8%7d name.Password">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_F76D9515C6BE4B21B99007DFD076CBC8" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_F76D9515C6BE4B21B99007DFD076CBC8_text" class="scfPasswordLabel">Password</label>
													<div class="scfPasswordGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_F76D9515C6BE4B21B99007DFD076CBC8" type="password" maxlength="256" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_F76D9515C6BE4B21B99007DFD076CBC8" class="scfPasswordTextBox">
														<span class="scfPasswordUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_F76D9515C6BE4B21B99007DFD076CBC86ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bF76D9515-C6BE-4B21-B990-07DFD076CBC8%7d inner.1" style="display:none;">Password must have at least 0 and no more than 256 characters.</span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_F76D9515C6BE4B21B99007DFD076CBC8070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bF76D9515-C6BE-4B21-B990-07DFD076CBC8%7d inner.1" style="display:none;">The value of the Password field is not valid.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div class="scfPasswordConfirmation fieldid.%7b688A46E2-2ADB-4A72-9E28-7D49D463584B%7d name.Password+Confirmation validationgroup.form_6A9985C58EFE43209A66B4A98BDE5EA0_submit" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder">
													<div>
														<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_passwordBorder" class="scfConfirmPasswordBorder">
															<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_field_688A46E22ADB4A729E287D49D463584B" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_passwordTitle" class="scfConfirmPasswordLabel">Password</label>
															<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_passwordPanel" class="scfConfirmPasswordGeneralPanel">
																<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_688A46E22ADB4A729E287D49D463584Bborder$field_688A46E22ADB4A729E287D49D463584B" type="password" maxlength="256" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_field_688A46E22ADB4A729E287D49D463584B" class="scfConfirmPasswordTextBox password.1 fieldid.{688A46E2-2ADB-4A72-9E28-7D49D463584B}/{953A6FD7-CF54-4895-9CD4-39804961A776}">
																<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_passwordHelp" class="scfConfirmPasswordUsefulInfo" style="display:none"></span>
																<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_field_688A46E22ADB4A729E287D49D463584B66D1164796C44B8BB5719F9C91AE6760_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b688A46E2-2ADB-4A72-9E28-7D49D463584B%7d confirmationControlId.confirmation inner.1" style="display:none;"></span>
																<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_field_688A46E22ADB4A729E287D49D463584B6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7b688A46E2-2ADB-4A72-9E28-7D49D463584B%7d confirmationControlId.confirmation inner.1 fieldid.{688A46E2-2ADB-4A72-9E28-7D49D463584B}" style="display:none;">Password must have at least 0 and no more than 256 characters.</span>
																<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_field_688A46E22ADB4A729E287D49D463584B070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b688A46E2-2ADB-4A72-9E28-7D49D463584B%7d confirmationControlId.confirmation inner.1 fieldid.{688A46E2-2ADB-4A72-9E28-7D49D463584B}" style="display:block;">The value of the Password field is not valid.</span>
															</div>
															<span class="scfRequired">*</span>
														</div>
													</div>
													<div>
														<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_confirmationBorder" class="scfConfirmPasswordBorder">
															<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_confirmation" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_confirmationTitle" class="scfConfirmPasswordLabel">Confirmation</label>
															<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_confirmationPanel" class="scfConfirmPasswordGeneralPanel">
																<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_688A46E22ADB4A729E287D49D463584Bborder$confirmation" type="password" maxlength="256" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_confirmation" class="scfConfirmPasswordTextBox confirmation.1 fieldid.{688A46E2-2ADB-4A72-9E28-7D49D463584B}/{A47F22B9-C1E8-41AB-A36E-2A0C634E800B}">
																<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_confimationHelp" class="scfConfirmPasswordUsefulInfo" style="display:none"></span>
																<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_field_688A46E22ADB4A729E287D49D463584B6ADFFAE3DADB451AB530D89A2FD0307B_validatorconfirmation" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7b688A46E2-2ADB-4A72-9E28-7D49D463584B%7d fieldid.{688A46E2-2ADB-4A72-9E28-7D49D463584B}" inner="1" style="display:none;">Confirmation must have at least 0 and no more than 256 characters.</span>
															</div>
															<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_688A46E22ADB4A729E287D49D463584Bborder_confirmationmarker" class="scfRequired">*</span>
														</div>
													</div>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A_scope" class="scfEmailBorder fieldid.%7bCAE5C925-52B5-4F53-86C6-12395C37DC5A%7d name.Email">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A_text" class="scfEmailLabel">Email</label>
													<div class="scfEmailGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_CAE5C92552B54F5386C612395C37DC5A" type="text" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A" class="scfEmailTextBox">
														<span class="scfEmailUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A5D10AF7533054C39908EB25E8CB4ABDC_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bCAE5C925-52B5-4F53-86C6-12395C37DC5A%7d inner.1" style="display:none;">Enter a valid e-mail address.</span>
													</div>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4_scope" class="scfTelephoneBorder fieldid.%7b7C714B5E-B312-4C7E-B843-559D45928CE4%7d name.Telephone">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4_text" class="scfTelephoneLabel">Telephone</label>
													<div class="scfTelephoneGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_7C714B5EB3124C7EB843559D45928CE4" type="text" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4" class="scfTelephoneTextBox"><span class="scfTelephoneUsefulInfo" style="display:none;"></span><span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4C3CC8A327EF14D138163826E11482E4D_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b7C714B5E-B312-4C7E-B843-559D45928CE4%7d inner.1" style="display:none;">Enter a valid telephone number.</span>
													</div>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_1A0B092374B04AFAB0FCB61C006D19AB_scope" class="scfNumberBorder fieldid.%7b1A0B0923-74B0-4AFA-B0FC-B61C006D19AB%7d name.Number">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_1A0B092374B04AFAB0FCB61C006D19AB" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_1A0B092374B04AFAB0FCB61C006D19AB_text" class="scfNumberLabel">Number</label>
													<div class="scfNumberGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_1A0B092374B04AFAB0FCB61C006D19AB" type="text" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_1A0B092374B04AFAB0FCB61C006D19AB" class="scfNumberTextBox">
														<span class="scfNumberUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_1A0B092374B04AFAB0FCB61C006D19AB1314218A8FF1480BA29A1C5A9DE4E1DE_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b1A0B0923-74B0-4AFA-B0FC-B61C006D19AB%7d inner.1" style="display:none;">Enter a number.</span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_1A0B092374B04AFAB0FCB61C006D19AB522778A59B4D440FA5B975910CC55428_validator" class="minimum.0 maximum.2.147484E%2b09 scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7b1A0B0923-74B0-4AFA-B0FC-B61C006D19AB%7d inner.1" style="display:none;">The number in Number must be at least 0 and no more than 2.147484E+09.</span>
													</div>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_E50FCB88E6A04B139BDF50A804A6663A" class="scfDateSelectorBorder DateFormat.yyyy-MMMM-dd startDate.20000101T120000 endDate.20140430T000000 fieldid.%7bE50FCB88-E6A0-4B13-9BDF-50A804A6663A%7d name.Date">
													<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_E50FCB88E6A04B139BDF50A804A6663A_text" class="scfDateSelectorLabel">Date</span>
													<div class="scfDateSelectorGeneralPanel">
														<input type="hidden" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_E50FCB88E6A04B139BDF50A804A6663A_complexvalue" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_E50FCB88E6A04B139BDF50A804A6663A_complexvalue" value="20130430T000000">
														<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_E50FCB88E6A04B139BDF50A804A6663A_year" class="scfDateSelectorShortLabelYear">Year</label>
														<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_E50FCB88E6A04B139BDF50A804A6663A_month" class="scfDateSelectorShortLabelMonth">Month</label>
														<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_E50FCB88E6A04B139BDF50A804A6663A_day" class="scfDateSelectorShortLabelDay">Day</label>
														<select name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_E50FCB88E6A04B139BDF50A804A6663A_year" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_E50FCB88E6A04B139BDF50A804A6663A_year" class="scfDateSelectorYear" onclick="javascript:return $scw.webform.controls.updateDateSelector(this);">
															<option value="2000">2000</option>
															<option value="2001">2001</option>
															<option value="2002">2002</option>
															<option value="2003">2003</option>
															<option value="2004">2004</option>
															<option value="2005">2005</option>
															<option value="2006">2006</option>
															<option value="2007">2007</option>
															<option value="2008">2008</option>
															<option value="2009">2009</option>
															<option value="2010">2010</option>
															<option value="2011">2011</option>
															<option value="2012">2012</option>
															<option selected="selected" value="2013">2013</option>
															<option value="2014">2014</option>
														</select>
														<select name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_E50FCB88E6A04B139BDF50A804A6663A_month" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_E50FCB88E6A04B139BDF50A804A6663A_month" class="scfDateSelectorMonth" onclick="javascript:return $scw.webform.controls.updateDateSelector(this);">
															<option value="1">January</option>
															<option value="2">February</option>
															<option value="3">March</option>
															<option selected="selected" value="4">April</option>
															<option value="5">May</option>
															<option value="6">June</option>
															<option value="7">July</option>
															<option value="8">August</option>
															<option value="9">September</option>
															<option value="10">October</option>
															<option value="11">November</option>
															<option value="12">December</option>
														</select>
														<select name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_E50FCB88E6A04B139BDF50A804A6663A_day" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_E50FCB88E6A04B139BDF50A804A6663A_day" class="scfDateSelectorDay" onclick="javascript:return $scw.webform.controls.updateDateSelector(this);">
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
															<option value="7">7</option>
															<option value="8">8</option>
															<option value="9">9</option>
															<option value="10">10</option>
															<option value="11">11</option>
															<option value="12">12</option>
															<option value="13">13</option>
															<option value="14">14</option>
															<option value="15">15</option>
															<option value="16">16</option>
															<option value="17">17</option>
															<option value="18">18</option>
															<option value="19">19</option>
															<option value="20">20</option>
															<option value="21">21</option>
															<option value="22">22</option>
															<option value="23">23</option>
															<option value="24">24</option>
															<option value="25">25</option>
															<option value="26">26</option>
															<option value="27">27</option>
															<option value="28">28</option>
															<option value="29">29</option>
															<option selected="selected" value="30">30</option>
														</select>
														<span class="scfDateSelectorUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_E50FCB88E6A04B139BDF50A804A6663AE1FD76F9111E447085C46006EDEF8134_validator" class="startdate.20000101T120000 enddate.20140430T000000 scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bE50FCB88-E6A0-4B13-9BDF-50A804A6663A%7d inner.1" style="display:none;">Date must be later than Saturday, January 01, 2000 and before Wednesday, April 30, 2014.</span>
													</div>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_29DDB4D14B82413083EFAFD0F3172F60_scope" class="scfDatePickerBorder DateFormat.yy-MM-dd startDate.20000101T120000 endDate.20140430T000000 fieldid.%7b29DDB4D1-4B82-4130-83EF-AFD0F3172F60%7d name.Date+picker">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_29DDB4D14B82413083EFAFD0F3172F60" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_29DDB4D14B82413083EFAFD0F3172F60_text" class="scfDatePickerLabel">Date picker</label>
													<div class="scfDatePickerGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_29DDB4D14B82413083EFAFD0F3172F60" type="text" value="13-04-30" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_29DDB4D14B82413083EFAFD0F3172F60" class="scfDatePickerTextBox hasDatepicker" readonly="">
														<span class="ui-icon ui-icon-calendar ui-icon-datepicker"></span>
														<span class="scfDatePickerUsefulInfo" style="display:none;"></span>
													</div>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_364BDF5FC34B4E1C92AC973AE37DFEBCscope" class="scfFileUploadBorder fieldid.%7b364BDF5F-C34B-4E1C-92AC-973AE37DFEBC%7d name.File+upload">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_364BDF5FC34B4E1C92AC973AE37DFEBC" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_364BDF5FC34B4E1C92AC973AE37DFEBCtext" class="scfFileUploadLabel">File upload</label>
													<div class="scfFileUploadGeneralPanel">
														<input type="file" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_364BDF5FC34B4E1C92AC973AE37DFEBC" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_364BDF5FC34B4E1C92AC973AE37DFEBC" class="scfFileUpload">
														<span class="scfFileUploadUsefulInfo" style="display:none;"></span>
													</div>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_AAB805BE14F7495DA2146263952CD8EFscope" class="scfDropListBorder fieldid.%7bAAB805BE-14F7-495D-A214-6263952CD8EF%7d name.Drop+list">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_AAB805BE14F7495DA2146263952CD8EF" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_AAB805BE14F7495DA2146263952CD8EFtext" class="scfDropListLabel">Drop list</label>
													<div class="scfDropListGeneralPanel">
														<select name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_AAB805BE14F7495DA2146263952CD8EF" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_AAB805BE14F7495DA2146263952CD8EF" class="scfDropList">
															<option value=""></option>
														</select>
														<span class="scfDropListUsefulInfo" style="display:none;"></span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_A1F49B192D9B4932AEED312C8B0410B5scope" class="scfCheckboxBorder fieldid.%7bA1F49B19-2D9B-4932-AEED-312C8B0410B5%7d name.Check+box">
													<span class="scfCheckbox">
														<input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_A1F49B192D9B4932AEED312C8B0410B5" type="checkbox" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_A1F49B192D9B4932AEED312C8B0410B5">
														<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_A1F49B192D9B4932AEED312C8B0410B5">Check box. Long line Long line test Long line test Long line test Long line test Long line test Long line test Long line test test Long line test Long line test Long line test Long line test Long line test</label>
													</span>
													<span class="scfCheckboxUsefulInfo" style="display:none;"></span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7A55983A6099455E8AADECDD597DC5CCscope" class="scfListBoxBorder fieldid.%7b7A55983A-6099-455E-8AAD-ECDD597DC5CC%7d name.List+box">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7A55983A6099455E8AADECDD597DC5CC" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7A55983A6099455E8AADECDD597DC5CCtext" class="scfListBoxLabel">List box</label>
													<div class="scfListBoxGeneralPanel">
														<select size="4" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_7A55983A6099455E8AADECDD597DC5CC" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7A55983A6099455E8AADECDD597DC5CC" class="scfListBox"></select>
														<span class="scfListBoxUsefulInfo" style="display:none;"></span>
													</div>
												</div>
											</div>
										</fieldset>
									</div>
									<div>
										<fieldset class="scfSectionBorderAsFieldSet">
											<legend class="scfSectionLegend">
												Section 1
											</legend>
											<div class="scfSectionContent">
												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451" class="scfRadioButtonListBorder fieldid.%7b142886E5-71B2-4B8D-8C12-690C66651451%7d name.Radio+button+list">
													<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451text" class="scfRadioButtonListLabel">Radio button list</span>
													<div class="scfRadioButtonListGeneralPanel">
														<table id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451scope" class="scfRadioButtonList">
															<tbody><tr>
																<td><input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451scope_0" type="radio" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_142886E571B24B8D8C12690C66651451scope" value="first"><label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451scope_0">first<br/>second</label></td>
															</tr><tr>
																<td><input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451scope_1" type="radio" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_142886E571B24B8D8C12690C66651451scope" value="second"><label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451scope_1">second</label></td>
															</tr><tr>
																<td><input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451scope_2" type="radio" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_142886E571B24B8D8C12690C66651451scope" value="third"><label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451scope_2">third</label></td>
															</tr>
														</tbody></table><span class="scfRadioButtonListUsefulInfo" style="display:none;"></span>
													</div>
												</div>
												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1" class="scfCheckBoxListBorder fieldid.%7bDC7BC868-43CD-40E0-A491-51F63DC557F1%7d name.Check+box+list">
													<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1text" class="scfCheckBoxListLabel">Check box list</span><div class="scfCheckBoxListGeneralPanel">
														<table id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list" class="scfCheckBoxList">
															<tbody><tr>
																<td><input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list_0" type="checkbox" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_DC7BC86843CD40E0A49151F63DC557F1list$0" value="first"><label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list_0">first<br/>second</label></td>
															</tr><tr>
																<td><input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list_1" type="checkbox" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_DC7BC86843CD40E0A49151F63DC557F1list$1" value="second"><label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list_1">second</label></td>
															</tr><tr>
																<td><input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list_2" type="checkbox" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_DC7BC86843CD40E0A49151F63DC557F1list$2" value="third"><label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_DC7BC86843CD40E0A49151F63DC557F1list_2">third</label></td>
															</tr>
														</tbody></table><span class="scfCheckBoxListUsefulInfo" style="display:none;"></span>
													</div>
												</div>
												<div class="scfCaptcha fieldid.%7b55A59538-2B41-493C-9A97-3407AA4CB8F4%7d name.captcha validationgroup.form_6A9985C58EFE43209A66B4A98BDE5EA0_submit" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border">
													<div class="scfCaptchTop">
														<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCodeBorder" class="scfCaptchaBorder">
															<span class="scfCaptchaLabel">&nbsp;</span>
															<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_Panel1" class="scfCaptchaGeneralPanel">
																<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCodeText" class="scfCaptchaLabel" style="display:none;"> </span>
																<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCodePanel" class="scfCaptchaLimitGeneralPanel">
																	<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode">
																		<table cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
																			<tbody>
																				<tr>
																					<td>
																						<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode_captchaCodeim">
																							<input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode_ctl01_uniqID" type="hidden" name="uniqID" value="91c10441-0f66-4f4b-888f-a63fc3692c8f">
																							<div style="background-color:White;">
																								<img src="/assets/fuji-xerox/images/dummy/captcha/captcha.jpg" width="180" height="50" alt="Captcha"/>
																							</div>
																						</div>
																					</td>
																					<td>
																						<input type="image" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$captchaCode$ctl02" title="Display another text." src="/assets/fuji-xerox/images/dummy/captcha/refresh.png" alt="Display another text." onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$captchaCode$ctl02&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))"><br>
																						<input type="image" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$captchaCode$captchaCodepb" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode_captchaCodepb" title="Play audio version of text." src="/assets/fuji-xerox/images/dummy/captcha/loudspeaker.png" alt="Play audio version of text." onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$captchaCode$captchaCodepb&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" href="CaptchaAudio.axd?guid=91c10441-0f66-4f4b-888f-a63fc3692c8f">
																					</td>
																				</tr>
																			</tbody>
																		</table>
																		<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode_captchaCodeph"></div>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div>
														<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaTextBorder" class="scfCaptchaBorder">
															<span class="scfCaptchaLabel">&nbsp;</span>
															<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaTextPanel" class="scfCaptchaGeneralPanel">
																<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_field_55A595382B41493C9A973407AA4CB8F4" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaTextTitle" class="scfCaptchaLabelText">captcha</label>
																<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchLimitTextPanel" class="scfCaptchaLimitGeneralPanel">
																	<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchStrongTextPanel" class="scfCaptchStrongTextPanel">
																		<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$field_55A595382B41493C9A973407AA4CB8F4" type="text" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_field_55A595382B41493C9A973407AA4CB8F4" class="scfCaptchaTextBox scWfmPassword">
																		<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaTextHelp" class="scfCaptchaUsefulInfo" style="display:none"></span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</fieldset>
									</div>
								</div>
								<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_footer" class="scfFooterBorder">
									<p>Footer Text</p>
								</div>
								<div class="scfSubmitButtonBorder">
									<input type="submit" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$form_6A9985C58EFE43209A66B4A98BDE5EA0_submit" value="Submit" onclick="$scw.webform.lastSubmit = this.id;WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;form_6A9985C58EFE43209A66B4A98BDE5EA0$form_6A9985C58EFE43209A66B4A98BDE5EA0_submit&quot;, &quot;&quot;, true, &quot;form_6A9985C58EFE43209A66B4A98BDE5EA0_submit&quot;, &quot;&quot;, false, false));$scw.webform.validators.setFocusToFirstNotValid('form_6A9985C58EFE43209A66B4A98BDE5EA0_submit')" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_form_6A9985C58EFE43209A66B4A98BDE5EA0_submit" class="scfSubmitButton">
								</div>


							</div> -->
							<form action="/form/Index?wffm.FormItemId=b8043c8c-4c61-4547-9258-98dab359c5ce&amp;wffm.Id=b274ccc6-69a1-41f3-bf1d-37dfbaff3f7d" class="text-left" data-wffm="{B8043C8C-4C61-4547-9258-98DAB359C5CE}" data-wffm-ajax="True" enctype="multipart/form-data" id="wffmb274ccc669a141f3bf1d37dfbaff3f7d" method="post" role="form" novalidate="novalidate"><input name="__RequestVerificationToken" type="hidden" value="OVuLOAZDGCZfpcOofT5HJ9dgk4P-BgyxitS2JeC1KkU23MkhGQvWviQB447IuCee1peTIeOfYRLrWz7VS_F-yY3qK39ddujifaYuK50UpP81"><input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Id" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Id" type="hidden" value="b274ccc6-69a1-41f3-bf1d-37dfbaff3f7d"><input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_FormItemId" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.FormItemId" type="hidden" value="{B8043C8C-4C61-4547-9258-98DAB359C5CE}"><div class="page-header"><h1>Customer feedback</h1></div>    <div class="has-error has-feedback">
								<div class="validation-summary-valid" data-valmsg-summary="true"><ul class="list-group"><li style="display:none"></li>
								</ul></div>
							</div>
							<div class="required-field  form-group has-feedback"><input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_0__Id" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[0].Id" type="hidden" value="{3D5CB88C-73A7-4417-AEA7-92820E2709A6}"><label class="control-label" for="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_0__Value">Nature of Enquiry</label><select class="form-control" data-val="true" data-val-required="The Nature of Enquiry field is required." data-val-required-tracking="{7E86B2F5-ACEC-4C60-8922-4EB5AE5D9874}" id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_0__Value" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[0].Value"><option value=""></option>
								<option value="phobe.fong@sgp.fujixerox.com; cecilia.yap@sgp.fujixerox.com">Compliments / Complaints</option>
								<option value="workscontrollers@sgp.fujixerox.com">Technical Support</option>
								<option value="terence.chia@sgp.fujixerox.com">Others</option>
							</select><span class="field-validation-valid help-block" data-valmsg-for="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[0].Value" data-valmsg-replace="true"></span></div><div class="required-field  form-group has-feedback"><input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_1__Id" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[1].Id" type="hidden" value="{40EA426F-0D9F-4B49-A749-3BB1AD5C4BAD}"><label class="control-label" for="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_1__Value">Name</label><input class=" form-control text-box single-line" data-val="true" data-val-length="The Name field must be a string with a minimum length of 0 and a maximum length of 256." data-val-length-max="256" data-val-required="The Name field is required." data-val-required-tracking="{7E86B2F5-ACEC-4C60-8922-4EB5AE5D9874}" id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_1__Value" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[1].Value" placeholder="" style="" type="text" value=""><span class="field-validation-valid help-block" data-valmsg-for="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[1].Value" data-valmsg-replace="true"></span></div><div class="required-field  form-group has-feedback"><input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_2__Id" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[2].Id" type="hidden" value="{7DE389A4-5643-4265-9251-5E808BC5E4E6}"><label class="control-label" for="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_2__Value">Email</label><input class=" form-control text-box single-line" data-val="true" data-val-length="The Email field must be a string with a minimum length of 0 and a maximum length of 256." data-val-length-max="256" data-val-multiregex="The value of the Email field is not valid." data-val-multiregex-pattern="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$" data-val-multiregex-tracking="{F3D7B20C-675C-4707-84CC-5E5B4481B0EE}" data-val-regex="The Email field contains an invalid email address." data-val-regex-pattern="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,17}$" data-val-required="The Email field is required." data-val-required-tracking="{7E86B2F5-ACEC-4C60-8922-4EB5AE5D9874}" id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_2__Value" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[2].Value" placeholder="" style="" type="email" value=""><span class="field-validation-valid help-block" data-valmsg-for="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[2].Value" data-valmsg-replace="true"></span></div><div class="required-field  form-group has-feedback"><input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_3__Id" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[3].Id" type="hidden" value="{A27AAC1E-A299-4571-8736-E385B395122A}"><label class="control-label" for="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_3__Value">Phone</label><input class=" form-control text-box single-line" data-val="true" data-val-length="The Phone field must be a string with a minimum length of 0 and a maximum length of 256." data-val-length-max="256" data-val-regex="The value of the Phone field is not valid." data-val-regex-pattern="^\+?\s{0,}\d{0,}\s{0,}(\(\s{0,}\d{1,}\s{0,}\)\s{0,}|\d{0,}\s{0,}-?\s{0,})\d{2,}\s{0,}-?\s{0,}\d{2,}\s{0,}(-?\s{0,}\d{2,}|\s{0,})\s{0,}$" data-val-regex-tracking="{844BBD40-91F6-42CE-8823-5EA4D089ECA2}" data-val-required="The Phone field is required." data-val-required-tracking="{7E86B2F5-ACEC-4C60-8922-4EB5AE5D9874}" id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_3__Value" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[3].Value" placeholder="" style="" type="tel" value=""><span class="field-validation-valid help-block" data-valmsg-for="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[3].Value" data-valmsg-replace="true"></span></div><div class="form-group has-feedback"><input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_4__Id" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[4].Id" type="hidden" value="{C38DA02C-B1C7-4902-815C-501E78F0666D}"><label class="control-label" for="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_4__Value">Fax</label><input class=" form-control text-box single-line" data-val="true" data-val-length="The Fax field must be a string with a minimum length of 0 and a maximum length of 256." data-val-length-max="256" data-val-regex="The value of the Fax field is not valid." data-val-regex-pattern="^\+?\s{0,}\d{0,}\s{0,}(\(\s{0,}\d{1,}\s{0,}\)\s{0,}|\d{0,}\s{0,}-?\s{0,})\d{2,}\s{0,}-?\s{0,}\d{2,}\s{0,}(-?\s{0,}\d{2,}|\s{0,})\s{0,}$" data-val-regex-tracking="{844BBD40-91F6-42CE-8823-5EA4D089ECA2}" id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_4__Value" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[4].Value" placeholder="" style="" type="tel" value=""><span class="field-validation-valid help-block" data-valmsg-for="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[4].Value" data-valmsg-replace="true"></span></div><div class="required-field  form-group has-feedback"><input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_5__Id" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[5].Id" type="hidden" value="{EB793361-1DED-492E-887F-8738EB375076}"><label class="control-label" for="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_5__Value">Company</label><input class=" form-control text-box single-line" data-val="true" data-val-length="The Company field must be a string with a minimum length of 0 and a maximum length of 256." data-val-length-max="256" data-val-required="The Company field is required." data-val-required-tracking="{7E86B2F5-ACEC-4C60-8922-4EB5AE5D9874}" id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_5__Value" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[5].Value" placeholder="" style="" type="text" value=""><span class="field-validation-valid help-block" data-valmsg-for="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[5].Value" data-valmsg-replace="true"></span></div><div class="form-group has-feedback"><input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_6__Id" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[6].Id" type="hidden" value="{23E307A8-4F31-4AAE-90A5-9F207E3F230F}"><label class="control-label" for="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_6__Value">Your feedback / enquiry / problem / description / event</label><textarea class="form-control" cols="1" data-val="true" data-val-length="The Your feedback / enquiry / problem / description / event field must be a string with a minimum length of 0 and a maximum length of 256." data-val-length-max="256" id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_6__Value" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[6].Value" rows="4"></textarea><span class="field-validation-valid help-block" data-valmsg-for="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[6].Value" data-valmsg-replace="true"></span></div><div class="form-group has-feedback"><input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_7__Id" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[7].Id" type="hidden" value="{3F8AEE53-972C-4BF4-84C3-BFDDF13A7FD3}"><label class="control-label" for="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_7__Value">Can we publish your feedback?</label>        <div class="radio">
							<label>
								<input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_7__Value" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[7].Value" type="radio" value="Yes">

								Yes
							</label>
						</div>
						<div class="radio">
							<label>
								<input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_7__Value" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[7].Value" type="radio" value="No">

								No
							</label>
						</div>
						<span class="field-validation-valid help-block" data-valmsg-for="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[7].Value" data-valmsg-replace="true"></span></div><div class="required-field  form-group has-feedback"><input id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_8__Id" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[8].Id" type="hidden" value="{BB06ECD9-A329-4566-A2E9-0383D1B25179}"><label class="control-label" for="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_8__Value">Please enter the letters shown in the image</label><input data-val="true" data-val-length="The Please enter the letters shown in the image field must be a string with a minimum length of 0 and a maximum length of 0." data-val-length-max="0" data-val-required="The Please enter the letters shown in the image field is required." data-val-required-tracking="{7E86B2F5-ACEC-4C60-8922-4EB5AE5D9874}" id="wffmb274ccc669a141f3bf1d37dfbaff3f7d_Sections_0__Fields_8__Value" name="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[8].Value" type="hidden" value=""><div class="g-recaptcha" data-sitekey="6Lcxlf4SAAAAAOr52tOrgmcedW7OZhWw2EiZFH7D" data-theme="light" data-type="image"><div><div style="width: 304px; height: 78px;"><iframe src="https://www.google.com/recaptcha/api2/anchor?k=6Lcxlf4SAAAAAOr52tOrgmcedW7OZhWw2EiZFH7D&amp;co=aHR0cDovL2Z4Ojgw&amp;hl=en&amp;type=image&amp;v=r20160615154650&amp;theme=light&amp;size=normal&amp;cb=47dk9emurta2" title="recaptcha widget" width="304" height="78" role="presentation" frameborder="0" scrolling="no" name="undefined"></iframe></div><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea></div></div><script src="https://www.google.com/recaptcha/api.js"></script><span class="field-validation-valid help-block" data-valmsg-for="wffmb274ccc669a141f3bf1d37dfbaff3f7d.Sections[0].Fields[8].Value" data-valmsg-replace="true"></span></div>
						<div class="form-submit-border"><input class="btn  btn-default" type="submit" value="Submit"></div></form>

					</div>

				</div>
			</div>
		</article>
		<!-- End: Article -->

		<?php include('footer.php') ?>
		<?php include('side-menu.php') ?>
		<?php include('bottom.php') ?>
	</body>
	</html>