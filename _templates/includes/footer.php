<!-- Site Footer -->
<footer class = "site-footer">
	<div class = "wingspan">

		<!-- Mega Footer -->
		<div class = "mega-footer row-col-15 hidden-xs">
			<div class = "col-sm-3">
				<h5><a href = "/solutions-and-services">Solutions &amp; Services</a></h5>

				<ul>
					<li><a href = "solutions-and-services/office-solutions/">Office Solutions</a></li>
					<li><a href = "/solutions-and-services/production-solutions/">Production Solutions</a></li>
					<li><a href = "/solutions-and-services/enterprise-solutions/">Enterprise Solutions</a></li>
					<li><a href = "/solutions-and-services/global-services/">Global Services</a></li>
					<li><a href = "/solutions-and-services/commercial-financing-solution-services/">Commercial Financing Solution Services</a></li>
				</ul>
			</div>

			<div class = "col-sm-3">
				<h5><a href = "/products">Products</a></h5>

				<ul>
					<li><a href = "http://localhost/products/?listing=printers">Printers</a></li>
					<li><a href = "http://localhost/products/?listing=supplies">Supplies</a></li>
					<li><a href = "http://localhost/products/?listing=software">Software</a></li>
				</ul>
			</div>

			<div class = "col-sm-3">
				<h5><a href = "http://onlinesupport.fujixerox.com/setupSupport.do?cid=1&lang_code=en&ctry_code=SG">Support &amp; Drivers</a></h5>

				<ul>
					<li><a href = "https://www.fujixerox.com.sg/contact/general-information/reseller-partners/authorized-resellers">Technical Support</a></li>
					<li><a href = "https://onlinesupport.fujixerox.com/setupRelocation.do?cid=1&lang_code=en&ctry_code=SG">Relocate Machine Request</a></li>
					<li><a href = "https://www.fujixerox.com.sg/support-and-drivers/tools-and-guides/quick-reference-guide">Quick Reference Guide</a></li>
					<li><a href = "https://www.fujixerox.com.sg/support-and-drivers/tools-and-guides/authentication-label">Authentication Label</a></li>
					<li><a href = "http://onlinesupport.fujixerox.com/setupSupport.do?cid=1&lang_code=en&ctry_code=SG">Self Help</a></li>
					<li><a href = "http://www.fujixeroxprinters.com.sg/en/support/warrantyregistration.aspx">Warranty Registration</a></li>
					<li><a href = "http://onlinesupport.fujixerox.com/setupSuppTool.do?cid=1&lang_code=en&ctry_code=SG">Support Tools</a></li>
					<li><a href = "http://onlinesupport.fujixerox.com/setupDriverList.do?cid=1&lang_code=en&ctry_code=SG">Downloads</a></li>
				</ul>
			</div>

			<div class = "col-sm-3">
				<h5><a href = "/company">Company</a></h5>

				<ul>
					<li><a href = "/company/about-fuji-xerox-singapore">About Fuji Xerox Singapore</a></li>
					<li><a href = "#">Testimonials</a></li>
					<li><a href = "/company/management-team">Management Team</a></li>
					<li><a href = "/company/awards-achievements">Awards &amp; Achievements</a></li>
					<li><a href = "/company/substainable-future">Towards a Sustainable Future</a></li>
					<li><a href = "#">Fuji Xerox Dream Service</a></li>
					<li><a href = "/company/success-stories">Success Stories</a></li>
					<li><a href = "/company/newsroom">Newsroom</a></li>
					<li><a href = "/company/career-with-us">Career with Us</a></li>
				</ul>
			</div>

			<div class = "col-sm-3">
				<h5><a href = "/contact">Contact Us</a></h5>

				<ul>
					<li><a href = "#">Contact Details</a></li>
					<li><a href = "#">Authorised Reseller</a></li>
				</ul>
			</div>
		</div>
		<!-- End: Mega Footer -->

		<!-- Footer Bottom -->
		<div class = "footer-bottom">
			<div class = "social-links">
				<span class = "label">Follow Us</span>
				<ul>
					<li><a href = "#" title = "Facebook"><i class = "icon-facebook"></i><span class = "sr-only">Facebook</span></a></li>
					<li><a href = "#" title = "Linkedin"><i class = "icon-linkedin"></i><span class = "sr-only">Linkedin</span></a></li>
					<li><a href = "#" title = "Twitter"><i class = "icon-twitter"></i><span class = "sr-only">Twitter</span></a></li>
					<li><a href = "#" title = "Youtube"><i class = "icon-youtube"></i><span class = "sr-only">Youtube</span></a></li>
				</ul>
			</div>

			<div class = "footer-info">
				<ul class = "footer-info__links">
					<li><a href = "#">Privacy Policy</a></li>
					<li><a href = "#">Terms of Use</a></li>
				</ul>

				<div class = "footer-info__copyright">
					<p>&copy; Fuji Xerox Co. Ltd. All rights reserved. </p>
					<p>Xerox, Xerox and Design, as well as Fuji Xerox and Design are registered trademarks or trademarks of Xerox Corporation in Japan and/or other countries.</p>
				</div>
			</div>


		</div>
		<!-- End: Footer Bottom -->
	</div>
</footer>
<!-- End: Site Footer -->