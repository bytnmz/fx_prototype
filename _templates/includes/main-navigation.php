<!-- Main Navigation -->
<div class = "main-navigation">
	<nav class = "wingspan">

		<!-- Main Navigation Menu -->
		<ul class = "main-navigation__menu row-col-12">


			<li class = "parent col-sm-3">
				<a href = "/solutions-and-services">Solutions &amp; Services</a>

				<!-- Flyout -->
				<div class = "flyout full-width">
					<div class = "row-col-12">
						<div class = "col-sm-4">
							<ul>
								<li>
									<a href = "/solutions-and-services/office-solutions">Office Solutions</a>

									<ul>
										<li><a href = "/solutions-and-services/office-solutions/print-management-solutions/">Print Management Solutions</a></li>
										<li><a href = "#">Document Security Solutions</a></li>
										<li><a href = "/solutions-and-services/office-solutions/office-automation-solutions/">Office Automation Solutions</a></li>
										<li><a href = "#">Content Management Solutions</a></li>
										<li><a href = "#">Cloud &amp; Mobility Solutions</a></li>
										<li><a href = "#">Device Remote Support Services</a></li>
									</ul>
								</li>

								<li>
									<a href = "/solutions-and-services/production-solutions">Production Solutions</a>

									<ul>
										<li><a href = "/solutions-and-services/production-solutions/customer-communication-management">Customer Communication Management (CCM) Solutions</a></li>
										<li><a href = "#">Print Workflow Automation</a></li>
										<li><a href = "#">Color Management Solutions &amp; Services</a></li>
										<li><a href = "/solutions-and-services/production-solutions/print-management-information-systems">Print Management Information Systems (MIS)</a></li>
										<li><a href = "#">Packaging Solutions</a></li>
										<li><a href = "#">Business Development Consultancy Services</a></li>
									</ul>
								</li>
							</ul>
						</div>

						<div class = "col-sm-4 push-left-25">
							<ul>
								<li>
									<a href = "/solutions-and-services/enterprise-solutions">Enterprise Solutions</a>

									<ul>
										<li><a href = "#">Business Process Management (BPM)</a></li>
										<li><a href = "#">Enterprise Content Management (ECM)</a></li>
										<li><a href = "#">Knowledge Management Solutions</a></li>
										<li><a href = "#">Enterprise Collaboration Solutions</a></li>
										<li><a href = "#">Infrastructure Solutions</a></li>
										<li><a href = "#">System Integration Services</a></li>
										<li><a href = "/solutions-and-services/enterprise-solutions/it-services/">IT Services</a></li>
									</ul>
								</li>

								<li>
									<a href = "/solutions-and-services/global-services">Global Services</a>

									<ul>
										<li><a href ="#">Managed Document/Print Services</a></li>
										<li><a href ="/solutions-and-services/global-services/marketing-communication-services">Marketing Communications Services</a></li>
										<li><a href ="#">Imaging &amp; Document Management Services</a></li>
										<li><a href ="#">Short-term Rental</a></li>
									</ul>
								</li>
							</ul>
						</div>

						<div class = "col-sm-4 push-left-25">
							<ul>
								<li>
									<a href = "/solutions-and-services/commercial-financing-solution-services/">Commercial Financing Solution Services</a>
								</li>
								<li>
									<a href = "#">Industries</a>

									<ul class = "sitemap__mega-menu-links--lvl3">
										<li><a href = "#">Aerospace</a></li>
										<li><a href = "#">Architecture, Engineering, and Construction</a></li>
										<li><a href = "#">Education</a></li>
										<li><a href = "#">Finance</a></li>
										<li><a href = "#">Government</a></li>
										<li><a href = "#">Healthcare</a></li>
										<li><a href = "#">Insurance</a></li>
										<li><a href = "#">Logistics</a></li>
									</ul>
								</li>
							</ul>
						</div>

					</div>
				</div>
				<!-- End: Flyout -->
			</li>

			<li class = "parent col-sm-3 short-flyout-menu">
				<a href = "/products/">Products</a>

				<!-- Flyout -->
				<div class = "flyout">
					<ul>
						<li>
							<a href="/products/?listing=printer">Printers</a>
						</li>
						<li>
							<a href="/products/?listing=supplies">Supplies</a>
						</li>
						<li>
							<a href="/products/?listing=software">Softwares</a>
						</li>
					</ul>
				</div>
				<!-- End: Flyout -->
			</li>

			<li class = "parent col-sm-2">
				<a href = "http://onlinesupport.fujixerox.com/setupSupport.do?cid=1&lang_code=en&ctry_code=SG">Support &amp; Drivers</a>

				<!-- Flyout -->
				<div class = "flyout half-width">
					<div class = "row-col-12">
						<div class = "col-sm-6">
							<ul>
								<li>
									<ul>
										<li><a href = "https://www.fujixerox.com.sg/contact/general-information/reseller-partners/authorized-resellers">Technical Support</a></li>
										<li><a href = "https://onlinesupport.fujixerox.com/setupRelocation.do?cid=1&lang_code=en&ctry_code=SG">Relocate Machine Request</a></li>
										<li><a href = "https://www.fujixerox.com.sg/support-and-drivers/tools-and-guides/quick-reference-guide">Quick Reference Guide</a></li>
										<li><a href = "https://www.fujixerox.com.sg/support-and-drivers/tools-and-guides/authentication-label">Authentication Label</a></li>
										
									</ul>
								</li>
							</ul>
						</div>

						<div class = "col-sm-6 push-left-15">
							<ul>
								<li>
									<ul>
										<li><a href = "http://onlinesupport.fujixerox.com/setupSupport.do?cid=1&lang_code=en&ctry_code=SG">Self Help</a></li>
										<li><a href = "http://www.fujixeroxprinters.com.sg/en/support/warrantyregistration.aspx">Warranty Registration</a></li>
										<li><a href = "http://onlinesupport.fujixerox.com/setupSuppTool.do?cid=1&lang_code=en&ctry_code=SG">Support Tools</a></li>
										<li><a href = "http://onlinesupport.fujixerox.com/setupDriverList.do?cid=1&lang_code=en&ctry_code=SG">Downloads</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- End: Flyout -->
			</li>
			<li class = "parent col-sm-2">
				<a href = "/company">Company</a>

				<!-- Flyout -->
				<div class = "flyout half-width">
					<div class = "row-col-12">
						<div class = "col-sm-6">
							<ul>
								<li>
									<span class = "category-title">Company</span>

									<ul>
										<li><a href = "/company/about-fuji-xerox-singapore">About Fuji Xerox Singapore</a></li>
										<li><a href = "/company/management-team">Management Team</a></li>
										<li><a href = "/company/awards-achievements">Awards &amp; Achievements</a></li>
										<li><a href = "/company/substainable-future">Towards a Sustainable Future</a></li>
									</ul>
								</li>
							</ul>
						</div>

						<div class = "col-sm-6 push-left-15">
							<ul>
								<li>
									<ul class = "no-parent">
										<li><a href = "#">Fuji Xerox Dream Service</a></li>
										<li><a href = "/company/success-stories">Success Stories</a></li>
										<li><a href = "/company/newsroom">Newsroom</a></li>
										<li><a href = "/company/career-with-us">Career with Us</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- End: Flyout -->
			</li>
			<li class = "parent col-sm-2">
				<a href = "/contact">Contact</a>

				<!-- Flyout -->
				<div class = "flyout half-width">
					<div class = "row-col-12">
						<div class = "col-sm-6">
							<ul>
								<li>
									<span class = "category-title">General Information</span>

									<ul>
										<li><a href = "#">Contact Details</a></li>
										<li><a href = "#">Authorised Resellers</a></li>
									</ul>
								</li>
							</ul>
						</div>

						<div class = "col-sm-6 push-left-15">
							<ul>
								<li>
									<span class = "category-title">Online Forms</span>

									<ul>
										<li><a href = "#">Request Pricing</a></li>
										<li><a href = "/contact/customer-feedback">Customer Feedback</a></li>
										<li><a href = "#">Technical Support</a></li>
										<li><a href = "#">Submit Meter Readings</a></li>
										<li><a href = "#">Toner Ordering</a></li>
										<li><a href = "#">Customer Software Training</a></li>
										<li><a href = "#">Customer Rental Programme</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- End: Flyout -->
			</li>
		</ul>
		<!-- End: Main Navigation Menu -->
	</nav>
</div>
<!-- End: Main Navigation -->