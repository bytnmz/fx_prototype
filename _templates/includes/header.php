<!-- Site Header -->
<header class = "site-header">
	<div class = "header-top wingspan">
		<div class = "site-logo">
			<?php if($primary === 0): ?>
				<h1><img src = "/assets/fuji-xerox/images/main-logo.png" alt = "Fuji Xerox"/></h1>
			<?php else: ?>
				<a href = "/"><img src = "/assets/fuji-xerox/images/main-logo.png" alt = "Fuji Xerox"/></a>
			<?php endif; ?>
			
		</div>

		<div class = "header-utilites">

			<div class = "header-utilities__search">
				<form action="/search" method="GET">
					<fieldset>
						<legend class = "sr-only">Site Search</legend>
						<label for="mod-search-searchword" class = "sr-only">Search</label>
						<input name="searchword" 
								id="mod-search-searchword"
								maxlength="20"
								class="search inputbox search-query"
								type="text"
								size="20"
								value="Search..."
								onblur="if (this.value=='') this.value='Search...';"
								onfocus="if (this.value=='Search...') this.value='';">
						<input type="hidden" name="task" value="search">
					</fieldset>
					<button type="submit" class="btn-search" onclick="this.form.searchword.focus();">Search</button>
				</form>

			</div>

			<ul class="header-utilites__links">
				<li><a href="/sitemap">Sitemap</a></li>
				<li><a href="#">Blog</a></li>
				<li><a href="https://shop.fujixerox.com.sg/" target="_blank">eShop</a></li>
				<li class = "login"><a href = "#">Login</a></li>
			</ul>
		</div>
	</div>

</header>
<!-- End: Site Header -->