<!DOCTYPE html>
<!--[if IE 8]>      <html class="no-js is-ie lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>      <html class="no-js is-ie lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang = "en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $title; ?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Place favicon.ico in the root directory -->
	<link rel="stylesheet" href="/assets/fuji-xerox/css/fuji-xerox.css">
	<link rel="stylesheet" href="/assets/fuji-xerox/css/Default.css">
	<link media="print" rel="stylesheet" href="/assets/fuji-xerox/css/fuji-xerox-print.css">


	<!--[if lt IE 9 ]>
		<link rel="stylesheet" href="/assets/fuji-xerox/css/fuji-xerox-legacy.css">
		<link rel="stylesheet" href="/assets/fuji-xerox/css/ie.css">
	<![endif]-->

	<script src="/assets/fuji-xerox/js/vendor/modernizr.js"></script>
</head>