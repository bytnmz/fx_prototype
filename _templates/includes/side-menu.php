<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56efd9924678a455"></script>
<script type="text/javascript">
	var addthis_config = {
		data_ga_property: 'UA-xxxxxx-x',

	}
</script>

<!-- Side Menu -->
<div class = "side-menu">
	<ul>
		<li>
			<a href = "#notifications" class = "fancybox"><i class = "icon-notification"></i><span class = "label">Notification</span></a>
		</li>

		<li>
			<a href = "#enquiryForm" class = "modalbox"><i class = "icon-enquiry"></i><span class = "label">Enquiry</span></a>
		</li>

		<li>
			<a href = "#" class = "js-livechat" onclick="window.open('https://secure.livechatinc.com/licence/2559781/open_chat.cgi?groups=0','newChatWindow','height=520,width=530,top=' + (screen.height / 2 - 260) + ',left=' + (screen.width / 2 - 265) + ',resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');"><i class = "icon-chat"></i><span class = "label">Chat</span></a>
		</li>

		<li>
			<a href = "#" class="addthis_button_compact"><i class = "icon-share"></i><span class = "label">Share this page</span></a>
		</li>
	</ul>
</div>
<!-- End: Side Menu -->

<!-- Notifications -->
<div class = "notifications" id = "notifications">
	<h3>Notifications</h3>
	<ul>
		<li>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sem tellus, hendrerit quis libero vel, laoreet suscipit nisl. Vivamus a sem bibendum elit iaculis tristique ut et tellus.
		</li>
		<li>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sem tellus, hendrerit quis libero vel, laoreet suscipit nisl. Vivamus a sem bibendum elit iaculis tristique ut et tellus.
		</li>
	</ul>
</div>
<!-- End: Notifications -->

<!-- Talk To Us Form Modal -->
<div class = "modal" id = "enquiryForm">
	<div class = "wingspan">
		<div class = "modal-close-button">
			<a href = "#"><i class = "icon-cross"></i></a>
		</div>
		<div class = "modal-form">
			<div class = "modal-form__intro">
				<h3>Talk to us</h3>
				<p>Have a sales enquiry? Simply fill up our call back form and we will assign a sales representative to attend to your needs during our business hours between 8.30am - 5.30pm, Monday - Friday.</p>
				<p>We appreciate your time in filling up the required fields so we can handle your enquiry quickly and efficiently.</p>
			</div>
			<div class = "legend">
				Field marked with <span class = "required">*</span> are compulsory
			</div>
			<form>
				<fieldset>
					<legend class = "sr-only">Enquiry Form</legend>

					<input type = "hidden" name = "productId" id = "productId" value = ""/>

					<div class = "form-row row-col-12">
						<div class = "col-sm-6 push-right-10">
							<div class = "input-group-text">
								<label for = "productField">Find Out More About Product</label>
								<input type = "text" name = "productField" id = "productField" placeholder = "Product Name"/>
							</div>
						</div>
					</div>

					<div class = "form-row row-col-12">
						<div class = "col-sm-6 push-right-10">
							<div class = "input-group-select">
								<label for = "topicField">I'm interested in<span class = "required">*</span></label>
								<select class = "custom-select" id = "topicField" name = "topicField">
									<option>Select topic</option>
									<option>Topic 1</option>
									<option>Topic 2</option>
									<option>Topic 3</option>
								</select>
							</div>
						</div>
					</div>

					<div class = "form-row row-col-12">
						<div class = "col-sm-6 push-right-10">
							<div class = "input-group-text">
								<label for = "companyField">Company<span class = "required">*</span></label>
								<input type = "text" name = "companyField" id = "companyField" placeholder = "Company"/>
							</div>
						</div>
					</div>

					<div class = "form-row row-col-12">
						<div class = "col-sm-6 push-right-10">
							<div class = "input-group-text">
								<label for = "firstnameField">First Name<span class = "required">*</span></label>
								<input type = "text" name = "firstnameField" id = "firstnameField" placeholder = "First Name"/>
							</div>
						</div>

						<div class = "col-sm-6 push-left-10">
							<div class = "input-group-text">
								<label for = "lastnameField">Last Name<span class = "required">*</span></label>
								<input type = "text" name = "lastnameField" id = "lastnameField" placeholder = "Last Name"/>
							</div>
						</div>
					</div>

					<div class = "form-row row-col-12">
						<div class = "col-sm-6 push-right-10">
							<div class = "input-group-text">
								<label for = "jobTitleField">Job Title<span class = "required">*</span></label>
								<input type = "text" name = "jobTitleField" id = "jobTitleField" placeholder = "Job Title"/>
							</div>
						</div>
					</div>

					<div class = "form-row row-col-12">
						<div class = "col-sm-6 push-right-10">
							<div class = "input-group-text">
								<label for = "emailField">Business Email<span class = "required">*</span></label>
								<input type = "text" name = "emailField" id = "emailField" placeholder = "Email Address"/>
							</div>
						</div>

						<div class = "col-sm-6 push-left-10">
							<div class = "input-group-text">
								<label for = "phoneField">Business Phone<span class = "required">*</span></label>
								<input type = "text" name = "phoneField" id = "phoneField" placeholder = "Number"/>
							</div>
						</div>
					</div>

					<div class = "form-row">
						<div class = "input-group-textarea">
							<label for = "messageField">How can we assist you?</label>
							<textarea name = "messageField" id = "messageField" placeholder = "Type here..."></textarea>
						</div>
					</div>

					<div  class = "form-row row-col-12">
						<div class = "captcha-group col-sm-7 ">
							<div class = "input-group-captcha">
								<label for = "captchaField">Please enter the letters (a-z) shown in the image<span class = "required">*</span></label>
								<div class = "captcha-wrapper">
									<div class = "captcha-image">
										<img src = "/assets/fuji-xerox/images/content/captcha.png" alt = ""/>
									</div>
									<span class = "captcha-refresh">
										<img src = "/assets/fuji-xerox/images/content/captcha-refresh.png" alt = ""/>
									</span>
								</div>
								<input type = "text" name = "captchaField" id = "captchaField" placeholder = "Type here..."/>
							</div>
						</div>
					</div>

					<div class = "form-row row-col-12">
						<div class = "checkbox-group">
							<label><input type = "checkbox">I have read, understood and agreed to the full terms and confitions of the <a href = "https://www.fujixerox.com.sg/privacy-policy" target="_blank">privacy policy</a></label>
						</div>
					</div>
				</fieldset>

				<div class = "btn-group">
					<button type = "submit">Submit</button>
					<button type = "reset">Reset</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- End: Talk To Us -->

<!-- Product Enquiry Form -->
<div class = "modal" id = "productForm">
	<div class = "wingspan">
		<div class = "modal-close-button">
			<a href = "#"><i class = "icon-cross"></i></a>
		</div>
		<div class="modal-form"></div>
	</div>
</div>
<!-- End: Product Enquiry Form -->