<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Color Management Solutions & Services';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "/">Solutions &amp; Services</a></li>
			<li><a href = "/">Production Solutions</a></li>
			<li><span>Color Management Solutions &amp; Services</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4 col-md-3">
				<h1>Color Management Solutions &amp; Services</h1>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->

	<!-- Quick Links -->
	<div class = "quick-links">
		<div class = "wingspan">
			<!-- Dynamic Navigation - rr.articleQuicklinks.js -->
		</div>
	</div>
	<!-- End: Quick Links -->

	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 col-md-8 offset-sm-1 offset-md-2">
				<section class = "section">
					<a id = "" class = "anchor-link" data-label = "Overview"></a>
					<h2>Overview</h2>

					<p>Please your customer with accurate and consistent colours, that's the key to winning them over. But it's not easy because color can be a moving target. The number of variables is daunting.</p>

					<p>The good news: you can put this worry behind with Fuji Xerox's Confident Colour Programme.</p>

					<p>Our programme's goal is to help you take the uncertainty out of colour management, so repeatable colour can be achieved accurately, quickly and easily. This way, you can focus on other core business.</p>

					<h3>Why you should invest in Colour Management System</h3>

					<p>Ineffective colour management can mean higher costs and inefficiency to your business. Trying to meet your customer’s expectations in colour can be a complex and frustrating process when there is no benchmark and standardisation. This can further lead to declining customers and employee’s satisfaction. The long turnaround times can also cause a major impact on production and eventual loss of clientele.</p>

					<p>Achieve success by investing in an effective colour management system and eliminate all pain points.</p>
				</section>

				<section class = "section">
					<h2>Point at a glance</h2>
					<div class = "points">
							<ul>
								<li>
									<h3>Economic</h3>

									<!-- <div class = "icon">
										<image src = "/assets/fuji-xerox/images/icons/piggybank.png" alt = "" />
									</div> -->

									<ul>
										<li>Standardize &amp; improve workflow</li>
										<li>Eliminate errors &amp; reworks</li>
										<li>Minimise rejects from customers</li>
									</ul>
								</li>

								<li>
									<h3>Power</h3>

									<!-- <div class = "icon">
										<image src = "/assets/fuji-xerox/images/icons/handshake.png" alt = "" />
									</div> -->

									<ul>
										<li>Achieve accurate and consistent colours</li>
										<li>Deliver good quality prints</li>
									</ul>
								</li>

								<li>
									<h3>Performance</h3>

									<!-- <div class = "icon">
										<image src = "/assets/fuji-xerox/images/icons/chart.png" alt = "" />
									</div> -->

									<ul>
										<li>Gain international acceptance on print</li>
										<li>Quality</li>
										<li>Differentiate from competition</li>
										<li>Capture new market opportunities</li>
									</ul>
								</li>
							</ul>
						</div>
					</section>

				<section class = "section">
					<a id = "" class = "anchor-link" data-label = "How can we help"></a>
					<h2>How we can help in managing your colour issues?</h2>

					<ul>
						<li>We have a strong team of 7 Fogra Certified Digital Print Experts – the largest in the industry.</li>
						<li>We are the 1st in South East Asia to obtain certification from Fogra in Contract Proof Creation (CPC) and Process Standard Digital (PSD).</li>
					</ul>

					<h3>With our team of Fogra Certified Digital Print Experts, we can help you to:</h3>

					<ul>
						<li>Analyze colour and print related issues.</li>
						<li>Undertake corrective actions to align systems and processes against ISO standard.</li>
						<li>Prepare for FograCert Validation Print Creation (VPC), Contract Proof Creation (CPC), Process Standard Digital (PSD) and obtain certification from Fogra.</li>
						<li>Achieve a standard and systematic approach to consistent colour and quality prints.</li>
					</ul>
				</section>

				<section class = "section">
					<a id = "" class = "anchor-link" data-label = "Why Fuji Xerox"></a>
					<h2>Why Fuji Xerox</h2>
					<!-- <div class = "thumbnail-text-block row-col-12">

						<div class  = "col-sm-3 thumbnail">
							<image src = "/assets/fuji-xerox/images/solutions/square-photo.png" alt = "" class = "circle-image"/>
						</div>

						<div class = "text">
							<p>From cutting edge technology to value-added services, Fuji Xerox Singapore has just the right technology, workflow and professional team to address our customers’ business requirements and challenges.</p>
						</div>
					</div> -->

					<h3>In the aspect of colour management, Fuji Xerox Singapore has:</h3>

					<ul>
						<li>A comprehensive range of colour management solutions which include CGS Oris, EFI Colour Profiler Suite etc.</li>
						<li>Fleet of Fogra Certified Validation Printing System ranging from DC1450GA to iGen4 and.</li>
						<li>A team of 7 FograCert Digital Print Experts – the largest team in the industry. In addition, we are also the 1st in South East Asia to obtain certification from Fogra in Contract Proof Creation (CPC) and Process Standard Digital (PSD).</li>
					</ul>

					<table class = "stackable">
						<thead>
							<tr>
								<th>Programme</th>
								<th>Function</th>
								<th>Benefits</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<p>Colour Match</p>
								</td>

								<td>
									<ul>
										<li>System setup</li>
										<li>Manage colour output</li>
										<li>Assigning colour profile</li>
										<li>Colour match to a sample/reference file</li>
									</ul>
								</td>

								<td>
									<ul>
										<li>Achieve accurate and repeatable colours.</li>
										<li>Improve print quality and job turnaround time</li>
										<li>Reduce wastage and costs</li>
										<li>Consistent and matching colours between devices</li>
									</ul>
								</td>
							</tr>

							<tr>
								<td>
									<p>Colour Management Training</p>
								</td>

								<td>
									<ul>
										<li>Fundamental of Colour</li>
										<li>Colour manage workflow - Creative application</li>
										<li>Learn how to control colour from design to delivery</li>
									</ul>
								</td>

								<td>
									<ul>
										<li>Improved knowledge and skill set in colour management.</li>
										<li>Improve productivity</li>
									</ul>
								</td>
							</tr>

							<tr>
								<td>
									<p>FograCert Validation Print Creation</p>
								</td>

								<td>
									<ul>
										<li>Assessment &amp; process check</li>
										<li>Recommendation &amp; correction</li>
										<li>Verify the prints compliance to ISO 12647-8 (FograCert Validation Print Creation) & ISO 12647-7 (FograCert Contract Proof Creation)</li>
									</ul>
								</td>

								<td>
									<ul>
										<li>Validate your print against ISO standards</li>
										<li>Gain credibility and International acceptance on print quality</li>
									</ul>
								</td>
							</tr>

							<tr>
								<td>
									<p>FograCert Contract Proof Creation</p>
								</td>

								<td>
									<ul>
										<li>Assessment &amp; process check</li>
										<li>Recommendation &amp; correction</li>
										<li>Verify the prints compliance to ISO 12647-8 (FograCert Validation Print Creation) & ISO 12647-7 (FograCert Contract Proof</li>
									</ul>
								</td>

								<td>
									<ul>
										<li>Validate your print against ISO standards</li>
										<li>Gain credibility and International acceptance on print quality</li>
									</ul>
								</td>
							</tr>

							<tr>
								<td>
									<p>FograCert Contract Proof Creation</p>
								</td>

								<td>
									<ul>
										<li>Assessment &amp; process check</li>
										<li>Recommendation &amp; correction</li>
										<li>Verify the prints compliance to ISO 12647-7/12647-8</li>
										<li>Training on PDF/X files created and generate output PDF/X data in accordance to ISO 15930</li>
										<li>Quality assurance on process &amp; Environment</li>
										<li>Submit audit results to Fogra for certification</li>
									</ul>
								</td>

								<td>
									<ul>
										<li>Benchmark your print gainst ISO standards</li>
										<li>Gain credibility &amp; International acceptance on print quality</li>
										<li>Improved &amp; standardized workflow</li>
									</ul>
								</td>
							</tr>
						</tbody>
					</table>
				</section>

				<!-- Accordion Widget -->
				<section class = "section accordion-widget">


					<h2>Accordion Section</h2>
					<div class = "accordion-widget__blocks">

						<div class = "accordion-widget__block">
							<h3 class = "accordion-widget__block-title">xSpring Technology</h3>
							<div class = "accordion-widget__block-content">

								<div class = "row-col-12">
									<div class = "col-sm-4 thumbnail">
										<image src = "/assets/fuji-xerox/images/logos/hp.png" alt = "HP Logo" class = "logo"/>
									</div>

									<div  class = "description-text">

										<p>HP is a technology company that operates in more than 170 countries around the world. We explore how technology and services can help people and companies address their problems and challenges, and realize their possibilities, aspirations and dreams. We apply new thinking and ideas to create more simple, valuable and trusted experiences with technology, continuously improving the way our customers live and work.</p>

										<h4>HP Trim</h4>

										<p>HP TRIM software is a scalable enterprise document and records management solution that simplifies the capture, management, security, and access to your information—in business context. It enables your organization to more easily comply with regulations and corporate policies, and it helps you secure information from inappropriate access and misuse. With HP TRIM software, you will know what you need to retain, what you need for legal and operational purposes and can dispose of the rest.</p>

										<p>click to view the <a href = "#">HR Trim Brochure</a></p>

										<h4>Autonomy</h4>

										<p>Autonomy, an HP company, is a market-leading software company that helps organizations all over the world understand the meaning in information. Autonomy provides meaning-based solutions such as Knowledge Management and Records Management to help organisations understand the full spectrum of enterprise informations as well as the relationships that exists within it.</p>

									</div>

								</div>

							</div>
						</div>

						<div class = "accordion-widget__block">
							<h3 class = "accordion-widget__block-title">HP</h3>
							<div class = "accordion-widget__block-content">

								<div class = "row-col-12">
									<div class = "col-sm-4 thumbnail">
										<image src = "/assets/fuji-xerox/images/logos/hp.png" alt = "HP Logo" class = "logo"/>
									</div>

									<div  class = "description-text">

										<p>HP is a technology company that operates in more than 170 countries around the world. We explore how technology and services can help people and companies address their problems and challenges, and realize their possibilities, aspirations and dreams. We apply new thinking and ideas to create more simple, valuable and trusted experiences with technology, continuously improving the way our customers live and work.</p>

										<h4>HP Trim</h4>

										<p>HP TRIM software is a scalable enterprise document and records management solution that simplifies the capture, management, security, and access to your information—in business context. It enables your organization to more easily comply with regulations and corporate policies, and it helps you secure information from inappropriate access and misuse. With HP TRIM software, you will know what you need to retain, what you need for legal and operational purposes and can dispose of the rest.</p>

										<p>click to view the <a href = "#">HR Trim Brochure</a></p>

										<h4>Autonomy</h4>

										<p>Autonomy, an HP company, is a market-leading software company that helps organizations all over the world understand the meaning in information. Autonomy provides meaning-based solutions such as Knowledge Management and Records Management to help organisations understand the full spectrum of enterprise informations as well as the relationships that exists within it.</p>

									</div>

								</div>

							</div>
						</div>

						<div class = "accordion-widget__block">
							<h3 class = "accordion-widget__block-title">EMC</h3>
							<div class = "accordion-widget__block-content">

								<div class = "row-col-12">
									<div class = "col-sm-4 thumbnail">
										<image src = "/assets/fuji-xerox/images/logos/hp.png" alt = "HP Logo" class = "logo"/>
									</div>

									<div  class = "description-text">

										<p>HP is a technology company that operates in more than 170 countries around the world. We explore how technology and services can help people and companies address their problems and challenges, and realize their possibilities, aspirations and dreams. We apply new thinking and ideas to create more simple, valuable and trusted experiences with technology, continuously improving the way our customers live and work.</p>

										<h4>HP Trim</h4>

										<p>HP TRIM software is a scalable enterprise document and records management solution that simplifies the capture, management, security, and access to your information—in business context. It enables your organization to more easily comply with regulations and corporate policies, and it helps you secure information from inappropriate access and misuse. With HP TRIM software, you will know what you need to retain, what you need for legal and operational purposes and can dispose of the rest.</p>

										<p>click to view the <a href = "#">HR Trim Brochure</a></p>

										<h4>Autonomy</h4>

										<p>Autonomy, an HP company, is a market-leading software company that helps organizations all over the world understand the meaning in information. Autonomy provides meaning-based solutions such as Knowledge Management and Records Management to help organisations understand the full spectrum of enterprise informations as well as the relationships that exists within it.</p>

									</div>

								</div>

							</div>
						</div>

						<div class = "accordion-widget__block">
							<h3 class = "accordion-widget__block-title">ParaDM</h3>
							<div class = "accordion-widget__block-content">

								<div class = "row-col-12">
									<div class = "col-sm-4 thumbnail">
										<image src = "/assets/fuji-xerox/images/logos/hp.png" alt = "HP Logo" class = "logo"/>
									</div>

									<div  class = "description-text">

										<p>HP is a technology company that operates in more than 170 countries around the world. We explore how technology and services can help people and companies address their problems and challenges, and realize their possibilities, aspirations and dreams. We apply new thinking and ideas to create more simple, valuable and trusted experiences with technology, continuously improving the way our customers live and work.</p>

										<h4>HP Trim</h4>

										<p>HP TRIM software is a scalable enterprise document and records management solution that simplifies the capture, management, security, and access to your information—in business context. It enables your organization to more easily comply with regulations and corporate policies, and it helps you secure information from inappropriate access and misuse. With HP TRIM software, you will know what you need to retain, what you need for legal and operational purposes and can dispose of the rest.</p>

										<p>click to view the <a href = "#">HR Trim Brochure</a></p>

										<h4>Autonomy</h4>

										<p>Autonomy, an HP company, is a market-leading software company that helps organizations all over the world understand the meaning in information. Autonomy provides meaning-based solutions such as Knowledge Management and Records Management to help organisations understand the full spectrum of enterprise informations as well as the relationships that exists within it.</p>

									</div>

								</div>

							</div>
						</div>

						<div class = "accordion-widget__block">
							<h3 class = "accordion-widget__block-title">Kofax</h3>
							<div class = "accordion-widget__block-content">

								<div class = "row-col-12">
									<div class = "col-sm-4 thumbnail">
										<image src = "/assets/fuji-xerox/images/logos/hp.png" alt = "HP Logo" class = "logo"/>
									</div>

									<div  class = "description-text">

										<p>HP is a technology company that operates in more than 170 countries around the world. We explore how technology and services can help people and companies address their problems and challenges, and realize their possibilities, aspirations and dreams. We apply new thinking and ideas to create more simple, valuable and trusted experiences with technology, continuously improving the way our customers live and work.</p>

										<h4>HP Trim</h4>

										<p>HP TRIM software is a scalable enterprise document and records management solution that simplifies the capture, management, security, and access to your information—in business context. It enables your organization to more easily comply with regulations and corporate policies, and it helps you secure information from inappropriate access and misuse. With HP TRIM software, you will know what you need to retain, what you need for legal and operational purposes and can dispose of the rest.</p>

										<p>click to view the <a href = "#">HR Trim Brochure</a></p>

										<h4>Autonomy</h4>

										<p>Autonomy, an HP company, is a market-leading software company that helps organizations all over the world understand the meaning in information. Autonomy provides meaning-based solutions such as Knowledge Management and Records Management to help organisations understand the full spectrum of enterprise informations as well as the relationships that exists within it.</p>

									</div>

								</div>

							</div>
						</div>

						<div class = "accordion-widget__block">
							<h3 class = "accordion-widget__block-title">K2</h3>
							<div class = "accordion-widget__block-content">

								<div class = "row-col-12">
									<div class = "col-sm-4 thumbnail">
										<image src = "/assets/fuji-xerox/images/logos/hp.png" alt = "HP Logo" class = "logo"/>
									</div>

									<div  class = "description-text">

										<p>HP is a technology company that operates in more than 170 countries around the world. We explore how technology and services can help people and companies address their problems and challenges, and realize their possibilities, aspirations and dreams. We apply new thinking and ideas to create more simple, valuable and trusted experiences with technology, continuously improving the way our customers live and work.</p>

										<h4>HP Trim</h4>

										<p>HP TRIM software is a scalable enterprise document and records management solution that simplifies the capture, management, security, and access to your information—in business context. It enables your organization to more easily comply with regulations and corporate policies, and it helps you secure information from inappropriate access and misuse. With HP TRIM software, you will know what you need to retain, what you need for legal and operational purposes and can dispose of the rest.</p>

										<p>click to view the <a href = "#">HR Trim Brochure</a></p>

										<h4>Autonomy</h4>

										<p>Autonomy, an HP company, is a market-leading software company that helps organizations all over the world understand the meaning in information. Autonomy provides meaning-based solutions such as Knowledge Management and Records Management to help organisations understand the full spectrum of enterprise informations as well as the relationships that exists within it.</p>

									</div>

								</div>

							</div>
						</div>
					</div>
				</section>
				<!-- End: Accordion Widget -->

				<!-- Resources Section -->
				<section class = "section resources">
					<a id = "" class = "anchor-link" data-label = "Resources"></a>
					<h3>Resources</h3>

					<div class = "resources__block">
						<h4>Colour Management Brochure</h4>
						<dl>
							<dt>File Size:</dt>
							<dd>4100kb</dd>
							<dt>Document type:</dt>
							<dd>Portable Document Format</dd>
						</dl>

						<!-- DEV NOTE:  -->
						<!-- IF Email Subscription is switched off -->
						<!-- <a href = "#" class = "resources__block__download-btn" title = "Download Colour Management Brochure"><i class = "icon-download"></i><span class = "sr-only">Download Resource</span></a> -->
						<!-- ELSE -->
						<a href = "#subscriptionCall" class = "fancybox resources__block__download-btn" title = "Download Colour Management Brochure"><i class = "icon-download"></i><span class = "sr-only">Download Resource</span></a>
						<!-- END -->
					</div>

					<!-- Subscription Call -->
					<div class = "subscribe" id = "subscriptionCall">
						<div class = "row-col-12">
							<div class = "col-sm-6 col-md-6 col match-height">
								<h3>You may download the PDF here</h3>
								<a href="#" target = "_blank" class = "btn-download">Colour Management Brochure<i class="icon-download"></i></a>
							</div>
							<div class = "col-sm-6 col-md-6 col match-height">
								<h3>Subscribe to our newsletter</h3>
								<!-- <p>Keep yourself update to Fuji Xerox latest product!</p> -->
								<div class = "form-row row-col-12">
									<div class = "col-sm-6">
										<div class = "input-group-text">
											<label for = "subscribeName">Name<span class = "required">*</span></label>
											<input type = "text" id = "subscribeName" placeholder = "Name">
										</div>
										<div class = "input-group-text">
											<label for = "subscribeEmail">Email<span class = "required">*</span></label>
											<input type = "email" id = "subscribeEmail" placeholder = "abc@example.com">
										</div>
									</div>
								</div>
								<div class = "btn-group">
									<button type = "submit">Submit<i class = "icon-chevron-with-circle-right"></i></button>
								</div>
							</div>
						</div>
					</div>
					<!-- End: Subscription Call -->
				</section>
				<!-- End: Resources Section -->

				<section class = "section related-pages">
					<a id = "" class = "anchor-link" data-label = "Related Pages"></a>
					<h3>Related Pages</h3>

					<div class = "blocks">
						<div class = "related-pages__block">
							<a href = "#">
								<h4>Customer Communication Management (CCM) Solutions</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>

						<div class = "related-pages__block">
							<a href = "#">
								<h4>Color Management Solutions &amp; Services</h4>
								<span class = "category">Production Solutions</span>
							</a>
						</div>
					</div>
				</section>
			</div>
			<!-- End: Article Content -->

			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "#">Production Solutions</a></h3>
					<ul>
						<li><a href = "#">Customer Communication Management (CCM) Solutions</a></li>
						<li><a href = "#">Color Management Solutions &amp; Services</a></li>
						<li><a href = "#">Print Management Information Systems(MIS)</a></li>
						<li><a href = "#">Packaging Solutions</a></li>
						<li><a href = "#">Business Development Consultancy Services</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>