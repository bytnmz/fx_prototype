<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox';
$primary = 0;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Homepage Carousel -->
	<div class = "home-carousel wingspan" data-carousel-interval = "5000">
		<div class = "slider-widget">
			<div class = "slider">
				<div class = "slide" data-mobile-banner = "/assets/fuji-xerox/images/content/banners/home-01-small.jpg">
					<a href = "#"><img src= "/assets/fuji-xerox/images/content/banners/home-01.jpg" alt = ""/></a>
				</div>

				<div class = "slide" data-mobile-banner = "/assets/fuji-xerox/images/content/banners/home-02-small.jpg">
					<a href = "#"><img src= "/assets/fuji-xerox/images/content/banners/home-02.jpg" alt = ""/></a>
				</div>

				<div class = "slide" data-mobile-banner = "/assets/fuji-xerox/images/content/banners/home-03-small.jpg">
					<a href = "#"><img src= "/assets/fuji-xerox/images/content/banners/home-03.jpg" alt = ""/></a>
				</div>

				<div class = "slide" data-mobile-banner = "/assets/fuji-xerox/images/content/banners/home-04-small.jpg">
					<a href = "/office-supplies"><img src= "/assets/fuji-xerox/images/content/banners/home-04.jpg" alt = ""/></a>
				</div>
			</div>

			<div class = "slider-custom-control visible-md-block visible-lg-block">
				<!-- <div class = "slider-custom-control__pager">
					<ul>
						<li><a href = "#"><img src= "/assets/fuji-xerox/images/content/banners/home-01.jpg" alt = ""/></a></li>
						<li><a href = "#"><img src= "/assets/fuji-xerox/images/content/banners/home-02.jpg" alt = ""/></a></li>
						<li><a href = "#"><img src= "/assets/fuji-xerox/images/content/banners/home-03.jpg" alt = ""/></a></li>
						<li><a href = "#"><img src= "/assets/fuji-xerox/images/content/banners/home-04.jpg" alt = ""/></a></li>
					</ul>
				</div> -->

				<div class = "slider-custom-control__play-toggle autoplay">
					<a href = "#"><span class = "icon icon-controller-paus"></span></a>
				</div>
			</div>
		</div>
	</div>
	<!-- Homepage Carousel -->

	<section class = "home-ctas wingspan">
			<div class = "home-ctas__block">
				<h2><a href = "#">Product Finder <span class = "icon"><img src= "/assets/fuji-xerox/images/content/printer-icon.png" alt = ""/></span></a></h2>

				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>

				<a href = "#" class = "link-to">Find product</a>
			</div>

			<div class = "home-ctas__block">
				<h2><a href = "#">Corporate Gifts <span class = "icon"><img src= "/assets/fuji-xerox/images/content/printer-icon.png" alt = ""/></span></a></h2>

				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>

				<a href = "#" class = "link-to">View gifts options</a>
			</div>

			<div class = "home-ctas__block">
				<h2><a href = "#">Promotions <span class = "icon"><img src= "/assets/fuji-xerox/images/content/printer-icon.png" alt = ""/></span></a></h2>

				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>

				<a href = "#" class = "link-to">View Promotions</a>
			</div>
	</section>

	<div class = "wingspan">
		<div class = "row-col-12">
			<section class = "home-news-list col-sm-6 push-right-15">
				<h2>Latest News</h2>

				<ul>
					<li>
						<h3><a href = "#">Fuji Xerox launches Xerox iGen 5 press with a fifth color toner</a></h3>
						<span class = "date">Mar 7, 2015</span>
					</li>
					<li>
						<h3><a href = "#">Fuji Xerox Empowers Youths in Arts and Design through the Digital Print Design Award</a></h3>
						<span class = "date">Mar 7, 2015</span>
					</li>
					<li>
						<h3><a href = "#">SG50 Mass Tree Planting Program: Fuji Xerox contributes toward Singapore's green city of tomorrow</a></h3>
						<span class = "date">Mar 7, 2015</span>
					</li>
					<li>
						<h3><a href = "#">First Win for Fuji Xerox Asia Pacific at the Sustainable Business Awards 2015</a></h3>
						<span class = "date">Mar 7, 2015</span>
					</li>
				</ul>

				<div class = "more-link"><a href = "#">Read more</a></div>
			</section>

			<section class = "home-news-list col-sm-6 push-left-15">
				<h2>Upcoming Events</h2>

				<ul>
					<li>
						<h3><a href = "#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a></h3>
						<span class = "date">Mar 7, 2015</span>
					</li>
					<li>
						<h3><a href = "#">Aenean commodo ligula eget dolor. Aenean massa.</a></h3>
						<span class = "date">Mar 7, 2015</span>
					</li>
					<li>
						<h3><a href = "#">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</a></h3>
						<span class = "date">Mar 7, 2015</span>
					</li>
					<li>
						<h3><a href = "#">Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</a></h3>
						<span class = "date">Mar 7, 2015</span>
					</li>
				</ul>

				<div class = "more-link"><a href = "#">View all events</a></div>

			</section>
		</div>

	</div>


	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>