<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Solutions & Services';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><span>404 - Page not found</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<div class = "article wingspan">

		<div class = "404-banner">
			<span class = "logo"><img src = "/assets/fuji-xerox/images/404-banner-logo.jpg" alt = "Fuji Xerox"/></span>
			<p>If yuo end up hree, prehpas yup are tpying it worngly.</p>
			<p class = "black-text">Let us get you back on track</p>
		</div>

		<ul class = "404-links row-col-12">
			<li class = "col-sm-6">
				<a href = ""><span class = "ico"><img src = "/assets/fuji-xerox/images/home-blue.png" alt = ""/></span><span class = "label">Return to homepage</span></a>
			</li>
			<li class = "col-sm-6">
				<a href = ""><span class = "ico"><img src = "/assets/fuji-xerox/images/office-imaging-blue.png" alt = ""/></span><span class = "label">View our full list of products</span></a>
			</li>
			<li class = "col-sm-6">
				<a href = ""><span class = "ico"><img src = "/assets/fuji-xerox/images/chat-blue.png" alt = ""/></span><span class = "label">Talk to us on Live Chat</span></a>
			</li>
			<li class = "col-sm-6">
				<a href = ""><span class = "ico"><img src = "/assets/fuji-xerox/images/cloud-blue.png" alt = ""/></span><span class = "label">Check out our range of services and solutions</span></a>
			</li>
		</ul>
	</div>


	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>