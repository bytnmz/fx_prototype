<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Customer Feedback Form';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><a href = "../">Contact</a></li>
			<li><span>Customer Feedback Form</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4">
				<h1>Customer Feedback</h1>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->


	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content col-sm-10 offset-sm-1 col-md-8 offset-md-2">

				<div class = "sitecore-form">

							<div class="scfForm" id="form_6A9985C58EFE43209A66B4A98BDE5EA0">
								<input type="hidden" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$formreference" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_formreference" value="form_6A9985C58EFE43209A66B4A98BDE5EA0">
								<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0__summary" class="scfValidationSummary" style="display:none;"></div>
								<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_form_6A9985C58EFE43209A66B4A98BDE5EA0_submitSummary" class="scfSubmitSummary" style="color:Red;"></div>
								<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_fieldContainer" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'form_6A9985C58EFE43209A66B4A98BDE5EA0_form_6A9985C58EFE43209A66B4A98BDE5EA0_submit')">
									<div>
										<fieldset class="scfSectionBorderAsFieldSet">
											<div class="scfSectionContent">

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_AAB805BE14F7495DA2146263952CD8EFscope" class="scfDropListBorder fieldid.%7bAAB805BE-14F7-495D-A214-6263952CD8EF%7d name.Drop+list">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_AAB805BE14F7495DA2146263952CD8EF" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_AAB805BE14F7495DA2146263952CD8EFtext" class="scfDropListLabel">Nature of Enquiry</label>
													<div class="scfDropListGeneralPanel">
														<select name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_AAB805BE14F7495DA2146263952CD8EF" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_AAB805BE14F7495DA2146263952CD8EF" class="scfDropList">
															<option value="">Select a category</option>
															<option value="">Compliments / Complaints</option>
															<option value="">Technical Support</option>
															<option value="">Others</option>
														</select>
														<span class="scfDropListUsefulInfo" style="display:none;"></span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_scope" class="scfSingleLineTextBorder fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d name.Single+Line">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_text" class="scfSingleLineTextLabel">Name</label>
													<div class="scfSingleLineGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_ADB11D7524A8411C8089A8DD897A031A" type="text" maxlength="256" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" class="scfSingleLineTextBox">
														<span class="scfSingleLineTextUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">Single Line must have at least 0 and no more than 256 characters.</span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">The value of the Single Line field is not valid.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A_scope" class="scfEmailBorder fieldid.%7bCAE5C925-52B5-4F53-86C6-12395C37DC5A%7d name.Email">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A_text" class="scfEmailLabel">Email</label>
													<div class="scfEmailGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_CAE5C92552B54F5386C612395C37DC5A" type="text" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A" class="scfEmailTextBox">
														<span class="scfEmailUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_CAE5C92552B54F5386C612395C37DC5A5D10AF7533054C39908EB25E8CB4ABDC_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bCAE5C925-52B5-4F53-86C6-12395C37DC5A%7d inner.1" style="display:none;">Enter a valid e-mail address.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4_scope" class="scfTelephoneBorder fieldid.%7b7C714B5E-B312-4C7E-B843-559D45928CE4%7d name.Telephone">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4_text" class="scfTelephoneLabel">Telephone</label>
													<div class="scfTelephoneGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_7C714B5EB3124C7EB843559D45928CE4" type="text" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4" class="scfTelephoneTextBox"><span class="scfTelephoneUsefulInfo" style="display:none;"></span><span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4C3CC8A327EF14D138163826E11482E4D_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b7C714B5E-B312-4C7E-B843-559D45928CE4%7d inner.1" style="display:none;">Enter a valid telephone number.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4_scope" class="scfTelephoneBorder fieldid.%7b7C714B5E-B312-4C7E-B843-559D45928CE4%7d name.Telephone">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4_text" class="scfTelephoneLabel">Fax</label>
													<div class="scfTelephoneGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_7C714B5EB3124C7EB843559D45928CE4" type="text" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4" class="scfTelephoneTextBox"><span class="scfTelephoneUsefulInfo" style="display:none;"></span><span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_7C714B5EB3124C7EB843559D45928CE4C3CC8A327EF14D138163826E11482E4D_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b7C714B5E-B312-4C7E-B843-559D45928CE4%7d inner.1" style="display:none;">Enter a valid fax number.</span>
													</div>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_scope" class="scfSingleLineTextBorder fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d name.Single+Line">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A_text" class="scfSingleLineTextLabel">Company</label>
													<div class="scfSingleLineGeneralPanel">
														<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_ADB11D7524A8411C8089A8DD897A031A" type="text" maxlength="256" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A" class="scfSingleLineTextBox">
														<span class="scfSingleLineTextUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">Single Line must have at least 0 and no more than 256 characters.</span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_ADB11D7524A8411C8089A8DD897A031A070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bADB11D75-24A8-411C-8089-A8DD897A031A%7d inner.1" style="display:none;">The value of the Single Line field is not valid.</span>
													</div>
													<span class="scfRequired">*</span>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E_scope" class="scfMultipleLineTextBorder fieldid.%7bD1C52150-7AA9-4035-AB45-8A744E0CE06E%7d name.Multi+Line">
													<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E_text" class="scfMultipleLineTextLabel">Your feedback/enquiry/problem description/event:</label>
													<div class="scfMultipleLineGeneralPanel">
														<textarea name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_D1C521507AA94035AB458A744E0CE06E" rows="4" cols="20" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E" class="scfMultipleLineTextBox"></textarea>
														<span class="scfMultipleLineTextUsefulInfo" style="display:none;"></span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E6ADFFAE3DADB451AB530D89A2FD0307B_validator" class="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7bD1C52150-7AA9-4035-AB45-8A744E0CE06E%7d inner.1" style="display:none;">Multi Line must have at least 0 and no more than 512 characters.</span>
														<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_D1C521507AA94035AB458A744E0CE06E070FCA141E9A45D78611EA650F20FE77_validator" class="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7bD1C52150-7AA9-4035-AB45-8A744E0CE06E%7d inner.1" style="display:none;">The value of the Multi Line field is not valid.</span>
													</div>
												</div>

												<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451" class="scfRadioButtonListBorder fieldid.%7b142886E5-71B2-4B8D-8C12-690C66651451%7d name.Radio+button+list">
													<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451text" class="scfRadioButtonListLabel">Can we publish your feedback?</span>
													<div class="scfRadioButtonListGeneralPanel">
														<table id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451scope" class="scfRadioButtonList">
															<tbody><tr>
																<td><input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451scope_0" type="radio" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_142886E571B24B8D8C12690C66651451scope" value="first"><label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451scope_0">Yes</label></td>
															</tr><tr>
																<td><input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451scope_1" type="radio" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_142886E571B24B8D8C12690C66651451scope" value="second"><label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_142886E571B24B8D8C12690C66651451scope_1">No</label></td>
															</tr>
														</tbody></table><span class="scfRadioButtonListUsefulInfo" style="display:none;"></span>
													</div>
												</div>

												<div class="scfCaptcha fieldid.%7b55A59538-2B41-493C-9A97-3407AA4CB8F4%7d name.captcha validationgroup.form_6A9985C58EFE43209A66B4A98BDE5EA0_submit" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border">
													<div class="scfCaptchTop">
														<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCodeBorder" class="scfCaptchaBorder">
															<span class="scfCaptchaLabel">&nbsp;</span>
															<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_Panel1" class="scfCaptchaGeneralPanel">
																<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCodeText" class="scfCaptchaLabel" style="display:none;"> </span>
																<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCodePanel" class="scfCaptchaLimitGeneralPanel">
																	<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode">
																		<table cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
																			<tbody>
																				<tr>
																					<td>
																						<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode_captchaCodeim">
																							<input id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode_ctl01_uniqID" type="hidden" name="uniqID" value="91c10441-0f66-4f4b-888f-a63fc3692c8f">
																							<div style="background-color:White;">
																								<img src="/assets/fuji-xerox/images/dummy/captcha/captcha.jpg" width="180" height="50" alt="Captcha"/>
																							</div>
																						</div>
																					</td>
																					<td>
																						<input type="image" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$captchaCode$ctl02" title="Display another text." src="/assets/fuji-xerox/images/dummy/captcha/refresh.png" alt="Display another text." onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$captchaCode$ctl02&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))"><br>
																						<input type="image" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$captchaCode$captchaCodepb" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode_captchaCodepb" title="Play audio version of text." src="/assets/fuji-xerox/images/dummy/captcha/loudspeaker.png" alt="Play audio version of text." onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$captchaCode$captchaCodepb&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" href="CaptchaAudio.axd?guid=91c10441-0f66-4f4b-888f-a63fc3692c8f">
																					</td>
																				</tr>
																			</tbody>
																		</table>
																		<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaCode_captchaCodeph"></div>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div>
														<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaTextBorder" class="scfCaptchaBorder">
															<span class="scfCaptchaLabel">&nbsp;</span>
															<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaTextPanel" class="scfCaptchaGeneralPanel">
																<label for="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_field_55A595382B41493C9A973407AA4CB8F4" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaTextTitle" class="scfCaptchaLabelText">Captcha</label>
																<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchLimitTextPanel" class="scfCaptchaLimitGeneralPanel">
																	<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchStrongTextPanel" class="scfCaptchStrongTextPanel">
																		<input name="form_6A9985C58EFE43209A66B4A98BDE5EA0$field_55A595382B41493C9A973407AA4CB8F4border$field_55A595382B41493C9A973407AA4CB8F4" type="text" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_field_55A595382B41493C9A973407AA4CB8F4" class="scfCaptchaTextBox scWfmPassword">
																		<span id="form_6A9985C58EFE43209A66B4A98BDE5EA0_field_55A595382B41493C9A973407AA4CB8F4border_captchaTextHelp" class="scfCaptchaUsefulInfo" style="display:none"></span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</fieldset>
									</div>
								</div>
								<div id="form_6A9985C58EFE43209A66B4A98BDE5EA0_footer" class="scfFooterBorder">
								</div>
								<div class="scfSubmitButtonBorder">
									<input type="submit" name="form_6A9985C58EFE43209A66B4A98BDE5EA0$form_6A9985C58EFE43209A66B4A98BDE5EA0_submit" value="Submit" onclick="$scw.webform.lastSubmit = this.id;WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;form_6A9985C58EFE43209A66B4A98BDE5EA0$form_6A9985C58EFE43209A66B4A98BDE5EA0_submit&quot;, &quot;&quot;, true, &quot;form_6A9985C58EFE43209A66B4A98BDE5EA0_submit&quot;, &quot;&quot;, false, false));$scw.webform.validators.setFocusToFirstNotValid('form_6A9985C58EFE43209A66B4A98BDE5EA0_submit')" id="form_6A9985C58EFE43209A66B4A98BDE5EA0_form_6A9985C58EFE43209A66B4A98BDE5EA0_submit" class="scfSubmitButton">
								</div>


							</div>

						</div>

			</div>


			<!-- Article Aside -->
			<aside class = "article-aside col-sm-10 offset-sm-1 col-md-2 offset-md-0">
				<div class = "article-aside__block aside-navigation">
					<h3><a href = "/contact">Contact</a></h3>
					<h4>General Information</h4>
					<ul>
						<li><a href="#">Contact Details</a></li>
						<li><a href="#">Authorised Resellers</a></li>
					</ul>
					<h4>Online Forms</h4>
					<ul>
						<li><a href="#">Request Pricing</a></li>
						<li><a href="/contact/customer-feedback" class = "active">Customer Feedback</a></li>
						<li><a href="#">Technical Support</a></li>
						<li><a href="#">Submit Meter Readings</a></li>
						<li><a href="#">Toner Ordering</a></li>
						<li><a href="#">Customer Software Training</a></li>
						<li><a href="#">Customer Rental Programme</a></li>
					</ul>
				</div>
			</aside>
			<!-- End: Article Aside -->
		</div>
	</article>
	<!-- End: Article -->

	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>