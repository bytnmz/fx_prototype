<?php
set_include_path($_SERVER['DOCUMENT_ROOT'] . '/includes');
$title = 'Fuji Xerox | Contact';
$primary = 2;
$secondary = 0;
?>
<?php include('top.php') ?>

<body>
	<?php include('header.php') ?>
	<?php include('main-navigation.php') ?>

	<!-- Breadcrumb -->
	<div class = "breadcrumb wingspan">
		<ul>
			<li><a href = "/">Home</a></li>
			<li><span>Contact</span></li>
		</ul>
	</div>
	<!-- End: Breadcrumb -->

	<!-- Top Banner -->
	<div class = "top-banner wingspan">
		<div class = "row-col-12">
			<div class = "top-banner__image">
				<img src = "/assets/fuji-xerox/images/content/banners/solutions.jpg" alt = ""/>
			</div>

			<div class = "top-banner__content col-sm-4">
				<h1>Contact</h1>
				<p>Easily reachable, always at your service</p>
			</div>

		</div>
	</div>
	<!-- End: Top Banner -->


	<!-- Article -->
	<article class = "article">
		<div class = "row-col-12 wingspan">

			<!-- Article Content -->
			<div class = "article-content wide">

				<p>At Fuji Xerox, we welcome all our customer enquiries. You can reach us through various channels.</p>

				<table class = "stackable">
					<thead>
						<tr>
							<th></th>
							<th>Call Us</th>
							<th>Online Forms</th>
					<thead>

					<tbody>
						<tr>
							<td><strong>For General Matters</strong></td>
							<td>6766 8888<br>&amp; Press '0'</td>
							<td>
								<a href="#">Submit Meter Readings</a><br>
								<a href="#">Customer Software Training</a><br>
								<a href="/contact/customer-feedback">Customer Feedback</a>
							</td>
						</tr>

						<tr>
							<td><strong>For Technical Support, Toner Ordering and Billing Enquiries</strong></td>
							<td>6766 8888<br>&amp; Press '1'</td>
							<td>
								<a href="#">Technical Support</a><br>
								<a href="#">Toner Ordering</a><br>
							</td>
						</tr>

						<tr>
							<td><strong>For Paper and Office Supplies</strong></td>
							<td>6766 8888<br>&amp; Press '2'</td>
							<td>
								<a href="#">Request Pricing</a><br>
							</td>
						</tr>


						<tr>
							<td><strong>For Equipment and Software Sales Enquiries</strong></td>
							<td>6766 8888<br>&amp; Press '3'</td>
							<td>
								<a href="#">Request Pricing</a><br>
							</td>
						</tr>


					</tbody>

				</table>
			</div>
		</div>
	</article>
	<!-- End: Article -->

	<!-- Featured Linkboxs -->
	<section class = "section featured-linkboxs wingspan">
		<div class = "section-content">
			<div>
				<div class = "col match-height">
					<a href = "#">
						<img src = "/assets/fuji-xerox/images/icons/branch_office_wht.png" alt = ""/>
						<p>Location Details</p>
					</a>
				</div>

				<div class = "col match-height">
					<a href = "#">
						<img src = "/assets/fuji-xerox/images/icons/delivery_cart_wht.png" alt = ""/>
						<p>Authorised Reseller</p>
					</a>
				</div>

				<div class = "col match-height">
					<a href = "#" onclick="window.open('https://secure.livechatinc.com/licence/2559781/open_chat.cgi?groups=0','newChatWindow','height=520,width=530,top=' + (screen.height / 2 - 260) + ',left=' + (screen.width / 2 - 265) + ',resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');">
						<img src = "/assets/fuji-xerox/images/icons/chat_white.png" alt = ""/>
						<p>Live Chat</p>
					</a>
				</div>
			</div>
		</div>
	</section>
	<!-- End: Featured Linkboxs -->



	<?php include('footer.php') ?>
	<?php include('side-menu.php') ?>
	<?php include('bottom.php') ?>
</body>
</html>